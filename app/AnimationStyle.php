<?php

namespace App;

use App\Template;
use Illuminate\Database\Eloquent\Model;

class AnimationStyle extends Model
{
    //

    const REPOSITORY = 'library/photos/';
    const getPath = 'api/storage/library/photos/';

    protected $fillable = [
        'name',
        'description',
        'preview_image',
        'including_video',
        'trending',
        'likes'
    ];

    protected $casts = [
        'including_video' => 'boolean',
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];


    public function templates()
    {
        return $this->hasMany(Template::class, 'animation_style_id');
    }

    public function getPreviewImageAttribute($value)
    {
        return url(AnimationStyle::getPath . $value);
    }

}
