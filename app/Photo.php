<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    const REPOSITORY = 'library/photos/';
    const getPath = 'api/storage/library/photos/';
    //
    protected $fillable = [
        'name',
        'file',
        'user_id',
        'size'
    ];

    public function projects()
    {
        return $this->belongsToMany(Project::class,'project_photos');
    }

    public function getFileAttribute($value)
    {
        return url(Photo::getPath . $value);
    }
}
