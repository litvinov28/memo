<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPhotos extends Model
{
    protected $fillable = ['photo_id','project_id'];
}
