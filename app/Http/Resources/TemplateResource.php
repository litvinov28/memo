<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TemplateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'id' => $this->id,
            'pics' => $this->pics,
            'duration' => $this->duration,
            'types' => $this->types,
            'format' => $this->format,
            'animation_style_id' => $this->animation_style_id,
            'ae_file_name' => $this->ae_file_name,
            'ae_file_path' => $this->ae_file_path,
            'preview_video' => $this->getVideoUrl($this->preview_video),
            'checked' => $this->checked,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),

        ];
    }
}
