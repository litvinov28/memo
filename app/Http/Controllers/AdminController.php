<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserDeleteRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\RestoreRequest;
class AdminController extends Controller
{
    public function userList()
    {
        return response()->json(['success' => true, 'data' => User::where('role_id','!=','2')->get()]);
    }


    public function userDelete(UserDeleteRequest $request)
    {
        $credentials = $request->only('ids');
        User::whereIn('id', $credentials['ids'])->delete();
        return response()->json(['success' => true, 'message' => 'Users has been deleted']);

    }

    public function listDelete()
    {

        $users = User::onlyTrashed()->get();
        return response()->json(['success' => true, 'data' => $users]);

    }

    public function restore(RestoreRequest $request)
    {
        $users = User::onlyTrashed()->findOrFail($request->user_id);
           $users->restore();

        return response()->json(['success' =>  true, 'data' => $users]);
    }
}
