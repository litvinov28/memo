<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectStoreRequest;
use App\Project;
use App\ProjectPhotos;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class ProjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Project::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectStoreRequest $request)
    {
        $userId = auth()->id();
        $styles = Project::addSelectedProperties($request->templates);

        $project = Project::create([
            'name' => $request->name,
            'user_id' => $userId,
            'total_video_duration' => $styles['duration'],
            'animation' => $styles['animations'],
            'audio' => $request->audio,
        ]);

        foreach ($request->all()['photo_id'] as $item) {
            ProjectPhotos::create([
                'project_id' => $project->id,
                'photo_id' => $item
            ]);
        }

        return response()->json($project->load('photos'), 201);
    }
}
