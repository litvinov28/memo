<?php

namespace App\Http\Controllers;

use App\Helpers\FileManager;
use App\Http\Requests\ImageRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $photos = User::current()->photos()->get();
        return response()->json($photos, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ImageRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $userId = auth()->id();
        $file = $request->file('file');
        $size = File::size($file);

        $photo = Photo::create([
            'user_id' => $userId,
            'name' => $file->getClientOriginalName(),
            'file' => FileManager::downloadFiles($file, Photo::REPOSITORY),
            'size' => $size
        ]);

        return response()->json($photo, 201);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showAllPhoto()
    {
        return response()->json(Photo::all(), 200);
    }

    public function photoCheck(Request $request){
        $photo = Photo::whereIn('id', $request['id'])->get();
        return response()->json($photo);
    }
}
