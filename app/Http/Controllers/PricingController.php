<?php

namespace App\Http\Controllers;

use App\Http\Requests\PricingRequest;
use App\Pricing;
use Illuminate\Http\Request;

class PricingController extends Controller
{
    public function index()
    {
        return response()->json(['success' => true, 'data' => Pricing::all()]);
    }

    public function store(PricingRequest $request)
    {
        $pricing = new Pricing($request->all());
        return response()->json(['success' => $pricing->save(), 'data' => $pricing]);
    }

    public function update(PricingRequest $request, Pricing $pricing)
    {
         return response()->json(['success' => $pricing->update($request->all()),'data' => $pricing]);
    }

    public function show(Pricing $pricing)
    {
        return response()->json(['success' => true,'data' => $pricing]);
    }

    public function destroy(Pricing $pricing)
    {
        return response()->json(['success' => $pricing->delete(),'message' => 'Pricing has been deleted']);
    }
}
