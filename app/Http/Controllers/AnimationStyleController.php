<?php

namespace App\Http\Controllers;


use App\AnimationStyle;
use App\Helpers\FileManager;
use App\Http\Requests\ImageRequest;
use App\Http\Resources\AnimationStyleResourceCollection;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;

class AnimationStyleController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $styles = AnimationStyle::all()->load('templates');
        //return $this->ok($styles);
//        return response()->json($styles, 200);
        $data = AnimationStyleResourceCollection::make($styles);

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ImageRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $file = $request->file('file');
        $size = File::size($file);
        $fileName = 'image_'.time().'.png';
        $photo = AnimationStyle::create([
            'name' => $request->name,
            'preview_image' => $fileName,
            'description' => $request->description,
            'file' => FileManager::downloadFiles($file, AnimationStyle::REPOSITORY)
//            'size' => $size
        ]);

        return response()->json($photo, 201);



    }
}
