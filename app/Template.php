<?php

namespace App;

use App\AnimationStyle;
use App\Screen;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    //

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'format' => 'array',
        'types' => 'array'
    ];

//    protected $appends = [
//        'screens'
//    ];


    public function style()
    {
        return $this->belongsTo(AnimationStyle::class, 'animation_style_id');
    }

    public function screens()
    {
        return $this->hasMany(Screen::class);
    }

    public function getVideoUrl($video): string
    {
        return url('api/storage/library/videos/'.$video);
    }


}
