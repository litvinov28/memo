<?php

namespace App\Helpers;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileManager
{
    const WATERMARK_REPOSITORY = 'watermark/';
    //const WATERMARK_FILE_NAME = 'watermark.png';
    /**
     * Download new file to storage.
     *
     * @param $file
     * @param $repository
     * @param bool $compression
     * @return string
     */
    public static function downloadFiles($file, $repository)
    {
        $fileName = 'image_'.time().'.png';
        $storagePath = Storage::disk('public')->putFileAs($repository, $file, $fileName);
//        $filename = basename($storagePath);
        
        return $fileName;
    }

    /**
     * Get random name and original extension.
     *
     * @param $file
     * @return string
     */
    public static function getRandomName($file)
    {
        $hash = Str::random(33);
        return $hash . '.' . $file->getClientOriginalExtension();
    }
}