<?php

namespace App;

use App\AnimationStyle;
use App\Template;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $fillable = [
        'name',
        'user_id',
        'total_video_duration',
        'total_audio_duration',
        //'status',
        'animation',
        'images',
        'audio',
    ];

    protected $guarded = [
        'paid',
        'rendered',
        'complete_url'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'animation' => 'array',
        'rendered' => 'boolean',
        'audio' => 'array',
        'paid' => 'boolean',
        'images' => 'array'
    ];
    /******* Static functions *******/

    /**
     * Select the desired styles and add additional information.
     *
     * @param $templatesId
     * @return array
     */
    public static function addSelectedProperties($templatesId)
    {
        $collection = collect(Template::find($templatesId)->load(['screens']))->map(function ($template) {
//            dd($template);
            $animation = AnimationStyle::find($template['animation_style_id'])
                ->makeHidden(['templates']);

            return collect($animation)->merge($template);
        });

        return ["animations" => $collection, "duration" => $collection->sum('duration')];
    }
public function photos()
{
    return $this->belongsToMany(Photo::class,'project_photos');
}
}
