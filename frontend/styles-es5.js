(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["styles"],{

/***/ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/styles.scss":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src??embedded!./node_modules/sass-loader/lib/loader.js??ref--15-3!./src/styles.scss ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = [[module.i, "body * {\n  box-sizing: border-box;\n}\n\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */\n}\n\nbody {\n  margin: 0;\n}\n\na, a:hover, a:focus {\n  text-decoration: none;\n  cursor: pointer;\n}\n\na {\n  background-color: transparent;\n}\n\nbutton {\n  cursor: pointer;\n}\n\nbutton:focus {\n  outline: none;\n}\n\nimg {\n  max-width: 100%;\n}\n\nbody {\n  font-family: \"Caviar Dreams\", sans-serif;\n  color: #000;\n  position: relative;\n}\n\nbutton, input, optgroup, select, textarea {\n  font-family: inherit;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */\n}\n\nbutton, input {\n  overflow: visible;\n}\n\nbutton, select {\n  text-transform: none;\n}\n\ntextarea {\n  overflow: auto;\n}\n\ninput, textarea {\n  border: 1px solid #000000;\n  background-color: #ffffff;\n  height: 2.5vw;\n  border-radius: 1.25vw;\n  padding: 0 1.302vw;\n  font-size: 0.833vw;\n}\n\n.btn-custom {\n  border: 1px solid #000000;\n  background-color: #000000;\n  font-weight: 700;\n  color: #fff;\n  display: -webkit-inline-box;\n  display: inline-flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  min-width: 7.813vw;\n  height: 2.344vw;\n  border-radius: 1.198vw;\n  font-size: 0.938vw;\n}\n\n.facebook {\n  background-color: #3b5998;\n  border-color: #3b5998;\n  text-transform: uppercase;\n}\n\n.google {\n  background-color: #c74a2d;\n  border-color: #c74a2d;\n  text-transform: uppercase;\n}\n\n.btn-custom.facebook,\n.btn-custom.google {\n  font-size: 0.833vw;\n  width: 14.583vw;\n  height: 2.5vw;\n  padding-left: 0.417vw;\n  -webkit-box-pack: start;\n          justify-content: flex-start;\n}\n\n.btn-custom.facebook img,\n.btn-custom.google img {\n  width: 1.719vw;\n  height: 1.719vw;\n}\n\n.btn-custom.facebook p,\n.btn-custom.google p {\n  padding-left: 1.302vw;\n}\n\n.facebook p, .google p {\n  margin: 0 !important;\n}\n\ninput:focus, textarea:focus {\n  outline: none;\n}\n\n.page-title {\n  color: #d6e2ec;\n  font-family: \"Copperplate Gothic\", serif;\n  font-weight: 700;\n  text-align: center;\n  font-size: 6.25vw;\n  margin: 0.573vw 0 0;\n}\n\n.container {\n  margin: 0 auto;\n  width: 56.667vw;\n}\n\n.light {\n  background: #fff;\n  color: #000;\n}\n\n.warning {\n  font-weight: 400;\n}\n\n.gallery {\n  padding-top: 5vw;\n}\n\n.gallery__wrap {\n  margin-bottom: 1.875vw;\n}\n\n.gallery__wrap:last-child {\n  margin-bottom: 6.094vw;\n}\n\n.gallery__title {\n  font-size: 1.563vw;\n  margin-left: 1.667vw;\n  margin-bottom: 2.917vw;\n  font-weight: 700;\n  position: relative;\n}\n\n.gallery__title:after {\n  content: \"\";\n  position: absolute;\n  background-color: #000000;\n  width: 9.375vw;\n  height: 0.781vw;\n  bottom: -1.198vw;\n  left: 0.156vw;\n}\n\n.gallery__row {\n  display: -webkit-box;\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.gallery__item {\n  padding-left: 1.042vw;\n  margin-bottom: 1.51vw;\n  width: 33.333%;\n  cursor: pointer;\n}\n\n.gallery__item:hover .item-top {\n  z-index: 9;\n}\n\n.gallery__item .item-top {\n  padding-left: 0.677vw;\n  position: relative;\n  z-index: 2;\n}\n\n.gallery .time {\n  margin-right: 0.573vw;\n  font-size: 0.833vw;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: end;\n          align-items: flex-end;\n  font-weight: 700;\n}\n\n.gallery .time img {\n  padding-right: 5px;\n}\n\n.gallery .info {\n  padding-bottom: 0.104vw;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n}\n\n.gallery .info__item {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: end;\n          align-items: flex-end;\n}\n\n.gallery .info__item:last-child span {\n  margin-left: 10px;\n}\n\n.gallery .quality {\n  font-size: 0.833vw;\n  font-weight: 700;\n}\n\n.gallery .details {\n  width: 15.625vw;\n  min-height: 4.74vw;\n  margin-top: -2.135vw;\n  padding: 0 0.417vw 0.677vw 0.781vw;\n  font-size: 1.25vw;\n  background-color: #d6e2ec;\n  max-width: 100%;\n  position: relative;\n  z-index: 1;\n  overflow: hidden;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: end;\n          align-items: flex-end;\n}\n\n.gallery .details__title {\n  font-size: 1.25vw;\n  font-weight: 400;\n}\n\n.gallery .details .date {\n  font-size: 0.833vw;\n  font-weight: 700;\n}\n\n.gallery .details p {\n  margin: 0;\n}\n\n.gallery .download {\n  padding: 0 0.417vw 0.677vw 0.781vw;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: end;\n          align-items: flex-end;\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  top: -100%;\n  right: 0;\n  left: 0;\n  background: #000;\n  color: #fff;\n  -webkit-transition: ease 0.3s;\n  transition: ease 0.3s;\n}\n\n.gallery__item:hover .download,\n.show-video + .details .download {\n  top: 0;\n  bottom: 0;\n}\n\n.search {\n  padding-top: 1.563vw;\n}\n\n.search form {\n  width: 37.604vw;\n  padding-left: 1.667vw;\n  text-align: center;\n  margin: 0 auto;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n}\n\n.search input {\n  width: 26.563vw;\n  height: 2.344vw;\n  padding: 0 2.188vw;\n  background: url(/assets/img/icon/search.png) 8px center no-repeat;\n  background-size: auto 85%;\n}\n\n.side-menu-page-wrap {\n  display: -webkit-inline-box;\n  display: inline-flex;\n}\n\n.overlay {\n  position: absolute;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: rgba(214, 226, 236, 0.8);\n  z-index: 9999;\n  padding-top: 180px;\n}\n\n.popup {\n  border: 1px solid #000000;\n  background-color: #ffffff;\n  margin: auto;\n  position: relative;\n  max-width: 100%;\n}\n\n.popup .close-btn {\n  cursor: pointer;\n  position: absolute;\n}\n\n.popup p {\n  color: #000000;\n  font-weight: 700;\n  margin: 0;\n}\n\n.popup__title {\n  color: #000000;\n  font-weight: 700;\n  text-transform: uppercase;\n  margin: 0;\n  text-align: center;\n}\n\n.upload-pictures .popup {\n  text-align: center;\n}\n\n.popup .images-counter {\n  color: #85a3bd;\n  font-weight: 700;\n  font-style: italic;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n}\n\n.add-more-pictures .popup-gallery {\n  display: -webkit-box;\n  display: flex;\n  flex-wrap: wrap;\n  /* max-height: 91vh;\n   overflow: auto;*/\n}\n\n.upload-pictures .popup {\n  width: 40.625vw;\n  height: 39.844vw;\n}\n\n.upload-pictures .popup__title {\n  margin-bottom: 4.323vw;\n}\n\n.upload-pictures p {\n  margin-bottom: 1.25vw;\n}\n\n.upload-pictures .btn-wrap.first {\n  padding-top: 1.875vw;\n  margin-bottom: 2.344vw;\n}\n\n.upload-pictures .btn-custom {\n  width: 10.938vw;\n}\n\n.upload-pictures img {\n  width: 10.417vw;\n}\n\n.upload-pictures .images-counter img {\n  width: 2.5vw;\n}\n\n.popup .images-counter {\n  font-size: 0.833vw;\n  padding-top: 0.938vw;\n}\n\n.popup .images-counter span {\n  padding-right: 1.146vw;\n}\n\n.add-more-pictures .popup {\n  width: 53.125vw;\n}\n\n.add-more-pictures .popup-gallery {\n  padding: 2.24vw 4.115vw 0;\n  max-height: 75vh;\n  overflow: auto;\n}\n\n.add-more-pictures .popup-gallery__item {\n  padding: 0 0.313vw 1.198vw;\n}\n\n.add-more-pictures .popup-gallery__item .top p {\n  font-size: 0.833vw;\n}\n\n.add-more-pictures .btn-wrap {\n  padding-bottom: 2.24vw;\n}\n\n.add-more-pictures .btn-wrap .btn-custom {\n  width: 10.938vw;\n}\n\n.popup {\n  box-shadow: 0 0 0.573vw 0.26vw rgba(0, 0, 0, 0.14);\n  border-radius: 1.25vw;\n  padding-top: 2.292vw;\n}\n\n.popup .close-btn {\n  width: 1.25vw;\n  height: 1.25vw;\n  right: 0.677vw;\n  top: 0.729vw;\n}\n\n.popup p {\n  font-size: 1.354vw;\n}\n\n.popup__title {\n  font-size: 1.354vw;\n}\n\n.add-more-pictures .popup-gallery .popup-gallery__item {\n  width: 25%;\n}\n\n.add-more-pictures .popup-gallery__item .top {\n  display: -webkit-box;\n  display: flex;\n  flex-wrap: wrap;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: end;\n          align-items: flex-end;\n  padding-right: 5px;\n}\n\n.add-more-pictures .btn-wrap {\n  text-align: center;\n}\n\n.delete {\n  cursor: pointer;\n}\n\n.add-more-pictures .popup-gallery__item img {\n  width: 100%;\n}\n\n.add-more-pictures .popup-gallery__item .top p {\n  color: #180a21;\n  font-weight: 700;\n}\n\n@media screen and (max-width: 1200px) {\n  app-side-menu {\n    width: 27%;\n  }\n\n  input, textarea {\n    font-size: 14px;\n  }\n\n  .gallery .time,\n.gallery .quality,\n.gallery .details .details__title,\n.add-more-pictures .popup-gallery__item .top p {\n    font-size: 14px;\n  }\n\n  .gallery .info__item {\n    padding-bottom: 10px;\n  }\n\n  .gallery__title {\n    font-size: 20px;\n  }\n\n  .side-menu-page-wrap {\n    display: -webkit-box;\n    display: flex;\n  }\n\n  .search form {\n    width: 100%;\n  }\n\n  .search form input {\n    width: 75%;\n  }\n\n  .gallery .details,\n.gallery .download {\n    padding: 19px 7px 7px 7px;\n  }\n\n  .gallery .details {\n    min-height: 48px;\n    margin-top: -21px;\n    width: 95%;\n  }\n\n  .gallery .details .date {\n    font-size: 12px;\n  }\n\n  .gallery .info {\n    flex-wrap: wrap;\n    -webkit-box-pack: center;\n            justify-content: center;\n  }\n\n  .search form .btn-custom {\n    min-width: 20%;\n  }\n\n  .add-more-pictures .popup {\n    width: 75%;\n  }\n\n  .overlay {\n    padding-top: 80px;\n  }\n\n  .popup__title {\n    font-size: 24px;\n  }\n\n  .upload-pictures .popup {\n    width: 75%;\n    height: auto;\n  }\n\n  .upload-pictures img {\n    width: 200px;\n  }\n\n  .popup p {\n    font-size: 16px;\n  }\n\n  .popup .images-counter {\n    font-size: 14px;\n  }\n}\n\n@media screen and (max-width: 992px) {\n  body * {\n    line-height: normal !important;\n  }\n\n  input {\n    height: 40px;\n    border-radius: 20px;\n  }\n\n  textarea {\n    border-radius: 20px;\n  }\n\n  .btn-custom {\n    font-size: 16px;\n  }\n\n  .btn-custom, .edit-profile .btn-custom:not(.facebook):not(.google) {\n    height: 40px;\n    min-width: 80px;\n    border-radius: 21px;\n    padding: 0 12px;\n  }\n\n  .gallery .download img {\n    display: none;\n  }\n\n  .search form input {\n    height: 40px;\n  }\n\n  .upload-pictures .images-counter img {\n    width: 30px;\n  }\n\n  .popup {\n    border-radius: 20px;\n    padding-top: 10px;\n  }\n\n  .add-more-pictures .btn-wrap .btn-custom {\n    width: auto;\n  }\n\n  .add-more-pictures .btn-wrap {\n    padding: 15px 0;\n  }\n\n  .add-more-pictures .popup-gallery .popup-gallery__item {\n    width: 33.333%;\n    padding: 5px 5px 0;\n  }\n\n  .popup .close-btn {\n    width: 24px;\n    height: 24px;\n    right: 10px;\n    top: 12px;\n  }\n\n  .upload-pictures .btn-custom {\n    width: auto;\n  }\n\n  .upload-pictures .btn-variant {\n    margin-bottom: 20px;\n  }\n\n  .popup .images-counter {\n    padding-top: 20px;\n    padding-bottom: 10px;\n  }\n}\n\n@media screen and (max-width: 767px) {\n  .container {\n    width: 100%;\n  }\n\n  .search {\n    padding: 75px 15px 0;\n  }\n\n  .gallery {\n    padding: 30px 15px 0;\n  }\n\n  app-side-menu {\n    width: 0;\n  }\n\n  .side-menu-page-wrap {\n    position: relative;\n  }\n\n  .upload-pictures .popup {\n    width: 95%;\n  }\n\n  .add-more-pictures .popup {\n    width: 95%;\n  }\n\n  .add-more-pictures .popup-gallery .popup-gallery__item {\n    width: 50%;\n  }\n}\n\n@media screen and (max-width: 600px) {\n  .gallery__item {\n    width: 50%;\n  }\n}\n\n@media screen and (max-width: 492px) {\n  .gallery__item {\n    width: 100%;\n  }\n\n  .popup {\n    padding-top: 41px;\n  }\n\n  .add-more-pictures .popup-gallery .popup-gallery__item {\n    width: 100%;\n  }\n\n  .gallery__item .item-top > img {\n    width: 100%;\n  }\n}\n\n@font-face {\n  font-family: \"Caviar Dreams\";\n  src: local(\"Caviar Dreams\"), local(\"CaviarDreams\"), url(/assets/fonts/caviardreams.woff2) format(\"woff2\"), url(/assets/fonts/caviardreams.woff) format(\"woff\"), url(/assets/fonts/caviardreams.ttf) format(\"truetype\");\n  font-weight: 400;\n  font-style: normal;\n}\n\n@font-face {\n  font-family: \"Caviar Dreams\";\n  src: local(\"Caviar Dreams Bold\"), local(\"CaviarDreams-Bold\"), url(/assets/fonts/caviardreamsbold.woff2) format(\"woff2\"), url(/assets/fonts/caviardreamsbold.woff) format(\"woff\"), url(/assets/fonts/caviardreamsbold.ttf) format(\"truetype\");\n  font-weight: 700;\n  font-style: normal;\n}\n\n@font-face {\n  font-family: \"Caviar Dreams\";\n  src: local(\"Caviar Dreams Italic\"), local(\"CaviarDreams-Italic\"), url(/assets/fonts/caviardreamsitalic.woff2) format(\"woff2\"), url(/assets/fonts/caviardreamsitalic.woff) format(\"woff\"), url(/assets/fonts/caviardreamsitalic.ttf) format(\"truetype\");\n  font-weight: 400;\n  font-style: italic;\n}\n\n@font-face {\n  font-family: \"Caviar Dreams\";\n  src: local(\"Caviar Dreams Bold Italic\"), local(\"CaviarDreams-BoldItalic\"), url(/assets/fonts/caviardreamsbolditalic.woff2) format(\"woff2\"), url(/assets/fonts/caviardreamsbolditalic.woff) format(\"woff\"), url(/assets/fonts/caviardreamsbolditalic.ttf) format(\"truetype\");\n  font-weight: 700;\n  font-style: italic;\n}\n\n@font-face {\n  font-family: \"Copperplate Gothic\";\n  src: url(/assets/fonts/CopperplateGothic-Bold.eot);\n  src: url(/assets/fonts/CopperplateGothic-Bold.eot?#iefix) format(\"embedded-opentype\"), url(/assets/fonts/CopperplateGothic-Bold.woff2) format(\"woff2\"), url(/assets/fonts/CopperplateGothic-Bold.woff) format(\"woff\"), url(/assets/fonts/CopperplateGothic-Bold.ttf) format(\"truetype\"), url(/assets/fonts/CopperplateGothic-Bold.svg#CopperplateGothic-Bold) format(\"svg\");\n  font-weight: bold;\n  font-style: normal;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9DOlxcVXNlcnNcXFZsYWREXFxEZXNrdG9wXFxNZW1vIChhbGwpXFxtZW1vL3NyY1xcc3R5bGVzLnNjc3MiLCJzcmMvc3R5bGVzLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxWbGFkRFxcRGVza3RvcFxcTWVtbyAoYWxsKVxcbWVtby9zcmNcXGFzc2V0c1xcdGhlbWVcXGZvbnRzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxzQkFBQTtBQ0NGOztBREVBO0VBQ0UsaUJBQUE7RUFBbUIsTUFBQTtFQUNuQiw4QkFBQTtFQUFnQyxNQUFBO0FDR2xDOztBREFBO0VBQ0UsU0FBQTtBQ0dGOztBREFBO0VBQ0UscUJBQUE7RUFDQSxlQUFBO0FDR0Y7O0FEQUE7RUFDRSw2QkFBQTtBQ0dGOztBREFBO0VBQ0UsZUFBQTtBQ0dGOztBREFBO0VBQ0UsYUFBQTtBQ0dGOztBREFBO0VBQ0UsZUFBQTtBQ0dGOztBREFBO0VBQ0Usd0NBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNHRjs7QURBQTtFQUNFLG9CQUFBO0VBQXNCLE1BQUE7RUFDdEIsZUFBQTtFQUFpQixNQUFBO0VBQ2pCLGlCQUFBO0VBQW1CLE1BQUE7RUFDbkIsU0FBQTtFQUFXLE1BQUE7QUNPYjs7QURKQTtFQUNFLGlCQUFBO0FDT0Y7O0FESkE7RUFDRSxvQkFBQTtBQ09GOztBREpBO0VBQ0UsY0FBQTtBQ09GOztBREpBO0VBQ0UseUJBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNPRjs7QURKQTtFQUNFLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFHQSwyQkFBQTtFQUFBLG9CQUFBO0VBR0EseUJBQUE7VUFBQSxtQkFBQTtFQUdBLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0FDT0Y7O0FESkE7RUFDRSx5QkFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7QUNPRjs7QURKQTtFQUNFLHlCQUFBO0VBQ0EscUJBQUE7RUFDQSx5QkFBQTtBQ09GOztBREpBOztFQUVFLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUdBLHVCQUFBO1VBQUEsMkJBQUE7QUNPRjs7QURORTs7RUFDRSxjQUFBO0VBQ0EsZUFBQTtBQ1NKOztBRFBFOztFQUNFLHFCQUFBO0FDVUo7O0FETkE7RUFDRSxvQkFBQTtBQ1NGOztBRE5BO0VBQ0UsYUFBQTtBQ1NGOztBRE5BO0VBQ0UsY0FBQTtFQUNBLHdDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNTRjs7QUROQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0FDU0Y7O0FETkE7RUFDRSxnQkFBQTtFQUNBLFdBQUE7QUNTRjs7QUROQTtFQUNFLGdCQUFBO0FDU0Y7O0FETkE7RUFDRSxnQkFBQTtBQ1NGOztBRFJFO0VBQ0Usc0JBQUE7QUNVSjs7QURUSTtFQUNFLHNCQUFBO0FDV047O0FEUkU7RUFDRSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUVBLGtCQUFBO0FDU0o7O0FEUkk7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0FDVU47O0FEUEU7RUFHRSxvQkFBQTtFQUFBLGFBQUE7RUFHQSxlQUFBO0FDU0o7O0FEUEU7RUFDRSxxQkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNTSjs7QURSSTtFQUNFLFVBQUE7QUNVTjs7QURSSTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FDVU47O0FEUEU7RUFDRSxxQkFBQTtFQUNBLGtCQUFBO0VBR0Esb0JBQUE7RUFBQSxhQUFBO0VBR0Esc0JBQUE7VUFBQSxxQkFBQTtFQUNBLGdCQUFBO0FDU0o7O0FEUkk7RUFDRSxrQkFBQTtBQ1VOOztBRFBFO0VBQ0UsdUJBQUE7RUFHQSxvQkFBQTtFQUFBLGFBQUE7RUFHQSx5QkFBQTtVQUFBLDhCQUFBO0FDU0o7O0FEUkk7RUFHRSxvQkFBQTtFQUFBLGFBQUE7RUFHQSxzQkFBQTtVQUFBLHFCQUFBO0FDVU47O0FEVE07RUFDRSxpQkFBQTtBQ1dSOztBRFBFO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBQ1NKOztBRFBFO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQ0FBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFHQSxvQkFBQTtFQUFBLGFBQUE7RUFHQSx5QkFBQTtVQUFBLDhCQUFBO0VBR0Esc0JBQUE7VUFBQSxxQkFBQTtBQ1NKOztBRFJJO0VBQ0UsaUJBQUE7RUFDQSxnQkFBQTtBQ1VOOztBRFJJO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBQ1VOOztBRFJJO0VBQ0UsU0FBQTtBQ1VOOztBRFBFO0VBQ0Usa0NBQUE7RUFHQSxvQkFBQTtFQUFBLGFBQUE7RUFHQSx5QkFBQTtVQUFBLDhCQUFBO0VBR0Esc0JBQUE7VUFBQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtFQUNBLE9BQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSw2QkFBQTtFQUlBLHFCQUFBO0FDU0o7O0FETEE7O0VBRUUsTUFBQTtFQUNBLFNBQUE7QUNRRjs7QURKQTtFQUNFLG9CQUFBO0FDT0Y7O0FETkU7RUFDRSxlQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFHQSxvQkFBQTtFQUFBLGFBQUE7RUFHQSx5QkFBQTtVQUFBLDhCQUFBO0FDUUo7O0FETkU7RUFDRSxlQUFBO0VBQ0EsZUFBQTtFQUVBLGtCQUFBO0VBQ0EsaUVBQUE7RUFDQSx5QkFBQTtBQ09KOztBREhBO0VBR0UsMkJBQUE7RUFBQSxvQkFBQTtBQ01GOztBREhBO0VBQ0Usa0JBQUE7RUFDQSxNQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxvQ0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtBQ01GOztBREhBO0VBQ0UseUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUNNRjs7QURIQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBQ01GOztBREhBO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsU0FBQTtBQ01GOztBREhBO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7QUNNRjs7QURIQTtFQUNFLGtCQUFBO0FDTUY7O0FESEE7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUdBLG9CQUFBO0VBQUEsYUFBQTtFQUdBLHdCQUFBO1VBQUEsdUJBQUE7RUFHQSx5QkFBQTtVQUFBLG1CQUFBO0FDTUY7O0FESEE7RUFHRSxvQkFBQTtFQUFBLGFBQUE7RUFHQSxlQUFBO0VBQ0E7bUJBQUE7QUNPRjs7QURKQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtBQ09GOztBREpBO0VBQ0Usc0JBQUE7QUNPRjs7QURKQTtFQUNFLHFCQUFBO0FDT0Y7O0FESkE7RUFDRSxvQkFBQTtFQUNBLHNCQUFBO0FDT0Y7O0FESkE7RUFDRSxlQUFBO0FDT0Y7O0FESkE7RUFDRSxlQUFBO0FDT0Y7O0FESkE7RUFDRSxZQUFBO0FDT0Y7O0FESkE7RUFDRSxrQkFBQTtFQUNBLG9CQUFBO0FDT0Y7O0FESkE7RUFDRSxzQkFBQTtBQ09GOztBREpBO0VBQ0UsZUFBQTtBQ09GOztBREpBO0VBQ0UseUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUNPRjs7QURKQTtFQUNFLDBCQUFBO0FDT0Y7O0FESkE7RUFDRSxrQkFBQTtBQ09GOztBREpBO0VBQ0Usc0JBQUE7QUNPRjs7QURKQTtFQUNFLGVBQUE7QUNPRjs7QURMQTtFQUNFLGtEQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtBQ1FGOztBRExBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ1FGOztBRExBO0VBQ0Usa0JBQUE7QUNRRjs7QURMQTtFQUNFLGtCQUFBO0FDUUY7O0FETEE7RUFDRSxVQUFBO0FDUUY7O0FETEE7RUFHRSxvQkFBQTtFQUFBLGFBQUE7RUFHQSxlQUFBO0VBR0EseUJBQUE7VUFBQSw4QkFBQTtFQUdBLHNCQUFBO1VBQUEscUJBQUE7RUFDQSxrQkFBQTtBQ1FGOztBRExBO0VBQ0Usa0JBQUE7QUNRRjs7QURMQTtFQUNFLGVBQUE7QUNRRjs7QURMQTtFQUNFLFdBQUE7QUNRRjs7QURMQTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtBQ1FGOztBREhBO0VBRUU7SUFDRSxVQUFBO0VDS0Y7O0VERkE7SUFDRSxlQUFBO0VDS0Y7O0VERkE7Ozs7SUFJRSxlQUFBO0VDS0Y7O0VERkE7SUFDRSxvQkFBQTtFQ0tGOztFREZBO0lBQ0UsZUFBQTtFQ0tGOztFREZBO0lBR0Usb0JBQUE7SUFBQSxhQUFBO0VDS0Y7O0VERkE7SUFDRSxXQUFBO0VDS0Y7O0VERkE7SUFDRSxVQUFBO0VDS0Y7O0VEREE7O0lBRUUseUJBQUE7RUNJRjs7RURBQTtJQUNFLGdCQUFBO0lBQ0EsaUJBQUE7SUFDQSxVQUFBO0VDR0Y7O0VEQUE7SUFDRSxlQUFBO0VDR0Y7O0VEQUE7SUFHRSxlQUFBO0lBR0Esd0JBQUE7WUFBQSx1QkFBQTtFQ0dGOztFREFBO0lBQ0UsY0FBQTtFQ0dGOztFREFBO0lBQ0UsVUFBQTtFQ0dGOztFREFBO0lBQ0UsaUJBQUE7RUNHRjs7RURBQTtJQUNFLGVBQUE7RUNHRjs7RURBQTtJQUNFLFVBQUE7SUFDQSxZQUFBO0VDR0Y7O0VEQUE7SUFDRSxZQUFBO0VDR0Y7O0VEQUE7SUFDRSxlQUFBO0VDR0Y7O0VEQUE7SUFDRSxlQUFBO0VDR0Y7QUFDRjs7QURBQTtFQUNFO0lBQ0UsOEJBQUE7RUNFRjs7RURDQTtJQUNFLFlBQUE7SUFDQSxtQkFBQTtFQ0VGOztFRENBO0lBQ0UsbUJBQUE7RUNFRjs7RURDQTtJQUNFLGVBQUE7RUNFRjs7RURDQTtJQUNFLFlBQUE7SUFDQSxlQUFBO0lBQ0EsbUJBQUE7SUFDQSxlQUFBO0VDRUY7O0VEQ0E7SUFDRSxhQUFBO0VDRUY7O0VEQ0E7SUFDRSxZQUFBO0VDRUY7O0VEQ0E7SUFDRSxXQUFBO0VDRUY7O0VEQ0E7SUFDRSxtQkFBQTtJQUNBLGlCQUFBO0VDRUY7O0VEQ0E7SUFDRSxXQUFBO0VDRUY7O0VEQ0E7SUFDRSxlQUFBO0VDRUY7O0VEQ0E7SUFDRSxjQUFBO0lBQ0Esa0JBQUE7RUNFRjs7RURDQTtJQUNFLFdBQUE7SUFDQSxZQUFBO0lBQ0EsV0FBQTtJQUNBLFNBQUE7RUNFRjs7RURDQTtJQUNFLFdBQUE7RUNFRjs7RURDQTtJQUNFLG1CQUFBO0VDRUY7O0VEQUE7SUFDRSxpQkFBQTtJQUNBLG9CQUFBO0VDR0Y7QUFDRjs7QURBQTtFQUNFO0lBQ0UsV0FBQTtFQ0VGOztFRENBO0lBQ0Usb0JBQUE7RUNFRjs7RURDQTtJQUNFLG9CQUFBO0VDRUY7O0VEQ0E7SUFDRSxRQUFBO0VDRUY7O0VEQ0E7SUFDRSxrQkFBQTtFQ0VGOztFRENBO0lBQ0UsVUFBQTtFQ0VGOztFRENBO0lBQ0UsVUFBQTtFQ0VGOztFRENBO0lBQ0UsVUFBQTtFQ0VGO0FBQ0Y7O0FEQ0E7RUFDRTtJQUNFLFVBQUE7RUNDRjtBQUNGOztBREVBO0VBQ0U7SUFDRSxXQUFBO0VDQUY7O0VER0E7SUFDRSxpQkFBQTtFQ0FGOztFREVBO0lBQ0UsV0FBQTtFQ0NGOztFREVBO0lBQ0UsV0FBQTtFQ0NGO0FBQ0Y7O0FDaHZCQTtFQUNFLDRCQUFBO0VBQ0Esc05BQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FEa3ZCRjs7QUMvdUJBO0VBQ0UsNEJBQUE7RUFDQSw0T0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QURpdkJGOztBQzl1QkE7RUFDRSw0QkFBQTtFQUNBLHNQQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBRGd2QkY7O0FDN3VCQTtFQUNFLDRCQUFBO0VBQ0EsMlFBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FEK3VCRjs7QUM1dUJBO0VBQ0UsaUNBQUE7RUFDQSxrREFBQTtFQUNBLDJXQUFBO0VBS0EsaUJBQUE7RUFDQSxrQkFBQTtBRDB1QkYiLCJmaWxlIjoic3JjL3N0eWxlcy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYm9keSAqIHtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG59XHJcblxyXG5odG1sIHtcclxuICBsaW5lLWhlaWdodDogMS4xNTsgLyogMSAqL1xyXG4gIC13ZWJraXQtdGV4dC1zaXplLWFkanVzdDogMTAwJTsgLyogMiAqL1xyXG59XHJcblxyXG5ib2R5IHtcclxuICBtYXJnaW46IDA7XHJcbn1cclxuXHJcbmEsIGE6aG92ZXIsIGE6Zm9jdXMge1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbmEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG5idXR0b24ge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuYnV0dG9uOmZvY3VzIHtcclxuICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG5pbWcge1xyXG4gIG1heC13aWR0aDogMTAwJTtcclxufVxyXG5cclxuYm9keSB7XHJcbiAgZm9udC1mYW1pbHk6ICdDYXZpYXIgRHJlYW1zJywgc2Fucy1zZXJpZjtcclxuICBjb2xvcjogIzAwMDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbmJ1dHRvbiwgaW5wdXQsIG9wdGdyb3VwLCBzZWxlY3QsIHRleHRhcmVhIHtcclxuICBmb250LWZhbWlseTogaW5oZXJpdDsgLyogMSAqL1xyXG4gIGZvbnQtc2l6ZTogMTAwJTsgLyogMSAqL1xyXG4gIGxpbmUtaGVpZ2h0OiAxLjE1OyAvKiAxICovXHJcbiAgbWFyZ2luOiAwOyAvKiAyICovXHJcbn1cclxuXHJcbmJ1dHRvbiwgaW5wdXQge1xyXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xyXG59XHJcblxyXG5idXR0b24sIHNlbGVjdCB7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbn1cclxuXHJcbnRleHRhcmVhIHtcclxuICBvdmVyZmxvdzogYXV0bztcclxufVxyXG5cclxuaW5wdXQsIHRleHRhcmVhIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjMDAwMDAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgaGVpZ2h0OiAyLjUwMHZ3O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEuMjUwdnc7XHJcbiAgcGFkZGluZzogMCAxLjMwMnZ3O1xyXG4gIGZvbnQtc2l6ZTogMC44MzN2dztcclxufVxyXG5cclxuLmJ0bi1jdXN0b20ge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDAwMDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGRpc3BsYXk6IC1tcy1pbmxpbmUtZmxleGJveDtcclxuICBkaXNwbGF5OiAtd2Via2l0LWlubGluZS1mbGV4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWluLXdpZHRoOiA3LjgxM3Z3O1xyXG4gIGhlaWdodDogMi4zNDR2dztcclxuICBib3JkZXItcmFkaXVzOiAxLjE5OHZ3O1xyXG4gIGZvbnQtc2l6ZTogMC45Mzh2dztcclxufVxyXG5cclxuLmZhY2Vib29rIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2I1OTk4O1xyXG4gIGJvcmRlci1jb2xvcjogIzNiNTk5ODtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG4uZ29vZ2xlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzc0YTJkO1xyXG4gIGJvcmRlci1jb2xvcjogI2M3NGEyZDtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG4uYnRuLWN1c3RvbS5mYWNlYm9vayxcclxuLmJ0bi1jdXN0b20uZ29vZ2xlIHtcclxuICBmb250LXNpemU6IDAuODMzdnc7XHJcbiAgd2lkdGg6IDE0LjU4M3Z3O1xyXG4gIGhlaWdodDogMi41MDB2dztcclxuICBwYWRkaW5nLWxlZnQ6IDAuNDE3dnc7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gIGltZyB7XHJcbiAgICB3aWR0aDogMS43MTl2dztcclxuICAgIGhlaWdodDogMS43MTl2dztcclxuICB9XHJcbiAgcCB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEuMzAydnc7XHJcbiAgfVxyXG59XHJcblxyXG4uZmFjZWJvb2sgcCwgLmdvb2dsZSBwIHtcclxuICBtYXJnaW46IDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW5wdXQ6Zm9jdXMsIHRleHRhcmVhOmZvY3VzIHtcclxuICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4ucGFnZS10aXRsZSB7XHJcbiAgY29sb3I6ICNkNmUyZWM7XHJcbiAgZm9udC1mYW1pbHk6IFwiQ29wcGVycGxhdGUgR290aGljXCIsIHNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogNi4yNTB2dztcclxuICBtYXJnaW46IDAuNTczdncgMCAwO1xyXG59XHJcblxyXG4uY29udGFpbmVyIHtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICB3aWR0aDogNTYuNjY3dnc7XHJcbn1cclxuXHJcbi5saWdodCB7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBjb2xvcjogIzAwMDtcclxufVxyXG5cclxuLndhcm5pbmcge1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuXHJcbi5nYWxsZXJ5IHtcclxuICBwYWRkaW5nLXRvcDogNXZ3O1xyXG4gICZfX3dyYXAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMS44NzV2dztcclxuICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDYuMDk0dnc7XHJcbiAgICB9XHJcbiAgfVxyXG4gICZfX3RpdGxlIHtcclxuICAgIGZvbnQtc2l6ZTogMS41NjN2dztcclxuICAgIG1hcmdpbi1sZWZ0OiAxLjY2N3Z3O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMi45MTd2dztcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICAvL21hcmdpbjogMDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICY6YWZ0ZXIge1xyXG4gICAgICBjb250ZW50OiAnJztcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwO1xyXG4gICAgICB3aWR0aDogOS4zNzV2dztcclxuICAgICAgaGVpZ2h0OiAwLjc4MXZ3O1xyXG4gICAgICBib3R0b206IC0xLjE5OHZ3O1xyXG4gICAgICBsZWZ0OiAwLjE1NnZ3O1xyXG4gICAgfVxyXG4gIH1cclxuICAmX19yb3cge1xyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gIH1cclxuICAmX19pdGVtIHtcclxuICAgIHBhZGRpbmctbGVmdDogMS4wNDJ2dztcclxuICAgIG1hcmdpbi1ib3R0b206IDEuNTEwdnc7XHJcbiAgICB3aWR0aDogMzMuMzMzJTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICY6aG92ZXIgLml0ZW0tdG9wIHtcclxuICAgICAgei1pbmRleDogOTtcclxuICAgIH1cclxuICAgIC5pdGVtLXRvcCB7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMC42Nzd2dztcclxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICB6LWluZGV4OiAyO1xyXG4gICAgfVxyXG4gIH1cclxuICAudGltZSB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDAuNTczdnc7XHJcbiAgICBmb250LXNpemU6IDAuODMzdnc7XHJcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICAgIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIC1tcy1mbGV4LWFsaWduOiBlbmQ7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgaW1nIHtcclxuICAgICAgcGFkZGluZy1yaWdodDogNXB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuaW5mbyB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMC4xMDR2dztcclxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICZfX2l0ZW17XHJcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gICAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIC13ZWJraXQtYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgICAtbXMtZmxleC1hbGlnbjogZW5kO1xyXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICAgICY6bGFzdC1jaGlsZCBzcGFuIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAucXVhbGl0eSB7XHJcbiAgICBmb250LXNpemU6IDAuODMzdnc7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG4gIH1cclxuICAuZGV0YWlscyB7XHJcbiAgICB3aWR0aDogMTUuNjI1dnc7XHJcbiAgICBtaW4taGVpZ2h0OiA0Ljc0MHZ3O1xyXG4gICAgbWFyZ2luLXRvcDogLTIuMTM1dnc7XHJcbiAgICBwYWRkaW5nOiAwIDAuNDE3dncgMC42Nzd2dyAwLjc4MXZ3O1xyXG4gICAgZm9udC1zaXplOiAxLjI1MHZ3O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Q2ZTJlYztcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgLXdlYmtpdC1hbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICAtbXMtZmxleC1hbGlnbjogZW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgJl9fdGl0bGUge1xyXG4gICAgICBmb250LXNpemU6IDEuMjUwdnc7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICB9XHJcbiAgICAuZGF0ZSB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMC44MzN2dztcclxuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgIH1cclxuICAgIHAge1xyXG4gICAgICBtYXJnaW46IDA7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5kb3dubG9hZCB7XHJcbiAgICBwYWRkaW5nOiAwIDAuNDE3dncgMC42Nzd2dyAwLjc4MXZ3O1xyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgLXdlYmtpdC1hbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICAtbXMtZmxleC1hbGlnbjogZW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICB0b3A6IC0xMDAlO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBlYXNlIDAuM3M7XHJcbiAgICAtbW96LXRyYW5zaXRpb246IGVhc2UgMC4zcztcclxuICAgIC1tcy10cmFuc2l0aW9uOiBlYXNlIDAuM3M7XHJcbiAgICAtby10cmFuc2l0aW9uOiBlYXNlIDAuM3M7XHJcbiAgICB0cmFuc2l0aW9uOiBlYXNlIDAuM3M7XHJcbiAgfVxyXG59XHJcblxyXG4uZ2FsbGVyeV9faXRlbTpob3ZlciAuZG93bmxvYWQsXHJcbi5zaG93LXZpZGVvICsgLmRldGFpbHMgLmRvd25sb2FkIHtcclxuICB0b3A6IDA7XHJcbiAgYm90dG9tOiAwO1xyXG59XHJcblxyXG5cclxuLnNlYXJjaCB7XHJcbiAgcGFkZGluZy10b3A6IDEuNTYzdnc7XHJcbiAgZm9ybSB7XHJcbiAgICB3aWR0aDogMzcuNjA0dnc7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEuNjY3dnc7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB9XHJcbiAgaW5wdXQge1xyXG4gICAgd2lkdGg6IDI2LjU2M3Z3O1xyXG4gICAgaGVpZ2h0OiAyLjM0NHZ3O1xyXG4gICAgLy9iYWNrZ3JvdW5kLXBvc2l0aW9uOiAwLjQxN3Z3IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDAgMi4xODh2dztcclxuICAgIGJhY2tncm91bmQ6IHVybCgvYXNzZXRzL2ltZy9pY29uL3NlYXJjaC5wbmcpIDhweCBjZW50ZXIgbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBhdXRvIDg1JTtcclxuICB9XHJcbn1cclxuXHJcbi5zaWRlLW1lbnUtcGFnZS13cmFwIHtcclxuICBkaXNwbGF5OiAtbXMtaW5saW5lLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1pbmxpbmUtZmxleDtcclxuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxufVxyXG5cclxuLm92ZXJsYXkge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMjE0LCAyMjYsIDIzNiwgMC44KTtcclxuICB6LWluZGV4OiA5OTk5O1xyXG4gIHBhZGRpbmctdG9wOiAxODBweDtcclxufVxyXG5cclxuLnBvcHVwIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjMDAwMDAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBtYXgtd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5wb3B1cCAuY2xvc2UtYnRuIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcblxyXG4ucG9wdXAgcCB7XHJcbiAgY29sb3I6ICMwMDAwMDA7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBtYXJnaW46IDA7XHJcbn1cclxuXHJcbi5wb3B1cF9fdGl0bGUge1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBtYXJnaW46IDA7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4udXBsb2FkLXBpY3R1cmVzIC5wb3B1cCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ucG9wdXAgLmltYWdlcy1jb3VudGVyIHtcclxuICBjb2xvcjogIzg1YTNiZDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwLWdhbGxlcnkge1xyXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xyXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG4gIC8qIG1heC1oZWlnaHQ6IDkxdmg7XHJcbiAgIG92ZXJmbG93OiBhdXRvOyovXHJcbn1cclxuLnVwbG9hZC1waWN0dXJlcyAucG9wdXAge1xyXG4gIHdpZHRoOiA0MC42MjV2dztcclxuICBoZWlnaHQ6IDM5Ljg0NHZ3O1xyXG59XHJcblxyXG4udXBsb2FkLXBpY3R1cmVzIC5wb3B1cF9fdGl0bGUge1xyXG4gIG1hcmdpbi1ib3R0b206IDQuMzIzdnc7XHJcbn1cclxuXHJcbi51cGxvYWQtcGljdHVyZXMgcCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMS4yNTB2dztcclxufVxyXG5cclxuLnVwbG9hZC1waWN0dXJlcyAuYnRuLXdyYXAuZmlyc3Qge1xyXG4gIHBhZGRpbmctdG9wOiAxLjg3NXZ3O1xyXG4gIG1hcmdpbi1ib3R0b206IDIuMzQ0dnc7XHJcbn1cclxuXHJcbi51cGxvYWQtcGljdHVyZXMgLmJ0bi1jdXN0b20ge1xyXG4gIHdpZHRoOiAxMC45Mzh2dztcclxufVxyXG5cclxuLnVwbG9hZC1waWN0dXJlcyBpbWcge1xyXG4gIHdpZHRoOiAxMC40MTd2dztcclxufVxyXG5cclxuLnVwbG9hZC1waWN0dXJlcyAuaW1hZ2VzLWNvdW50ZXIgaW1nIHtcclxuICB3aWR0aDogMi41MDB2dztcclxufVxyXG5cclxuLnBvcHVwIC5pbWFnZXMtY291bnRlciB7XHJcbiAgZm9udC1zaXplOiAwLjgzM3Z3O1xyXG4gIHBhZGRpbmctdG9wOiAwLjkzOHZ3O1xyXG59XHJcblxyXG4ucG9wdXAgLmltYWdlcy1jb3VudGVyIHNwYW4ge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDEuMTQ2dnc7XHJcbn1cclxuXHJcbi5hZGQtbW9yZS1waWN0dXJlcyAucG9wdXAge1xyXG4gIHdpZHRoOiA1My4xMjV2dztcclxufVxyXG5cclxuLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cC1nYWxsZXJ5IHtcclxuICBwYWRkaW5nOiAyLjI0MHZ3IDQuMTE1dncgMDtcclxuICBtYXgtaGVpZ2h0OiA3NXZoO1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG59XHJcblxyXG4uYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwLWdhbGxlcnlfX2l0ZW0ge1xyXG4gIHBhZGRpbmc6IDAgMC4zMTN2dyAxLjE5OHZ3O1xyXG59XHJcblxyXG4uYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwLWdhbGxlcnlfX2l0ZW0gLnRvcCBwIHtcclxuICBmb250LXNpemU6IDAuODMzdnc7XHJcbn1cclxuXHJcbi5hZGQtbW9yZS1waWN0dXJlcyAuYnRuLXdyYXAge1xyXG4gIHBhZGRpbmctYm90dG9tOiAyLjI0MHZ3O1xyXG59XHJcblxyXG4uYWRkLW1vcmUtcGljdHVyZXMgLmJ0bi13cmFwIC5idG4tY3VzdG9tIHtcclxuICB3aWR0aDogMTAuOTM4dnc7XHJcbn1cclxuLnBvcHVwIHtcclxuICBib3gtc2hhZG93OiAwIDAgMC41NzN2dyAwLjI2MHZ3IHJnYmEoMCwgMCwgMCwgMC4xNCk7XHJcbiAgYm9yZGVyLXJhZGl1czogMS4yNTB2dztcclxuICBwYWRkaW5nLXRvcDogMi4yOTJ2dztcclxufVxyXG5cclxuLnBvcHVwIC5jbG9zZS1idG4ge1xyXG4gIHdpZHRoOiAxLjI1MHZ3O1xyXG4gIGhlaWdodDogMS4yNTB2dztcclxuICByaWdodDogMC42Nzd2dztcclxuICB0b3A6IDAuNzI5dnc7XHJcbn1cclxuXHJcbi5wb3B1cCBwIHtcclxuICBmb250LXNpemU6IDEuMzU0dnc7XHJcbn1cclxuXHJcbi5wb3B1cF9fdGl0bGUge1xyXG4gIGZvbnQtc2l6ZTogMS4zNTR2dztcclxufVxyXG5cclxuLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cC1nYWxsZXJ5IC5wb3B1cC1nYWxsZXJ5X19pdGVtIHtcclxuICB3aWR0aDogMjUlO1xyXG59XHJcblxyXG4uYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwLWdhbGxlcnlfX2l0ZW0gLnRvcCB7XHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgLW1zLWZsZXgtYWxpZ246IGVuZDtcclxuICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgcGFkZGluZy1yaWdodDogNXB4O1xyXG59XHJcblxyXG4uYWRkLW1vcmUtcGljdHVyZXMgLmJ0bi13cmFwIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5kZWxldGUge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cC1nYWxsZXJ5X19pdGVtIGltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5hZGQtbW9yZS1waWN0dXJlcyAucG9wdXAtZ2FsbGVyeV9faXRlbSAudG9wIHAge1xyXG4gIGNvbG9yOiAjMTgwYTIxO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSB7XHJcblxyXG4gIGFwcC1zaWRlLW1lbnUge1xyXG4gICAgd2lkdGg6IDI3JTtcclxuICB9XHJcblxyXG4gIGlucHV0LCB0ZXh0YXJlYSB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgfVxyXG5cclxuICAuZ2FsbGVyeSAudGltZSxcclxuICAuZ2FsbGVyeSAucXVhbGl0eSxcclxuICAuZ2FsbGVyeSAuZGV0YWlscyAuZGV0YWlsc19fdGl0bGUsXHJcbiAgLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cC1nYWxsZXJ5X19pdGVtIC50b3AgcCB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgfVxyXG5cclxuICAuZ2FsbGVyeSAuaW5mb19faXRlbSB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICB9XHJcblxyXG4gIC5nYWxsZXJ5X190aXRsZSB7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgfVxyXG5cclxuICAuc2lkZS1tZW51LXBhZ2Utd3JhcCB7XHJcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICAgIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgfVxyXG5cclxuICAuc2VhcmNoIGZvcm0ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuICAuc2VhcmNoIGZvcm0gaW5wdXQge1xyXG4gICAgd2lkdGg6IDc1JTtcclxuICB9XHJcblxyXG5cclxuICAuZ2FsbGVyeSAuZGV0YWlscyxcclxuICAuZ2FsbGVyeSAuZG93bmxvYWQge1xyXG4gICAgcGFkZGluZzogMTlweCA3cHggN3B4IDdweDtcclxuICB9XHJcblxyXG5cclxuICAuZ2FsbGVyeSAuZGV0YWlscyB7XHJcbiAgICBtaW4taGVpZ2h0OiA0OHB4O1xyXG4gICAgbWFyZ2luLXRvcDogLTIxcHg7XHJcbiAgICB3aWR0aDogOTUlO1xyXG4gIH1cclxuXHJcbiAgLmdhbGxlcnkgLmRldGFpbHMgLmRhdGUge1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gIH1cclxuXHJcbiAgLmdhbGxlcnkgLmluZm8ge1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgLnNlYXJjaCBmb3JtIC5idG4tY3VzdG9tIHtcclxuICAgIG1pbi13aWR0aDogMjAlO1xyXG4gIH1cclxuXHJcbiAgLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cCB7XHJcbiAgICB3aWR0aDogNzUlO1xyXG4gIH1cclxuXHJcbiAgLm92ZXJsYXkge1xyXG4gICAgcGFkZGluZy10b3A6IDgwcHg7XHJcbiAgfVxyXG5cclxuICAucG9wdXBfX3RpdGxlIHtcclxuICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICB9XHJcblxyXG4gIC51cGxvYWQtcGljdHVyZXMgLnBvcHVwIHtcclxuICAgIHdpZHRoOiA3NSU7XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbiAgfVxyXG5cclxuICAudXBsb2FkLXBpY3R1cmVzIGltZyB7XHJcbiAgICB3aWR0aDogMjAwcHg7XHJcbiAgfVxyXG5cclxuICAucG9wdXAgcCB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgfVxyXG5cclxuICAucG9wdXAgLmltYWdlcy1jb3VudGVyIHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XHJcbiAgYm9keSAqIHtcclxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWwgIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIGlucHV0IHtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgfVxyXG5cclxuICB0ZXh0YXJlYSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIH1cclxuXHJcbiAgLmJ0bi1jdXN0b20ge1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gIH1cclxuXHJcbiAgLmJ0bi1jdXN0b20sIC5lZGl0LXByb2ZpbGUgLmJ0bi1jdXN0b206bm90KC5mYWNlYm9vayk6bm90KC5nb29nbGUpIHtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIG1pbi13aWR0aDogODBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XHJcbiAgICBwYWRkaW5nOiAwIDEycHg7XHJcbiAgfVxyXG5cclxuICAuZ2FsbGVyeSAuZG93bmxvYWQgaW1nIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAuc2VhcmNoIGZvcm0gaW5wdXQge1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gIH1cclxuXHJcbiAgLnVwbG9hZC1waWN0dXJlcyAuaW1hZ2VzLWNvdW50ZXIgaW1nIHtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG4gIH1cclxuXHJcbiAgLnBvcHVwIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICB9XHJcblxyXG4gIC5hZGQtbW9yZS1waWN0dXJlcyAuYnRuLXdyYXAgLmJ0bi1jdXN0b20ge1xyXG4gICAgd2lkdGg6IGF1dG87XHJcbiAgfVxyXG5cclxuICAuYWRkLW1vcmUtcGljdHVyZXMgLmJ0bi13cmFwIHtcclxuICAgIHBhZGRpbmc6IDE1cHggMDtcclxuICB9XHJcblxyXG4gIC5hZGQtbW9yZS1waWN0dXJlcyAucG9wdXAtZ2FsbGVyeSAucG9wdXAtZ2FsbGVyeV9faXRlbSB7XHJcbiAgICB3aWR0aDogMzMuMzMzJTtcclxuICAgIHBhZGRpbmc6IDVweCA1cHggMDtcclxuICB9XHJcblxyXG4gIC5wb3B1cCAuY2xvc2UtYnRuIHtcclxuICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgcmlnaHQ6IDEwcHg7XHJcbiAgICB0b3A6IDEycHg7XHJcbiAgfVxyXG5cclxuICAudXBsb2FkLXBpY3R1cmVzIC5idG4tY3VzdG9tIHtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gIH1cclxuXHJcbiAgLnVwbG9hZC1waWN0dXJlcyAuYnRuLXZhcmlhbnQge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICB9XHJcbiAgLnBvcHVwIC5pbWFnZXMtY291bnRlciB7XHJcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcclxuICAuY29udGFpbmVyIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLnNlYXJjaCB7XHJcbiAgICBwYWRkaW5nOiA3NXB4IDE1cHggMDtcclxuICB9XHJcblxyXG4gIC5nYWxsZXJ5IHtcclxuICAgIHBhZGRpbmc6IDMwcHggMTVweCAwO1xyXG4gIH1cclxuXHJcbiAgYXBwLXNpZGUtbWVudSB7XHJcbiAgICB3aWR0aDogMDtcclxuICB9XHJcblxyXG4gIC5zaWRlLW1lbnUtcGFnZS13cmFwIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB9XHJcblxyXG4gIC51cGxvYWQtcGljdHVyZXMgLnBvcHVwIHtcclxuICAgIHdpZHRoOiA5NSU7XHJcbiAgfVxyXG5cclxuICAuYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwIHtcclxuICAgIHdpZHRoOiA5NSU7XHJcbiAgfVxyXG5cclxuICAuYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwLWdhbGxlcnkgLnBvcHVwLWdhbGxlcnlfX2l0ZW0ge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XHJcbiAgLmdhbGxlcnlfX2l0ZW0ge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ5MnB4KSB7XHJcbiAgLmdhbGxlcnlfX2l0ZW0ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuICAucG9wdXAge1xyXG4gICAgcGFkZGluZy10b3A6IDQxcHg7XHJcbiAgfVxyXG4gIC5hZGQtbW9yZS1waWN0dXJlcyAucG9wdXAtZ2FsbGVyeSAucG9wdXAtZ2FsbGVyeV9faXRlbSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5nYWxsZXJ5X19pdGVtIC5pdGVtLXRvcCA+IGltZyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbn1cclxuIiwiYm9keSAqIHtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbn1cblxuaHRtbCB7XG4gIGxpbmUtaGVpZ2h0OiAxLjE1O1xuICAvKiAxICovXG4gIC13ZWJraXQtdGV4dC1zaXplLWFkanVzdDogMTAwJTtcbiAgLyogMiAqL1xufVxuXG5ib2R5IHtcbiAgbWFyZ2luOiAwO1xufVxuXG5hLCBhOmhvdmVyLCBhOmZvY3VzIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbmEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cblxuYnV0dG9uIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5idXR0b246Zm9jdXMge1xuICBvdXRsaW5lOiBub25lO1xufVxuXG5pbWcge1xuICBtYXgtd2lkdGg6IDEwMCU7XG59XG5cbmJvZHkge1xuICBmb250LWZhbWlseTogXCJDYXZpYXIgRHJlYW1zXCIsIHNhbnMtc2VyaWY7XG4gIGNvbG9yOiAjMDAwO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbmJ1dHRvbiwgaW5wdXQsIG9wdGdyb3VwLCBzZWxlY3QsIHRleHRhcmVhIHtcbiAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XG4gIC8qIDEgKi9cbiAgZm9udC1zaXplOiAxMDAlO1xuICAvKiAxICovXG4gIGxpbmUtaGVpZ2h0OiAxLjE1O1xuICAvKiAxICovXG4gIG1hcmdpbjogMDtcbiAgLyogMiAqL1xufVxuXG5idXR0b24sIGlucHV0IHtcbiAgb3ZlcmZsb3c6IHZpc2libGU7XG59XG5cbmJ1dHRvbiwgc2VsZWN0IHtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG59XG5cbnRleHRhcmVhIHtcbiAgb3ZlcmZsb3c6IGF1dG87XG59XG5cbmlucHV0LCB0ZXh0YXJlYSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDAwMDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIGhlaWdodDogMi41dnc7XG4gIGJvcmRlci1yYWRpdXM6IDEuMjV2dztcbiAgcGFkZGluZzogMCAxLjMwMnZ3O1xuICBmb250LXNpemU6IDAuODMzdnc7XG59XG5cbi5idG4tY3VzdG9tIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwMDAwMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgY29sb3I6ICNmZmY7XG4gIGRpc3BsYXk6IC1tcy1pbmxpbmUtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1pbmxpbmUtZmxleDtcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWluLXdpZHRoOiA3LjgxM3Z3O1xuICBoZWlnaHQ6IDIuMzQ0dnc7XG4gIGJvcmRlci1yYWRpdXM6IDEuMTk4dnc7XG4gIGZvbnQtc2l6ZTogMC45Mzh2dztcbn1cblxuLmZhY2Vib29rIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNiNTk5ODtcbiAgYm9yZGVyLWNvbG9yOiAjM2I1OTk4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uZ29vZ2xlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2M3NGEyZDtcbiAgYm9yZGVyLWNvbG9yOiAjYzc0YTJkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uYnRuLWN1c3RvbS5mYWNlYm9vayxcbi5idG4tY3VzdG9tLmdvb2dsZSB7XG4gIGZvbnQtc2l6ZTogMC44MzN2dztcbiAgd2lkdGg6IDE0LjU4M3Z3O1xuICBoZWlnaHQ6IDIuNXZ3O1xuICBwYWRkaW5nLWxlZnQ6IDAuNDE3dnc7XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xufVxuLmJ0bi1jdXN0b20uZmFjZWJvb2sgaW1nLFxuLmJ0bi1jdXN0b20uZ29vZ2xlIGltZyB7XG4gIHdpZHRoOiAxLjcxOXZ3O1xuICBoZWlnaHQ6IDEuNzE5dnc7XG59XG4uYnRuLWN1c3RvbS5mYWNlYm9vayBwLFxuLmJ0bi1jdXN0b20uZ29vZ2xlIHAge1xuICBwYWRkaW5nLWxlZnQ6IDEuMzAydnc7XG59XG5cbi5mYWNlYm9vayBwLCAuZ29vZ2xlIHAge1xuICBtYXJnaW46IDAgIWltcG9ydGFudDtcbn1cblxuaW5wdXQ6Zm9jdXMsIHRleHRhcmVhOmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuLnBhZ2UtdGl0bGUge1xuICBjb2xvcjogI2Q2ZTJlYztcbiAgZm9udC1mYW1pbHk6IFwiQ29wcGVycGxhdGUgR290aGljXCIsIHNlcmlmO1xuICBmb250LXdlaWdodDogNzAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogNi4yNXZ3O1xuICBtYXJnaW46IDAuNTczdncgMCAwO1xufVxuXG4uY29udGFpbmVyIHtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHdpZHRoOiA1Ni42Njd2dztcbn1cblxuLmxpZ2h0IHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgY29sb3I6ICMwMDA7XG59XG5cbi53YXJuaW5nIHtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbn1cblxuLmdhbGxlcnkge1xuICBwYWRkaW5nLXRvcDogNXZ3O1xufVxuLmdhbGxlcnlfX3dyYXAge1xuICBtYXJnaW4tYm90dG9tOiAxLjg3NXZ3O1xufVxuLmdhbGxlcnlfX3dyYXA6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDYuMDk0dnc7XG59XG4uZ2FsbGVyeV9fdGl0bGUge1xuICBmb250LXNpemU6IDEuNTYzdnc7XG4gIG1hcmdpbi1sZWZ0OiAxLjY2N3Z3O1xuICBtYXJnaW4tYm90dG9tOiAyLjkxN3Z3O1xuICBmb250LXdlaWdodDogNzAwO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZ2FsbGVyeV9fdGl0bGU6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XG4gIHdpZHRoOiA5LjM3NXZ3O1xuICBoZWlnaHQ6IDAuNzgxdnc7XG4gIGJvdHRvbTogLTEuMTk4dnc7XG4gIGxlZnQ6IDAuMTU2dnc7XG59XG4uZ2FsbGVyeV9fcm93IHtcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbiAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cbi5nYWxsZXJ5X19pdGVtIHtcbiAgcGFkZGluZy1sZWZ0OiAxLjA0MnZ3O1xuICBtYXJnaW4tYm90dG9tOiAxLjUxdnc7XG4gIHdpZHRoOiAzMy4zMzMlO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uZ2FsbGVyeV9faXRlbTpob3ZlciAuaXRlbS10b3Age1xuICB6LWluZGV4OiA5O1xufVxuLmdhbGxlcnlfX2l0ZW0gLml0ZW0tdG9wIHtcbiAgcGFkZGluZy1sZWZ0OiAwLjY3N3Z3O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHotaW5kZXg6IDI7XG59XG4uZ2FsbGVyeSAudGltZSB7XG4gIG1hcmdpbi1yaWdodDogMC41NzN2dztcbiAgZm9udC1zaXplOiAwLjgzM3Z3O1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgLW1zLWZsZXgtYWxpZ246IGVuZDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICBmb250LXdlaWdodDogNzAwO1xufVxuLmdhbGxlcnkgLnRpbWUgaW1nIHtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xufVxuLmdhbGxlcnkgLmluZm8ge1xuICBwYWRkaW5nLWJvdHRvbTogMC4xMDR2dztcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5nYWxsZXJ5IC5pbmZvX19pdGVtIHtcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogZmxleC1lbmQ7XG4gIC1tcy1mbGV4LWFsaWduOiBlbmQ7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbn1cbi5nYWxsZXJ5IC5pbmZvX19pdGVtOmxhc3QtY2hpbGQgc3BhbiB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmdhbGxlcnkgLnF1YWxpdHkge1xuICBmb250LXNpemU6IDAuODMzdnc7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG4uZ2FsbGVyeSAuZGV0YWlscyB7XG4gIHdpZHRoOiAxNS42MjV2dztcbiAgbWluLWhlaWdodDogNC43NHZ3O1xuICBtYXJnaW4tdG9wOiAtMi4xMzV2dztcbiAgcGFkZGluZzogMCAwLjQxN3Z3IDAuNjc3dncgMC43ODF2dztcbiAgZm9udC1zaXplOiAxLjI1dnc7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkNmUyZWM7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgLW1zLWZsZXgtYWxpZ246IGVuZDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xufVxuLmdhbGxlcnkgLmRldGFpbHNfX3RpdGxlIHtcbiAgZm9udC1zaXplOiAxLjI1dnc7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG59XG4uZ2FsbGVyeSAuZGV0YWlscyAuZGF0ZSB7XG4gIGZvbnQtc2l6ZTogMC44MzN2dztcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cbi5nYWxsZXJ5IC5kZXRhaWxzIHAge1xuICBtYXJnaW46IDA7XG59XG4uZ2FsbGVyeSAuZG93bmxvYWQge1xuICBwYWRkaW5nOiAwIDAuNDE3dncgMC42Nzd2dyAwLjc4MXZ3O1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgLW1zLWZsZXgtYWxpZ246IGVuZDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIHRvcDogLTEwMCU7XG4gIHJpZ2h0OiAwO1xuICBsZWZ0OiAwO1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBjb2xvcjogI2ZmZjtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBlYXNlIDAuM3M7XG4gIC1tb3otdHJhbnNpdGlvbjogZWFzZSAwLjNzO1xuICAtbXMtdHJhbnNpdGlvbjogZWFzZSAwLjNzO1xuICAtby10cmFuc2l0aW9uOiBlYXNlIDAuM3M7XG4gIHRyYW5zaXRpb246IGVhc2UgMC4zcztcbn1cblxuLmdhbGxlcnlfX2l0ZW06aG92ZXIgLmRvd25sb2FkLFxuLnNob3ctdmlkZW8gKyAuZGV0YWlscyAuZG93bmxvYWQge1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbn1cblxuLnNlYXJjaCB7XG4gIHBhZGRpbmctdG9wOiAxLjU2M3Z3O1xufVxuLnNlYXJjaCBmb3JtIHtcbiAgd2lkdGg6IDM3LjYwNHZ3O1xuICBwYWRkaW5nLWxlZnQ6IDEuNjY3dnc7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uc2VhcmNoIGlucHV0IHtcbiAgd2lkdGg6IDI2LjU2M3Z3O1xuICBoZWlnaHQ6IDIuMzQ0dnc7XG4gIHBhZGRpbmc6IDAgMi4xODh2dztcbiAgYmFja2dyb3VuZDogdXJsKC9hc3NldHMvaW1nL2ljb24vc2VhcmNoLnBuZykgOHB4IGNlbnRlciBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogYXV0byA4NSU7XG59XG5cbi5zaWRlLW1lbnUtcGFnZS13cmFwIHtcbiAgZGlzcGxheTogLW1zLWlubGluZS1mbGV4Ym94O1xuICBkaXNwbGF5OiAtd2Via2l0LWlubGluZS1mbGV4O1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbn1cblxuLm92ZXJsYXkge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgcmlnaHQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgYmFja2dyb3VuZDogcmdiYSgyMTQsIDIyNiwgMjM2LCAwLjgpO1xuICB6LWluZGV4OiA5OTk5O1xuICBwYWRkaW5nLXRvcDogMTgwcHg7XG59XG5cbi5wb3B1cCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDAwMDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIG1hcmdpbjogYXV0bztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG59XG5cbi5wb3B1cCAuY2xvc2UtYnRuIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5wb3B1cCBwIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIG1hcmdpbjogMDtcbn1cblxuLnBvcHVwX190aXRsZSB7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBmb250LXdlaWdodDogNzAwO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBtYXJnaW46IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnVwbG9hZC1waWN0dXJlcyAucG9wdXAge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5wb3B1cCAuaW1hZ2VzLWNvdW50ZXIge1xuICBjb2xvcjogIzg1YTNiZDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5hZGQtbW9yZS1waWN0dXJlcyAucG9wdXAtZ2FsbGVyeSB7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xuICAtbXMtZmxleC13cmFwOiB3cmFwO1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIC8qIG1heC1oZWlnaHQ6IDkxdmg7XG4gICBvdmVyZmxvdzogYXV0bzsqL1xufVxuXG4udXBsb2FkLXBpY3R1cmVzIC5wb3B1cCB7XG4gIHdpZHRoOiA0MC42MjV2dztcbiAgaGVpZ2h0OiAzOS44NDR2dztcbn1cblxuLnVwbG9hZC1waWN0dXJlcyAucG9wdXBfX3RpdGxlIHtcbiAgbWFyZ2luLWJvdHRvbTogNC4zMjN2dztcbn1cblxuLnVwbG9hZC1waWN0dXJlcyBwIHtcbiAgbWFyZ2luLWJvdHRvbTogMS4yNXZ3O1xufVxuXG4udXBsb2FkLXBpY3R1cmVzIC5idG4td3JhcC5maXJzdCB7XG4gIHBhZGRpbmctdG9wOiAxLjg3NXZ3O1xuICBtYXJnaW4tYm90dG9tOiAyLjM0NHZ3O1xufVxuXG4udXBsb2FkLXBpY3R1cmVzIC5idG4tY3VzdG9tIHtcbiAgd2lkdGg6IDEwLjkzOHZ3O1xufVxuXG4udXBsb2FkLXBpY3R1cmVzIGltZyB7XG4gIHdpZHRoOiAxMC40MTd2dztcbn1cblxuLnVwbG9hZC1waWN0dXJlcyAuaW1hZ2VzLWNvdW50ZXIgaW1nIHtcbiAgd2lkdGg6IDIuNXZ3O1xufVxuXG4ucG9wdXAgLmltYWdlcy1jb3VudGVyIHtcbiAgZm9udC1zaXplOiAwLjgzM3Z3O1xuICBwYWRkaW5nLXRvcDogMC45Mzh2dztcbn1cblxuLnBvcHVwIC5pbWFnZXMtY291bnRlciBzcGFuIHtcbiAgcGFkZGluZy1yaWdodDogMS4xNDZ2dztcbn1cblxuLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cCB7XG4gIHdpZHRoOiA1My4xMjV2dztcbn1cblxuLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cC1nYWxsZXJ5IHtcbiAgcGFkZGluZzogMi4yNHZ3IDQuMTE1dncgMDtcbiAgbWF4LWhlaWdodDogNzV2aDtcbiAgb3ZlcmZsb3c6IGF1dG87XG59XG5cbi5hZGQtbW9yZS1waWN0dXJlcyAucG9wdXAtZ2FsbGVyeV9faXRlbSB7XG4gIHBhZGRpbmc6IDAgMC4zMTN2dyAxLjE5OHZ3O1xufVxuXG4uYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwLWdhbGxlcnlfX2l0ZW0gLnRvcCBwIHtcbiAgZm9udC1zaXplOiAwLjgzM3Z3O1xufVxuXG4uYWRkLW1vcmUtcGljdHVyZXMgLmJ0bi13cmFwIHtcbiAgcGFkZGluZy1ib3R0b206IDIuMjR2dztcbn1cblxuLmFkZC1tb3JlLXBpY3R1cmVzIC5idG4td3JhcCAuYnRuLWN1c3RvbSB7XG4gIHdpZHRoOiAxMC45Mzh2dztcbn1cblxuLnBvcHVwIHtcbiAgYm94LXNoYWRvdzogMCAwIDAuNTczdncgMC4yNnZ3IHJnYmEoMCwgMCwgMCwgMC4xNCk7XG4gIGJvcmRlci1yYWRpdXM6IDEuMjV2dztcbiAgcGFkZGluZy10b3A6IDIuMjkydnc7XG59XG5cbi5wb3B1cCAuY2xvc2UtYnRuIHtcbiAgd2lkdGg6IDEuMjV2dztcbiAgaGVpZ2h0OiAxLjI1dnc7XG4gIHJpZ2h0OiAwLjY3N3Z3O1xuICB0b3A6IDAuNzI5dnc7XG59XG5cbi5wb3B1cCBwIHtcbiAgZm9udC1zaXplOiAxLjM1NHZ3O1xufVxuXG4ucG9wdXBfX3RpdGxlIHtcbiAgZm9udC1zaXplOiAxLjM1NHZ3O1xufVxuXG4uYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwLWdhbGxlcnkgLnBvcHVwLWdhbGxlcnlfX2l0ZW0ge1xuICB3aWR0aDogMjUlO1xufVxuXG4uYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwLWdhbGxlcnlfX2l0ZW0gLnRvcCB7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xuICAtbXMtZmxleC13cmFwOiB3cmFwO1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIC13ZWJraXQtYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAtbXMtZmxleC1hbGlnbjogZW5kO1xuICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbn1cblxuLmFkZC1tb3JlLXBpY3R1cmVzIC5idG4td3JhcCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmRlbGV0ZSB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cC1nYWxsZXJ5X19pdGVtIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwLWdhbGxlcnlfX2l0ZW0gLnRvcCBwIHtcbiAgY29sb3I6ICMxODBhMjE7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xuICBhcHAtc2lkZS1tZW51IHtcbiAgICB3aWR0aDogMjclO1xuICB9XG5cbiAgaW5wdXQsIHRleHRhcmVhIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gIH1cblxuICAuZ2FsbGVyeSAudGltZSxcbi5nYWxsZXJ5IC5xdWFsaXR5LFxuLmdhbGxlcnkgLmRldGFpbHMgLmRldGFpbHNfX3RpdGxlLFxuLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cC1nYWxsZXJ5X19pdGVtIC50b3AgcCB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICB9XG5cbiAgLmdhbGxlcnkgLmluZm9fX2l0ZW0ge1xuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICB9XG5cbiAgLmdhbGxlcnlfX3RpdGxlIHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gIH1cblxuICAuc2lkZS1tZW51LXBhZ2Utd3JhcCB7XG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gIH1cblxuICAuc2VhcmNoIGZvcm0ge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG5cbiAgLnNlYXJjaCBmb3JtIGlucHV0IHtcbiAgICB3aWR0aDogNzUlO1xuICB9XG5cbiAgLmdhbGxlcnkgLmRldGFpbHMsXG4uZ2FsbGVyeSAuZG93bmxvYWQge1xuICAgIHBhZGRpbmc6IDE5cHggN3B4IDdweCA3cHg7XG4gIH1cblxuICAuZ2FsbGVyeSAuZGV0YWlscyB7XG4gICAgbWluLWhlaWdodDogNDhweDtcbiAgICBtYXJnaW4tdG9wOiAtMjFweDtcbiAgICB3aWR0aDogOTUlO1xuICB9XG5cbiAgLmdhbGxlcnkgLmRldGFpbHMgLmRhdGUge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgfVxuXG4gIC5nYWxsZXJ5IC5pbmZvIHtcbiAgICAtd2Via2l0LWZsZXgtd3JhcDogd3JhcDtcbiAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xuICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAtd2Via2l0LWp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgfVxuXG4gIC5zZWFyY2ggZm9ybSAuYnRuLWN1c3RvbSB7XG4gICAgbWluLXdpZHRoOiAyMCU7XG4gIH1cblxuICAuYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwIHtcbiAgICB3aWR0aDogNzUlO1xuICB9XG5cbiAgLm92ZXJsYXkge1xuICAgIHBhZGRpbmctdG9wOiA4MHB4O1xuICB9XG5cbiAgLnBvcHVwX190aXRsZSB7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICB9XG5cbiAgLnVwbG9hZC1waWN0dXJlcyAucG9wdXAge1xuICAgIHdpZHRoOiA3NSU7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICB9XG5cbiAgLnVwbG9hZC1waWN0dXJlcyBpbWcge1xuICAgIHdpZHRoOiAyMDBweDtcbiAgfVxuXG4gIC5wb3B1cCBwIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIH1cblxuICAucG9wdXAgLmltYWdlcy1jb3VudGVyIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XG4gIGJvZHkgKiB7XG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbCAhaW1wb3J0YW50O1xuICB9XG5cbiAgaW5wdXQge1xuICAgIGhlaWdodDogNDBweDtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICB9XG5cbiAgdGV4dGFyZWEge1xuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIH1cblxuICAuYnRuLWN1c3RvbSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG5cbiAgLmJ0bi1jdXN0b20sIC5lZGl0LXByb2ZpbGUgLmJ0bi1jdXN0b206bm90KC5mYWNlYm9vayk6bm90KC5nb29nbGUpIHtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgbWluLXdpZHRoOiA4MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XG4gICAgcGFkZGluZzogMCAxMnB4O1xuICB9XG5cbiAgLmdhbGxlcnkgLmRvd25sb2FkIGltZyB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIC5zZWFyY2ggZm9ybSBpbnB1dCB7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICB9XG5cbiAgLnVwbG9hZC1waWN0dXJlcyAuaW1hZ2VzLWNvdW50ZXIgaW1nIHtcbiAgICB3aWR0aDogMzBweDtcbiAgfVxuXG4gIC5wb3B1cCB7XG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgfVxuXG4gIC5hZGQtbW9yZS1waWN0dXJlcyAuYnRuLXdyYXAgLmJ0bi1jdXN0b20ge1xuICAgIHdpZHRoOiBhdXRvO1xuICB9XG5cbiAgLmFkZC1tb3JlLXBpY3R1cmVzIC5idG4td3JhcCB7XG4gICAgcGFkZGluZzogMTVweCAwO1xuICB9XG5cbiAgLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cC1nYWxsZXJ5IC5wb3B1cC1nYWxsZXJ5X19pdGVtIHtcbiAgICB3aWR0aDogMzMuMzMzJTtcbiAgICBwYWRkaW5nOiA1cHggNXB4IDA7XG4gIH1cblxuICAucG9wdXAgLmNsb3NlLWJ0biB7XG4gICAgd2lkdGg6IDI0cHg7XG4gICAgaGVpZ2h0OiAyNHB4O1xuICAgIHJpZ2h0OiAxMHB4O1xuICAgIHRvcDogMTJweDtcbiAgfVxuXG4gIC51cGxvYWQtcGljdHVyZXMgLmJ0bi1jdXN0b20ge1xuICAgIHdpZHRoOiBhdXRvO1xuICB9XG5cbiAgLnVwbG9hZC1waWN0dXJlcyAuYnRuLXZhcmlhbnQge1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gIH1cblxuICAucG9wdXAgLmltYWdlcy1jb3VudGVyIHtcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgLmNvbnRhaW5lciB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cblxuICAuc2VhcmNoIHtcbiAgICBwYWRkaW5nOiA3NXB4IDE1cHggMDtcbiAgfVxuXG4gIC5nYWxsZXJ5IHtcbiAgICBwYWRkaW5nOiAzMHB4IDE1cHggMDtcbiAgfVxuXG4gIGFwcC1zaWRlLW1lbnUge1xuICAgIHdpZHRoOiAwO1xuICB9XG5cbiAgLnNpZGUtbWVudS1wYWdlLXdyYXAge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuXG4gIC51cGxvYWQtcGljdHVyZXMgLnBvcHVwIHtcbiAgICB3aWR0aDogOTUlO1xuICB9XG5cbiAgLmFkZC1tb3JlLXBpY3R1cmVzIC5wb3B1cCB7XG4gICAgd2lkdGg6IDk1JTtcbiAgfVxuXG4gIC5hZGQtbW9yZS1waWN0dXJlcyAucG9wdXAtZ2FsbGVyeSAucG9wdXAtZ2FsbGVyeV9faXRlbSB7XG4gICAgd2lkdGg6IDUwJTtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcbiAgLmdhbGxlcnlfX2l0ZW0ge1xuICAgIHdpZHRoOiA1MCU7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ5MnB4KSB7XG4gIC5nYWxsZXJ5X19pdGVtIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5wb3B1cCB7XG4gICAgcGFkZGluZy10b3A6IDQxcHg7XG4gIH1cblxuICAuYWRkLW1vcmUtcGljdHVyZXMgLnBvcHVwLWdhbGxlcnkgLnBvcHVwLWdhbGxlcnlfX2l0ZW0ge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG5cbiAgLmdhbGxlcnlfX2l0ZW0gLml0ZW0tdG9wID4gaW1nIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufVxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBcIkNhdmlhciBEcmVhbXNcIjtcbiAgc3JjOiBsb2NhbChcIkNhdmlhciBEcmVhbXNcIiksIGxvY2FsKFwiQ2F2aWFyRHJlYW1zXCIpLCB1cmwoL2Fzc2V0cy9mb250cy9jYXZpYXJkcmVhbXMud29mZjIpIGZvcm1hdChcIndvZmYyXCIpLCB1cmwoL2Fzc2V0cy9mb250cy9jYXZpYXJkcmVhbXMud29mZikgZm9ybWF0KFwid29mZlwiKSwgdXJsKC9hc3NldHMvZm9udHMvY2F2aWFyZHJlYW1zLnR0ZikgZm9ybWF0KFwidHJ1ZXR5cGVcIik7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbn1cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogXCJDYXZpYXIgRHJlYW1zXCI7XG4gIHNyYzogbG9jYWwoXCJDYXZpYXIgRHJlYW1zIEJvbGRcIiksIGxvY2FsKFwiQ2F2aWFyRHJlYW1zLUJvbGRcIiksIHVybCgvYXNzZXRzL2ZvbnRzL2NhdmlhcmRyZWFtc2JvbGQud29mZjIpIGZvcm1hdChcIndvZmYyXCIpLCB1cmwoL2Fzc2V0cy9mb250cy9jYXZpYXJkcmVhbXNib2xkLndvZmYpIGZvcm1hdChcIndvZmZcIiksIHVybCgvYXNzZXRzL2ZvbnRzL2NhdmlhcmRyZWFtc2JvbGQudHRmKSBmb3JtYXQoXCJ0cnVldHlwZVwiKTtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xufVxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBcIkNhdmlhciBEcmVhbXNcIjtcbiAgc3JjOiBsb2NhbChcIkNhdmlhciBEcmVhbXMgSXRhbGljXCIpLCBsb2NhbChcIkNhdmlhckRyZWFtcy1JdGFsaWNcIiksIHVybCgvYXNzZXRzL2ZvbnRzL2NhdmlhcmRyZWFtc2l0YWxpYy53b2ZmMikgZm9ybWF0KFwid29mZjJcIiksIHVybCgvYXNzZXRzL2ZvbnRzL2NhdmlhcmRyZWFtc2l0YWxpYy53b2ZmKSBmb3JtYXQoXCJ3b2ZmXCIpLCB1cmwoL2Fzc2V0cy9mb250cy9jYXZpYXJkcmVhbXNpdGFsaWMudHRmKSBmb3JtYXQoXCJ0cnVldHlwZVwiKTtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xufVxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBcIkNhdmlhciBEcmVhbXNcIjtcbiAgc3JjOiBsb2NhbChcIkNhdmlhciBEcmVhbXMgQm9sZCBJdGFsaWNcIiksIGxvY2FsKFwiQ2F2aWFyRHJlYW1zLUJvbGRJdGFsaWNcIiksIHVybCgvYXNzZXRzL2ZvbnRzL2NhdmlhcmRyZWFtc2JvbGRpdGFsaWMud29mZjIpIGZvcm1hdChcIndvZmYyXCIpLCB1cmwoL2Fzc2V0cy9mb250cy9jYXZpYXJkcmVhbXNib2xkaXRhbGljLndvZmYpIGZvcm1hdChcIndvZmZcIiksIHVybCgvYXNzZXRzL2ZvbnRzL2NhdmlhcmRyZWFtc2JvbGRpdGFsaWMudHRmKSBmb3JtYXQoXCJ0cnVldHlwZVwiKTtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xufVxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBcIkNvcHBlcnBsYXRlIEdvdGhpY1wiO1xuICBzcmM6IHVybCgvYXNzZXRzL2ZvbnRzL0NvcHBlcnBsYXRlR290aGljLUJvbGQuZW90KTtcbiAgc3JjOiB1cmwoL2Fzc2V0cy9mb250cy9Db3BwZXJwbGF0ZUdvdGhpYy1Cb2xkLmVvdD8jaWVmaXgpIGZvcm1hdChcImVtYmVkZGVkLW9wZW50eXBlXCIpLCB1cmwoL2Fzc2V0cy9mb250cy9Db3BwZXJwbGF0ZUdvdGhpYy1Cb2xkLndvZmYyKSBmb3JtYXQoXCJ3b2ZmMlwiKSwgdXJsKC9hc3NldHMvZm9udHMvQ29wcGVycGxhdGVHb3RoaWMtQm9sZC53b2ZmKSBmb3JtYXQoXCJ3b2ZmXCIpLCB1cmwoL2Fzc2V0cy9mb250cy9Db3BwZXJwbGF0ZUdvdGhpYy1Cb2xkLnR0ZikgZm9ybWF0KFwidHJ1ZXR5cGVcIiksIHVybCgvYXNzZXRzL2ZvbnRzL0NvcHBlcnBsYXRlR290aGljLUJvbGQuc3ZnI0NvcHBlcnBsYXRlR290aGljLUJvbGQpIGZvcm1hdChcInN2Z1wiKTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbn0iLCJAZm9udC1mYWNlIHtcclxuICBmb250LWZhbWlseTogJ0NhdmlhciBEcmVhbXMnO1xyXG4gIHNyYzogbG9jYWwoJ0NhdmlhciBEcmVhbXMnKSwgbG9jYWwoJ0NhdmlhckRyZWFtcycpLCB1cmwoL2Fzc2V0cy9mb250cy9jYXZpYXJkcmVhbXMud29mZjIpIGZvcm1hdCgnd29mZjInKSwgdXJsKC9hc3NldHMvZm9udHMvY2F2aWFyZHJlYW1zLndvZmYpIGZvcm1hdCgnd29mZicpLCB1cmwoL2Fzc2V0cy9mb250cy9jYXZpYXJkcmVhbXMudHRmKSBmb3JtYXQoJ3RydWV0eXBlJyk7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICBmb250LXN0eWxlOiBub3JtYWw7XHJcbn1cclxuXHJcbkBmb250LWZhY2Uge1xyXG4gIGZvbnQtZmFtaWx5OiAnQ2F2aWFyIERyZWFtcyc7XHJcbiAgc3JjOiBsb2NhbCgnQ2F2aWFyIERyZWFtcyBCb2xkJyksIGxvY2FsKCdDYXZpYXJEcmVhbXMtQm9sZCcpLCB1cmwoL2Fzc2V0cy9mb250cy9jYXZpYXJkcmVhbXNib2xkLndvZmYyKSBmb3JtYXQoJ3dvZmYyJyksIHVybCgvYXNzZXRzL2ZvbnRzL2NhdmlhcmRyZWFtc2JvbGQud29mZikgZm9ybWF0KCd3b2ZmJyksIHVybCgvYXNzZXRzL2ZvbnRzL2NhdmlhcmRyZWFtc2JvbGQudHRmKSBmb3JtYXQoJ3RydWV0eXBlJyk7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBmb250LXN0eWxlOiBub3JtYWw7XHJcbn1cclxuXHJcbkBmb250LWZhY2Uge1xyXG4gIGZvbnQtZmFtaWx5OiAnQ2F2aWFyIERyZWFtcyc7XHJcbiAgc3JjOiBsb2NhbCgnQ2F2aWFyIERyZWFtcyBJdGFsaWMnKSwgbG9jYWwoJ0NhdmlhckRyZWFtcy1JdGFsaWMnKSwgdXJsKC9hc3NldHMvZm9udHMvY2F2aWFyZHJlYW1zaXRhbGljLndvZmYyKSBmb3JtYXQoJ3dvZmYyJyksIHVybCgvYXNzZXRzL2ZvbnRzL2NhdmlhcmRyZWFtc2l0YWxpYy53b2ZmKSBmb3JtYXQoJ3dvZmYnKSwgdXJsKC9hc3NldHMvZm9udHMvY2F2aWFyZHJlYW1zaXRhbGljLnR0ZikgZm9ybWF0KCd0cnVldHlwZScpO1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgZm9udC1zdHlsZTogaXRhbGljO1xyXG59XHJcblxyXG5AZm9udC1mYWNlIHtcclxuICBmb250LWZhbWlseTogJ0NhdmlhciBEcmVhbXMnO1xyXG4gIHNyYzogbG9jYWwoJ0NhdmlhciBEcmVhbXMgQm9sZCBJdGFsaWMnKSwgbG9jYWwoJ0NhdmlhckRyZWFtcy1Cb2xkSXRhbGljJyksIHVybCgvYXNzZXRzL2ZvbnRzL2NhdmlhcmRyZWFtc2JvbGRpdGFsaWMud29mZjIpIGZvcm1hdCgnd29mZjInKSwgdXJsKC9hc3NldHMvZm9udHMvY2F2aWFyZHJlYW1zYm9sZGl0YWxpYy53b2ZmKSBmb3JtYXQoJ3dvZmYnKSwgdXJsKC9hc3NldHMvZm9udHMvY2F2aWFyZHJlYW1zYm9sZGl0YWxpYy50dGYpIGZvcm1hdCgndHJ1ZXR5cGUnKTtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcclxufVxyXG5cclxuQGZvbnQtZmFjZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdDb3BwZXJwbGF0ZSBHb3RoaWMnO1xyXG4gIHNyYzogdXJsKC9hc3NldHMvZm9udHMvQ29wcGVycGxhdGVHb3RoaWMtQm9sZC5lb3QpO1xyXG4gIHNyYzogdXJsKC9hc3NldHMvZm9udHMvQ29wcGVycGxhdGVHb3RoaWMtQm9sZC5lb3Q/I2llZml4KSBmb3JtYXQoJ2VtYmVkZGVkLW9wZW50eXBlJyksXHJcbiAgdXJsKC9hc3NldHMvZm9udHMvQ29wcGVycGxhdGVHb3RoaWMtQm9sZC53b2ZmMikgZm9ybWF0KCd3b2ZmMicpLFxyXG4gIHVybCgvYXNzZXRzL2ZvbnRzL0NvcHBlcnBsYXRlR290aGljLUJvbGQud29mZikgZm9ybWF0KCd3b2ZmJyksXHJcbiAgdXJsKC9hc3NldHMvZm9udHMvQ29wcGVycGxhdGVHb3RoaWMtQm9sZC50dGYpIGZvcm1hdCgndHJ1ZXR5cGUnKSxcclxuICB1cmwoL2Fzc2V0cy9mb250cy9Db3BwZXJwbGF0ZUdvdGhpYy1Cb2xkLnN2ZyNDb3BwZXJwbGF0ZUdvdGhpYy1Cb2xkKSBmb3JtYXQoJ3N2ZycpO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxufVxyXG4iXX0= */", '', '']]

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/styles.scss":
/*!*************************!*\
  !*** ./src/styles.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!../node_modules/postcss-loader/src??embedded!../node_modules/sass-loader/lib/loader.js??ref--15-3!./styles.scss */ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/styles.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 2:
/*!*******************************!*\
  !*** multi ./src/styles.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\VladD\Desktop\Memo (all)\memo\src\styles.scss */"./src/styles.scss");


/***/ })

},[[2,"runtime"]]]);
//# sourceMappingURL=styles-es5.js.map