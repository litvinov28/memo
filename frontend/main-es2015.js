(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./auth/auth.module": "./src/app/auth/auth.module.ts",
	"./main/main.module": "./src/app/main/main.module.ts"
};

function webpackAsyncContext(req) {
	return Promise.resolve().then(function() {
		if(!__webpack_require__.o(map, req)) {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		}

		var id = map[req];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/auth/auth.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/auth/auth.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"auth\">\n  <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/auth/login/login.component.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/auth/login/login.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<div class=\"login-reg-wrap\">\n  <div class=\"login-reg-wrap__item\">\n    <div class=\"login-reg-form\">\n      <h2 class=\"title\">Login</h2>\n      <form [formGroup]=\"loginForm\" (ngSubmit)=\"loginUser()\">\n        <div class=\"input-wrap\">\n          <input formControlName=\"email\" type=\"email\" placeholder=\"E-mail\">\n        </div>\n        <div class=\"input-wrap\">\n          <input formControlName=\"password\" type=\"password\" placeholder=\"Password\">\n        </div>\n        <button type=\"submit\" class=\"btn-custom\">sing in</button>\n      </form>\n    </div>\n    <div class=\"login-reg-network\">\n      <h2 class=\"title\">Login with networks</h2>\n\n      <div class=\"btn-wrap\">\n        <a (click)=\"signInWithFB()\" class=\"btn-custom facebook\">\n          <img src=\"../../../assets/img/icon/facebook-btn.png\" alt=\"facebook\">\n          <p>JOIN with facebook</p>\n        </a>\n      </div>\n      <div class=\"btn-wrap\">\n        <a (click)=\"signInWithGoogle()\" class=\"btn-custom google\">\n          <img src=\"../../../assets/img/icon/google-btn.png\" alt=\"google\">\n          <p>JOIN with google</p>\n        </a>\n      </div>\n      <p>Have no an account?</p>\n      <a routerLink=\"/auth/sign-up\" class=\"reg-link\">SIGN up</a>\n    </div>\n  </div>\n</div>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/auth/registration/registration.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/auth/registration/registration.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<div class=\"login-reg-wrap\">\n  <div class=\"login-reg-wrap__item\">\n    <div class=\"login-reg-form reg-form\">\n      <h2 class=\"title\">Registration</h2>\n      <form [formGroup]=\"register\" (ngSubmit)=\"registration()\">\n        <div class=\"input-wrap\">\n          <input formControlName=\"name\" type=\"text\" placeholder=\"Name\">\n        </div>\n        <div class=\"input-wrap\">\n          <input formControlName=\"last_name\" type=\"text\" placeholder=\"Last Name\">\n        </div>\n        <div class=\"input-wrap\">\n          <input formControlName=\"email\" type=\"email\" placeholder=\"E-mail\">\n        </div>\n        <div class=\"input-wrap\">\n          <input formControlName=\"password\" type=\"password\" placeholder=\"Password\">\n          <p *ngIf=\"register.value.password.length < 8\">* Password must contain 8 characters </p>\n        </div>\n        <div class=\"input-wrap last-child\">\n          <input formControlName=\"password_confirmation\" type=\"password\" placeholder=\"Re-Type Password\">\n        </div>\n        <div class=\"checkbox-wrap\">\n          <input type=\"checkbox\" id=\"subscribe\">\n          <label for=\"subscribe\">I want to receive newsletters</label>\n        </div>\n        <div class=\"checkbox-wrap\">\n          <input #terms type=\"checkbox\" (change)=\"defVoid(terms.checked)\" id=\"terms\">\n          <label for=\"terms\">I agree to the <a href=\"#\">Terms & conditions</a></label>\n        </div>\n        <button type=\"submit\" class=\"btn-custom\">CREATE ACCOUNT</button>\n      </form>\n    </div>\n    <div class=\"login-reg-network\">\n      <h2 class=\"title\">Join with networks</h2>\n\n      <div class=\"btn-wrap\">\n        <a (click)=\"registrationWithFB()\" class=\"btn-custom facebook\">\n          <img src=\"../../../assets/img/icon/facebook-btn.png\" alt=\"facebook\">\n          <p>JOIN with facebook</p>\n        </a>\n      </div>\n      <div class=\"btn-wrap\">\n        <a (click)=\"registrationWithGoogle()\" class=\"btn-custom google\">\n          <img src=\"../../../assets/img/icon/google-btn.png\" alt=\"google\">\n          <p>JOIN with google</p>\n        </a>\n      </div>\n      <p>Already have an account?</p>\n      <a routerLink=\"/auth/sign-in\" class=\"reg-link\">SIGN IN</a>\n    </div>\n  </div>\n</div>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/about/about.component.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/about/about.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<h1 class=\"page-title\">about</h1>\n<div class=\"container\">\n  <div class=\"about__title\">\n    <h2>{{about.title}}</h2>\n  </div>\n  <div class=\"about-info\">\n    <div class=\"about-info__item\">\n      <p>{{about.description}}</p>\n    </div>\n    <div class=\"about-info__item\">\n      <p>{{about.description_1}}</p>\n    </div>\n  </div>\n</div>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/contact-us/contact-us.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/contact-us/contact-us.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<h1 class=\"page-title\">contact us</h1>\n<div class=\"contact-us__wrap container\">\n  <div class=\"contact-us__form\">\n    <form [formGroup]=\"contactUs\" (ngSubmit)=\"sendMessage()\">\n      <div class=\"input-wrap\">\n        <input formControlName=\"name\" type=\"text\" placeholder=\"What is your name?\" id=\"contact-us-name\">\n      </div>\n      <div class=\"input-wrap\">\n        <input formControlName=\"email\" type=\"email\" placeholder=\"What is your email?\" id=\"contact-us-email\">\n      </div>\n      <textarea formControlName=\"message\" placeholder=\"Please enter your message\" id=\"contact-us-message\"></textarea>\n      <div class=\"btn-wrap\">\n        <button class=\"btn-custom\" type=\"submit\">send</button>\n      </div>\n    </form>\n  </div>\n  <div class=\"contact-us__info\">\n    <h1 class=\"title\">{{contactUsText.title}}</h1>\n    <p>{{contactUsText.description}}</p>\n  </div>\n</div>\n<app-footer></app-footer>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/faq/faq.component.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/faq/faq.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<h1 class=\"page-title\">FAQ</h1>\n<div class=\"container\">\n  <div class=\"faq-wrap\">\n    <div class=\"faq-wrap__item active\">\n      <h4 class=\"title\">{{faqText.title}}</h4>\n      <div class=\"hidden\"><p>{{faqText.description}}</p></div>\n    </div>\n    <div class=\"faq-wrap__item\">\n      <h4 class=\"title\">{{faqText.title}}</h4>\n    </div>\n    <div class=\"faq-wrap__item\">\n      <h4 class=\"title\">{{faqText.title}}</h4>\n    </div>\n    <div class=\"faq-wrap__item\">\n      <h4 class=\"title\">{{faqText.title}}</h4>\n    </div>\n  </div>\n</div>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/finished-projects/finished-projects.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/finished-projects/finished-projects.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<h1 class=\"page-title\">Finished projects</h1>\n<div class=\"side-menu-page-wrap\">\n  <app-side-menu [sideMenu]=\"'profile'\"></app-side-menu>\n  <div class=\"container\">\n    <div class=\"search\">\n      <form [formGroup]=\"\">\n        <input type=\"text\">\n        <button type=\"submit\" class=\"btn-custom\">search</button>\n      </form>\n    </div>\n    <div class=\"gallery\">\n      <div class=\"gallery__wrap\">\n        <h3 class=\"gallery__title\">My trips</h3>\n        <div class=\"gallery__row\">\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <div class=\"info\">\n                <span class=\"time\"><img src=\"../../../../assets/img/icon/time.png\" alt=\"time\">6:04</span><span\n                class=\"quality\">1080p</span>\n              </div>\n              <!--<img src=\"img/389.jpg\" alt=\"img\">-->\n              <img src=\"../../../../assets/img/wp.jpg\" alt=\"img\">\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Album name</p>\n              <p class=\"date\">01/01/19</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Download</p>\n                <img src=\"../../../../assets/img/icon/download.png\" alt=\"download\">\n                <p class=\"date\">01/01/19</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <div class=\"info\">\n                <span class=\"time\"><img src=\"../../../../assets/img/icon/time.png\" alt=\"time\">6:04</span><span\n                class=\"quality\">1080p</span>\n              </div>\n              <img src=\"../../../../assets/img/wp.jpg\" alt=\"img\">\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Album name</p>\n              <p class=\"date\">01/01/19</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Download</p>\n                <img src=\"../../../../assets/img/icon/download.png\" alt=\"download\">\n                <p class=\"date\">01/01/19</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <div class=\"info\">\n                <span class=\"time\"><img src=\"../../../../assets/img/icon/time.png\" alt=\"time\">6:04</span><span\n                class=\"quality\">1080p</span>\n              </div>\n              <!--<img src=\"img/1526.jpg\" alt=\"img\">-->\n              <img src=\"../../../../assets/img/wp.jpg\" alt=\"img\">\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Album name</p>\n              <p class=\"date\">01/01/19</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Download</p>\n                <img src=\"../../../../assets/img/icon/download.png\" alt=\"download\">\n                <p class=\"date\">01/01/19</p>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"gallery__wrap\">\n        <h3 class=\"gallery__title\">My family</h3>\n        <div class=\"gallery__row\">\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <div class=\"info\">\n                <div class=\"info__item\">\n                  <span class=\"time\"><img src=\"../../../../assets/img/icon/time.png\" alt=\"time\">6:04</span>\n                  <span class=\"quality\">1080p</span>\n                </div>\n                <div class=\"info__item\"></div>\n              </div>\n              <!--<img src=\"img/4764.jpg\" alt=\"img\">-->\n              <img src=\"../../../../assets/img/wp.jpg\" alt=\"img\">\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Album name</p>\n              <p class=\"date\">01/01/19</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Download</p>\n                <img src=\"../../../../assets/img/icon/download.png\" alt=\"download\">\n                <p class=\"date\">01/01/19</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <div class=\"info\">\n                <div class=\"info__item\">\n                  <span class=\"time\"><img src=\"../../../../assets/img/icon/time.png\" alt=\"time\">6:04</span>\n                  <span class=\"quality\">1080p</span>\n                </div>\n                <div class=\"info__item\"></div>\n              </div>\n              <img src=\"../../../../assets/img/wp.jpg\" alt=\"img\">\n              <!--<img src=\"img/536183-PJII0I-588.jpg\" alt=\"img\">-->\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Album name</p>\n              <p class=\"date\">01/01/19</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Download</p>\n                <img src=\"../../../../assets/img/icon/download.png\" alt=\"download\">\n                <p class=\"date\">01/01/19</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <div class=\"info\">\n                <div class=\"info__item\">\n                  <span class=\"time\"><img src=\"../../../../assets/img/icon/time.png\" alt=\"time\">6:04</span>\n                  <span class=\"quality\">1080p</span>\n                </div>\n                <div class=\"info__item\"></div>\n              </div>\n              <img src=\"../../../../assets/img/wp.jpg\" alt=\"img\">\n              <!-- <img src=\"img/17139.jpg\" alt=\"img\">-->\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Album name</p>\n              <p class=\"date\">01/01/19</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Download</p>\n                <img src=\"../../../../assets/img/icon/download.png\" alt=\"download\">\n                <p class=\"date\">01/01/19</p>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<app-footer></app-footer>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/gallery/gallery.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/gallery/gallery.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<h1 class=\"page-title\">Category name</h1>\n<div class=\"side-menu-page-wrap\">\n  <app-side-menu [sideMenu]=\"'gallery'\"></app-side-menu>\n  <div class=\"container\">\n    <div class=\"search\">\n      <form [formGroup]=\"\">\n        <input type=\"text\">\n        <button type=\"submit\" class=\"btn-custom\">search</button>\n      </form>\n    </div>\n    <div class=\"gallery\">\n      <div class=\"gallery__wrap\">\n        <div class=\"gallery__row\">\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <img src=\"../../../../assets/img/g1.jpg\" alt=\"img\">\n              <div class=\"video\">\n                <a (click)=\"popupHide = !popupHide\" class=\"btn-custom light\">start creating</a>\n                <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/eSEQXcG1pZc\" frameborder=\"0\"\n                        allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                        allowfullscreen></iframe>\n              </div>\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Template name</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Template name</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <img src=\"../../../../assets/img/g2.jpg\" alt=\"img\">\n              <div class=\"video\">\n                <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/eSEQXcG1pZc\" frameborder=\"0\"\n                        allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                        allowfullscreen></iframe>\n              </div>\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Template name</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Template name</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <img src=\"../../../../assets/img/g3.jpg\" alt=\"img\">\n              <div class=\"video\">\n                <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/eSEQXcG1pZc\" frameborder=\"0\"\n                        allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                        allowfullscreen></iframe>\n              </div>\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Template name</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Template name</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <img src=\"../../../../assets/img/g4.jpg\" alt=\"img\">\n              <div class=\"video\">\n                <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/eSEQXcG1pZc\" frameborder=\"0\"\n                        allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                        allowfullscreen></iframe>\n              </div>\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Template name</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Template name</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <img src=\"../../../../assets/img/g5.jpg\" alt=\"img\">\n              <div class=\"video\">\n                <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/eSEQXcG1pZc\" frameborder=\"0\"\n                        allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                        allowfullscreen></iframe>\n              </div>\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Template name</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Template name</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <img src=\"../../../../assets/img/g6.jpg\" alt=\"img\">\n              <div class=\"video\">\n                <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/eSEQXcG1pZc\" frameborder=\"0\"\n                        allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                        allowfullscreen></iframe>\n              </div>\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Template name</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Template name</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <img src=\"../../../../assets/img/g7.jpg\" alt=\"img\">\n              <div class=\"video\">\n                <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/eSEQXcG1pZc\" frameborder=\"0\"\n                        allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                        allowfullscreen></iframe>\n              </div>\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Template name</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Template name</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <img src=\"../../../../assets/img/g8.jpg\" alt=\"img\">\n              <div class=\"video\">\n                <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/eSEQXcG1pZc\" frameborder=\"0\"\n                        allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                        allowfullscreen></iframe>\n              </div>\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Template name</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Template name</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <img src=\"../../../../assets/img/g9.jpg\" alt=\"img\">\n              <div class=\"video\">\n                <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/eSEQXcG1pZc\" frameborder=\"0\"\n                        allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                        allowfullscreen></iframe>\n              </div>\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Template name</p>\n              <div class=\"download\">\n                <p class=\"details__title\">Template name</p>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<app-footer></app-footer>\n<div *ngIf=\"!popupHide\" class=\"overlay upload-pictures\">\n  <div class=\"popup\">\n    <h1 class=\"popup__title\">Upload your pictures</h1>\n    <p>Drag & Drop here</p>\n    <img src=\"../../../assets/img/upload-pictures.png\" alt=\"upload pictures\">\n    <div class=\"btn-wrap first\">\n      <button class=\"btn-custom\" (click)=\"openPopup()\">Brows My PC</button>\n    </div>\n    <p class=\"btn-variant\">or</p>\n    <div class=\"btn-wrap\">\n      <button class=\"btn-custom light\">Select from gallery</button>\n    </div>\n    <p class=\"images-counter\">\n      <span>Images in gallery 312</span>\n      <img src=\"../../../assets/img/icon/gallery-grey.png\" alt=\"gallery\">\n    </p>\n    <div class=\"close-btn\" (click)=\"popupHide = !popupHide\">\n      <img src=\"../../../assets/img/icon/close.png\" alt=\"close\">\n    </div>\n  </div>\n</div>\n<div *ngIf=\"!addMorePopup\" class=\"overlay add-more-pictures\">\n  <div class=\"popup\">\n    <h1 class=\"popup__title\">Add more pictures</h1>\n    <div class=\"popup-gallery\">\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi1.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi2.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi3.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi4.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi1.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi2.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi3.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi4.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi1.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi2.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi3.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi4.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi1.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi2.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi3.jpg\" alt=\"\">\n        </div>\n      </div>\n      <div class=\"popup-gallery__item\">\n        <div class=\"top\">\n          <p>File_name.jpg</p>\n          <div class=\"delete\">\n            <img src=\"../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n        <div class=\"bottom\">\n          <img src=\"../../../assets/img/gi4.jpg\" alt=\"\">\n        </div>\n      </div>\n    </div>\n    <div class=\"btn-wrap\">\n      <button class=\"btn-custom\" routerLink=\"/main/project\">Start creating</button>\n    </div>\n    <div class=\"close-btn\" (click)=\"addMorePopup = !addMorePopup\">\n      <img src=\"../../../assets/img/icon/close.png\" alt=\"close\">\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/home/home.component.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/home/home.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"home\">\n  <app-header></app-header>\n  <div class=\"go-to\">\n    <a href=\"#\">\n      <img src=\"../../../../assets/img/icon/left-arrow.png\" alt=\"icon\">\n    </a>\n  </div>\n  <section class=\"start-now\">\n    <h1>Transform your pictures into a slideshow eventd</h1>\n    <button class=\"btn-custom\">start now</button>\n    <p>Thousands of options. Unique animations for every occasion<br>\n      Create an original, one of a kind slideshow in minutes<br>\n      No experience or design knowledge necessary\n    </p>\n  </section>\n  <section class=\"try-for-free\">\n    <h2>In three easy steps your mountain of pictures <br>\n      <span>can turn into a stunning slideshow</span>\n    </h2>\n    <div class=\"steps\">\n      <div class=\"steps__item\">\n        <h3>I</h3>\n        <p>Upload your pictures</p>\n        <img src=\"../../../../assets/img/icon/upload.png\" alt=\"icon\">\n      </div>\n      <div class=\"steps__item\">\n        <h3>II</h3>\n        <p>Select the topic\n          of the slide show </p>\n        <img src=\"../../../../assets/img/icon/cat.png\" alt=\"icon\">\n      </div>\n      <div class=\"steps__item\">\n        <h3>III</h3>\n        <p>Choose your animations</p>\n        <img src=\"../../../../assets/img/icon/video-player.png\" alt=\"icon\">\n      </div>\n    </div>\n    <p class=\"try-for-free__descr\">Our smart algorithm does the rest - selects the pace of the slide show, animates each\n      picture, analyzes contents\n      to adjust screen time for content - and more</p>\n    <div class=\"btn-wrap\">\n      <a href=\"#\" class=\"btn-custom\">Click here to try it out for free</a>\n    </div>\n  </section>\n  <app-footer></app-footer>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/main.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/main.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [ngClass]=\"{'main-background': router.url !== '/main/home'}\">\n  <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/pricing/pricing.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/pricing/pricing.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<h1 class=\"page-title\">Pricing</h1>\n<div class=\"pricing__wrap container\">\n  <div class=\"pricing__item basic\">\n    <a href=\"#\">\n      <img src=\"../../../../assets/img/basic.png\" alt=\"basic\">\n      <h3 class=\"quality\">{{basic.title}}</h3>\n      <p>{{basic.description}}</p>\n      <!--<p>+$1</p>-->\n      <p class=\"price\">{{basic.price}}</p>\n    </a>\n  </div>\n  <div class=\"pricing__item advanced\">\n    <a href=\"#\" class=\"best-deal\">\n      <img src=\"../../../../assets/img/advanced.png\" alt=\"advanced\">\n      <h3 class=\"quality\">{{advanced.title}}</h3>\n      <p>{{advanced.description}}</p>\n      <!--<p>+$3</p>-->\n      <p class=\"price\">{{advanced.price}}</p>\n    </a>\n  </div>\n  <div class=\"pricing__item pro\">\n    <a href=\"#\">\n      <img src=\"../../../../assets/img/pro.png\" alt=\"pro\">\n      <h3 class=\"quality\">{{pro.title}}</h3>\n      <p>{{pro.description}}</p>\n      <!--<p>+$2</p>-->\n      <p class=\"price\">{{pro.price}}</p>\n    </a>\n  </div>\n</div>\n<app-footer></app-footer>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/profile/profile.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/profile/profile.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<h1 class=\"page-title\">edit profile</h1>\n<div class=\"side-menu-page-wrap\">\n  <app-side-menu [sideMenu]=\"'profile'\"></app-side-menu>\n  <div class=\"login-reg-wrap__item\">\n    <div class=\"login-reg-form\">\n      <h2 class=\"title\">User details</h2>\n\n      <form [formGroup]=\"\">\n        <div class=\"input-wrap\">\n          <input type=\"text\" placeholder=\"Name\">\n        </div>\n        <div class=\"input-wrap\">\n          <input type=\"text\" placeholder=\"Last Name\">\n        </div>\n        <div class=\"input-wrap last-child\">\n          <input type=\"email\" placeholder=\"E-mail\">\n        </div>\n        <div class=\"btn-wrap\">\n          <button type=\"submit\" class=\"btn-custom\">save</button>\n        </div>\n      </form>\n      <h2 class=\"title\">Change password</h2>\n\n      <form [formGroup]=\"\">\n        <div class=\"input-wrap\">\n          <input type=\"password\" placeholder=\"Old password\">\n        </div>\n        <div class=\"input-wrap\">\n          <input type=\"password\" placeholder=\"New Password\">\n\n          <p>* Password must contain 8 characters </p>\n        </div>\n        <div class=\"input-wrap last-child\">\n          <input type=\"password\" placeholder=\"Re-Type Password\">\n        </div>\n        <div class=\"btn-wrap\">\n          <button type=\"submit\" class=\"btn-custom\">save</button>\n        </div>\n      </form>\n    </div>\n    <div class=\"login-reg-network\">\n      <h2 class=\"title\">Link social network for fast login</h2>\n\n      <div class=\"btn-wrap\">\n        <a href=\"#\" class=\"btn-custom facebook\">\n          <img src=\"../../../../assets/img/icon/facebook-btn.png\" alt=\"facebook\">\n\n          <p>singin with facebook</p>\n        </a>\n      </div>\n      <div class=\"btn-wrap\">\n        <a href=\"#\" class=\"btn-custom google\">\n          <img src=\"../../../../assets/img/icon/google-btn.png\" alt=\"google\">\n\n          <p>singin with google</p>\n        </a>\n      </div>\n    </div>\n  </div>\n</div>\n<app-footer></app-footer>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/project-in-progress/project-in-progress.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/project-in-progress/project-in-progress.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<h1 class=\"page-title\">in progress</h1>\n<div class=\"side-menu-page-wrap\">\n  <app-side-menu [sideMenu]=\"'profile'\"></app-side-menu>\n  <div class=\"container\">\n    <div class=\"search\">\n      <form [formGroup]=\"\">\n        <input type=\"text\">\n        <button type=\"submit\" class=\"btn-custom\">search</button>\n      </form>\n    </div>\n    <div class=\"gallery\">\n      <div class=\"gallery__wrap\">\n        <h3 class=\"gallery__title\">My trips</h3>\n\n        <div class=\"gallery__row\">\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <div class=\"info\">\n                <div class=\"info__item\">\n                  <span class=\"time\"><img src=\"../../../../assets/img/icon/time.png\" alt=\"time\">6:04</span>\n                  <span class=\"quality\">1080p</span>\n                </div>\n                <div class=\"info__item\">\n                  <span><img src=\"../../../../assets/img/icon/gallery.png\" alt=\"gallery\"></span>\n                  <span><img src=\"../../../../assets/img/icon/edit.png\" alt=\"edit\"></span>\n                  <span><img src=\"../../../../assets/img/icon/garbage.png\" alt=\"delete\"></span>\n                </div>\n              </div>\n              <img src=\"../../../../assets/img/389.jpg\" alt=\"img\">\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Album name</p>\n\n              <p class=\"date\">01/01/19</p>\n\n              <div class=\"download\">\n                <p class=\"details__title\">Continue editing</p>\n\n                <p class=\"date\">01/01/19</p>\n              </div>\n            </div>\n            <p class=\"warning\">This project will be removed in 10 days</p>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <div class=\"info\">\n                <div class=\"info__item\">\n                  <span class=\"time\"><img src=\"../../../../assets/img/icon/time.png\" alt=\"time\">6:04</span>\n                  <span class=\"quality\">1080p</span>\n                </div>\n                <div class=\"info__item\">\n                  <span><img src=\"../../../../assets/img/icon/gallery.png\" alt=\"gallery\"></span>\n                  <span><img src=\"../../../../assets/img/icon/edit.png\" alt=\"edit\"></span>\n                  <span><img src=\"../../../../assets/img/icon/garbage.png\" alt=\"delete\"></span>\n                </div>\n              </div>\n              <img routerLink=\"/main/project\" src=\"../../../../assets/img/wp.jpg\" alt=\"img\">\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Album name</p>\n\n              <p class=\"date\">01/01/19</p>\n\n              <div class=\"download\">\n                <p class=\"details__title\">Continue editing</p>\n\n                <p class=\"date\">01/01/19</p>\n              </div>\n            </div>\n            <p class=\"warning\">This project will be removed in 10 days</p>\n          </div>\n          <div class=\"gallery__item\">\n            <div class=\"item-top\">\n              <div class=\"info\">\n                <div class=\"info__item\">\n                  <span class=\"time\"><img src=\"../../../../assets/img/icon/time.png\" alt=\"time\">6:04</span>\n                  <span class=\"quality\">1080p</span>\n                </div>\n                <div class=\"info__item\">\n                  <span><img src=\"../../../../assets/img/icon/gallery.png\" alt=\"gallery\"></span>\n                  <span><img src=\"../../../../assets/img/icon/edit.png\" alt=\"edit\"></span>\n                  <span><img src=\"../../../../assets/img/icon/garbage.png\" alt=\"delete\"></span>\n                </div>\n              </div>\n              <img src=\"../../../../assets/img/1526.jpg\" alt=\"img\">\n            </div>\n            <div class=\"details\">\n              <p class=\"details__title\">Album name</p>\n\n              <p class=\"date\">01/01/19</p>\n\n              <div class=\"download\">\n                <p class=\"details__title\">Continue editing</p>\n\n                <p class=\"date\">01/01/19</p>\n              </div>\n            </div>\n            <p class=\"warning\">This project will be removed in 10 days</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<app-footer></app-footer>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/projects/projects.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/projects/projects.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<div class=\"my-project__wrap\">\n  <h2 class=\"my-project__title\">\n    <span>Project Name</span>\n    <img src=\"../../../../assets/img/icon/edit2.png\" alt=\"edit\">\n  </h2>\n  <div class=\"creating-wrap\">\n    <div class=\"creating-wrap__item collage\">\n      <div class=\"left\">\n        <div class=\"add-photo\"><img src=\"../../../../assets/img/upload-pictures.png\" alt=\"add photo\"></div>\n        <div class=\"add-photo\"><img src=\"../../../../assets/img/upload-pictures.png\" alt=\"add photo\"></div>\n        <div class=\"add-photo\"><img src=\"../../../../assets/img/upload-pictures.png\" alt=\"add photo\"></div>\n        <div class=\"add-photo\"><img src=\"../../../../assets/img/upload-pictures.png\" alt=\"add photo\"></div>\n        <div class=\"collage__title\">Your\n          Title\n        </div>\n      </div>\n      <div class=\"right\">\n        <img src=\"../../../../assets/img/10509sm.jpg\" alt=\"photo\">\n        <div class=\"tools\">\n          <div class=\"div\">\n            <img src=\"../../../../assets/img/icon/edit.png\" alt=\"edit\">\n          </div>\n          <div class=\"div\">\n            <img src=\"../../../../assets/img/icon/garbage.png\" alt=\"delete\">\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"creating-wrap__item\">\n      <div class=\"video\">\n        <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/uNCr7NdOJgw\" frameborder=\"0\"\n                allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                allowfullscreen></iframe>\n        <div class=\"progress-wrap\">\n          <p>Rendering...</p>\n          <div class=\"progress\">\n            <div class=\"progress__bar\"></div>\n            <div class=\"progress__counter\">65%</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"music-add\">\n    <button class=\"btn-custom light\" *ngIf=\"addMusicBtn\" (click)=\"addMusic = !addMusic\">add music</button>\n    <div class=\"added\" *ngIf=\"!addMusicBtn\">\n      <img src=\"../../../../assets/img/sound.png\" alt=\"img\">\n      <div class=\"tools\">\n        <div class=\"div\">\n          <img src=\"../../../../assets/img/icon/edit.png\" alt=\"edit\" (click)=\"openMusicList()\">\n        </div>\n        <div class=\"div\">\n          <img src=\"../../../../assets/img/icon/garbage.png\" alt=\"delete\" (click)=\"addMusicBtn = !addMusicBtn\">\n        </div>\n      </div>\n      <div class=\"start-finish\">\n        <div class=\"start\">00:00:00</div>\n        <div class=\"finish\">00:05:41</div>\n      </div>\n    </div>\n  </div>\n  <div class=\"choose-frame\">\n    <div class=\"choose-frame__item\">\n      <img src=\"../../../../assets/img/mp1.jpg\" alt=\"frame\">\n      <p></p>\n    </div>\n    <div class=\"choose-frame__item active\">\n      <img src=\"../../../../assets/img/mp2.jpg\" alt=\"frame\">\n      <p>Frame 2</p>\n    </div>\n    <div class=\"choose-frame__item\">\n      <img src=\"../../../../assets/img/mp3.jpg\" alt=\"frame\">\n      <p></p>\n    </div>\n    <div class=\"choose-frame__item\">\n      <img src=\"../../../../assets/img/mp4.jpg\" alt=\"frame\">\n      <p></p>\n    </div>\n    <div class=\"choose-frame__item\">\n      <img src=\"../../../../assets/img/mp5.jpg\" alt=\"frame\">\n      <p></p>\n    </div>\n    <div class=\"choose-frame__item\">\n      <img src=\"../../../../assets/img/mp6.jpg\" alt=\"frame\">\n      <p></p>\n    </div>\n    <div class=\"choose-frame__item\">\n      <img src=\"../../../../assets/img/mp7.jpg\" alt=\"frame\">\n      <p></p>\n    </div>\n    <div class=\"choose-frame__item\">\n      <img src=\"../../../../assets/img/mp8.jpg\" alt=\"frame\">\n      <p></p>\n    </div>\n  </div>\n  <div class=\"btn-wrap\">\n    <button class=\"btn-custom light\" (click)=\"progress = !progress\">Download Video</button>\n    <button class=\"btn-custom\">save</button>\n  </div>\n</div>\n<app-footer></app-footer>\n<div class=\"overlay save-loader\" *ngIf=\"progress\">\n  <p class=\"description\">You can continue editing you project from your projects library in your profile</p>\n  <div class=\"progress\">\n    <div class=\"progress__bar\"></div>\n    <div class=\"progress__counter\">65%</div>\n  </div>\n  <p>LOADING</p>\n  <p class=\"wait\">Please wait</p>\n</div>\n<div class=\"overlay add-music\" *ngIf=\"addMusic\">\n  <div class=\"popup\">\n    <div class=\"btn-wrap first\">\n      <button class=\"btn-custom light\">Use your own music</button>\n      <button class=\"btn-custom light\" (click)=\"changePopup()\">Music library</button>\n    </div>\n    <p>Drag &amp; Drop here</p>\n    <img src=\"../../../assets/img/add-music.png\" alt=\"upload pictures\">\n    <div class=\"btn-wrap\">\n      <button class=\"btn-custom\">Brows My PC</button>\n    </div>\n    <div class=\"close-btn\" (click)=\"addMusic = !addMusic\">\n      <img src=\"../../../assets/img/icon/close.png\" alt=\"close\">\n    </div>\n  </div>\n</div>\n<div class=\"overlay music-library\" *ngIf=\"musicList\">\n  <div class=\"popup\">\n    <div class=\"btn-wrap first\">\n      <button class=\"btn-custom light\">Use your own music</button>\n      <button class=\"btn-custom light active\">Music library</button>\n    </div>\n    <div class=\"search\">\n      <form [formGroup]=\"searchForm\">\n        <input formControlName=\"search\" type=\"text\">\n        <button type=\"submit\" class=\"btn-custom\">search</button>\n      </form>\n    </div>\n    <div class=\"music\">\n      <div class=\"music__item active\">\n        <div class=\"play-btn\">\n          <img src=\"../../../assets/img/icon/play.png\" alt=\"play\" class=\"play\">\n          <img src=\"../../../assets/img/icon/pause.png\" alt=\"pause\" class=\"pause\">\n        </div>\n        <div class=\"music__name\">Music Name</div>\n        <div class=\"music__progress\">\n          <img src=\"../../../assets/img/music-progress.png\" alt=\"bar\">\n        </div>\n        <div class=\"music__time\">01:00</div>\n        <div class=\"btn-custom active\" (click)=\"closePopup()\">add</div>\n      </div>\n      <div class=\"music__item\">\n        <div class=\"play-btn\">\n          <img src=\"../../../assets/img/icon/play.png\" alt=\"play\" class=\"play\">\n          <img src=\"../../../assets/img/icon/pause.png\" alt=\"pause\" class=\"pause\">\n        </div>\n        <div class=\"music__name\">Music Name</div>\n        <div class=\"music__progress\">\n          <img src=\"../../../assets/img/music-progress.png\" alt=\"bar\">\n        </div>\n        <div class=\"music__time\">01:00</div>\n        <div class=\"btn-custom\" (click)=\"closePopup()\">add</div>\n      </div>\n      <div class=\"music__item\">\n        <div class=\"play-btn\">\n          <img src=\"../../../assets/img/icon/play.png\" alt=\"play\" class=\"play\">\n          <img src=\"../../../assets/img/icon/pause.png\" alt=\"pause\" class=\"pause\">\n        </div>\n        <div class=\"music__name\">Music Name</div>\n        <div class=\"music__progress\">\n          <img src=\"../../../assets/img/music-progress.png\" alt=\"bar\">\n        </div>\n        <div class=\"music__time\">01:00</div>\n        <div class=\"btn-custom\" (click)=\"closePopup()\">add</div>\n      </div>\n      <div class=\"music__item\">\n        <div class=\"play-btn\">\n          <img src=\"../../../assets/img/icon/play.png\" alt=\"play\" class=\"play\">\n          <img src=\"../../../assets/img/icon/pause.png\" alt=\"pause\" class=\"pause\">\n        </div>\n        <div class=\"music__name\">Music Name</div>\n        <div class=\"music__progress\">\n          <img src=\"../../../assets/img/music-progress.png\" alt=\"bar\">\n        </div>\n        <div class=\"music__time\">01:00</div>\n        <div class=\"btn-custom\" (click)=\"closePopup()\">add</div>\n      </div>\n      <div class=\"music__item\">\n        <div class=\"play-btn\">\n          <img src=\"../../../assets/img/icon/play.png\" alt=\"play\" class=\"play\">\n          <img src=\"../../../assets/img/icon/pause.png\" alt=\"pause\" class=\"pause\">\n        </div>\n        <div class=\"music__name\">Music Name</div>\n        <div class=\"music__progress\">\n          <img src=\"../../../assets/img/music-progress.png\" alt=\"bar\">\n        </div>\n        <div class=\"music__time\">01:00</div>\n        <div class=\"btn-custom\" (click)=\"closePopup()\">add</div>\n      </div>\n      <div class=\"music__item\">\n        <div class=\"play-btn\">\n          <img src=\"../../../assets/img/icon/play.png\" alt=\"play\" class=\"play\">\n          <img src=\"../../../assets/img/icon/pause.png\" alt=\"pause\" class=\"pause\">\n        </div>\n        <div class=\"music__name\">Music Name</div>\n        <div class=\"music__progress\">\n          <img src=\"../../../assets/img/music-progress.png\" alt=\"bar\">\n        </div>\n        <div class=\"music__time\">01:00</div>\n        <div class=\"btn-custom\" (click)=\"closePopup()\">add</div>\n      </div>\n      <div class=\"music__item\">\n        <div class=\"play-btn\">\n          <img src=\"../../../assets/img/icon/play.png\" alt=\"play\" class=\"play\">\n          <img src=\"../../../assets/img/icon/pause.png\" alt=\"pause\" class=\"pause\">\n        </div>\n        <div class=\"music__name\">Music Name</div>\n        <div class=\"music__progress\">\n          <img src=\"../../../assets/img/music-progress.png\" alt=\"bar\">\n        </div>\n        <div class=\"music__time\">01:00</div>\n        <div class=\"btn-custom\" (click)=\"closePopup()\">add</div>\n      </div>\n      <div class=\"music__item\">\n        <div class=\"play-btn\">\n          <img src=\"../../../assets/img/icon/play.png\" alt=\"play\" class=\"play\">\n          <img src=\"../../../assets/img/icon/pause.png\" alt=\"pause\" class=\"pause\">\n        </div>\n        <div class=\"music__name\">Music Name</div>\n        <div class=\"music__progress\">\n          <img src=\"../../../assets/img/music-progress.png\" alt=\"bar\">\n        </div>\n        <div class=\"music__time\">01:00</div>\n        <div class=\"btn-custom\" (click)=\"closePopup()\">add</div>\n      </div>\n    </div>\n    <div class=\"close-btn\">\n      <img src=\"../../../assets/img/icon/close.png\" alt=\"close\" (click)=\"musicList = !musicList\">\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/components/footer/footer.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/components/footer/footer.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer\">\n  <div class=\"footer__logo\">\n    <img src=\"../../../../assets/img/logo2.png\" alt=\"logo\">\n  </div>\n  <div class=\"container\">\n    <div class=\"secondary-menu\">\n      <div class=\"secondary-menu__item\">\n        <h4 class=\"title\">gallery</h4>\n\n      </div>\n      <div class=\"secondary-menu__item\">\n        <h4 class=\"title\">info</h4>\n\n        <p><a href=\"#\">About us</a></p>\n\n        <p><a href=\"#\">Terms & Conditions</a></p>\n\n        <p><a href=\"#\">Privacy policy</a></p>\n      </div>\n      <div class=\"secondary-menu__item\">\n        <h4 class=\"title\">help & support</h4>\n\n        <p><a routerLink=\"/main/contact-us\">Contact us</a></p>\n\n        <p><a routerLink=\"/main/profile\">My profile</a></p>\n\n        <p><a routerLink=\"/main/faq\">FAQ</a></p>\n      </div>\n      <div class=\"secondary-menu__item\">\n        <h4 class=\"title\">pricing</h4>\n\n      </div>\n    </div>\n  </div>\n  <div class=\"footer__social\">\n    <a routerLink=\"\">\n      <img src=\"../../../../assets/img/icon/facebook.png\" alt=\"facebook\">\n    </a>\n    <a routerLink=\"\">\n      <img src=\"../../../../assets/img/icon/youtube.png\" alt=\"youtube\">\n    </a>\n    <a routerLink=\"\">\n      <img src=\"../../../../assets/img/icon/instagram.png\" alt=\"instagram\">\n    </a>\n    <a routerLink=\"\">\n      <img src=\"../../../../assets/img/icon/twitter.png\" alt=\"twitter\">\n    </a>\n  </div>\n</footer>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/components/header/header.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/components/header/header.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"header\">\n  <div class=\"header__logo\" [ngStyle]=\"{'top': headerLogo ? '1.979vw' : '0'}\">\n    <a routerLink=\"/main/index\">\n      <img [ngStyle]=\"{'width': headerLogo ? '100%' : '0'}\" src=\"../../../../assets/img/logo.png\" alt=\"logo\">\n    </a>\n  </div>\n  <div class=\"nav-toggle\" (click)=\"openMenu()\">\n    <span></span>\n    <span></span>\n    <span></span>\n  </div>\n  <nav class=\"header__menu\">\n    <ul>\n      <li><a routerLink=\"/main/about\" routerLinkActive=\"activeLink\">about</a></li>\n      <li><a routerLink=\"/main/contact-us\" routerLinkActive=\"activeLink\">contact us</a></li>\n      <li><a routerLink=\"/main/gallery\">gallery</a></li>\n      <li><a routerLink=\"/main/pricing\">pricing</a></li>\n    </ul>\n  </nav>\n  <div *ngIf=\"!userLogin\" class=\"header__buttons\">\n    <a routerLink=\"/auth/sign-in\" class=\"btn-custom light\">sing in</a>\n    <a routerLink=\"/auth/sign-up\" class=\"btn-custom\">sing up</a>\n  </div>\n  <div *ngIf=\"userLogin\" class=\"header__buttons log-in\">\n    <a routerLink=\"/main/profile\" class=\"btn-custom light\">my profile</a>\n    <p>Hi, {{user}}!</p>\n    <a (click)=\"logOut()\"><img src=\"../../../../assets/img/icon/logout.png\" alt=\"logout\"></a>\n  </div>\n</header>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/components/side-menu/side-menu.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/components/side-menu/side-menu.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"side-menu\" (click)=\"openSideMenu()\">\n  <ul *ngIf=\"sideMenu === 'gallery'\">\n    <li><a href=\"#\" class=\"active\">category name</a></li>\n    <li><a href=\"#\">category name</a></li>\n    <li><a href=\"#\">category name</a></li>\n    <li><a href=\"#\">category name</a></li>\n    <li><a href=\"#\">category name</a></li>\n    <li><a href=\"#\">category name</a></li>\n    <li><a href=\"#\">category name</a></li>\n    <li><a href=\"#\">category name</a></li>\n    <li><a href=\"#\">category name</a></li>\n  </ul>\n  <ul *ngIf=\"sideMenu === 'profile'\">\n    <li><a routerLink=\"/main/profile\" routerLinkActive=\"active\">edit profile </a></li>\n    <li><a routerLink=\"/main/finished-project\" routerLinkActive=\"active\">finished projects </a></li>\n    <li><a routerLink=\"/main/project-in-progress\" routerLinkActive=\"active\">projects in progress </a></li>\n  </ul>\n  <div class=\"side-menu__toggle\">\n    <img src=\"../../../../assets/img/icon/right-arrow.png\" alt=\"left arrow\">\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _main_main_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main/main.module */ "./src/app/main/main.module.ts");




const routes = [
    { path: 'main', loadChildren: './main/main.module#MainModule' },
    { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
    { path: '', redirectTo: 'main', pathMatch: 'full' },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes), _main_main_module__WEBPACK_IMPORTED_MODULE_3__["MainModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let AppComponent = class AppComponent {
    constructor(router) {
        this.router = router;
        if (localStorage.getItem('token')) {
            this.router.navigate(['/main']);
        }
        else {
            this.router.navigate(['/auth']);
        }
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: provideConfig, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "provideConfig", function() { return provideConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _shared_interseptors_authentication_interseptors__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared/interseptors/authentication.interseptors */ "./src/app/shared/interseptors/authentication.interseptors.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _main_main_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./main/main.module */ "./src/app/main/main.module.ts");
/* harmony import */ var _auth_auth_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./auth/auth.module */ "./src/app/auth/auth.module.ts");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! angularx-social-login */ "./node_modules/angularx-social-login/angularx-social-login.js");













let config = new angularx_social_login__WEBPACK_IMPORTED_MODULE_11__["AuthServiceConfig"]([
    {
        id: angularx_social_login__WEBPACK_IMPORTED_MODULE_11__["GoogleLoginProvider"].PROVIDER_ID,
        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_11__["GoogleLoginProvider"]('40794855768-q0gog4p6257skagd1okisdj44kjppc3g.apps.googleusercontent.com')
    },
    {
        id: angularx_social_login__WEBPACK_IMPORTED_MODULE_11__["FacebookLoginProvider"].PROVIDER_ID,
        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_11__["FacebookLoginProvider"]('365018910844645')
    }
]);
function provideConfig() {
    return config;
}
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
            _main_main_module__WEBPACK_IMPORTED_MODULE_9__["MainModule"],
            _auth_auth_module__WEBPACK_IMPORTED_MODULE_10__["AuthModule"],
            angularx_social_login__WEBPACK_IMPORTED_MODULE_11__["SocialLoginModule"]
        ],
        providers: [{
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"],
                useClass: _shared_interseptors_authentication_interseptors__WEBPACK_IMPORTED_MODULE_7__["AuthenticationInterceptor"],
                multi: true
            },
            {
                provide: angularx_social_login__WEBPACK_IMPORTED_MODULE_11__["AuthServiceConfig"],
                useFactory: provideConfig
            }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/auth/auth-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/auth/auth-routing.module.ts ***!
  \*********************************************/
/*! exports provided: AuthRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRoutingModule", function() { return AuthRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _auth_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth.component */ "./src/app/auth/auth.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/auth/registration/registration.component.ts");






const routes = [
    { path: '', component: _auth_component__WEBPACK_IMPORTED_MODULE_3__["AuthComponent"], children: [
            { path: 'sign-in', component: _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"] },
            { path: 'sign-up', component: _registration_registration_component__WEBPACK_IMPORTED_MODULE_5__["RegistrationComponent"] },
            { path: '', redirectTo: 'sign-in', pathMatch: 'full' },
        ] },
];
let AuthRoutingModule = class AuthRoutingModule {
};
AuthRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AuthRoutingModule);



/***/ }),

/***/ "./src/app/auth/auth.component.scss":
/*!******************************************!*\
  !*** ./src/app/auth/auth.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".auth {\n  background: url('login-bg.png') top left no-repeat;\n  background-size: 100%;\n}\n\n.login-reg-wrap {\n  padding: 2.552vw 8.229vw 11.771vw;\n}\n\n.login-reg-wrap__item {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.login-reg-wrap__item .login-reg-form {\n  margin-right: 4.219vw;\n  text-align: center;\n  /*padding-top: 1.250vw;*/\n}\n\n.login-reg-wrap__item .login-reg-form h2 {\n  font-weight: 700;\n  font-size: 1.563vw;\n  letter-spacing: 0.052vw;\n}\n\n.login-reg-wrap__item .login-reg-form .input-wrap {\n  position: relative;\n  margin-bottom: 1.719vw;\n}\n\n.login-reg-wrap__item .login-reg-form .input-wrap:last-of-type {\n  margin-bottom: 4.531vw;\n}\n\n.login-reg-wrap__item .login-reg-form .input-wrap p {\n  position: absolute;\n  margin: 0;\n  color: #cc0033;\n}\n\n.login-reg-wrap__item .login-reg-form input {\n  width: 22.5vw;\n}\n\n.login-reg-wrap__item .login-reg-form .checkbox-wrap {\n  text-align: left;\n  margin-bottom: 0.833vw;\n}\n\n.login-reg-wrap__item .login-reg-form .checkbox-wrap:last-child {\n  margin-bottom: 1.146vw;\n}\n\n.login-reg-wrap__item .login-reg-form .checkbox-wrap input {\n  position: absolute;\n  opacity: 0;\n  width: 1px;\n}\n\n.login-reg-wrap__item .login-reg-form .checkbox-wrap label {\n  position: relative;\n  letter-spacing: 1px;\n  padding-left: 1.563vw;\n}\n\n.login-reg-wrap__item .login-reg-form .checkbox-wrap label a {\n  color: #000000;\n  font-weight: 400;\n  text-decoration: underline;\n  font-size: 0.833vw;\n}\n\n.login-reg-wrap__item .login-reg-form .checkbox-wrap label:before {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  box-sizing: border-box;\n  width: 0.833vw;\n  height: 0.833vw;\n  border: 0.104vw solid #180a21;\n}\n\n.login-reg-wrap__item .login-reg-form .checkbox-wrap label:after {\n  content: \"\";\n  background: url('check.png') no-repeat;\n  background-size: 100% 100%;\n  position: absolute;\n  display: none;\n  width: 1.042vw;\n  height: 1.094vw;\n  left: 0.052vw;\n  top: -0.313vw;\n}\n\n.login-reg-wrap__item .login-reg-form .checkbox-wrap input:checked + label:after {\n  display: block;\n}\n\n.login-reg-wrap__item .login-reg-form form {\n  padding-top: 0.469vw;\n}\n\n.login-reg-wrap__item .login-reg-form form .btn-custom {\n  width: 11.667vw;\n  height: 2.5vw;\n}\n\n.login-reg-wrap__item .login-reg-network {\n  text-align: center;\n}\n\n.login-reg-wrap__item .login-reg-network h2 {\n  font-weight: 700;\n  position: relative;\n  font-size: 1.25vw;\n  letter-spacing: 0.052vw;\n  margin-bottom: 3.646vw;\n}\n\n.login-reg-wrap__item .login-reg-network h2:after {\n  content: \"\";\n  position: absolute;\n  left: 50%;\n  -webkit-transform: translate(-50%);\n          transform: translate(-50%);\n  background-color: #000000;\n  top: 2.135vw;\n  width: 10.417vw;\n  height: 0.781vw;\n}\n\n.login-reg-wrap__item .login-reg-network p {\n  font-weight: 700;\n  font-size: 0.833vw;\n  letter-spacing: 0.052vw;\n  margin-top: 1.875vw;\n  margin-bottom: 1.094vw;\n}\n\n.login-reg-wrap__item .login-reg-network .btn-wrap {\n  padding-bottom: 1.771vw;\n}\n\n.reg-form {\n  padding-top: 0;\n  margin-top: -2.604vw;\n}\n\n.reg-link {\n  font-weight: 700;\n  text-decoration: underline;\n  text-transform: uppercase;\n  color: #000;\n  font-size: 0.833vw;\n  letter-spacing: 0.052vw;\n}\n\n@media screen and (max-width: 1200px) {\n  .login-reg-wrap__item .login-reg-form h2 {\n    font-size: 19px;\n  }\n  .login-reg-wrap__item .login-reg-form .input-wrap p {\n    font-size: 12px;\n  }\n  .login-reg-wrap__item .login-reg-form .checkbox-wrap {\n    margin-bottom: 10px;\n  }\n  .login-reg-wrap__item .login-reg-form .checkbox-wrap label {\n    padding-left: 30px;\n  }\n  .login-reg-wrap__item .login-reg-form .checkbox-wrap label:before {\n    width: 20px;\n    height: 20px;\n    border: 1px solid #180a21;\n  }\n  .login-reg-wrap__item .login-reg-form .checkbox-wrap label:after {\n    width: 20px;\n    height: 21px;\n    left: 5px;\n    top: -3px;\n  }\n  .login-reg-wrap__item .login-reg-form .checkbox-wrap label a {\n    font-size: 14px;\n  }\n  .login-reg-wrap__item .reg-form .input-wrap.last-child {\n    margin-bottom: 20px;\n  }\n  .login-reg-wrap__item .login-reg-network h2 {\n    font-size: 14px;\n  }\n  .login-reg-wrap__item .login-reg-network .btn-custom.facebook,\n.login-reg-wrap__item .login-reg-network .btn-custom.google {\n    width: 216px;\n    padding-left: 5px;\n  }\n  .login-reg-wrap__item .login-reg-network .btn-custom.facebook p,\n.login-reg-wrap__item .login-reg-network .btn-custom.google p {\n    font-size: 14px;\n  }\n  .login-reg-wrap__item .login-reg-network p {\n    font-size: 14px;\n  }\n  .login-reg-wrap__item .login-reg-network .reg-link {\n    font-size: 14px;\n  }\n}\n\n@media screen and (max-width: 992px) {\n  .login-reg-wrap__item .login-reg-form h2 {\n    font-size: 20px;\n    margin-bottom: 20px;\n  }\n  .login-reg-wrap__item .login-reg-form .input-wrap {\n    margin-bottom: 24px;\n  }\n  .login-reg-wrap__item .login-reg-form .input-wrap p {\n    font-size: 14px;\n  }\n  .login-reg-wrap__item .login-reg-form input {\n    width: 250px;\n  }\n  .login-reg-wrap__item .login-reg-form form .btn-custom {\n    width: auto;\n    height: 40px;\n    border-radius: 20px;\n  }\n  .login-reg-wrap__item .login-reg-network .title {\n    font-size: 16px;\n  }\n  .login-reg-wrap__item .login-reg-network .title:after {\n    bottom: -11px;\n    width: 50%;\n    top: auto;\n    height: 6px;\n  }\n  .login-reg-wrap__item .login-reg-network .btn-custom.facebook,\n.login-reg-wrap__item .login-reg-network .btn-custom.google {\n    height: 40px;\n    width: 100%;\n  }\n  .login-reg-wrap__item .login-reg-network .btn-custom.facebook img,\n.login-reg-wrap__item .login-reg-network .btn-custom.google img {\n    width: 28px;\n    height: 28px;\n  }\n}\n\n@media screen and (max-width: 767px) {\n  .login-reg-wrap {\n    padding: 0 20px;\n  }\n  .login-reg-wrap__item {\n    width: 100%;\n    padding: 0 20px 40px !important;\n    -webkit-box-pack: justify;\n            justify-content: space-between;\n  }\n  .login-reg-wrap__item .login-reg-form {\n    width: 50%;\n  }\n  .login-reg-wrap__item .login-reg-form input {\n    width: 100%;\n  }\n  .login-reg-wrap__item .login-reg-network .title {\n    margin-bottom: 20px;\n  }\n}\n\n@media screen and (max-width: 600px) {\n  .login-reg-wrap__item {\n    flex-wrap: wrap;\n  }\n  .login-reg-wrap__item .login-reg-form {\n    margin: 0;\n    width: 100%;\n  }\n  .login-reg-wrap__item .login-reg-network {\n    width: 100%;\n  }\n  .login-reg-wrap__item .login-reg-network .btn-custom.facebook,\n.login-reg-wrap__item .login-reg-network .btn-custom.google {\n    width: 300px;\n    max-width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9DOlxcVXNlcnNcXFZsYWREXFxEZXNrdG9wXFxNZW1vIChhbGwpXFxtZW1vL3NyY1xcYXBwXFxhdXRoXFxhdXRoLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hdXRoL2F1dGguY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrREFBQTtFQUVBLHFCQUFBO0FDQ0Y7O0FERUE7RUFDRSxpQ0FBQTtBQ0NGOztBREFFO0VBR0Usb0JBQUE7RUFBQSxhQUFBO0VBR0EscUJBQUE7VUFBQSx5QkFBQTtBQ0VKOztBRERJO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0FDR047O0FERk07RUFDRSxnQkFBQTtFQUVBLGtCQUFBO0VBQ0EsdUJBQUE7QUNHUjs7QURETTtFQUNFLGtCQUFBO0VBQ0Esc0JBQUE7QUNHUjs7QURGUTtFQUNFLHNCQUFBO0FDSVY7O0FERlE7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxjQUFBO0FDSVY7O0FERE07RUFDRSxhQUFBO0FDR1I7O0FERE07RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0FDR1I7O0FERlE7RUFDRSxzQkFBQTtBQ0lWOztBREZRO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtBQ0lWOztBREZRO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0FDSVY7O0FESFU7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0FDS1o7O0FESFU7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLDZCQUFBO0FDS1o7O0FESFU7RUFDRSxXQUFBO0VBQ0Esc0NBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLGFBQUE7QUNLWjs7QURGUTtFQUNFLGNBQUE7QUNJVjs7QURETTtFQUNFLG9CQUFBO0FDR1I7O0FERlE7RUFDRSxlQUFBO0VBQ0EsYUFBQTtBQ0lWOztBREFJO0VBQ0Usa0JBQUE7QUNFTjs7QURETTtFQUNFLGdCQUFBO0VBRUEsa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7QUNFUjs7QURBUTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxrQ0FBQTtVQUFBLDBCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7QUNFVjs7QURDTTtFQUNFLGdCQUFBO0VBRUEsa0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNBUjs7QURFTTtFQUNFLHVCQUFBO0FDQVI7O0FETUE7RUFDRSxjQUFBO0VBQ0Esb0JBQUE7QUNIRjs7QURNQTtFQUNFLGdCQUFBO0VBQ0EsMEJBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0FDSEY7O0FETUE7RUFJTTtJQUNFLGVBQUE7RUNOTjtFRFFJO0lBQ0UsZUFBQTtFQ05OO0VEUUk7SUFDRSxtQkFBQTtFQ05OO0VET007SUFDRSxrQkFBQTtFQ0xSO0VETVE7SUFDRSxXQUFBO0lBQ0EsWUFBQTtJQUNBLHlCQUFBO0VDSlY7RURNUTtJQUNFLFdBQUE7SUFDQSxZQUFBO0lBQ0EsU0FBQTtJQUNBLFNBQUE7RUNKVjtFRE1RO0lBQ0UsZUFBQTtFQ0pWO0VEVUk7SUFDRSxtQkFBQTtFQ1JOO0VEWUk7SUFDRSxlQUFBO0VDVk47RURZSTs7SUFFRSxZQUFBO0lBQ0EsaUJBQUE7RUNWTjtFRFdNOztJQUNFLGVBQUE7RUNSUjtFRFdJO0lBQ0UsZUFBQTtFQ1ROO0VEV0k7SUFDRSxlQUFBO0VDVE47QUFDRjs7QURjQTtFQUlNO0lBQ0UsZUFBQTtJQUNBLG1CQUFBO0VDZk47RURpQkk7SUFDRSxtQkFBQTtFQ2ZOO0VEZ0JNO0lBQ0UsZUFBQTtFQ2RSO0VEaUJJO0lBQ0UsWUFBQTtFQ2ZOO0VEaUJJO0lBQ0UsV0FBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtFQ2ZOO0VEbUJJO0lBQ0UsZUFBQTtFQ2pCTjtFRGtCTTtJQUNBLGFBQUE7SUFDQSxVQUFBO0lBQ0EsU0FBQTtJQUNBLFdBQUE7RUNoQk47RURvQkk7O0lBRUUsWUFBQTtJQUNBLFdBQUE7RUNsQk47RURtQk07O0lBQ0UsV0FBQTtJQUNBLFlBQUE7RUNoQlI7QUFDRjs7QURzQkE7RUFDRTtJQUNFLGVBQUE7RUNwQkY7RURxQkU7SUFDRSxXQUFBO0lBQ0EsK0JBQUE7SUFHQSx5QkFBQTtZQUFBLDhCQUFBO0VDbkJKO0VEb0JJO0lBQ0UsVUFBQTtFQ2xCTjtFRG1CTTtJQUNFLFdBQUE7RUNqQlI7RURvQkk7SUFDRSxtQkFBQTtFQ2xCTjtBQUNGOztBRHVCQTtFQUNFO0lBR0UsZUFBQTtFQ3JCRjtFRHNCRTtJQUNFLFNBQUE7SUFDQSxXQUFBO0VDcEJKO0VEc0JFO0lBQ0UsV0FBQTtFQ3BCSjtFRHFCSTs7SUFFRSxZQUFBO0lBQ0EsZUFBQTtFQ25CTjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvYXV0aC9hdXRoLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmF1dGgge1xyXG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1nL2xvZ2luLWJnLnBuZykgdG9wIGxlZnQgbm8tcmVwZWF0O1xyXG4gIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcclxufVxyXG5cclxuLmxvZ2luLXJlZy13cmFwIHtcclxuICBwYWRkaW5nOiAyLjU1MnZ3IDguMjI5dncgMTEuNzcxdnc7XHJcbiAgJl9faXRlbSB7XHJcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICAgIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAtd2Via2l0LWp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICAtbXMtZmxleC1wYWNrOiBlbmQ7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgLmxvZ2luLXJlZy1mb3JtIHtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiA0LjIxOXZ3O1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIC8qcGFkZGluZy10b3A6IDEuMjUwdnc7Ki9cclxuICAgICAgaDIge1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICAgICAgLy9sZXR0ZXItc3BhY2luZzogMXB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS41NjN2dztcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4wNTJ2dztcclxuICAgICAgfVxyXG4gICAgICAuaW5wdXQtd3JhcCB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEuNzE5dnc7XHJcbiAgICAgICAgJjpsYXN0LW9mLXR5cGUge1xyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogNC41MzF2dztcclxuICAgICAgICB9XHJcbiAgICAgICAgcCB7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgICBjb2xvcjogI2NjMDAzMztcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgaW5wdXQge1xyXG4gICAgICAgIHdpZHRoOiAyMi41MDB2dztcclxuICAgICAgfVxyXG4gICAgICAuY2hlY2tib3gtd3JhcCB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwLjgzM3Z3O1xyXG4gICAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxLjE0NnZ3O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpbnB1dCB7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICBvcGFjaXR5OiAwO1xyXG4gICAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwge1xyXG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcclxuICAgICAgICAgIHBhZGRpbmctbGVmdDogMS41NjN2dztcclxuICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMC44MzN2dztcclxuICAgICAgICAgIH1cclxuICAgICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbGVmdDogMDtcclxuICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgICAgd2lkdGg6IDAuODMzdnc7XHJcbiAgICAgICAgICAgIGhlaWdodDogMC44MzN2dztcclxuICAgICAgICAgICAgYm9yZGVyOiAwLjEwNHZ3IHNvbGlkICMxODBhMjE7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAmOmFmdGVyIHtcclxuICAgICAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1nL2ljb24vY2hlY2sucG5nKSBuby1yZXBlYXQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxLjA0MnZ3O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEuMDk0dnc7XHJcbiAgICAgICAgICAgIGxlZnQ6IDAuMDUydnc7XHJcbiAgICAgICAgICAgIHRvcDogLTAuMzEzdnc7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlucHV0OmNoZWNrZWQgKyBsYWJlbDphZnRlciB7XHJcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgZm9ybSB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDAuNDY5dnc7XHJcbiAgICAgICAgLmJ0bi1jdXN0b20ge1xyXG4gICAgICAgICAgd2lkdGg6IDExLjY2N3Z3O1xyXG4gICAgICAgICAgaGVpZ2h0OiAyLjUwMHZ3O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmxvZ2luLXJlZy1uZXR3b3JrIHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBoMiB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgICAgICAvL2xldHRlci1zcGFjaW5nOiAxcHg7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS4yNTB2dztcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4wNTJ2dztcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzLjY0NnZ3O1xyXG5cclxuICAgICAgICAmOmFmdGVyIHtcclxuICAgICAgICAgIGNvbnRlbnQ6ICcnO1xyXG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgbGVmdDogNTAlO1xyXG4gICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSk7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwO1xyXG4gICAgICAgICAgdG9wOiAyLjEzNXZ3O1xyXG4gICAgICAgICAgd2lkdGg6IDEwLjQxN3Z3O1xyXG4gICAgICAgICAgaGVpZ2h0OiAwLjc4MXZ3O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBwIHtcclxuICAgICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICAgIC8vbGV0dGVyLXNwYWNpbmc6IDFweDtcclxuICAgICAgICBmb250LXNpemU6IDAuODMzdnc7XHJcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMDUydnc7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMS44NzV2dztcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxLjA5NHZ3O1xyXG4gICAgICB9XHJcbiAgICAgIC5idG4td3JhcCB7XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDEuNzcxdnc7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5yZWctZm9ybSB7XHJcbiAgcGFkZGluZy10b3A6IDA7XHJcbiAgbWFyZ2luLXRvcDogLTIuNjA0dnc7XHJcbn1cclxuXHJcbi5yZWctbGluayB7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIGZvbnQtc2l6ZTogMC44MzN2dztcclxuICBsZXR0ZXItc3BhY2luZzogMC4wNTJ2dztcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSB7XHJcblxyXG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSB7XHJcbiAgICAubG9naW4tcmVnLWZvcm0ge1xyXG4gICAgICBoMiB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOXB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5pbnB1dC13cmFwIHAge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgfVxyXG4gICAgICAuY2hlY2tib3gtd3JhcCB7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgICAgICBsYWJlbCB7XHJcbiAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDMwcHg7XHJcbiAgICAgICAgICAmOmJlZm9yZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMxODBhMjE7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAmOmFmdGVyIHtcclxuICAgICAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogMjFweDtcclxuICAgICAgICAgICAgbGVmdDogNXB4O1xyXG4gICAgICAgICAgICB0b3A6IC0zcHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBhe1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAucmVnLWZvcm0ge1xyXG4gICAgICAuaW5wdXQtd3JhcC5sYXN0LWNoaWxkIHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAubG9naW4tcmVnLW5ldHdvcmsge1xyXG4gICAgICBoMiB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5idG4tY3VzdG9tLmZhY2Vib29rLFxyXG4gICAgICAuYnRuLWN1c3RvbS5nb29nbGUge1xyXG4gICAgICAgIHdpZHRoOiAyMTZweDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgICAgICBwIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgcCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5yZWctbGlua3tcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XHJcblxyXG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSB7XHJcbiAgICAubG9naW4tcmVnLWZvcm0ge1xyXG4gICAgICBoMiB7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgIH1cclxuICAgICAgLmlucHV0LXdyYXAge1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XHJcbiAgICAgICAgcCB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIGlucHV0IHtcclxuICAgICAgICB3aWR0aDogMjUwcHg7XHJcbiAgICAgIH1cclxuICAgICAgZm9ybSAuYnRuLWN1c3RvbSB7XHJcbiAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5sb2dpbi1yZWctbmV0d29yayB7XHJcbiAgICAgIC50aXRsZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgICY6YWZ0ZXIge1xyXG4gICAgICAgIGJvdHRvbTogLTExcHg7XHJcbiAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICB0b3A6IGF1dG87XHJcbiAgICAgICAgaGVpZ2h0OiA2cHg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIH1cclxuICAgICAgLmJ0bi1jdXN0b20uZmFjZWJvb2ssXHJcbiAgICAgIC5idG4tY3VzdG9tLmdvb2dsZSB7XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogMjhweDtcclxuICAgICAgICAgIGhlaWdodDogMjhweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XHJcbiAgLmxvZ2luLXJlZy13cmFwIHtcclxuICAgIHBhZGRpbmc6IDAgMjBweDtcclxuICAgICZfX2l0ZW0ge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgcGFkZGluZzogMCAyMHB4IDQwcHggIWltcG9ydGFudDtcclxuICAgICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgLmxvZ2luLXJlZy1mb3JtIHtcclxuICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgIGlucHV0IHtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAubG9naW4tcmVnLW5ldHdvcmsgLnRpdGxlIHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSB7XHJcbiAgICAtd2Via2l0LWZsZXgtd3JhcDogd3JhcDtcclxuICAgIC1tcy1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAubG9naW4tcmVnLWZvcm0ge1xyXG4gICAgICBtYXJnaW46IDA7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLmxvZ2luLXJlZy1uZXR3b3JrIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIC5idG4tY3VzdG9tLmZhY2Vib29rLFxyXG4gICAgICAuYnRuLWN1c3RvbS5nb29nbGUge1xyXG4gICAgICAgIHdpZHRoOiAzMDBweDtcclxuICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG5cclxufVxyXG4iLCIuYXV0aCB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1nL2xvZ2luLWJnLnBuZykgdG9wIGxlZnQgbm8tcmVwZWF0O1xuICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogMTAwJTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xufVxuXG4ubG9naW4tcmVnLXdyYXAge1xuICBwYWRkaW5nOiAyLjU1MnZ3IDguMjI5dncgMTEuNzcxdnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0ge1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIC1tcy1mbGV4LXBhY2s6IGVuZDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0ge1xuICBtYXJnaW4tcmlnaHQ6IDQuMjE5dnc7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgLypwYWRkaW5nLXRvcDogMS4yNTB2dzsqL1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSBoMiB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc2l6ZTogMS41NjN2dztcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDUydnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIC5pbnB1dC13cmFwIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAxLjcxOXZ3O1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSAuaW5wdXQtd3JhcDpsYXN0LW9mLXR5cGUge1xuICBtYXJnaW4tYm90dG9tOiA0LjUzMXZ3O1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSAuaW5wdXQtd3JhcCBwIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiAjY2MwMDMzO1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSBpbnB1dCB7XG4gIHdpZHRoOiAyMi41dnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIC5jaGVja2JveC13cmFwIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgbWFyZ2luLWJvdHRvbTogMC44MzN2dztcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gLmNoZWNrYm94LXdyYXA6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDEuMTQ2dnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIC5jaGVja2JveC13cmFwIGlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvcGFjaXR5OiAwO1xuICB3aWR0aDogMXB4O1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSAuY2hlY2tib3gtd3JhcCBsYWJlbCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgcGFkZGluZy1sZWZ0OiAxLjU2M3Z3O1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSAuY2hlY2tib3gtd3JhcCBsYWJlbCBhIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICBmb250LXNpemU6IDAuODMzdnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIC5jaGVja2JveC13cmFwIGxhYmVsOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgd2lkdGg6IDAuODMzdnc7XG4gIGhlaWdodDogMC44MzN2dztcbiAgYm9yZGVyOiAwLjEwNHZ3IHNvbGlkICMxODBhMjE7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIC5jaGVja2JveC13cmFwIGxhYmVsOmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWcvaWNvbi9jaGVjay5wbmcpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZGlzcGxheTogbm9uZTtcbiAgd2lkdGg6IDEuMDQydnc7XG4gIGhlaWdodDogMS4wOTR2dztcbiAgbGVmdDogMC4wNTJ2dztcbiAgdG9wOiAtMC4zMTN2dztcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gLmNoZWNrYm94LXdyYXAgaW5wdXQ6Y2hlY2tlZCArIGxhYmVsOmFmdGVyIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIGZvcm0ge1xuICBwYWRkaW5nLXRvcDogMC40Njl2dztcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gZm9ybSAuYnRuLWN1c3RvbSB7XG4gIHdpZHRoOiAxMS42Njd2dztcbiAgaGVpZ2h0OiAyLjV2dztcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIGgyIHtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXNpemU6IDEuMjV2dztcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDUydnc7XG4gIG1hcmdpbi1ib3R0b206IDMuNjQ2dnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIGgyOmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwO1xuICB0b3A6IDIuMTM1dnc7XG4gIHdpZHRoOiAxMC40MTd2dztcbiAgaGVpZ2h0OiAwLjc4MXZ3O1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayBwIHtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgZm9udC1zaXplOiAwLjgzM3Z3O1xuICBsZXR0ZXItc3BhY2luZzogMC4wNTJ2dztcbiAgbWFyZ2luLXRvcDogMS44NzV2dztcbiAgbWFyZ2luLWJvdHRvbTogMS4wOTR2dztcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi13cmFwIHtcbiAgcGFkZGluZy1ib3R0b206IDEuNzcxdnc7XG59XG5cbi5yZWctZm9ybSB7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBtYXJnaW4tdG9wOiAtMi42MDR2dztcbn1cblxuLnJlZy1saW5rIHtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGNvbG9yOiAjMDAwO1xuICBmb250LXNpemU6IDAuODMzdnc7XG4gIGxldHRlci1zcGFjaW5nOiAwLjA1MnZ3O1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSBoMiB7XG4gICAgZm9udC1zaXplOiAxOXB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gLmlucHV0LXdyYXAgcCB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gLmNoZWNrYm94LXdyYXAge1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSAuY2hlY2tib3gtd3JhcCBsYWJlbCB7XG4gICAgcGFkZGluZy1sZWZ0OiAzMHB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gLmNoZWNrYm94LXdyYXAgbGFiZWw6YmVmb3JlIHtcbiAgICB3aWR0aDogMjBweDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzE4MGEyMTtcbiAgfVxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIC5jaGVja2JveC13cmFwIGxhYmVsOmFmdGVyIHtcbiAgICB3aWR0aDogMjBweDtcbiAgICBoZWlnaHQ6IDIxcHg7XG4gICAgbGVmdDogNXB4O1xuICAgIHRvcDogLTNweDtcbiAgfVxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIC5jaGVja2JveC13cmFwIGxhYmVsIGEge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLnJlZy1mb3JtIC5pbnB1dC13cmFwLmxhc3QtY2hpbGQge1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayBoMiB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZmFjZWJvb2ssXG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIC5idG4tY3VzdG9tLmdvb2dsZSB7XG4gICAgd2lkdGg6IDIxNnB4O1xuICAgIHBhZGRpbmctbGVmdDogNXB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZmFjZWJvb2sgcCxcbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZ29vZ2xlIHAge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIHAge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIC5yZWctbGluayB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIGgyIHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgfVxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIC5pbnB1dC13cmFwIHtcbiAgICBtYXJnaW4tYm90dG9tOiAyNHB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gLmlucHV0LXdyYXAgcCB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gaW5wdXQge1xuICAgIHdpZHRoOiAyNTBweDtcbiAgfVxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIGZvcm0gLmJ0bi1jdXN0b20ge1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIGhlaWdodDogNDBweDtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLnRpdGxlIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayAudGl0bGU6YWZ0ZXIge1xuICAgIGJvdHRvbTogLTExcHg7XG4gICAgd2lkdGg6IDUwJTtcbiAgICB0b3A6IGF1dG87XG4gICAgaGVpZ2h0OiA2cHg7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayAuYnRuLWN1c3RvbS5mYWNlYm9vayxcbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZ29vZ2xlIHtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayAuYnRuLWN1c3RvbS5mYWNlYm9vayBpbWcsXG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIC5idG4tY3VzdG9tLmdvb2dsZSBpbWcge1xuICAgIHdpZHRoOiAyOHB4O1xuICAgIGhlaWdodDogMjhweDtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgLmxvZ2luLXJlZy13cmFwIHtcbiAgICBwYWRkaW5nOiAwIDIwcHg7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAwIDIwcHggNDBweCAhaW1wb3J0YW50O1xuICAgIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0ge1xuICAgIHdpZHRoOiA1MCU7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSBpbnB1dCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayAudGl0bGUge1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSB7XG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XG4gICAgLW1zLWZsZXgtd3JhcDogd3JhcDtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSB7XG4gICAgbWFyZ2luOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZmFjZWJvb2ssXG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIC5idG4tY3VzdG9tLmdvb2dsZSB7XG4gICAgd2lkdGg6IDMwMHB4O1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/auth/auth.component.ts":
/*!****************************************!*\
  !*** ./src/app/auth/auth.component.ts ***!
  \****************************************/
/*! exports provided: AuthComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthComponent", function() { return AuthComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AuthComponent = class AuthComponent {
    constructor() { }
    ngOnInit() {
    }
};
AuthComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-auth',
        template: __webpack_require__(/*! raw-loader!./auth.component.html */ "./node_modules/raw-loader/index.js!./src/app/auth/auth.component.html"),
        styles: [__webpack_require__(/*! ./auth.component.scss */ "./src/app/auth/auth.component.scss")]
    })
], AuthComponent);



/***/ }),

/***/ "./src/app/auth/auth.module.ts":
/*!*************************************!*\
  !*** ./src/app/auth/auth.module.ts ***!
  \*************************************/
/*! exports provided: AuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _auth_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth.component */ "./src/app/auth/auth.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/auth/registration/registration.component.ts");
/* harmony import */ var _auth_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth-routing.module */ "./src/app/auth/auth-routing.module.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");









let AuthModule = class AuthModule {
};
AuthModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _auth_component__WEBPACK_IMPORTED_MODULE_3__["AuthComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"],
            _registration_registration_component__WEBPACK_IMPORTED_MODULE_5__["RegistrationComponent"],
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _auth_routing_module__WEBPACK_IMPORTED_MODULE_6__["AuthRoutingModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
        ],
        exports: [
            _auth_component__WEBPACK_IMPORTED_MODULE_3__["AuthComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"],
            _registration_registration_component__WEBPACK_IMPORTED_MODULE_5__["RegistrationComponent"]
        ]
    })
], AuthModule);



/***/ }),

/***/ "./src/app/auth/login/login.component.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/login/login.component.ts ***!
  \***********************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angularx-social-login */ "./node_modules/angularx-social-login/angularx-social-login.js");







let LoginComponent = class LoginComponent {
    constructor(fb, authService, router, socialLogin) {
        this.fb = fb;
        this.authService = authService;
        this.router = router;
        this.socialLogin = socialLogin;
    }
    ngOnInit() {
        this.loginForm = this.fb.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    loginUser() {
        if (this.loginForm.value.email !== '' && this.loginForm.value.password !== '') {
            this.authService.login(this.loginForm.value)
                .subscribe(res => {
                if (res['success']) {
                    localStorage.setItem('token', res['token']);
                    localStorage.setItem('user', res['data']['name']);
                    this.router.navigate(['/main/profile']);
                }
            });
        }
        else
            alert('Please, fill in the fields');
    }
    loginUserWithSocialNet(id) {
        this.authService.socialRegister(id)
            .subscribe(res => {
            if (res['success']) {
                localStorage.setItem('token', res['token']);
                localStorage.setItem('user', res['data']['name']);
                this.router.navigate(['/main/profile']);
            }
        }, err => {
            console.log(err);
        });
    }
    signInWithFB() {
        this.socialLogin.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_5__["FacebookLoginProvider"].PROVIDER_ID);
        this.socialLogin.authState
            .subscribe(user => {
            console.log(user);
            if (user) {
                this.loginUserWithSocialNet({ social_id: user.id });
            }
        });
    }
    signInWithGoogle() {
        this.socialLogin.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_5__["GoogleLoginProvider"].PROVIDER_ID);
        this.socialLogin.authState
            .subscribe(user => {
            console.log(user);
            if (user) {
                this.loginUserWithSocialNet({ social_id: user.id });
            }
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: angularx_social_login__WEBPACK_IMPORTED_MODULE_5__["AuthService"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/auth/login/login.component.html"),
        styles: [__webpack_require__(/*! ../auth.component.scss */ "./src/app/auth/auth.component.scss")]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/auth/registration/registration.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/auth/registration/registration.component.ts ***!
  \*************************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angularx-social-login */ "./node_modules/angularx-social-login/angularx-social-login.js");







let RegistrationComponent = class RegistrationComponent {
    constructor(fb, authService, router, socialAuth) {
        this.fb = fb;
        this.authService = authService;
        this.router = router;
        this.socialAuth = socialAuth;
        this.isChecked = false;
    }
    ngOnInit() {
        this.register = this.fb.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password_confirmation: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    registration() {
        if (this.checkForm() && this.isChecked) {
            this.authService.register(this.register.value)
                .subscribe(res => {
                if (res['success']) {
                    localStorage.setItem('token', res['token']);
                    localStorage.setItem('user', JSON.stringify(res['data']['name']));
                    this.router.navigate(['/main/profile']);
                }
            });
        }
    }
    checkForm() {
        for (let prop in this.register.value) {
            if (this.register.value.hasOwnProperty(prop)) {
                if (this.register.value[prop] === '') {
                    alert('Please, fill in the fields');
                    return false;
                }
            }
        }
        return true;
    }
    defVoid(data) {
        this.isChecked = data ? true : false;
    }
    registrationWithFB() {
        this.socialAuth.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_5__["FacebookLoginProvider"].PROVIDER_ID);
        this.socialAuth.authState
            .subscribe(user => {
            console.log(user);
            if (user) {
                let fbUserData = {
                    social_id: user['id'],
                    name: user['firstName'],
                    last_name: user['lastName'],
                    email: user['email']
                };
                this.registerWithSocialNet(fbUserData);
            }
        });
    }
    registerWithSocialNet(data) {
        this.authService.socialRegister(data)
            .subscribe(res => {
            if (res['success']) {
                localStorage.setItem('token', res['token']);
                localStorage.setItem('user', res['data']['name']);
                this.router.navigate(['/main/profile']);
            }
        }, err => {
            console.log(err);
        });
    }
    registrationWithGoogle() {
        this.socialAuth.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_5__["GoogleLoginProvider"].PROVIDER_ID);
        this.socialAuth.authState
            .subscribe(user => {
            console.log(user);
            if (user) {
                let fbUserData = {
                    social_id: user['id'],
                    name: user['firstName'],
                    last_name: user['lastName'],
                    email: user['email']
                };
                this.registerWithSocialNet(fbUserData);
            }
        });
    }
};
RegistrationComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: angularx_social_login__WEBPACK_IMPORTED_MODULE_5__["AuthService"] }
];
RegistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-registration',
        template: __webpack_require__(/*! raw-loader!./registration.component.html */ "./node_modules/raw-loader/index.js!./src/app/auth/registration/registration.component.html"),
        styles: [__webpack_require__(/*! ../auth.component.scss */ "./src/app/auth/auth.component.scss")]
    })
], RegistrationComponent);



/***/ }),

/***/ "./src/app/main/about/about.component.scss":
/*!*************************************************!*\
  !*** ./src/app/main/about/about.component.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".about-info {\n  display: -webkit-box;\n  display: flex;\n  flex-wrap: wrap;\n  padding-left: 2.083vw;\n}\n.about-info__item {\n  font-size: 0.99vw;\n  font-weight: 400;\n  line-height: 1.667vw;\n  width: 50%;\n  padding-top: 1.979vw;\n  text-align: justify;\n  padding-bottom: 2.969vw;\n}\n.about-info__item:first-child {\n  padding-right: 2.344vw;\n}\n.about-info__item:last-child {\n  padding-left: 2.344vw;\n}\n.about__title {\n  width: 100%;\n  padding-top: 3.073vw;\n}\n.about__title h2 {\n  font-size: 1.354vw;\n  font-weight: 700;\n  text-align: center;\n  width: 50%;\n  margin: 0;\n}\n@media screen and (max-width: 1200px) {\n  .about-info__item {\n    font-size: 18px;\n    line-height: normal;\n  }\n\n  .about__title h2 {\n    font-size: 24px;\n  }\n}\n@media screen and (max-width: 992px) {\n  .about-info {\n    padding-left: 0;\n  }\n}\n@media screen and (max-width: 767px) {\n  .container {\n    padding: 15px;\n  }\n\n  .about-info {\n    padding-bottom: 50px;\n  }\n  .about-info .about-info__item {\n    padding: 0 !important;\n    width: 100%;\n  }\n  .about-info .about-info__item h2 {\n    width: 100%;\n    text-align: left;\n  }\n  .about-info .about-info__item p {\n    margin: 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9hYm91dC9DOlxcVXNlcnNcXFZsYWREXFxEZXNrdG9wXFxNZW1vIChhbGwpXFxtZW1vL3NyY1xcYXBwXFxtYWluXFxhYm91dFxcYWJvdXQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21haW4vYWJvdXQvYWJvdXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFHRSxvQkFBQTtFQUFBLGFBQUE7RUFHQSxlQUFBO0VBQ0EscUJBQUE7QUNDRjtBREFFO0VBQ0UsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsVUFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ0VKO0FEREk7RUFDRSxzQkFBQTtBQ0dOO0FEREk7RUFDRSxxQkFBQTtBQ0dOO0FERUE7RUFDRSxXQUFBO0VBQ0Esb0JBQUE7QUNDRjtBREFFO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7QUNFSjtBREVBO0VBQ0U7SUFDRSxlQUFBO0lBQ0EsbUJBQUE7RUNDRjs7RURDQTtJQUNFLGVBQUE7RUNFRjtBQUNGO0FEQ0E7RUFDRTtJQUNFLGVBQUE7RUNDRjtBQUNGO0FERUE7RUFDRTtJQUNFLGFBQUE7RUNBRjs7RURHQTtJQUNFLG9CQUFBO0VDQUY7RURDRTtJQUNFLHFCQUFBO0lBQ0EsV0FBQTtFQ0NKO0VEQUk7SUFDRSxXQUFBO0lBQ0EsZ0JBQUE7RUNFTjtFREFJO0lBQ0UsU0FBQTtFQ0VOO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9tYWluL2Fib3V0L2Fib3V0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFib3V0LWluZm8ge1xyXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xyXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG4gIHBhZGRpbmctbGVmdDogMi4wODN2dztcclxuICAmX19pdGVtIHtcclxuICAgIGZvbnQtc2l6ZTogMC45OTB2dztcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBsaW5lLWhlaWdodDogMS42Njd2dztcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBwYWRkaW5nLXRvcDogMS45Nzl2dztcclxuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMi45Njl2dztcclxuICAgICY6Zmlyc3QtY2hpbGQge1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAyLjM0NHZ3O1xyXG4gICAgfVxyXG4gICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgcGFkZGluZy1sZWZ0OiAyLjM0NHZ3O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmFib3V0X190aXRsZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcGFkZGluZy10b3A6IDMuMDczdnc7XHJcbiAgaDIge1xyXG4gICAgZm9udC1zaXplOiAxLjM1NHZ3O1xyXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcclxuICAuYWJvdXQtaW5mb19faXRlbSB7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gIH1cclxuICAuYWJvdXRfX3RpdGxlIGgyIHtcclxuICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XHJcbiAgLmFib3V0LWluZm8ge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcclxuICAuY29udGFpbmVye1xyXG4gICAgcGFkZGluZzogMTVweDtcclxuICB9XHJcblxyXG4gIC5hYm91dC1pbmZvIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiA1MHB4O1xyXG4gICAgLmFib3V0LWluZm9fX2l0ZW0ge1xyXG4gICAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBoMiB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgfVxyXG4gICAgICBwIHtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiLmFib3V0LWluZm8ge1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWZsZXgtd3JhcDogd3JhcDtcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBwYWRkaW5nLWxlZnQ6IDIuMDgzdnc7XG59XG4uYWJvdXQtaW5mb19faXRlbSB7XG4gIGZvbnQtc2l6ZTogMC45OXZ3O1xuICBmb250LXdlaWdodDogNDAwO1xuICBsaW5lLWhlaWdodDogMS42Njd2dztcbiAgd2lkdGg6IDUwJTtcbiAgcGFkZGluZy10b3A6IDEuOTc5dnc7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gIHBhZGRpbmctYm90dG9tOiAyLjk2OXZ3O1xufVxuLmFib3V0LWluZm9fX2l0ZW06Zmlyc3QtY2hpbGQge1xuICBwYWRkaW5nLXJpZ2h0OiAyLjM0NHZ3O1xufVxuLmFib3V0LWluZm9fX2l0ZW06bGFzdC1jaGlsZCB7XG4gIHBhZGRpbmctbGVmdDogMi4zNDR2dztcbn1cblxuLmFib3V0X190aXRsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXRvcDogMy4wNzN2dztcbn1cbi5hYm91dF9fdGl0bGUgaDIge1xuICBmb250LXNpemU6IDEuMzU0dnc7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDUwJTtcbiAgbWFyZ2luOiAwO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcbiAgLmFib3V0LWluZm9fX2l0ZW0ge1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICB9XG5cbiAgLmFib3V0X190aXRsZSBoMiB7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xuICAuYWJvdXQtaW5mbyB7XG4gICAgcGFkZGluZy1sZWZ0OiAwO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAuY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAxNXB4O1xuICB9XG5cbiAgLmFib3V0LWluZm8ge1xuICAgIHBhZGRpbmctYm90dG9tOiA1MHB4O1xuICB9XG4gIC5hYm91dC1pbmZvIC5hYm91dC1pbmZvX19pdGVtIHtcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgLmFib3V0LWluZm8gLmFib3V0LWluZm9fX2l0ZW0gaDIge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgLmFib3V0LWluZm8gLmFib3V0LWluZm9fX2l0ZW0gcCB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/main/about/about.component.ts":
/*!***********************************************!*\
  !*** ./src/app/main/about/about.component.ts ***!
  \***********************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _shared_services_static_texts_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/static-texts.service */ "./src/app/shared/services/static-texts.service.ts");



let AboutComponent = class AboutComponent {
    constructor(staticTextService) {
        this.staticTextService = staticTextService;
        this.about = {};
    }
    ngOnInit() {
        this.getAbout();
    }
    getAbout() {
        this.staticTextService.getAbout()
            .subscribe(res => {
            this.about = res['data'];
        });
    }
};
AboutComponent.ctorParameters = () => [
    { type: _shared_services_static_texts_service__WEBPACK_IMPORTED_MODULE_2__["StaticTextsService"] }
];
AboutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-about',
        template: __webpack_require__(/*! raw-loader!./about.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/about/about.component.html"),
        styles: [__webpack_require__(/*! ./about.component.scss */ "./src/app/main/about/about.component.scss")]
    })
], AboutComponent);



/***/ }),

/***/ "./src/app/main/contact-us/contact-us.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/main/contact-us/contact-us.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contact-us__wrap {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  padding: 0.521vw 0.573vw 3.229vw 1.458vw;\n}\n.contact-us__wrap .contact-us__form {\n  width: 55%;\n}\n.contact-us__wrap .contact-us__form input {\n  width: 100%;\n  font-size: 0.938vw;\n  line-height: 2.083vw;\n  padding: 0 2.344vw;\n  background-position: 0.781vw center !important;\n}\n.contact-us__wrap .contact-us__form textarea {\n  width: 100%;\n  resize: none;\n  font-size: 0.938vw;\n  line-height: 2.083vw;\n  padding: 0 2.344vw;\n  height: 12.292vw;\n  background-position: 0.781vw 0.677vw !important;\n}\n.contact-us__wrap .contact-us__info {\n  width: 42%;\n  padding-top: 2.604vw;\n  padding-left: 1.042vw;\n}\n.contact-us__wrap .contact-us__info .title {\n  font-weight: 700;\n  text-align: center;\n  margin: 0;\n  font-size: 1.25vw;\n  letter-spacing: 0.052vw;\n}\n.contact-us__wrap .contact-us__info p {\n  text-align: justify;\n  font-size: 0.885vw;\n  margin-top: 3.333vw;\n}\n.contact-us__wrap .btn-wrap {\n  text-align: right;\n  padding-top: 2.135vw;\n}\n.contact-us__wrap .input-wrap {\n  margin-bottom: 1.25vw;\n}\n#contact-us-name {\n  background: url('gemicon_name.png') no-repeat #fff;\n}\n#contact-us-email {\n  background: url('gemicon_email.png') no-repeat #fff;\n}\n#contact-us-message {\n  background: url('gemicon_message.png') no-repeat #fff;\n}\n@media screen and (max-width: 1200px) {\n  .contact-us__wrap .container {\n    width: 824px;\n  }\n  .contact-us__wrap .contact-us__form input {\n    font-size: 14px;\n    padding: 0 28px;\n  }\n  .contact-us__wrap .contact-us__form textarea {\n    font-size: 14px;\n    padding: 5px 28px 0 28px;\n  }\n  .contact-us__wrap .contact-us__info {\n    padding-top: 0;\n  }\n  .contact-us__wrap .contact-us__info .title {\n    font-size: 18px;\n  }\n  .contact-us__wrap .contact-us__info p {\n    font-size: 14px;\n    margin-top: 10px;\n  }\n}\n@media screen and (max-width: 1200px) and (max-width: 992px) {\n  .contact-us__wrap.container {\n    width: 95%;\n    flex-wrap: wrap;\n  }\n  .contact-us__wrap.container .contact-us__form input {\n    font-size: 16px;\n  }\n  .contact-us__wrap.container .contact-us__form textarea {\n    font-size: 16px;\n    height: 166px;\n  }\n  .contact-us__wrap.container .contact-us__info .title {\n    font-size: 20px;\n  }\n  .contact-us__wrap.container .contact-us__info p {\n    font-size: 16px;\n  }\n}\n@media screen and (max-width: 1200px) and (max-width: 767px) {\n  .contact-us__wrap .contact-us__form {\n    width: 100%;\n    padding: 0;\n  }\n  .contact-us__wrap .contact-us__form input {\n    padding: 0 35px;\n    background-position: 10px center !important;\n  }\n  .contact-us__wrap .contact-us__form textarea {\n    padding: 10px 35px 0 35px;\n    background-position: 10px 10px !important;\n  }\n  .contact-us__wrap .contact-us__info {\n    width: 100%;\n    padding: 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9jb250YWN0LXVzL0M6XFxVc2Vyc1xcVmxhZERcXERlc2t0b3BcXE1lbW8gKGFsbClcXG1lbW8vc3JjXFxhcHBcXG1haW5cXGNvbnRhY3QtdXNcXGNvbnRhY3QtdXMuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21haW4vY29udGFjdC11cy9jb250YWN0LXVzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBR0Usb0JBQUE7RUFBQSxhQUFBO0VBR0EseUJBQUE7VUFBQSw4QkFBQTtFQUNBLHdDQUFBO0FDQ0Y7QURBRTtFQUNFLFVBQUE7QUNFSjtBREFFO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLDhDQUFBO0FDRUo7QURBRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwrQ0FBQTtBQ0VKO0FEQUU7RUFDRSxVQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtBQ0VKO0FEREk7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7QUNHTjtBRERJO0VBQ0UsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDR047QURDRTtFQUNFLGlCQUFBO0VBQ0Esb0JBQUE7QUNDSjtBRENFO0VBQ0UscUJBQUE7QUNDSjtBREdBO0VBQ0Usa0RBQUE7QUNBRjtBREdBO0VBQ0UsbURBQUE7QUNBRjtBREdBO0VBQ0UscURBQUE7QUNBRjtBREdBO0VBRUk7SUFDRSxZQUFBO0VDREo7RURHRTtJQUNFLGVBQUE7SUFDQSxlQUFBO0VDREo7RURHRTtJQUNFLGVBQUE7SUFDQSx3QkFBQTtFQ0RKO0VESUU7SUFDRSxjQUFBO0VDRko7RURHSTtJQUNFLGVBQUE7RUNETjtFREdJO0lBQ0UsZUFBQTtJQUNBLGdCQUFBO0VDRE47QUFDRjtBREtFO0VBQ0U7SUFDRSxVQUFBO0lBR0EsZUFBQTtFQ0hKO0VESUk7SUFDRSxlQUFBO0VDRk47RURJSTtJQUNFLGVBQUE7SUFDQSxhQUFBO0VDRk47RURLTTtJQUNFLGVBQUE7RUNIUjtFREtNO0lBQ0UsZUFBQTtFQ0hSO0FBQ0Y7QURRRTtFQUVJO0lBQ0UsV0FBQTtJQUNBLFVBQUE7RUNQTjtFRFFNO0lBQ0UsZUFBQTtJQUNBLDJDQUFBO0VDTlI7RURRTTtJQUNFLHlCQUFBO0lBQ0EseUNBQUE7RUNOUjtFRFNJO0lBQ0UsV0FBQTtJQUNBLFVBQUE7RUNQTjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbWFpbi9jb250YWN0LXVzL2NvbnRhY3QtdXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFjdC11c19fd3JhcCB7XHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgcGFkZGluZzogMC41MjF2dyAwLjU3M3Z3IDMuMjI5dncgMS40NTh2dztcclxuICAuY29udGFjdC11c19fZm9ybSB7XHJcbiAgICB3aWR0aDogNTUlO1xyXG4gIH1cclxuICAuY29udGFjdC11c19fZm9ybSBpbnB1dCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGZvbnQtc2l6ZTogMC45Mzh2dztcclxuICAgIGxpbmUtaGVpZ2h0OiAyLjA4M3Z3O1xyXG4gICAgcGFkZGluZzogMCAyLjM0NHZ3O1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMC43ODF2dyBjZW50ZXIgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmNvbnRhY3QtdXNfX2Zvcm0gdGV4dGFyZWEge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICByZXNpemU6IG5vbmU7XHJcbiAgICBmb250LXNpemU6IDAuOTM4dnc7XHJcbiAgICBsaW5lLWhlaWdodDogMi4wODN2dztcclxuICAgIHBhZGRpbmc6IDAgMi4zNDR2dztcclxuICAgIGhlaWdodDogMTIuMjkydnc7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwLjc4MXZ3IDAuNjc3dncgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmNvbnRhY3QtdXNfX2luZm8ge1xyXG4gICAgd2lkdGg6IDQyJTtcclxuICAgIHBhZGRpbmctdG9wOiAyLjYwNHZ3O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxLjA0MnZ3O1xyXG4gICAgLnRpdGxlIHtcclxuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBtYXJnaW46IDA7XHJcbiAgICAgIGZvbnQtc2l6ZTogMS4yNTB2dztcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMDUydnc7XHJcbiAgICB9XHJcbiAgICBwIHtcclxuICAgICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxuICAgICAgZm9udC1zaXplOiAwLjg4NXZ3O1xyXG4gICAgICBtYXJnaW4tdG9wOiAzLjMzM3Z3O1xyXG4gICAgfVxyXG5cclxuICB9XHJcbiAgLmJ0bi13cmFwIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgcGFkZGluZy10b3A6IDIuMTM1dnc7XHJcbiAgfVxyXG4gIC5pbnB1dC13cmFwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEuMjUwdnc7XHJcbiAgfVxyXG59XHJcblxyXG4jY29udGFjdC11cy1uYW1lIHtcclxuICBiYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvaW1nL2ljb24vZ2VtaWNvbl9uYW1lLnBuZ1wiKSBuby1yZXBlYXQgI2ZmZjtcclxufVxyXG5cclxuI2NvbnRhY3QtdXMtZW1haWwge1xyXG4gIGJhY2tncm91bmQ6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvaWNvbi9nZW1pY29uX2VtYWlsLnBuZ1wiKSBuby1yZXBlYXQgI2ZmZjtcclxufVxyXG5cclxuI2NvbnRhY3QtdXMtbWVzc2FnZSB7XHJcbiAgYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy9pY29uL2dlbWljb25fbWVzc2FnZS5wbmdcIikgbm8tcmVwZWF0ICNmZmY7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xyXG4gIC5jb250YWN0LXVzX193cmFwIHtcclxuICAgIC5jb250YWluZXIge1xyXG4gICAgICB3aWR0aDogODI0cHg7XHJcbiAgICB9XHJcbiAgICAuY29udGFjdC11c19fZm9ybSBpbnB1dCB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgcGFkZGluZzogMCAyOHB4O1xyXG4gICAgfVxyXG4gICAgLmNvbnRhY3QtdXNfX2Zvcm0gdGV4dGFyZWEge1xyXG4gICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIHBhZGRpbmc6IDVweCAyOHB4IDAgMjhweDtcclxuXHJcbiAgICB9XHJcbiAgICAuY29udGFjdC11c19faW5mbyB7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAwO1xyXG4gICAgICAudGl0bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgfVxyXG4gICAgICBwIHtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAgIC5jb250YWN0LXVzX193cmFwLmNvbnRhaW5lciB7XHJcbiAgICAgIHdpZHRoOiA5NSU7XHJcbiAgICAgIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xyXG4gICAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xyXG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgIC5jb250YWN0LXVzX19mb3JtIGlucHV0IHtcclxuICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgIH1cclxuICAgICAgLmNvbnRhY3QtdXNfX2Zvcm0gdGV4dGFyZWEge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICBoZWlnaHQ6IDE2NnB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5jb250YWN0LXVzX19pbmZvIHtcclxuICAgICAgICAudGl0bGUge1xyXG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBwIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XHJcbiAgICAuY29udGFjdC11c19fd3JhcHtcclxuICAgICAgLmNvbnRhY3QtdXNfX2Zvcm17XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICBpbnB1dHtcclxuICAgICAgICAgIHBhZGRpbmc6IDAgMzVweDtcclxuICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDEwcHggY2VudGVyICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRleHRhcmVhIHtcclxuICAgICAgICAgIHBhZGRpbmc6IDEwcHggMzVweCAwIDM1cHg7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAxMHB4IDEwcHggIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLmNvbnRhY3QtdXNfX2luZm8ge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiLmNvbnRhY3QtdXNfX3dyYXAge1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nOiAwLjUyMXZ3IDAuNTczdncgMy4yMjl2dyAxLjQ1OHZ3O1xufVxuLmNvbnRhY3QtdXNfX3dyYXAgLmNvbnRhY3QtdXNfX2Zvcm0ge1xuICB3aWR0aDogNTUlO1xufVxuLmNvbnRhY3QtdXNfX3dyYXAgLmNvbnRhY3QtdXNfX2Zvcm0gaW5wdXQge1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAwLjkzOHZ3O1xuICBsaW5lLWhlaWdodDogMi4wODN2dztcbiAgcGFkZGluZzogMCAyLjM0NHZ3O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwLjc4MXZ3IGNlbnRlciAhaW1wb3J0YW50O1xufVxuLmNvbnRhY3QtdXNfX3dyYXAgLmNvbnRhY3QtdXNfX2Zvcm0gdGV4dGFyZWEge1xuICB3aWR0aDogMTAwJTtcbiAgcmVzaXplOiBub25lO1xuICBmb250LXNpemU6IDAuOTM4dnc7XG4gIGxpbmUtaGVpZ2h0OiAyLjA4M3Z3O1xuICBwYWRkaW5nOiAwIDIuMzQ0dnc7XG4gIGhlaWdodDogMTIuMjkydnc7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IDAuNzgxdncgMC42Nzd2dyAhaW1wb3J0YW50O1xufVxuLmNvbnRhY3QtdXNfX3dyYXAgLmNvbnRhY3QtdXNfX2luZm8ge1xuICB3aWR0aDogNDIlO1xuICBwYWRkaW5nLXRvcDogMi42MDR2dztcbiAgcGFkZGluZy1sZWZ0OiAxLjA0MnZ3O1xufVxuLmNvbnRhY3QtdXNfX3dyYXAgLmNvbnRhY3QtdXNfX2luZm8gLnRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDA7XG4gIGZvbnQtc2l6ZTogMS4yNXZ3O1xuICBsZXR0ZXItc3BhY2luZzogMC4wNTJ2dztcbn1cbi5jb250YWN0LXVzX193cmFwIC5jb250YWN0LXVzX19pbmZvIHAge1xuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICBmb250LXNpemU6IDAuODg1dnc7XG4gIG1hcmdpbi10b3A6IDMuMzMzdnc7XG59XG4uY29udGFjdC11c19fd3JhcCAuYnRuLXdyYXAge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgcGFkZGluZy10b3A6IDIuMTM1dnc7XG59XG4uY29udGFjdC11c19fd3JhcCAuaW5wdXQtd3JhcCB7XG4gIG1hcmdpbi1ib3R0b206IDEuMjV2dztcbn1cblxuI2NvbnRhY3QtdXMtbmFtZSB7XG4gIGJhY2tncm91bmQ6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvaWNvbi9nZW1pY29uX25hbWUucG5nXCIpIG5vLXJlcGVhdCAjZmZmO1xufVxuXG4jY29udGFjdC11cy1lbWFpbCB7XG4gIGJhY2tncm91bmQ6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvaWNvbi9nZW1pY29uX2VtYWlsLnBuZ1wiKSBuby1yZXBlYXQgI2ZmZjtcbn1cblxuI2NvbnRhY3QtdXMtbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvaWNvbi9nZW1pY29uX21lc3NhZ2UucG5nXCIpIG5vLXJlcGVhdCAjZmZmO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcbiAgLmNvbnRhY3QtdXNfX3dyYXAgLmNvbnRhaW5lciB7XG4gICAgd2lkdGg6IDgyNHB4O1xuICB9XG4gIC5jb250YWN0LXVzX193cmFwIC5jb250YWN0LXVzX19mb3JtIGlucHV0IHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgcGFkZGluZzogMCAyOHB4O1xuICB9XG4gIC5jb250YWN0LXVzX193cmFwIC5jb250YWN0LXVzX19mb3JtIHRleHRhcmVhIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgcGFkZGluZzogNXB4IDI4cHggMCAyOHB4O1xuICB9XG4gIC5jb250YWN0LXVzX193cmFwIC5jb250YWN0LXVzX19pbmZvIHtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgfVxuICAuY29udGFjdC11c19fd3JhcCAuY29udGFjdC11c19faW5mbyAudGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgfVxuICAuY29udGFjdC11c19fd3JhcCAuY29udGFjdC11c19faW5mbyBwIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLmNvbnRhY3QtdXNfX3dyYXAuY29udGFpbmVyIHtcbiAgICB3aWR0aDogOTUlO1xuICAgIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xuICAgIC1tcy1mbGV4LXdyYXA6IHdyYXA7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9XG4gIC5jb250YWN0LXVzX193cmFwLmNvbnRhaW5lciAuY29udGFjdC11c19fZm9ybSBpbnB1dCB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG4gIC5jb250YWN0LXVzX193cmFwLmNvbnRhaW5lciAuY29udGFjdC11c19fZm9ybSB0ZXh0YXJlYSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGhlaWdodDogMTY2cHg7XG4gIH1cbiAgLmNvbnRhY3QtdXNfX3dyYXAuY29udGFpbmVyIC5jb250YWN0LXVzX19pbmZvIC50aXRsZSB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICB9XG4gIC5jb250YWN0LXVzX193cmFwLmNvbnRhaW5lciAuY29udGFjdC11c19faW5mbyBwIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkgYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gIC5jb250YWN0LXVzX193cmFwIC5jb250YWN0LXVzX19mb3JtIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG4gIC5jb250YWN0LXVzX193cmFwIC5jb250YWN0LXVzX19mb3JtIGlucHV0IHtcbiAgICBwYWRkaW5nOiAwIDM1cHg7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMTBweCBjZW50ZXIgIWltcG9ydGFudDtcbiAgfVxuICAuY29udGFjdC11c19fd3JhcCAuY29udGFjdC11c19fZm9ybSB0ZXh0YXJlYSB7XG4gICAgcGFkZGluZzogMTBweCAzNXB4IDAgMzVweDtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAxMHB4IDEwcHggIWltcG9ydGFudDtcbiAgfVxuICAuY29udGFjdC11c19fd3JhcCAuY29udGFjdC11c19faW5mbyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMDtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/main/contact-us/contact-us.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/main/contact-us/contact-us.component.ts ***!
  \*********************************************************/
/*! exports provided: ContactUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsComponent", function() { return ContactUsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _shared_services_contact_us_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/contact-us.service */ "./src/app/shared/services/contact-us.service.ts");
/* harmony import */ var _shared_services_static_texts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/static-texts.service */ "./src/app/shared/services/static-texts.service.ts");





let ContactUsComponent = class ContactUsComponent {
    constructor(fb, contactService, staticTextService) {
        this.fb = fb;
        this.contactService = contactService;
        this.staticTextService = staticTextService;
        this.contactUsText = {};
    }
    ngOnInit() {
        this.contactUs = this.fb.group({
            name: [''],
            email: [''],
            message: ['']
        });
        this.getContactUs();
    }
    checkToken() {
        if (localStorage.getItem('token') != null) {
            return true;
        }
        else {
            alert('You must be logged in to send a message');
            return false;
        }
    }
    sendMessage() {
        if (this.checkToken()) {
            if (this.checkFormValue()) {
                this.contactService.send(this.contactUs.value)
                    .subscribe(res => {
                    console.log(res);
                    if (res['success']) {
                        this.clearForms();
                    }
                }, err => {
                    alert(err['error']['message']);
                });
            }
        }
    }
    checkFormValue() {
        for (let prop in this.contactUs.value) {
            if (this.contactUs.value.hasOwnProperty) {
                if (this.contactUs.value[prop] === '') {
                    alert('Please, fill in the fields');
                    return false;
                }
            }
        }
        return true;
    }
    clearForms() {
        this.contactUs.patchValue({
            name: '',
            email: '',
            message: ''
        });
    }
    getContactUs() {
        this.staticTextService.getContactUs()
            .subscribe(res => {
            this.contactUsText = res['data'];
        });
    }
};
ContactUsComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _shared_services_contact_us_service__WEBPACK_IMPORTED_MODULE_3__["ContactUsService"] },
    { type: _shared_services_static_texts_service__WEBPACK_IMPORTED_MODULE_4__["StaticTextsService"] }
];
ContactUsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contact-us',
        template: __webpack_require__(/*! raw-loader!./contact-us.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/contact-us/contact-us.component.html"),
        styles: [__webpack_require__(/*! ./contact-us.component.scss */ "./src/app/main/contact-us/contact-us.component.scss")]
    })
], ContactUsComponent);



/***/ }),

/***/ "./src/app/main/faq/faq.component.scss":
/*!*********************************************!*\
  !*** ./src/app/main/faq/faq.component.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".faq-wrap {\n  padding: 60px 0 49px 122px;\n}\n.faq-wrap__item {\n  margin-bottom: 55px;\n}\n.faq-wrap__item h4 {\n  font-size: 26px;\n  font-weight: 700;\n  position: relative;\n  margin: 0 0 42px;\n}\n.faq-wrap__item h4:before {\n  content: \"\";\n  width: 53px;\n  height: 53px;\n  background: url('faq-marker.png');\n  background-size: 100% 100%;\n  display: block;\n  position: absolute;\n  left: -88px;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n}\n.faq-wrap__item h4:after {\n  content: \"\";\n  width: 25px;\n  height: 25px;\n  background: url('quality2.png');\n  background-size: 100% 100%;\n  display: block;\n  position: absolute;\n  left: -74px;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n}\n.faq-wrap__item p {\n  font-size: 18px;\n  font-weight: 400;\n  margin: 0;\n  line-height: 1.5;\n}\n.faq-wrap__item.active h4:after {\n  left: -95px;\n}\n@media screen and (max-width: 767px) {\n  .faq-wrap {\n    padding: 60px 5px 49px 55px;\n  }\n  .faq-wrap__item {\n    font-size: 16px;\n  }\n  .faq-wrap__item h4 {\n    font-size: 23px;\n  }\n  .faq-wrap__item h4:before {\n    width: 35px;\n    height: 35px;\n    left: -43px;\n  }\n  .faq-wrap__item h4:after {\n    left: -38px;\n  }\n\n  .faq-wrap__item.active h4:after {\n    left: -50px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9mYXEvQzpcXFVzZXJzXFxWbGFkRFxcRGVza3RvcFxcTWVtbyAoYWxsKVxcbWVtby9zcmNcXGFwcFxcbWFpblxcZmFxXFxmYXEuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21haW4vZmFxL2ZhcS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDBCQUFBO0FDQ0Y7QURBRTtFQUNFLG1CQUFBO0FDRUo7QURESTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNHTjtBREZNO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUNBQUE7RUFDQSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsbUNBQUE7VUFBQSwyQkFBQTtBQ0lSO0FERk07RUFDRSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSwrQkFBQTtFQUNBLDBCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSxtQ0FBQTtVQUFBLDJCQUFBO0FDSVI7QURESTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtBQ0dOO0FERUE7RUFDRSxXQUFBO0FDQ0Y7QURFQTtFQUNFO0lBQ0UsMkJBQUE7RUNDRjtFREFFO0lBQ0UsZUFBQTtFQ0VKO0VEREk7SUFDRSxlQUFBO0VDR047RURGTTtJQUNFLFdBQUE7SUFDQSxZQUFBO0lBQ0EsV0FBQTtFQ0lSO0VERk07SUFDRSxXQUFBO0VDSVI7O0VER0E7SUFDRSxXQUFBO0VDQUY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL21haW4vZmFxL2ZhcS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mYXEtd3JhcCB7XHJcbiAgcGFkZGluZzogNjBweCAwIDQ5cHggMTIycHg7XHJcbiAgJl9faXRlbSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1NXB4O1xyXG4gICAgaDQge1xyXG4gICAgICBmb250LXNpemU6IDI2cHg7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgbWFyZ2luOiAwIDAgNDJweDtcclxuICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgIGNvbnRlbnQ6ICcnO1xyXG4gICAgICAgIHdpZHRoOiA1M3B4O1xyXG4gICAgICAgIGhlaWdodDogNTNweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZy9pY29uL2ZhcS1tYXJrZXIucG5nKTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgbGVmdDogLTg4cHg7XHJcbiAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpXHJcbiAgICAgIH1cclxuICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgd2lkdGg6IDI1cHg7XHJcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1nL2ljb24vcXVhbGl0eTIucG5nKTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgbGVmdDogLTc0cHg7XHJcbiAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpXHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHAge1xyXG4gICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICAgIG1hcmdpbjogMDtcclxuICAgICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5mYXEtd3JhcF9faXRlbS5hY3RpdmUgaDQ6YWZ0ZXIge1xyXG4gIGxlZnQ6IC05NXB4O1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gIC5mYXEtd3JhcCB7XHJcbiAgICBwYWRkaW5nOiA2MHB4IDVweCA0OXB4IDU1cHg7XHJcbiAgICAmX19pdGVtIHtcclxuICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICBoNCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAyM3B4O1xyXG4gICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgIHdpZHRoOiAzNXB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgICAgbGVmdDogLTQzcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICY6YWZ0ZXIge1xyXG4gICAgICAgICAgbGVmdDogLTM4cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmZhcS13cmFwX19pdGVtLmFjdGl2ZSBoNDphZnRlciB7XHJcbiAgICBsZWZ0OiAtNTBweDtcclxuICB9XHJcbn1cclxuIiwiLmZhcS13cmFwIHtcbiAgcGFkZGluZzogNjBweCAwIDQ5cHggMTIycHg7XG59XG4uZmFxLXdyYXBfX2l0ZW0ge1xuICBtYXJnaW4tYm90dG9tOiA1NXB4O1xufVxuLmZhcS13cmFwX19pdGVtIGg0IHtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBmb250LXdlaWdodDogNzAwO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbjogMCAwIDQycHg7XG59XG4uZmFxLXdyYXBfX2l0ZW0gaDQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgd2lkdGg6IDUzcHg7XG4gIGhlaWdodDogNTNweDtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWcvaWNvbi9mYXEtbWFya2VyLnBuZyk7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAtODhweDtcbiAgdG9wOiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbn1cbi5mYXEtd3JhcF9faXRlbSBoNDphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHdpZHRoOiAyNXB4O1xuICBoZWlnaHQ6IDI1cHg7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1nL2ljb24vcXVhbGl0eTIucG5nKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IC03NHB4O1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuLmZhcS13cmFwX19pdGVtIHAge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbn1cblxuLmZhcS13cmFwX19pdGVtLmFjdGl2ZSBoNDphZnRlciB7XG4gIGxlZnQ6IC05NXB4O1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAuZmFxLXdyYXAge1xuICAgIHBhZGRpbmc6IDYwcHggNXB4IDQ5cHggNTVweDtcbiAgfVxuICAuZmFxLXdyYXBfX2l0ZW0ge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxuICAuZmFxLXdyYXBfX2l0ZW0gaDQge1xuICAgIGZvbnQtc2l6ZTogMjNweDtcbiAgfVxuICAuZmFxLXdyYXBfX2l0ZW0gaDQ6YmVmb3JlIHtcbiAgICB3aWR0aDogMzVweDtcbiAgICBoZWlnaHQ6IDM1cHg7XG4gICAgbGVmdDogLTQzcHg7XG4gIH1cbiAgLmZhcS13cmFwX19pdGVtIGg0OmFmdGVyIHtcbiAgICBsZWZ0OiAtMzhweDtcbiAgfVxuXG4gIC5mYXEtd3JhcF9faXRlbS5hY3RpdmUgaDQ6YWZ0ZXIge1xuICAgIGxlZnQ6IC01MHB4O1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/main/faq/faq.component.ts":
/*!*******************************************!*\
  !*** ./src/app/main/faq/faq.component.ts ***!
  \*******************************************/
/*! exports provided: FaqComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaqComponent", function() { return FaqComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _shared_services_static_texts_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/static-texts.service */ "./src/app/shared/services/static-texts.service.ts");



let FaqComponent = class FaqComponent {
    constructor(staticTextService) {
        this.staticTextService = staticTextService;
        this.faqText = {};
    }
    ngOnInit() {
        this.getFAQ();
    }
    getFAQ() {
        this.staticTextService.getFAQ()
            .subscribe(res => {
            this.faqText = res['data'];
        });
    }
};
FaqComponent.ctorParameters = () => [
    { type: _shared_services_static_texts_service__WEBPACK_IMPORTED_MODULE_2__["StaticTextsService"] }
];
FaqComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-faq',
        template: __webpack_require__(/*! raw-loader!./faq.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/faq/faq.component.html"),
        styles: [__webpack_require__(/*! ./faq.component.scss */ "./src/app/main/faq/faq.component.scss")]
    })
], FaqComponent);



/***/ }),

/***/ "./src/app/main/finished-projects/finished-projects.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/main/finished-projects/finished-projects.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".gallery__wrap:last-child {\n  margin-bottom: 4.688vw;\n}\n\n.gallery__wrap {\n  margin-bottom: 0.26vw;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9maW5pc2hlZC1wcm9qZWN0cy9DOlxcVXNlcnNcXFZsYWREXFxEZXNrdG9wXFxNZW1vIChhbGwpXFxtZW1vL3NyY1xcYXBwXFxtYWluXFxmaW5pc2hlZC1wcm9qZWN0c1xcZmluaXNoZWQtcHJvamVjdHMuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21haW4vZmluaXNoZWQtcHJvamVjdHMvZmluaXNoZWQtcHJvamVjdHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxzQkFBQTtBQ0NGOztBREVBO0VBQ0UscUJBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL21haW4vZmluaXNoZWQtcHJvamVjdHMvZmluaXNoZWQtcHJvamVjdHMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZ2FsbGVyeV9fd3JhcDpsYXN0LWNoaWxkIHtcclxuICBtYXJnaW4tYm90dG9tOiA0LjY4OHZ3O1xyXG59XHJcblxyXG4uZ2FsbGVyeV9fd3JhcCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMC4yNjB2dztcclxufVxyXG4iLCIuZ2FsbGVyeV9fd3JhcDpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLWJvdHRvbTogNC42ODh2dztcbn1cblxuLmdhbGxlcnlfX3dyYXAge1xuICBtYXJnaW4tYm90dG9tOiAwLjI2dnc7XG59Il19 */"

/***/ }),

/***/ "./src/app/main/finished-projects/finished-projects.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/main/finished-projects/finished-projects.component.ts ***!
  \***********************************************************************/
/*! exports provided: FinishedProjectsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinishedProjectsComponent", function() { return FinishedProjectsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FinishedProjectsComponent = class FinishedProjectsComponent {
    constructor() { }
    ngOnInit() {
    }
};
FinishedProjectsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-finished-projects',
        template: __webpack_require__(/*! raw-loader!./finished-projects.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/finished-projects/finished-projects.component.html"),
        styles: [__webpack_require__(/*! ./finished-projects.component.scss */ "./src/app/main/finished-projects/finished-projects.component.scss")]
    })
], FinishedProjectsComponent);



/***/ }),

/***/ "./src/app/main/gallery/gallery.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/main/gallery/gallery.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".item-top {\n  position: relative;\n}\n\n.item-top .video {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  z-index: 9;\n  -webkit-transition: ease 0.3s;\n  transition: ease 0.3s;\n  height: 0;\n  overflow: hidden;\n}\n\n.gallery__item:nth-child(3n+3) .item-top .video {\n  left: auto;\n  right: 0;\n}\n\n.video .btn-custom {\n  position: absolute;\n  background: #fff;\n}\n\n.gallery__wrap:last-child {\n  margin-bottom: 0.99vw;\n}\n\n.gallery__item .video {\n  width: 41.667vw;\n}\n\n.gallery__item:hover .video {\n  height: 23.438vw;\n}\n\n.video .btn-custom {\n  top: 0.833vw;\n  left: 0.938vw;\n}\n\n.upload-pictures .btn-variant {\n  margin-bottom: 0.521vw;\n}\n\n@media screen and (max-width: 767px) {\n  .gallery__item .video {\n    width: 50vw;\n  }\n}\n\n@media screen and (max-width: 492px) {\n  .gallery__item .video {\n    width: 80vw;\n  }\n\n  .gallery__item:hover .video {\n    height: 60vw;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9nYWxsZXJ5L0M6XFxVc2Vyc1xcVmxhZERcXERlc2t0b3BcXE1lbW8gKGFsbClcXG1lbW8vc3JjXFxhcHBcXG1haW5cXGdhbGxlcnlcXGdhbGxlcnkuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21haW4vZ2FsbGVyeS9nYWxsZXJ5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7QUNDRjs7QURFQTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxVQUFBO0VBQ0EsNkJBQUE7RUFJQSxxQkFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREVBO0VBQ0UsVUFBQTtFQUNBLFFBQUE7QUNDRjs7QURFQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFQTtFQUNFLHFCQUFBO0FDQ0Y7O0FERUE7RUFDRSxlQUFBO0FDQ0Y7O0FERUE7RUFDRSxnQkFBQTtBQ0NGOztBREVBO0VBQ0UsWUFBQTtFQUNBLGFBQUE7QUNDRjs7QURFQTtFQUNFLHNCQUFBO0FDQ0Y7O0FERUE7RUFDRTtJQUNFLFdBQUE7RUNDRjtBQUNGOztBREVBO0VBQ0U7SUFDRSxXQUFBO0VDQUY7O0VER0E7SUFDRSxZQUFBO0VDQUY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL21haW4vZ2FsbGVyeS9nYWxsZXJ5LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLml0ZW0tdG9wIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5pdGVtLXRvcCAudmlkZW8ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDA7XHJcbiAgbGVmdDogMDtcclxuICB6LWluZGV4OiA5O1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogIGVhc2UgMC4zcztcclxuICAtbW96LXRyYW5zaXRpb246ICBlYXNlIDAuM3M7XHJcbiAgLW1zLXRyYW5zaXRpb246ICBlYXNlIDAuM3M7XHJcbiAgLW8tdHJhbnNpdGlvbjogIGVhc2UgMC4zcztcclxuICB0cmFuc2l0aW9uOiAgZWFzZSAwLjNzO1xyXG4gIGhlaWdodDogMDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4uZ2FsbGVyeV9faXRlbTpudGgtY2hpbGQoM24rMykgLml0ZW0tdG9wIC52aWRlbyB7XHJcbiAgbGVmdDogYXV0bztcclxuICByaWdodDogMDtcclxufVxyXG5cclxuLnZpZGVvIC5idG4tY3VzdG9tIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxufVxyXG5cclxuLmdhbGxlcnlfX3dyYXA6bGFzdC1jaGlsZCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMC45OTB2dztcclxufVxyXG5cclxuLmdhbGxlcnlfX2l0ZW0gLnZpZGVvIHtcclxuICB3aWR0aDogNDEuNjY3dnc7XHJcbn1cclxuXHJcbi5nYWxsZXJ5X19pdGVtOmhvdmVyIC52aWRlbyB7XHJcbiAgaGVpZ2h0OiAyMy40Mzh2dztcclxufVxyXG5cclxuLnZpZGVvIC5idG4tY3VzdG9tIHtcclxuICB0b3A6IDAuODMzdnc7XHJcbiAgbGVmdDogMC45Mzh2dztcclxufVxyXG5cclxuLnVwbG9hZC1waWN0dXJlcyAuYnRuLXZhcmlhbnQge1xyXG4gIG1hcmdpbi1ib3R0b206IDAuNTIxdnc7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XHJcbiAgLmdhbGxlcnlfX2l0ZW0gLnZpZGVvIHtcclxuICAgIHdpZHRoOiA1MHZ3O1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDkycHgpIHtcclxuICAuZ2FsbGVyeV9faXRlbSAudmlkZW8ge1xyXG4gICAgd2lkdGg6IDgwdnc7XHJcbiAgfVxyXG5cclxuICAuZ2FsbGVyeV9faXRlbTpob3ZlciAudmlkZW8ge1xyXG4gICAgaGVpZ2h0OiA2MHZ3O1xyXG4gIH1cclxufVxyXG4iLCIuaXRlbS10b3Age1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5pdGVtLXRvcCAudmlkZW8ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgei1pbmRleDogOTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBlYXNlIDAuM3M7XG4gIC1tb3otdHJhbnNpdGlvbjogZWFzZSAwLjNzO1xuICAtbXMtdHJhbnNpdGlvbjogZWFzZSAwLjNzO1xuICAtby10cmFuc2l0aW9uOiBlYXNlIDAuM3M7XG4gIHRyYW5zaXRpb246IGVhc2UgMC4zcztcbiAgaGVpZ2h0OiAwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uZ2FsbGVyeV9faXRlbTpudGgtY2hpbGQoM24rMykgLml0ZW0tdG9wIC52aWRlbyB7XG4gIGxlZnQ6IGF1dG87XG4gIHJpZ2h0OiAwO1xufVxuXG4udmlkZW8gLmJ0bi1jdXN0b20ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG5cbi5nYWxsZXJ5X193cmFwOmxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiAwLjk5dnc7XG59XG5cbi5nYWxsZXJ5X19pdGVtIC52aWRlbyB7XG4gIHdpZHRoOiA0MS42Njd2dztcbn1cblxuLmdhbGxlcnlfX2l0ZW06aG92ZXIgLnZpZGVvIHtcbiAgaGVpZ2h0OiAyMy40Mzh2dztcbn1cblxuLnZpZGVvIC5idG4tY3VzdG9tIHtcbiAgdG9wOiAwLjgzM3Z3O1xuICBsZWZ0OiAwLjkzOHZ3O1xufVxuXG4udXBsb2FkLXBpY3R1cmVzIC5idG4tdmFyaWFudCB7XG4gIG1hcmdpbi1ib3R0b206IDAuNTIxdnc7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gIC5nYWxsZXJ5X19pdGVtIC52aWRlbyB7XG4gICAgd2lkdGg6IDUwdnc7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ5MnB4KSB7XG4gIC5nYWxsZXJ5X19pdGVtIC52aWRlbyB7XG4gICAgd2lkdGg6IDgwdnc7XG4gIH1cblxuICAuZ2FsbGVyeV9faXRlbTpob3ZlciAudmlkZW8ge1xuICAgIGhlaWdodDogNjB2dztcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/main/gallery/gallery.component.ts":
/*!***************************************************!*\
  !*** ./src/app/main/gallery/gallery.component.ts ***!
  \***************************************************/
/*! exports provided: GalleryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryComponent", function() { return GalleryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let GalleryComponent = class GalleryComponent {
    constructor() {
        this.popupHide = true;
        this.addMorePopup = true;
    }
    ngOnInit() {
    }
    openPopup() {
        this.popupHide = !this.popupHide;
        this.addMorePopup = !this.addMorePopup;
    }
};
GalleryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-gallery',
        template: __webpack_require__(/*! raw-loader!./gallery.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/gallery/gallery.component.html"),
        styles: [__webpack_require__(/*! ./gallery.component.scss */ "./src/app/main/gallery/gallery.component.scss")]
    })
], GalleryComponent);



/***/ }),

/***/ "./src/app/main/home/home.component.scss":
/*!***********************************************!*\
  !*** ./src/app/main/home/home.component.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".go-to {\n  text-align: center;\n  margin-top: 37.917vw;\n}\n.go-to img {\n  width: 1.927vw;\n  height: 2.656vw;\n}\n.start-now {\n  text-align: center;\n  padding-top: 4.74vw;\n}\n.start-now h1 {\n  font-size: 1.875vw;\n  margin-bottom: 1.979vw;\n  font-weight: 700;\n  position: relative;\n}\n.start-now h1:before {\n  content: \"\";\n  background-color: #000000;\n  position: absolute;\n  left: 50%;\n  -webkit-transform: translate(-50%);\n          transform: translate(-50%);\n  width: 10.417vw;\n  height: 0.781vw;\n  top: -2.188vw;\n}\n.start-now p {\n  font-size: 1.25vw;\n  line-height: 2.5vw;\n  margin-top: 5.156vw;\n}\n.try-for-free {\n  background-color: rgba(230, 236, 240, 0.5);\n  position: relative;\n  z-index: 1;\n  padding-top: 2.344vw;\n}\n.try-for-free h2 {\n  color: #85a3bd;\n  font-family: \"Copperplate Gothic\", serif;\n  font-weight: 700;\n  text-transform: uppercase;\n  position: relative;\n  text-align: center;\n  line-height: 3.125vw;\n  font-size: 1.563vw;\n  margin-bottom: 3.021vw;\n}\n.try-for-free h2 span {\n  color: #ffffff;\n}\n.try-for-free h2:before {\n  content: \"\";\n  background: #85a3bd;\n  position: absolute;\n  left: 0;\n  z-index: -1;\n  width: 68.854vw;\n  height: 6.25vw;\n  top: 2.969vw;\n}\n.try-for-free__descr {\n  text-align: center;\n  margin: 2.083vw auto 0 auto;\n  font-weight: 400;\n  width: 47.969vw;\n  font-size: 0.938vw;\n}\n.try-for-free .btn-wrap {\n  text-align: center;\n  padding-top: 3.438vw;\n  padding-bottom: 2.396vw;\n}\n.try-for-free .btn-custom {\n  width: 18.802vw;\n}\n.steps {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  width: 55.792vw;\n  margin: 0 auto;\n}\n.steps__item {\n  width: 24.7%;\n  text-align: center;\n}\n.steps__item h3 {\n  color: #000000;\n  font-family: \"Copperplate Gothic\", serif;\n  font-weight: 700;\n  text-transform: uppercase;\n  font-size: 2.5vw;\n  margin: 1.354vw 0 0;\n}\n.steps__item p {\n  font-weight: 400;\n  font-size: 1.25vw;\n  line-height: 1.667vw;\n  margin: 0.885vw 0 0.938vw;\n}\n.steps__item img {\n  width: 2.448vw;\n}\n.steps__item:nth-child(2) p {\n  margin: 0.104vw 0 0.625vw;\n}\n.steps__item:last-child p {\n  margin-bottom: 1.458vw;\n}\n.steps__item:nth-child(2) img {\n  width: 2.813vw;\n}\n.home {\n  background: url('home-bg.png') top left no-repeat;\n  background-size: 100%;\n}\n@media screen and (max-width: 1200px) {\n  .try-for-free__descr,\n.steps__item p {\n    font-size: 14px;\n  }\n}\n@media screen and (max-width: 992px) {\n  .home {\n    background-position-y: 58px;\n  }\n\n  .try-for-free h2 {\n    font-size: 20px;\n    margin-bottom: 45px;\n  }\n\n  .try-for-free h2:before {\n    width: 100%;\n    height: 50px;\n  }\n\n  .start-now h1 {\n    font-size: 24px;\n  }\n\n  .start-now p {\n    font-size: 16px;\n  }\n\n  .try-for-free__descr, .steps__item p, .btn-custom {\n    font-size: 16px;\n  }\n\n  .steps__item h3 {\n    font-size: 36px;\n  }\n\n  .steps__item p {\n    min-height: 65px;\n  }\n\n  .steps__item img,\n.steps__item:nth-child(2) img {\n    width: 50px;\n  }\n\n  .try-for-free .btn-wrap .btn-custom {\n    width: auto;\n  }\n\n  .btn-custom {\n    height: 40px;\n    min-width: 80px;\n    border-radius: 21px;\n    padding: 0 12px;\n  }\n\n  .try-for-free__descr,\n.steps {\n    width: 95%;\n  }\n}\n@media screen and (max-width: 767px) {\n  .go-to img {\n    width: 30px;\n    height: auto;\n  }\n\n  .start-now {\n    padding-left: 15px;\n    padding-right: 15px;\n  }\n\n  .try-for-free h2 {\n    background: #85a3bd;\n    color: #fff;\n  }\n\n  .start-now {\n    padding-left: 15px;\n    padding-right: 15px;\n  }\n}\n@media screen and (max-width: 492px) {\n  .home {\n    background-position-y: 98px;\n  }\n\n  .steps {\n    flex-wrap: wrap;\n  }\n\n  .steps__item {\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9ob21lL0M6XFxVc2Vyc1xcVmxhZERcXERlc2t0b3BcXE1lbW8gKGFsbClcXG1lbW8vc3JjXFxhcHBcXG1haW5cXGhvbWVcXGhvbWUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21haW4vaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxvQkFBQTtBQ0NGO0FEQUU7RUFDRSxjQUFBO0VBQ0EsZUFBQTtBQ0VKO0FESUE7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0FDREY7QURFRTtFQUNFLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDQUo7QURDSTtFQUNFLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGtDQUFBO1VBQUEsMEJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7QUNDTjtBREVFO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDQUo7QURJQTtFQUNFLDBDQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0Esb0JBQUE7QUNERjtBREVFO0VBQ0UsY0FBQTtFQUNBLHdDQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUNBSjtBRENJO0VBQ0UsY0FBQTtBQ0NOO0FERUU7RUFDRSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDQUo7QURFRTtFQUNFLGtCQUFBO0VBQ0EsMkJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0FKO0FER0U7RUFDRSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsdUJBQUE7QUNESjtBRE1BO0VBQ0UsZUFBQTtBQ0hGO0FETUE7RUFHRSxvQkFBQTtFQUFBLGFBQUE7RUFHQSx5QkFBQTtVQUFBLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUNIRjtBRElFO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0FDRko7QURHSTtFQUNFLGNBQUE7RUFDQSx3Q0FBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDRE47QURHSTtFQUNFLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLHlCQUFBO0FDRE47QURHSTtFQUNFLGNBQUE7QUNETjtBRElFO0VBQ0UseUJBQUE7QUNGSjtBRElFO0VBQ0Usc0JBQUE7QUNGSjtBRElFO0VBQ0UsY0FBQTtBQ0ZKO0FETUE7RUFDRSxpREFBQTtFQUVBLHFCQUFBO0FDSEY7QURNQTtFQUNFOztJQUVFLGVBQUE7RUNIRjtBQUNGO0FET0E7RUFDRTtJQUNFLDJCQUFBO0VDTEY7O0VEUUE7SUFDRSxlQUFBO0lBQ0EsbUJBQUE7RUNMRjs7RURRQTtJQUNFLFdBQUE7SUFDQSxZQUFBO0VDTEY7O0VEUUE7SUFDRSxlQUFBO0VDTEY7O0VEUUE7SUFDRSxlQUFBO0VDTEY7O0VEUUE7SUFDRSxlQUFBO0VDTEY7O0VEUUE7SUFDRSxlQUFBO0VDTEY7O0VEUUE7SUFDRSxnQkFBQTtFQ0xGOztFRFFBOztJQUVFLFdBQUE7RUNMRjs7RURRQTtJQUNFLFdBQUE7RUNMRjs7RURRQTtJQUNFLFlBQUE7SUFDQSxlQUFBO0lBQ0EsbUJBQUE7SUFDQSxlQUFBO0VDTEY7O0VEUUE7O0lBRUUsVUFBQTtFQ0xGO0FBQ0Y7QURRQTtFQUNFO0lBQ0UsV0FBQTtJQUNBLFlBQUE7RUNORjs7RURTQTtJQUNFLGtCQUFBO0lBQ0EsbUJBQUE7RUNORjs7RURTQTtJQUNFLG1CQUFBO0lBQ0EsV0FBQTtFQ05GOztFRFNBO0lBQ0Usa0JBQUE7SUFDQSxtQkFBQTtFQ05GO0FBQ0Y7QURTQTtFQUNFO0lBQ0UsMkJBQUE7RUNQRjs7RURVQTtJQUdFLGVBQUE7RUNQRjs7RURVQTtJQUNFLFdBQUE7RUNQRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbWFpbi9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZ28tdG8ge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAzNy45MTd2dztcclxuICBpbWcge1xyXG4gICAgd2lkdGg6IDEuOTI3dnc7XHJcbiAgICBoZWlnaHQ6IDIuNjU2dnc7XHJcbiAgICAvL3dpZHRoOiAzMHB4O1xyXG4gICAgLy9oZWlnaHQ6IGF1dG87XHJcbiAgfVxyXG59XHJcblxyXG4uc3RhcnQtbm93IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZy10b3A6IDQuNzQwdnc7XHJcbiAgaDEge1xyXG4gICAgZm9udC1zaXplOiAxLjg3NXZ3O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMS45Nzl2dztcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAmOmJlZm9yZSB7XHJcbiAgICAgIGNvbnRlbnQ6ICcnO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSk7XHJcbiAgICAgIHdpZHRoOiAxMC40MTd2dztcclxuICAgICAgaGVpZ2h0OiAwLjc4MXZ3O1xyXG4gICAgICB0b3A6IC0yLjE4OHZ3O1xyXG4gICAgfVxyXG4gIH1cclxuICBwIHtcclxuICAgIGZvbnQtc2l6ZTogMS4yNTB2dztcclxuICAgIGxpbmUtaGVpZ2h0OiAyLjUwMHZ3O1xyXG4gICAgbWFyZ2luLXRvcDogNS4xNTZ2dztcclxuICB9XHJcbn1cclxuXHJcbi50cnktZm9yLWZyZWUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjMwLCAyMzYsIDI0MCwgMC41KTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgei1pbmRleDogMTtcclxuICBwYWRkaW5nLXRvcDogMi4zNDR2dztcclxuICBoMiB7XHJcbiAgICBjb2xvcjogIzg1YTNiZDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkNvcHBlcnBsYXRlIEdvdGhpY1wiLCBzZXJpZjtcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbGluZS1oZWlnaHQ6IDMuMTI1dnc7XHJcbiAgICBmb250LXNpemU6IDEuNTYzdnc7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzLjAyMXZ3O1xyXG4gICAgc3BhbiB7XHJcbiAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgfVxyXG4gIH1cclxuICBoMjpiZWZvcmUge1xyXG4gICAgY29udGVudDogJyc7XHJcbiAgICBiYWNrZ3JvdW5kOiAjODVhM2JkO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHotaW5kZXg6IC0xO1xyXG4gICAgd2lkdGg6IDY4Ljg1NHZ3O1xyXG4gICAgaGVpZ2h0OiA2LjI1MHZ3O1xyXG4gICAgdG9wOiAyLjk2OXZ3O1xyXG4gIH1cclxuICAmX19kZXNjciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDIuMDgzdncgYXV0byAwIGF1dG87XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgd2lkdGg6IDQ3Ljk2OXZ3O1xyXG4gICAgZm9udC1zaXplOiAwLjkzOHZ3O1xyXG4gICAgLy9tYXJnaW4tdG9wOiAyLjA4M3Z3O1xyXG4gIH1cclxuICAuYnRuLXdyYXAge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcGFkZGluZy10b3A6IDMuNDM4dnc7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMi4zOTZ2dztcclxuICAgIC8vd2lkdGg6IDE4LjgwMnZ3O1xyXG4gIH1cclxufVxyXG5cclxuLnRyeS1mb3ItZnJlZSAuYnRuLWN1c3RvbSB7XHJcbiAgd2lkdGg6IDE4LjgwMnZ3O1xyXG59XHJcblxyXG4uc3RlcHMge1xyXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIHdpZHRoOiA1NS43OTJ2dztcclxuICBtYXJnaW46IDAgYXV0bztcclxuICAmX19pdGVtIHtcclxuICAgIHdpZHRoOiAyNC43JTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGgzIHtcclxuICAgICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgICAgIGZvbnQtZmFtaWx5OiBcIkNvcHBlcnBsYXRlIEdvdGhpY1wiLCBzZXJpZjtcclxuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgZm9udC1zaXplOiAyLjUwMHZ3O1xyXG4gICAgICBtYXJnaW46IDEuMzU0dncgMCAwO1xyXG4gICAgfVxyXG4gICAgcCB7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICAgIGZvbnQtc2l6ZTogMS4yNTB2dztcclxuICAgICAgbGluZS1oZWlnaHQ6IDEuNjY3dnc7XHJcbiAgICAgIG1hcmdpbjogMC44ODV2dyAwIDAuOTM4dnc7XHJcbiAgICB9XHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogMi40NDh2dztcclxuICAgIH1cclxuICB9XHJcbiAgJl9faXRlbTpudGgtY2hpbGQoMikgcCB7XHJcbiAgICBtYXJnaW46IDAuMTA0dncgMCAwLjYyNXZ3O1xyXG4gIH1cclxuICAmX19pdGVtOmxhc3QtY2hpbGQgcCB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxLjQ1OHZ3O1xyXG4gIH1cclxuICAmX19pdGVtOm50aC1jaGlsZCgyKSBpbWcge1xyXG4gICAgd2lkdGg6IDIuODEzdnc7XHJcbiAgfVxyXG59XHJcblxyXG4uaG9tZSB7XHJcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWcvaG9tZS1iZy5wbmcpIHRvcCBsZWZ0IG5vLXJlcGVhdDtcclxuICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogMTAwJTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xyXG4gIC50cnktZm9yLWZyZWVfX2Rlc2NyLFxyXG4gIC5zdGVwc19faXRlbSBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICB9XHJcbn1cclxuXHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xyXG4gIC5ob21lIHtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogNThweDtcclxuICB9XHJcblxyXG4gIC50cnktZm9yLWZyZWUgaDIge1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNDVweDtcclxuICB9XHJcblxyXG4gIC50cnktZm9yLWZyZWUgaDI6YmVmb3JlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gIH1cclxuXHJcbiAgLnN0YXJ0LW5vdyBoMSB7XHJcbiAgICBmb250LXNpemU6IDI0cHg7XHJcbiAgfVxyXG5cclxuICAuc3RhcnQtbm93IHAge1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gIH1cclxuXHJcbiAgLnRyeS1mb3ItZnJlZV9fZGVzY3IsIC5zdGVwc19faXRlbSBwLCAuYnRuLWN1c3RvbSB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgfVxyXG5cclxuICAuc3RlcHNfX2l0ZW0gaDMge1xyXG4gICAgZm9udC1zaXplOiAzNnB4O1xyXG4gIH1cclxuXHJcbiAgLnN0ZXBzX19pdGVtIHAge1xyXG4gICAgbWluLWhlaWdodDogNjVweDtcclxuICB9XHJcblxyXG4gIC5zdGVwc19faXRlbSBpbWcsXHJcbiAgLnN0ZXBzX19pdGVtOm50aC1jaGlsZCgyKSBpbWcge1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgfVxyXG5cclxuICAudHJ5LWZvci1mcmVlIC5idG4td3JhcCAuYnRuLWN1c3RvbSB7XHJcbiAgICB3aWR0aDogYXV0bztcclxuICB9XHJcblxyXG4gIC5idG4tY3VzdG9tIHtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIG1pbi13aWR0aDogODBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XHJcbiAgICBwYWRkaW5nOiAwIDEycHg7XHJcbiAgfVxyXG5cclxuICAudHJ5LWZvci1mcmVlX19kZXNjcixcclxuICAuc3RlcHMge1xyXG4gICAgd2lkdGg6IDk1JTtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XHJcbiAgLmdvLXRvIGltZyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIGhlaWdodDogYXV0bztcclxuICB9XHJcblxyXG4gIC5zdGFydC1ub3cge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcclxuICB9XHJcblxyXG4gIC50cnktZm9yLWZyZWUgaDIge1xyXG4gICAgYmFja2dyb3VuZDogIzg1YTNiZDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuXHJcbiAgLnN0YXJ0LW5vdyB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDkycHgpIHtcclxuICAuaG9tZSB7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDk4cHg7XHJcbiAgfVxyXG5cclxuICAuc3RlcHMge1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gIH1cclxuXHJcbiAgLnN0ZXBzX19pdGVtIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufVxyXG5cclxuIiwiLmdvLXRvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAzNy45MTd2dztcbn1cbi5nby10byBpbWcge1xuICB3aWR0aDogMS45Mjd2dztcbiAgaGVpZ2h0OiAyLjY1NnZ3O1xufVxuXG4uc3RhcnQtbm93IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogNC43NHZ3O1xufVxuLnN0YXJ0LW5vdyBoMSB7XG4gIGZvbnQtc2l6ZTogMS44NzV2dztcbiAgbWFyZ2luLWJvdHRvbTogMS45Nzl2dztcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLnN0YXJ0LW5vdyBoMTpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSk7XG4gIHdpZHRoOiAxMC40MTd2dztcbiAgaGVpZ2h0OiAwLjc4MXZ3O1xuICB0b3A6IC0yLjE4OHZ3O1xufVxuLnN0YXJ0LW5vdyBwIHtcbiAgZm9udC1zaXplOiAxLjI1dnc7XG4gIGxpbmUtaGVpZ2h0OiAyLjV2dztcbiAgbWFyZ2luLXRvcDogNS4xNTZ2dztcbn1cblxuLnRyeS1mb3ItZnJlZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjMwLCAyMzYsIDI0MCwgMC41KTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxO1xuICBwYWRkaW5nLXRvcDogMi4zNDR2dztcbn1cbi50cnktZm9yLWZyZWUgaDIge1xuICBjb2xvcjogIzg1YTNiZDtcbiAgZm9udC1mYW1pbHk6IFwiQ29wcGVycGxhdGUgR290aGljXCIsIHNlcmlmO1xuICBmb250LXdlaWdodDogNzAwO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbGluZS1oZWlnaHQ6IDMuMTI1dnc7XG4gIGZvbnQtc2l6ZTogMS41NjN2dztcbiAgbWFyZ2luLWJvdHRvbTogMy4wMjF2dztcbn1cbi50cnktZm9yLWZyZWUgaDIgc3BhbiB7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuLnRyeS1mb3ItZnJlZSBoMjpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBiYWNrZ3JvdW5kOiAjODVhM2JkO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHotaW5kZXg6IC0xO1xuICB3aWR0aDogNjguODU0dnc7XG4gIGhlaWdodDogNi4yNXZ3O1xuICB0b3A6IDIuOTY5dnc7XG59XG4udHJ5LWZvci1mcmVlX19kZXNjciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAyLjA4M3Z3IGF1dG8gMCBhdXRvO1xuICBmb250LXdlaWdodDogNDAwO1xuICB3aWR0aDogNDcuOTY5dnc7XG4gIGZvbnQtc2l6ZTogMC45Mzh2dztcbn1cbi50cnktZm9yLWZyZWUgLmJ0bi13cmFwIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogMy40Mzh2dztcbiAgcGFkZGluZy1ib3R0b206IDIuMzk2dnc7XG59XG5cbi50cnktZm9yLWZyZWUgLmJ0bi1jdXN0b20ge1xuICB3aWR0aDogMTguODAydnc7XG59XG5cbi5zdGVwcyB7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHdpZHRoOiA1NS43OTJ2dztcbiAgbWFyZ2luOiAwIGF1dG87XG59XG4uc3RlcHNfX2l0ZW0ge1xuICB3aWR0aDogMjQuNyU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5zdGVwc19faXRlbSBoMyB7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBmb250LWZhbWlseTogXCJDb3BwZXJwbGF0ZSBHb3RoaWNcIiwgc2VyaWY7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMi41dnc7XG4gIG1hcmdpbjogMS4zNTR2dyAwIDA7XG59XG4uc3RlcHNfX2l0ZW0gcCB7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGZvbnQtc2l6ZTogMS4yNXZ3O1xuICBsaW5lLWhlaWdodDogMS42Njd2dztcbiAgbWFyZ2luOiAwLjg4NXZ3IDAgMC45Mzh2dztcbn1cbi5zdGVwc19faXRlbSBpbWcge1xuICB3aWR0aDogMi40NDh2dztcbn1cbi5zdGVwc19faXRlbTpudGgtY2hpbGQoMikgcCB7XG4gIG1hcmdpbjogMC4xMDR2dyAwIDAuNjI1dnc7XG59XG4uc3RlcHNfX2l0ZW06bGFzdC1jaGlsZCBwIHtcbiAgbWFyZ2luLWJvdHRvbTogMS40NTh2dztcbn1cbi5zdGVwc19faXRlbTpudGgtY2hpbGQoMikgaW1nIHtcbiAgd2lkdGg6IDIuODEzdnc7XG59XG5cbi5ob21lIHtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWcvaG9tZS1iZy5wbmcpIHRvcCBsZWZ0IG5vLXJlcGVhdDtcbiAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IDEwMCU7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSB7XG4gIC50cnktZm9yLWZyZWVfX2Rlc2NyLFxuLnN0ZXBzX19pdGVtIHAge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLmhvbWUge1xuICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogNThweDtcbiAgfVxuXG4gIC50cnktZm9yLWZyZWUgaDIge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA0NXB4O1xuICB9XG5cbiAgLnRyeS1mb3ItZnJlZSBoMjpiZWZvcmUge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNTBweDtcbiAgfVxuXG4gIC5zdGFydC1ub3cgaDEge1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgfVxuXG4gIC5zdGFydC1ub3cgcCB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG5cbiAgLnRyeS1mb3ItZnJlZV9fZGVzY3IsIC5zdGVwc19faXRlbSBwLCAuYnRuLWN1c3RvbSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG5cbiAgLnN0ZXBzX19pdGVtIGgzIHtcbiAgICBmb250LXNpemU6IDM2cHg7XG4gIH1cblxuICAuc3RlcHNfX2l0ZW0gcCB7XG4gICAgbWluLWhlaWdodDogNjVweDtcbiAgfVxuXG4gIC5zdGVwc19faXRlbSBpbWcsXG4uc3RlcHNfX2l0ZW06bnRoLWNoaWxkKDIpIGltZyB7XG4gICAgd2lkdGg6IDUwcHg7XG4gIH1cblxuICAudHJ5LWZvci1mcmVlIC5idG4td3JhcCAuYnRuLWN1c3RvbSB7XG4gICAgd2lkdGg6IGF1dG87XG4gIH1cblxuICAuYnRuLWN1c3RvbSB7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIG1pbi13aWR0aDogODBweDtcbiAgICBib3JkZXItcmFkaXVzOiAyMXB4O1xuICAgIHBhZGRpbmc6IDAgMTJweDtcbiAgfVxuXG4gIC50cnktZm9yLWZyZWVfX2Rlc2NyLFxuLnN0ZXBzIHtcbiAgICB3aWR0aDogOTUlO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAuZ28tdG8gaW1nIHtcbiAgICB3aWR0aDogMzBweDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gIH1cblxuICAuc3RhcnQtbm93IHtcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgfVxuXG4gIC50cnktZm9yLWZyZWUgaDIge1xuICAgIGJhY2tncm91bmQ6ICM4NWEzYmQ7XG4gICAgY29sb3I6ICNmZmY7XG4gIH1cblxuICAuc3RhcnQtbm93IHtcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDkycHgpIHtcbiAgLmhvbWUge1xuICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogOThweDtcbiAgfVxuXG4gIC5zdGVwcyB7XG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XG4gICAgLW1zLWZsZXgtd3JhcDogd3JhcDtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gIH1cblxuICAuc3RlcHNfX2l0ZW0ge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/main/home/home.component.ts":
/*!*********************************************!*\
  !*** ./src/app/main/home/home.component.ts ***!
  \*********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() { }
    ngOnInit() {
    }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-index',
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/main/home/home.component.scss")]
    })
], HomeComponent);



/***/ }),

/***/ "./src/app/main/main-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/main/main-routing.module.ts ***!
  \*********************************************/
/*! exports provided: MainRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainRoutingModule", function() { return MainRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _main_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/main/home/home.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/main/contact-us/contact-us.component.ts");
/* harmony import */ var _gallery_gallery_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./gallery/gallery.component */ "./src/app/main/gallery/gallery.component.ts");
/* harmony import */ var _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pricing/pricing.component */ "./src/app/main/pricing/pricing.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/main/profile/profile.component.ts");
/* harmony import */ var _finished_projects_finished_projects_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./finished-projects/finished-projects.component */ "./src/app/main/finished-projects/finished-projects.component.ts");
/* harmony import */ var _project_in_progress_project_in_progress_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./project-in-progress/project-in-progress.component */ "./src/app/main/project-in-progress/project-in-progress.component.ts");
/* harmony import */ var _projects_projects_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./projects/projects.component */ "./src/app/main/projects/projects.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./about/about.component */ "./src/app/main/about/about.component.ts");
/* harmony import */ var _faq_faq_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./faq/faq.component */ "./src/app/main/faq/faq.component.ts");
/* harmony import */ var _shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../shared/guards/auth.guard */ "./src/app/shared/guards/auth.guard.ts");















const routes = [
    {
        path: '', component: _main_component__WEBPACK_IMPORTED_MODULE_3__["MainComponent"], children: [
            { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
            { path: 'about', component: _about_about_component__WEBPACK_IMPORTED_MODULE_12__["AboutComponent"] },
            { path: 'contact-us', component: _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_5__["ContactUsComponent"] },
            { path: 'gallery', component: _gallery_gallery_component__WEBPACK_IMPORTED_MODULE_6__["GalleryComponent"] },
            { path: 'pricing', component: _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_7__["PricingComponent"] },
            { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_8__["ProfileComponent"], canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_14__["AuthGuard"]] },
            { path: 'finished-project', component: _finished_projects_finished_projects_component__WEBPACK_IMPORTED_MODULE_9__["FinishedProjectsComponent"], canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_14__["AuthGuard"]] },
            { path: 'project-in-progress', component: _project_in_progress_project_in_progress_component__WEBPACK_IMPORTED_MODULE_10__["ProjectInProgressComponent"], canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_14__["AuthGuard"]] },
            { path: 'project', component: _projects_projects_component__WEBPACK_IMPORTED_MODULE_11__["ProjectsComponent"], canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_14__["AuthGuard"]] },
            { path: 'faq', component: _faq_faq_component__WEBPACK_IMPORTED_MODULE_13__["FaqComponent"] },
            { path: '', redirectTo: 'home', pathMatch: 'full' },
        ]
    },
];
let MainRoutingModule = class MainRoutingModule {
};
MainRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], MainRoutingModule);



/***/ }),

/***/ "./src/app/main/main.component.scss":
/*!******************************************!*\
  !*** ./src/app/main/main.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-background {\n  background: url('gallery-bg.png') top left no-repeat;\n  background-size: 100%;\n}\n\n@media screen and (max-width: 992px) {\n  .main-background {\n    background-position-y: 58px;\n  }\n}\n\n@media screen and (max-width: 492px) {\n  .main-background {\n    background-position-y: 98px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9DOlxcVXNlcnNcXFZsYWREXFxEZXNrdG9wXFxNZW1vIChhbGwpXFxtZW1vL3NyY1xcYXBwXFxtYWluXFxtYWluLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tYWluL21haW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxvREFBQTtFQUVBLHFCQUFBO0FDQ0Y7O0FERUE7RUFDRTtJQUNFLDJCQUFBO0VDQ0Y7QUFDRjs7QURFQTtFQUNFO0lBQ0UsMkJBQUE7RUNBRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbWFpbi9tYWluLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW4tYmFja2dyb3VuZHtcclxuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9nYWxsZXJ5LWJnLnBuZykgdG9wIGxlZnQgbm8tcmVwZWF0O1xyXG4gIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAubWFpbi1iYWNrZ3JvdW5kIHtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogNThweDtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ5MnB4KSB7XHJcbiAgLm1haW4tYmFja2dyb3VuZCB7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDk4cHg7XHJcbiAgfVxyXG59XHJcbiIsIi5tYWluLWJhY2tncm91bmQge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9nYWxsZXJ5LWJnLnBuZykgdG9wIGxlZnQgbm8tcmVwZWF0O1xuICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogMTAwJTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xuICAubWFpbi1iYWNrZ3JvdW5kIHtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDU4cHg7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ5MnB4KSB7XG4gIC5tYWluLWJhY2tncm91bmQge1xuICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogOThweDtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/main/main.component.ts":
/*!****************************************!*\
  !*** ./src/app/main/main.component.ts ***!
  \****************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let MainComponent = class MainComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
};
MainComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
MainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-main',
        template: __webpack_require__(/*! raw-loader!./main.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/main.component.html"),
        styles: [__webpack_require__(/*! ./main.component.scss */ "./src/app/main/main.component.scss")]
    })
], MainComponent);



/***/ }),

/***/ "./src/app/main/main.module.ts":
/*!*************************************!*\
  !*** ./src/app/main/main.module.ts ***!
  \*************************************/
/*! exports provided: MainModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainModule", function() { return MainModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _main_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _main_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./main-routing.module */ "./src/app/main/main-routing.module.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home/home.component */ "./src/app/main/home/home.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/main/contact-us/contact-us.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pricing/pricing.component */ "./src/app/main/pricing/pricing.component.ts");
/* harmony import */ var _gallery_gallery_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./gallery/gallery.component */ "./src/app/main/gallery/gallery.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/main/profile/profile.component.ts");
/* harmony import */ var _projects_projects_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./projects/projects.component */ "./src/app/main/projects/projects.component.ts");
/* harmony import */ var _finished_projects_finished_projects_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./finished-projects/finished-projects.component */ "./src/app/main/finished-projects/finished-projects.component.ts");
/* harmony import */ var _project_in_progress_project_in_progress_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./project-in-progress/project-in-progress.component */ "./src/app/main/project-in-progress/project-in-progress.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./about/about.component */ "./src/app/main/about/about.component.ts");
/* harmony import */ var _faq_faq_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./faq/faq.component */ "./src/app/main/faq/faq.component.ts");

















let MainModule = class MainModule {
};
MainModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _main_component__WEBPACK_IMPORTED_MODULE_3__["MainComponent"],
            _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
            _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_7__["ContactUsComponent"],
            _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_9__["PricingComponent"],
            _gallery_gallery_component__WEBPACK_IMPORTED_MODULE_10__["GalleryComponent"],
            _profile_profile_component__WEBPACK_IMPORTED_MODULE_11__["ProfileComponent"],
            _projects_projects_component__WEBPACK_IMPORTED_MODULE_12__["ProjectsComponent"],
            _finished_projects_finished_projects_component__WEBPACK_IMPORTED_MODULE_13__["FinishedProjectsComponent"],
            _project_in_progress_project_in_progress_component__WEBPACK_IMPORTED_MODULE_14__["ProjectInProgressComponent"],
            _about_about_component__WEBPACK_IMPORTED_MODULE_15__["AboutComponent"],
            _faq_faq_component__WEBPACK_IMPORTED_MODULE_16__["FaqComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _main_routing_module__WEBPACK_IMPORTED_MODULE_4__["MainRoutingModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
        ],
        exports: [
            _main_component__WEBPACK_IMPORTED_MODULE_3__["MainComponent"],
            _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
            _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_7__["ContactUsComponent"],
            _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_9__["PricingComponent"],
            _gallery_gallery_component__WEBPACK_IMPORTED_MODULE_10__["GalleryComponent"],
            _profile_profile_component__WEBPACK_IMPORTED_MODULE_11__["ProfileComponent"],
            _projects_projects_component__WEBPACK_IMPORTED_MODULE_12__["ProjectsComponent"],
            _about_about_component__WEBPACK_IMPORTED_MODULE_15__["AboutComponent"],
            _faq_faq_component__WEBPACK_IMPORTED_MODULE_16__["FaqComponent"]
        ]
    })
], MainModule);



/***/ }),

/***/ "./src/app/main/pricing/pricing.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/main/pricing/pricing.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pricing__wrap {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n}\n\n.pricing__item a {\n  display: block;\n  background-size: 100% 100% !important;\n  text-align: center;\n}\n\n.pricing__item .quality {\n  color: #000000;\n  font-weight: 700;\n  text-transform: uppercase;\n}\n\n.pricing__item p {\n  color: #000000;\n  font-weight: 400;\n  margin: 0;\n}\n\n.pricing__item .price {\n  font-weight: 700;\n}\n\n.basic .price {\n  color: #00b6cc;\n}\n\n.advanced .price {\n  color: #a8054f;\n}\n\n.pro .price {\n  color: #f98008;\n}\n\n.basic a {\n  background: url('basic-bg.png');\n}\n\n.advanced a {\n  background: url('advanced-bg.png');\n}\n\n.pro a {\n  background: url('pro-bg.png');\n}\n\n.pricing__wrap {\n  padding-top: 6.563vw;\n  padding-bottom: 1.094vw;\n}\n\n.pricing__item:first-child,\n.pricing__item:last-child {\n  padding-top: 2.083vw;\n}\n\n.pricing__item a {\n  width: 16.979vw;\n  padding-top: 3.49vw;\n  padding-bottom: 5.417vw;\n}\n\n.pricing__item .best-deal {\n  width: 18.021vw;\n  padding-top: 4.948vw;\n  padding-bottom: 6.615vw;\n}\n\n.pricing__item .best-deal .price {\n  margin-top: 2.083vw;\n}\n\n.pricing__item .best-deal .quality {\n  margin: 0.938vw 0 2.188vw;\n}\n\n.pricing__item .quality {\n  font-size: 2.292vw;\n  margin: 0.156vw 0 2vw;\n}\n\n.pricing__item p {\n  font-size: 0.938vw;\n  line-height: 1.406vw;\n  overflow: hidden;\n}\n\n.pricing__item .price {\n  font-size: 1.25vw;\n  margin-top: 1.927vw;\n}\n\n@media screen and (max-width: 1200px) {\n  .pricing__item p {\n    font-size: 14px;\n    line-height: 1.4;\n  }\n\n  .pricing__wrap.container {\n    width: 79%;\n  }\n\n  .pricing__item a {\n    width: 263px;\n    height: 385px;\n    padding-left: 22px;\n    padding-right: 22px;\n  }\n\n  .pricing__item .best-deal {\n    width: 265px;\n    height: 443px;\n    padding-left: 22px;\n    padding-right: 22px;\n  }\n\n  .pricing__item .quality,\n.pricing__item .best-deal .quality {\n    margin: 11px 0 26px;\n  }\n\n  .pricing__item .price,\n.pricing__item .best-deal .price {\n    margin-top: 25px;\n  }\n}\n\n@media screen and (max-width: 992px) {\n  .pricing__item a {\n    padding-top: 55px;\n  }\n\n  .pricing__item .best-deal {\n    width: 260px;\n  }\n\n  .pricing__item a {\n    width: 253px;\n  }\n\n  .pricing__item .quality {\n    font-size: 24px;\n  }\n\n  .pricing__item p {\n    font-size: 16px;\n  }\n\n  .pricing__wrap.container {\n    width: 100%;\n  }\n\n  .pricing__item .best-deal {\n    padding-top: 55px;\n  }\n\n  .pricing__item .price {\n    font-size: 19px;\n    margin-top: 12px;\n  }\n}\n\n@media screen and (max-width: 767px) {\n  .pricing__item:first-child, .pricing__item:last-child {\n    padding-top: 0;\n  }\n\n  .pricing__item a {\n    display: inline-block;\n  }\n\n  .pricing__item {\n    width: 100%;\n    text-align: center;\n  }\n\n  .pricing__wrap.container {\n    flex-wrap: wrap;\n  }\n\n  h1.page-title {\n    font-size: 46px;\n  }\n}\n\n@media screen and (max-width: 450px) {\n  .pricing .header__logo img {\n    width: 100px;\n    min-width: 100px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9wcmljaW5nL0M6XFxVc2Vyc1xcVmxhZERcXERlc2t0b3BcXE1lbW8gKGFsbClcXG1lbW8vc3JjXFxhcHBcXG1haW5cXHByaWNpbmdcXHByaWNpbmcuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21haW4vcHJpY2luZy9wcmljaW5nLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBR0Usb0JBQUE7RUFBQSxhQUFBO0VBR0EseUJBQUE7VUFBQSw4QkFBQTtBQ0FGOztBREdBO0VBQ0UsY0FBQTtFQUNBLHFDQUFBO0VBQ0Esa0JBQUE7QUNBRjs7QURHQTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0FDQUY7O0FER0E7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDQUY7O0FER0E7RUFDRSxnQkFBQTtBQ0FGOztBREdBO0VBQ0UsY0FBQTtBQ0FGOztBREdBO0VBQ0UsY0FBQTtBQ0FGOztBREdBO0VBQ0UsY0FBQTtBQ0FGOztBREdBO0VBQ0UsK0JBQUE7QUNBRjs7QURHQTtFQUNFLGtDQUFBO0FDQUY7O0FESUE7RUFDRSw2QkFBQTtBQ0RGOztBRElBO0VBQ0Usb0JBQUE7RUFDQSx1QkFBQTtBQ0RGOztBRElBOztFQUVFLG9CQUFBO0FDREY7O0FESUE7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ0RGOztBREdBO0VBQ0UsZUFBQTtFQUNBLG9CQUFBO0VBQ0EsdUJBQUE7QUNBRjs7QURHQTtFQUNFLG1CQUFBO0FDQUY7O0FER0E7RUFDRSx5QkFBQTtBQ0FGOztBREdBO0VBQ0Usa0JBQUE7RUFDQSxxQkFBQTtBQ0FGOztBREdBO0VBQ0Usa0JBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0FDQUY7O0FER0E7RUFDRSxpQkFBQTtFQUNBLG1CQUFBO0FDQUY7O0FESUE7RUFDRTtJQUNFLGVBQUE7SUFDQSxnQkFBQTtFQ0RGOztFRElBO0lBQ0UsVUFBQTtFQ0RGOztFRElBO0lBQ0UsWUFBQTtJQUNBLGFBQUE7SUFDQSxrQkFBQTtJQUNBLG1CQUFBO0VDREY7O0VER0E7SUFDRSxZQUFBO0lBQ0EsYUFBQTtJQUNBLGtCQUFBO0lBQ0EsbUJBQUE7RUNBRjs7RURHQTs7SUFFRSxtQkFBQTtFQ0FGOztFREdBOztJQUVFLGdCQUFBO0VDQUY7QUFDRjs7QURHQTtFQUNFO0lBQ0UsaUJBQUE7RUNERjs7RURJQTtJQUNFLFlBQUE7RUNERjs7RURJQTtJQUNFLFlBQUE7RUNERjs7RURJQTtJQUNFLGVBQUE7RUNERjs7RURJQTtJQUNFLGVBQUE7RUNERjs7RURJQTtJQUNFLFdBQUE7RUNERjs7RURJQTtJQUNFLGlCQUFBO0VDREY7O0VESUE7SUFDRSxlQUFBO0lBQ0EsZ0JBQUE7RUNERjtBQUNGOztBRElBO0VBQ0U7SUFDRSxjQUFBO0VDRkY7O0VES0E7SUFDRSxxQkFBQTtFQ0ZGOztFREtBO0lBQ0UsV0FBQTtJQUNBLGtCQUFBO0VDRkY7O0VES0E7SUFHRSxlQUFBO0VDRkY7O0VESUE7SUFDRSxlQUFBO0VDREY7QUFDRjs7QURJQTtFQUNFO0lBQ0UsWUFBQTtJQUNBLGdCQUFBO0VDRkY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL21haW4vcHJpY2luZy9wcmljaW5nLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi5wcmljaW5nX193cmFwIHtcclxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG5cclxuLnByaWNpbmdfX2l0ZW0gYSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCUgIWltcG9ydGFudDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5wcmljaW5nX19pdGVtIC5xdWFsaXR5IHtcclxuICBjb2xvcjogIzAwMDAwMDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuXHJcbi5wcmljaW5nX19pdGVtIHAge1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4ucHJpY2luZ19faXRlbSAucHJpY2Uge1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbi5iYXNpYyAucHJpY2Uge1xyXG4gIGNvbG9yOiAjMDBiNmNjO1xyXG59XHJcblxyXG4uYWR2YW5jZWQgLnByaWNlIHtcclxuICBjb2xvcjogI2E4MDU0ZjtcclxufVxyXG5cclxuLnBybyAucHJpY2Uge1xyXG4gIGNvbG9yOiAjZjk4MDA4O1xyXG59XHJcblxyXG4uYmFzaWMgYSB7XHJcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWcvYmFzaWMtYmcucG5nKTtcclxufVxyXG5cclxuLmFkdmFuY2VkIGEge1xyXG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1nL2FkdmFuY2VkLWJnLnBuZyk7XHJcblxyXG59XHJcblxyXG4ucHJvIGEge1xyXG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1nL3Byby1iZy5wbmcpO1xyXG59XHJcblxyXG4ucHJpY2luZ19fd3JhcCB7XHJcbiAgcGFkZGluZy10b3A6IDYuNTYzdnc7XHJcbiAgcGFkZGluZy1ib3R0b206IDEuMDk0dnc7XHJcbn1cclxuXHJcbi5wcmljaW5nX19pdGVtOmZpcnN0LWNoaWxkLFxyXG4ucHJpY2luZ19faXRlbTpsYXN0LWNoaWxkIHtcclxuICBwYWRkaW5nLXRvcDogMi4wODN2dztcclxufVxyXG5cclxuLnByaWNpbmdfX2l0ZW0gYSB7XHJcbiAgd2lkdGg6IDE2Ljk3OXZ3O1xyXG4gIHBhZGRpbmctdG9wOiAzLjQ5MHZ3O1xyXG4gIHBhZGRpbmctYm90dG9tOiA1LjQxN3Z3O1xyXG59XHJcbi5wcmljaW5nX19pdGVtIC5iZXN0LWRlYWwge1xyXG4gIHdpZHRoOiAxOC4wMjF2dztcclxuICBwYWRkaW5nLXRvcDogNC45NDh2dztcclxuICBwYWRkaW5nLWJvdHRvbTogNi42MTV2dztcclxufVxyXG5cclxuLnByaWNpbmdfX2l0ZW0gLmJlc3QtZGVhbCAucHJpY2Uge1xyXG4gIG1hcmdpbi10b3A6IDIuMDgzdnc7XHJcbn1cclxuXHJcbi5wcmljaW5nX19pdGVtIC5iZXN0LWRlYWwgLnF1YWxpdHkge1xyXG4gIG1hcmdpbjogMC45Mzh2dyAwIDIuMTg4dnc7XHJcbn1cclxuXHJcbi5wcmljaW5nX19pdGVtIC5xdWFsaXR5IHtcclxuICBmb250LXNpemU6IDIuMjkydnc7XHJcbiAgbWFyZ2luOiAwLjE1NnZ3IDAgMnZ3O1xyXG59XHJcblxyXG4ucHJpY2luZ19faXRlbSBwIHtcclxuICBmb250LXNpemU6IDAuOTM4dnc7XHJcbiAgbGluZS1oZWlnaHQ6IDEuNDA2dnc7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuLnByaWNpbmdfX2l0ZW0gLnByaWNlIHtcclxuICBmb250LXNpemU6IDEuMjUwdnc7XHJcbiAgbWFyZ2luLXRvcDogMS45Mjd2dztcclxufVxyXG5cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xyXG4gIC5wcmljaW5nX19pdGVtIHAge1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNDtcclxuICB9XHJcblxyXG4gIC5wcmljaW5nX193cmFwLmNvbnRhaW5lciB7XHJcbiAgICB3aWR0aDogNzklO1xyXG4gIH1cclxuXHJcbiAgLnByaWNpbmdfX2l0ZW0gYSB7XHJcbiAgICB3aWR0aDogMjYzcHg7XHJcbiAgICBoZWlnaHQ6IDM4NXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyMnB4O1xyXG4gICAgcGFkZGluZy1yaWdodDogMjJweDtcclxuICB9XHJcbiAgLnByaWNpbmdfX2l0ZW0gLmJlc3QtZGVhbCB7XHJcbiAgICB3aWR0aDogMjY1cHg7XHJcbiAgICBoZWlnaHQ6IDQ0M3B4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyMnB4O1xyXG4gICAgcGFkZGluZy1yaWdodDogMjJweDtcclxuICB9XHJcblxyXG4gIC5wcmljaW5nX19pdGVtIC5xdWFsaXR5LFxyXG4gIC5wcmljaW5nX19pdGVtIC5iZXN0LWRlYWwgLnF1YWxpdHkge1xyXG4gICAgbWFyZ2luOiAxMXB4IDAgMjZweDtcclxuICB9XHJcblxyXG4gIC5wcmljaW5nX19pdGVtIC5wcmljZSxcclxuICAucHJpY2luZ19faXRlbSAuYmVzdC1kZWFsIC5wcmljZSB7XHJcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAucHJpY2luZ19faXRlbSBhIHtcclxuICAgIHBhZGRpbmctdG9wOiA1NXB4O1xyXG4gIH1cclxuXHJcbiAgLnByaWNpbmdfX2l0ZW0gLmJlc3QtZGVhbCB7XHJcbiAgICB3aWR0aDogMjYwcHg7XHJcbiAgfVxyXG5cclxuICAucHJpY2luZ19faXRlbSBhIHtcclxuICAgIHdpZHRoOiAyNTNweDtcclxuICB9XHJcblxyXG4gIC5wcmljaW5nX19pdGVtIC5xdWFsaXR5IHtcclxuICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICB9XHJcblxyXG4gIC5wcmljaW5nX19pdGVtIHAge1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gIH1cclxuXHJcbiAgLnByaWNpbmdfX3dyYXAuY29udGFpbmVyIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLnByaWNpbmdfX2l0ZW0gLmJlc3QtZGVhbCB7XHJcbiAgICBwYWRkaW5nLXRvcDogNTVweDtcclxuICB9XHJcblxyXG4gIC5wcmljaW5nX19pdGVtIC5wcmljZSB7XHJcbiAgICBmb250LXNpemU6IDE5cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxMnB4O1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcclxuICAucHJpY2luZ19faXRlbTpmaXJzdC1jaGlsZCwgLnByaWNpbmdfX2l0ZW06bGFzdC1jaGlsZCB7XHJcbiAgICBwYWRkaW5nLXRvcDogMDtcclxuICB9XHJcblxyXG4gIC5wcmljaW5nX19pdGVtIGEge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgLnByaWNpbmdfX2l0ZW0ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG5cclxuICAucHJpY2luZ19fd3JhcC5jb250YWluZXIge1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gIH1cclxuICBoMS5wYWdlLXRpdGxlIHtcclxuICAgIGZvbnQtc2l6ZTogNDZweDtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ1MHB4KSB7XHJcbiAgLnByaWNpbmcgLmhlYWRlcl9fbG9nbyBpbWcge1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgbWluLXdpZHRoOiAxMDBweDtcclxuICB9XHJcbn1cclxuIiwiLnByaWNpbmdfX3dyYXAge1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuXG4ucHJpY2luZ19faXRlbSBhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnByaWNpbmdfX2l0ZW0gLnF1YWxpdHkge1xuICBjb2xvcjogIzAwMDAwMDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLnByaWNpbmdfX2l0ZW0gcCB7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBmb250LXdlaWdodDogNDAwO1xuICBtYXJnaW46IDA7XG59XG5cbi5wcmljaW5nX19pdGVtIC5wcmljZSB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi5iYXNpYyAucHJpY2Uge1xuICBjb2xvcjogIzAwYjZjYztcbn1cblxuLmFkdmFuY2VkIC5wcmljZSB7XG4gIGNvbG9yOiAjYTgwNTRmO1xufVxuXG4ucHJvIC5wcmljZSB7XG4gIGNvbG9yOiAjZjk4MDA4O1xufVxuXG4uYmFzaWMgYSB7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1nL2Jhc2ljLWJnLnBuZyk7XG59XG5cbi5hZHZhbmNlZCBhIHtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWcvYWR2YW5jZWQtYmcucG5nKTtcbn1cblxuLnBybyBhIHtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWcvcHJvLWJnLnBuZyk7XG59XG5cbi5wcmljaW5nX193cmFwIHtcbiAgcGFkZGluZy10b3A6IDYuNTYzdnc7XG4gIHBhZGRpbmctYm90dG9tOiAxLjA5NHZ3O1xufVxuXG4ucHJpY2luZ19faXRlbTpmaXJzdC1jaGlsZCxcbi5wcmljaW5nX19pdGVtOmxhc3QtY2hpbGQge1xuICBwYWRkaW5nLXRvcDogMi4wODN2dztcbn1cblxuLnByaWNpbmdfX2l0ZW0gYSB7XG4gIHdpZHRoOiAxNi45Nzl2dztcbiAgcGFkZGluZy10b3A6IDMuNDl2dztcbiAgcGFkZGluZy1ib3R0b206IDUuNDE3dnc7XG59XG5cbi5wcmljaW5nX19pdGVtIC5iZXN0LWRlYWwge1xuICB3aWR0aDogMTguMDIxdnc7XG4gIHBhZGRpbmctdG9wOiA0Ljk0OHZ3O1xuICBwYWRkaW5nLWJvdHRvbTogNi42MTV2dztcbn1cblxuLnByaWNpbmdfX2l0ZW0gLmJlc3QtZGVhbCAucHJpY2Uge1xuICBtYXJnaW4tdG9wOiAyLjA4M3Z3O1xufVxuXG4ucHJpY2luZ19faXRlbSAuYmVzdC1kZWFsIC5xdWFsaXR5IHtcbiAgbWFyZ2luOiAwLjkzOHZ3IDAgMi4xODh2dztcbn1cblxuLnByaWNpbmdfX2l0ZW0gLnF1YWxpdHkge1xuICBmb250LXNpemU6IDIuMjkydnc7XG4gIG1hcmdpbjogMC4xNTZ2dyAwIDJ2dztcbn1cblxuLnByaWNpbmdfX2l0ZW0gcCB7XG4gIGZvbnQtc2l6ZTogMC45Mzh2dztcbiAgbGluZS1oZWlnaHQ6IDEuNDA2dnc7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5wcmljaW5nX19pdGVtIC5wcmljZSB7XG4gIGZvbnQtc2l6ZTogMS4yNXZ3O1xuICBtYXJnaW4tdG9wOiAxLjkyN3Z3O1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcbiAgLnByaWNpbmdfX2l0ZW0gcCB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjQ7XG4gIH1cblxuICAucHJpY2luZ19fd3JhcC5jb250YWluZXIge1xuICAgIHdpZHRoOiA3OSU7XG4gIH1cblxuICAucHJpY2luZ19faXRlbSBhIHtcbiAgICB3aWR0aDogMjYzcHg7XG4gICAgaGVpZ2h0OiAzODVweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDIycHg7XG4gICAgcGFkZGluZy1yaWdodDogMjJweDtcbiAgfVxuXG4gIC5wcmljaW5nX19pdGVtIC5iZXN0LWRlYWwge1xuICAgIHdpZHRoOiAyNjVweDtcbiAgICBoZWlnaHQ6IDQ0M3B4O1xuICAgIHBhZGRpbmctbGVmdDogMjJweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMnB4O1xuICB9XG5cbiAgLnByaWNpbmdfX2l0ZW0gLnF1YWxpdHksXG4ucHJpY2luZ19faXRlbSAuYmVzdC1kZWFsIC5xdWFsaXR5IHtcbiAgICBtYXJnaW46IDExcHggMCAyNnB4O1xuICB9XG5cbiAgLnByaWNpbmdfX2l0ZW0gLnByaWNlLFxuLnByaWNpbmdfX2l0ZW0gLmJlc3QtZGVhbCAucHJpY2Uge1xuICAgIG1hcmdpbi10b3A6IDI1cHg7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XG4gIC5wcmljaW5nX19pdGVtIGEge1xuICAgIHBhZGRpbmctdG9wOiA1NXB4O1xuICB9XG5cbiAgLnByaWNpbmdfX2l0ZW0gLmJlc3QtZGVhbCB7XG4gICAgd2lkdGg6IDI2MHB4O1xuICB9XG5cbiAgLnByaWNpbmdfX2l0ZW0gYSB7XG4gICAgd2lkdGg6IDI1M3B4O1xuICB9XG5cbiAgLnByaWNpbmdfX2l0ZW0gLnF1YWxpdHkge1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgfVxuXG4gIC5wcmljaW5nX19pdGVtIHAge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxuXG4gIC5wcmljaW5nX193cmFwLmNvbnRhaW5lciB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cblxuICAucHJpY2luZ19faXRlbSAuYmVzdC1kZWFsIHtcbiAgICBwYWRkaW5nLXRvcDogNTVweDtcbiAgfVxuXG4gIC5wcmljaW5nX19pdGVtIC5wcmljZSB7XG4gICAgZm9udC1zaXplOiAxOXB4O1xuICAgIG1hcmdpbi10b3A6IDEycHg7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gIC5wcmljaW5nX19pdGVtOmZpcnN0LWNoaWxkLCAucHJpY2luZ19faXRlbTpsYXN0LWNoaWxkIHtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgfVxuXG4gIC5wcmljaW5nX19pdGVtIGEge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgfVxuXG4gIC5wcmljaW5nX19pdGVtIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICAucHJpY2luZ19fd3JhcC5jb250YWluZXIge1xuICAgIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xuICAgIC1tcy1mbGV4LXdyYXA6IHdyYXA7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9XG5cbiAgaDEucGFnZS10aXRsZSB7XG4gICAgZm9udC1zaXplOiA0NnB4O1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0NTBweCkge1xuICAucHJpY2luZyAuaGVhZGVyX19sb2dvIGltZyB7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIG1pbi13aWR0aDogMTAwcHg7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/main/pricing/pricing.component.ts":
/*!***************************************************!*\
  !*** ./src/app/main/pricing/pricing.component.ts ***!
  \***************************************************/
/*! exports provided: PricingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PricingComponent", function() { return PricingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _shared_services_static_texts_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/static-texts.service */ "./src/app/shared/services/static-texts.service.ts");



let PricingComponent = class PricingComponent {
    constructor(staticTextService) {
        this.staticTextService = staticTextService;
    }
    ngOnInit() {
        this.getPrice();
    }
    getPrice() {
        this.staticTextService.getPricing()
            .subscribe(res => {
            res['data'].forEach(item => {
                if (item.id === 1) {
                    this.basic = item;
                }
                if (item.id === 2) {
                    this.advanced = item;
                }
                if (item.id === 3) {
                    this.pro = item;
                }
            });
        });
    }
};
PricingComponent.ctorParameters = () => [
    { type: _shared_services_static_texts_service__WEBPACK_IMPORTED_MODULE_2__["StaticTextsService"] }
];
PricingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-pricing',
        template: __webpack_require__(/*! raw-loader!./pricing.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/pricing/pricing.component.html"),
        styles: [__webpack_require__(/*! ./pricing.component.scss */ "./src/app/main/pricing/pricing.component.scss")]
    })
], PricingComponent);



/***/ }),

/***/ "./src/app/main/profile/profile.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/main/profile/profile.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-reg-wrap__item {\n  -webkit-box-pack: start;\n          justify-content: flex-start;\n  padding-left: 1.823vw;\n  display: -webkit-box;\n  display: flex;\n}\n.login-reg-wrap__item .login-reg-network {\n  padding-top: 3.281vw;\n  text-align: center;\n}\n.login-reg-wrap__item .login-reg-network h2 {\n  font-weight: 700;\n  position: relative;\n  letter-spacing: 0.052vw;\n  margin-bottom: 3.646vw;\n}\n.login-reg-wrap__item .login-reg-network h2:after {\n  content: \"\";\n  position: absolute;\n  left: 50%;\n  -webkit-transform: translate(-50%);\n          transform: translate(-50%);\n  background-color: #000000;\n  top: 2.135vw;\n  width: 10.417vw;\n  height: 0.781vw;\n}\n.login-reg-wrap__item .login-reg-network p {\n  font-weight: 700;\n  letter-spacing: 1px;\n}\n.login-reg-wrap__item .login-reg-network .title {\n  font-size: 0.938vw;\n}\n.login-reg-wrap__item .login-reg-network .btn-wrap {\n  text-align: center;\n  padding-bottom: 1.771vw;\n}\n.login-reg-wrap__item .login-reg-network .btn-custom.facebook,\n.login-reg-wrap__item .login-reg-network .btn-custom.google {\n  font-size: 0.833vw;\n  width: 14.583vw;\n  height: 2.5vw;\n  padding-left: 0.417vw;\n}\n.login-reg-wrap__item .login-reg-network .btn-custom.facebook p,\n.login-reg-wrap__item .login-reg-network .btn-custom.google p {\n  padding-left: 0.521vw;\n}\n.login-reg-wrap__item .login-reg-network .btn-custom.facebook img,\n.login-reg-wrap__item .login-reg-network .btn-custom.google img {\n  width: 1.719vw;\n  height: 1.719vw;\n}\n.login-reg-wrap__item .login-reg-form {\n  margin-right: 11.146vw;\n  text-align: center;\n}\n.login-reg-wrap__item .login-reg-form h2 {\n  font-weight: 700;\n  font-size: 1.563vw;\n  letter-spacing: 0.052vw;\n}\n.login-reg-wrap__item .login-reg-form .input-wrap {\n  position: relative;\n  margin-bottom: 1.719vw;\n}\n.login-reg-wrap__item .login-reg-form .input-wrap p {\n  position: absolute;\n  margin: 0;\n  color: #cc0033;\n}\n.login-reg-wrap__item .login-reg-form input {\n  width: 22.5vw;\n}\n.login-reg-wrap__item .login-reg-form form {\n  padding-top: 0.469vw;\n}\n.login-reg-wrap__item .btn-wrap {\n  text-align: right;\n}\n.login-reg-wrap__item .last-child {\n  margin-bottom: 0.99vw;\n}\n.login-reg-wrap__item form {\n  margin-bottom: 2.552vw;\n}\n.btn-custom:not(.facebook):not(.google) {\n  height: 2.344vw;\n  width: 7.813vw;\n}\n.facebook,\n.google {\n  font-weight: 700;\n  text-transform: uppercase;\n}\n.facebook p,\n.google p {\n  margin: 0 !important;\n}\n.btn-custom.facebook,\n.btn-custom.google {\n  -webkit-box-pack: start;\n          justify-content: flex-start;\n}\n.btn-custom.facebook {\n  background-color: #3b5998;\n  border-color: #3b5998;\n}\n.btn-custom.google {\n  background-color: #c74a2d;\n  border-color: #c74a2d;\n}\n@media screen and (max-width: 1200px) {\n  .login-reg-wrap__item .login-reg-form h2 {\n    font-size: 19px;\n  }\n  .login-reg-wrap__item .login-reg-form p {\n    font-size: 14px;\n  }\n  .login-reg-wrap__item .login-reg-network .title {\n    font-size: 14px;\n  }\n  .login-reg-wrap__item .login-reg-network p {\n    font-size: 14px;\n  }\n  .login-reg-wrap__item .login-reg-network .btn-custom.facebook,\n.login-reg-wrap__item .login-reg-network .btn-custom.google {\n    width: 216px;\n    padding-left: 5px;\n  }\n}\n@media screen and (max-width: 992px) {\n  .login-reg-wrap__item .login-reg-network h2:after {\n    bottom: -11px;\n    width: 50%;\n    top: auto;\n    height: 6px;\n  }\n  .login-reg-wrap__item .login-reg-network .title {\n    font-size: 14px;\n  }\n  .login-reg-wrap__item .login-reg-network .btn-custom.facebook,\n.login-reg-wrap__item .login-reg-network .btn-custom.google {\n    height: 40px;\n    width: 100%;\n  }\n  .login-reg-wrap__item .login-reg-network .btn-custom.facebook img,\n.login-reg-wrap__item .login-reg-network .btn-custom.google img {\n    width: 28px;\n    height: 28px;\n  }\n  .login-reg-wrap__item .login-reg-form {\n    margin-right: 20px;\n  }\n  .login-reg-wrap__item .login-reg-form h2 {\n    font-size: 20px;\n    margin-bottom: 20px;\n  }\n  .login-reg-wrap__item .login-reg-form .input-wrap {\n    margin-bottom: 24px;\n  }\n  .login-reg-wrap__item .login-reg-form .input-wrap input {\n    width: 250px;\n  }\n  .login-reg-wrap__item .login-reg-form .btn-custom {\n    width: auto;\n    height: 40px;\n    border-radius: 20px;\n  }\n  .login-reg-wrap__item .login-reg-form .last-child {\n    margin-bottom: 0.99vw;\n  }\n}\n@media screen and (max-width: 767px) {\n  .login-reg-wrap__item {\n    width: 100%;\n    padding: 0 20px 40px !important;\n  }\n  .login-reg-wrap__item .login-reg-form {\n    width: 50%;\n  }\n  .login-reg-wrap__item .login-reg-form .input-wrap input {\n    width: 100%;\n  }\n  .login-reg-wrap__item .login-reg-network .title {\n    margin-bottom: 20px;\n  }\n}\n@media screen and (max-width: 600px) {\n  .login-reg-wrap__item {\n    flex-wrap: wrap;\n  }\n  .login-reg-wrap__item .login-reg-form {\n    margin: 0;\n    width: 100%;\n  }\n  .login-reg-wrap__item .login-reg-network {\n    width: 100%;\n  }\n  .login-reg-wrap__item .login-reg-network .btn-custom.facebook,\n.login-reg-wrap__item .login-reg-network .btn-custom.google {\n    width: 300px;\n    max-width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9wcm9maWxlL0M6XFxVc2Vyc1xcVmxhZERcXERlc2t0b3BcXE1lbW8gKGFsbClcXG1lbW8vc3JjXFxhcHBcXG1haW5cXHByb2ZpbGVcXHByb2ZpbGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21haW4vcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBR0UsdUJBQUE7VUFBQSwyQkFBQTtFQUNBLHFCQUFBO0VBR0Esb0JBQUE7RUFBQSxhQUFBO0FDQ0Y7QURBRTtFQUNFLG9CQUFBO0VBQ0Esa0JBQUE7QUNFSjtBRERJO0VBQ0UsZ0JBQUE7RUFFQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7QUNFTjtBRERNO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGtDQUFBO1VBQUEsMEJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQ0dSO0FEQUk7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDRU47QURBSTtFQUNFLGtCQUFBO0FDRU47QURBSTtFQUNFLGtCQUFBO0VBQ0EsdUJBQUE7QUNFTjtBRENJOztFQUVFLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtBQ0NOO0FEQU07O0VBQ0UscUJBQUE7QUNHUjtBRERNOztFQUNFLGNBQUE7RUFDQSxlQUFBO0FDSVI7QURBRTtFQUNFLHNCQUFBO0VBQ0Esa0JBQUE7QUNFSjtBRERJO0VBQ0UsZ0JBQUE7RUFFQSxrQkFBQTtFQUNBLHVCQUFBO0FDRU47QURBSTtFQUNFLGtCQUFBO0VBQ0Esc0JBQUE7QUNFTjtBRERNO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsY0FBQTtBQ0dSO0FEQUk7RUFDRSxhQUFBO0FDRU47QURBSTtFQUNHLG9CQUFBO0FDRVA7QURFRTtFQUNFLGlCQUFBO0FDQUo7QURFRTtFQUNFLHFCQUFBO0FDQUo7QURFRTtFQUNFLHNCQUFBO0FDQUo7QURJQTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FDREY7QURJQTs7RUFFRSxnQkFBQTtFQUNBLHlCQUFBO0FDREY7QURJQTs7RUFFRSxvQkFBQTtBQ0RGO0FESUE7O0VBSUUsdUJBQUE7VUFBQSwyQkFBQTtBQ0RGO0FESUE7RUFDRSx5QkFBQTtFQUNBLHFCQUFBO0FDREY7QURJQTtFQUNFLHlCQUFBO0VBQ0EscUJBQUE7QUNERjtBRElBO0VBSU07SUFDRSxlQUFBO0VDSk47RURNSTtJQUNFLGVBQUE7RUNKTjtFRFFJO0lBQ0UsZUFBQTtFQ05OO0VEUUk7SUFDRSxlQUFBO0VDTk47RURRSTs7SUFFRSxZQUFBO0lBQ0EsaUJBQUE7RUNOTjtBQUNGO0FEV0E7RUFJTTtJQUNFLGFBQUE7SUFDQSxVQUFBO0lBQ0EsU0FBQTtJQUNBLFdBQUE7RUNaTjtFRGNJO0lBQ0UsZUFBQTtFQ1pOO0VEY0k7O0lBRUUsWUFBQTtJQUNBLFdBQUE7RUNaTjtFRGFNOztJQUNFLFdBQUE7SUFDQSxZQUFBO0VDVlI7RURjRTtJQUNFLGtCQUFBO0VDWko7RURhSTtJQUNFLGVBQUE7SUFDQSxtQkFBQTtFQ1hOO0VEYUk7SUFDRSxtQkFBQTtFQ1hOO0VEWU07SUFDRSxZQUFBO0VDVlI7RURhSTtJQUNFLFdBQUE7SUFDQSxZQUFBO0lBQ0EsbUJBQUE7RUNYTjtFRGFJO0lBQ0UscUJBQUE7RUNYTjtBQUNGO0FEaUJBO0VBQ0U7SUFDRSxXQUFBO0lBQ0EsK0JBQUE7RUNmRjtFRGdCRTtJQUNFLFVBQUE7RUNkSjtFRGdCTTtJQUNFLFdBQUE7RUNkUjtFRGtCRTtJQUNFLG1CQUFBO0VDaEJKO0FBQ0Y7QURvQkE7RUFDRTtJQUdFLGVBQUE7RUNsQkY7RURtQkU7SUFDRSxTQUFBO0lBQ0EsV0FBQTtFQ2pCSjtFRG1CRTtJQUNFLFdBQUE7RUNqQko7RURrQkk7O0lBRUUsWUFBQTtJQUNBLGVBQUE7RUNoQk47QUFDRiIsImZpbGUiOiJzcmMvYXBwL21haW4vcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ2luLXJlZy13cmFwX19pdGVtIHtcclxuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgcGFkZGluZy1sZWZ0OiAxLjgyM3Z3O1xyXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIC5sb2dpbi1yZWctbmV0d29yayB7XHJcbiAgICBwYWRkaW5nLXRvcDogMy4yODF2dztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGgyIHtcclxuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgICAgLy9sZXR0ZXItc3BhY2luZzogMXB4O1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIGxldHRlci1zcGFjaW5nOiAwLjA1MnZ3O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAzLjY0NnZ3O1xyXG4gICAgICAmOmFmdGVyIHtcclxuICAgICAgICBjb250ZW50OiAnJztcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgbGVmdDogNTAlO1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUpO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XHJcbiAgICAgICAgdG9wOiAyLjEzNXZ3O1xyXG4gICAgICAgIHdpZHRoOiAxMC40MTd2dztcclxuICAgICAgICBoZWlnaHQ6IDAuNzgxdnc7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHAge1xyXG4gICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xyXG4gICAgfVxyXG4gICAgLnRpdGxlIHtcclxuICAgICAgZm9udC1zaXplOiAwLjkzOHZ3O1xyXG4gICAgfVxyXG4gICAgLmJ0bi13cmFwIHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMS43NzF2dztcclxuXHJcbiAgICB9XHJcbiAgICAuYnRuLWN1c3RvbS5mYWNlYm9vayxcclxuICAgIC5idG4tY3VzdG9tLmdvb2dsZSB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMC44MzN2dztcclxuICAgICAgd2lkdGg6IDE0LjU4M3Z3O1xyXG4gICAgICBoZWlnaHQ6IDIuNTAwdnc7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMC40MTd2dztcclxuICAgICAgcCB7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwLjUyMXZ3O1xyXG4gICAgICB9XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDEuNzE5dnc7XHJcbiAgICAgICAgaGVpZ2h0OiAxLjcxOXZ3O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5sb2dpbi1yZWctZm9ybSB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDExLjE0NnZ3O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgaDIge1xyXG4gICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICAvL2xldHRlci1zcGFjaW5nOiAxcHg7XHJcbiAgICAgIGZvbnQtc2l6ZTogMS41NjN2dztcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMDUydnc7XHJcbiAgICB9XHJcbiAgICAuaW5wdXQtd3JhcCB7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMS43MTl2dztcclxuICAgICAgcCB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICBjb2xvcjogI2NjMDAzMztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgaW5wdXQge1xyXG4gICAgICB3aWR0aDogMjIuNTAwdnc7XHJcbiAgICB9XHJcbiAgICBmb3JtIHtcclxuICAgICAgIHBhZGRpbmctdG9wOiAwLjQ2OXZ3O1xyXG4gICAgIH1cclxuXHJcbiAgfVxyXG4gIC5idG4td3JhcCB7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICB9XHJcbiAgLmxhc3QtY2hpbGQge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC45OTB2dztcclxuICB9XHJcbiAgZm9ybSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyLjU1MnZ3O1xyXG4gIH1cclxufVxyXG5cclxuLmJ0bi1jdXN0b206bm90KC5mYWNlYm9vayk6bm90KC5nb29nbGUpIHtcclxuICBoZWlnaHQ6IDIuMzQ0dnc7XHJcbiAgd2lkdGg6IDcuODEzdnc7XHJcbn1cclxuXHJcbi5mYWNlYm9vayxcclxuLmdvb2dsZSB7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG4uZmFjZWJvb2sgcCxcclxuLmdvb2dsZSBwIHtcclxuICBtYXJnaW46IDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmJ0bi1jdXN0b20uZmFjZWJvb2ssXHJcbi5idG4tY3VzdG9tLmdvb2dsZSB7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgLW1zLWZsZXgtcGFjazogc3RhcnQ7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG59XHJcblxyXG4uYnRuLWN1c3RvbS5mYWNlYm9vayB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNiNTk5ODtcclxuICBib3JkZXItY29sb3I6ICMzYjU5OTg7XHJcbn1cclxuXHJcbi5idG4tY3VzdG9tLmdvb2dsZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2M3NGEyZDtcclxuICBib3JkZXItY29sb3I6ICNjNzRhMmQ7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xyXG5cclxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0ge1xyXG4gICAgLmxvZ2luLXJlZy1mb3JtIHtcclxuICAgICAgaDIge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTlweDtcclxuICAgICAgfVxyXG4gICAgICBwIHtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5sb2dpbi1yZWctbmV0d29yayB7XHJcbiAgICAgIC50aXRsZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICB9XHJcbiAgICAgIHAge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgfVxyXG4gICAgICAuYnRuLWN1c3RvbS5mYWNlYm9vayxcclxuICAgICAgLmJ0bi1jdXN0b20uZ29vZ2xlIHtcclxuICAgICAgICB3aWR0aDogMjE2cHg7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XHJcblxyXG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSB7XHJcbiAgICAubG9naW4tcmVnLW5ldHdvcmsge1xyXG4gICAgICBoMjphZnRlciB7XHJcbiAgICAgICAgYm90dG9tOiAtMTFweDtcclxuICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgIHRvcDogYXV0bztcclxuICAgICAgICBoZWlnaHQ6IDZweDtcclxuICAgICAgfVxyXG4gICAgICAudGl0bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgfVxyXG4gICAgICAuYnRuLWN1c3RvbS5mYWNlYm9vayxcclxuICAgICAgLmJ0bi1jdXN0b20uZ29vZ2xlIHtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyOHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAyOHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmxvZ2luLXJlZy1mb3JtIHtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gICAgICBoMiB7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgIH1cclxuICAgICAgLmlucHV0LXdyYXAge1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XHJcbiAgICAgICAgaW5wdXQge1xyXG4gICAgICAgICAgd2lkdGg6IDI1MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAuYnRuLWN1c3RvbSB7XHJcbiAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICAgIH1cclxuICAgICAgLmxhc3QtY2hpbGQge1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDAuOTkwdnc7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmc6IDAgMjBweCA0MHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAubG9naW4tcmVnLWZvcm0ge1xyXG4gICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAuaW5wdXQtd3JhcCB7XHJcbiAgICAgICAgaW5wdXQge1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAubG9naW4tcmVnLW5ldHdvcmsgLnRpdGxlIHtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XHJcbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIHtcclxuICAgIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xyXG4gICAgLW1zLWZsZXgtd3JhcDogd3JhcDtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgIC5sb2dpbi1yZWctZm9ybSB7XHJcbiAgICAgIG1hcmdpbjogMDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAubG9naW4tcmVnLW5ldHdvcmsge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgLmJ0bi1jdXN0b20uZmFjZWJvb2ssXHJcbiAgICAgIC5idG4tY3VzdG9tLmdvb2dsZSB7XHJcbiAgICAgICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuIiwiLmxvZ2luLXJlZy13cmFwX19pdGVtIHtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIHBhZGRpbmctbGVmdDogMS44MjN2dztcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsge1xuICBwYWRkaW5nLXRvcDogMy4yODF2dztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayBoMiB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDUydnc7XG4gIG1hcmdpbi1ib3R0b206IDMuNjQ2dnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIGgyOmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwO1xuICB0b3A6IDIuMTM1dnc7XG4gIHdpZHRoOiAxMC40MTd2dztcbiAgaGVpZ2h0OiAwLjc4MXZ3O1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayBwIHtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLnRpdGxlIHtcbiAgZm9udC1zaXplOiAwLjkzOHZ3O1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayAuYnRuLXdyYXAge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmctYm90dG9tOiAxLjc3MXZ3O1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayAuYnRuLWN1c3RvbS5mYWNlYm9vayxcbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZ29vZ2xlIHtcbiAgZm9udC1zaXplOiAwLjgzM3Z3O1xuICB3aWR0aDogMTQuNTgzdnc7XG4gIGhlaWdodDogMi41dnc7XG4gIHBhZGRpbmctbGVmdDogMC40MTd2dztcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZmFjZWJvb2sgcCxcbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZ29vZ2xlIHAge1xuICBwYWRkaW5nLWxlZnQ6IDAuNTIxdnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIC5idG4tY3VzdG9tLmZhY2Vib29rIGltZyxcbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZ29vZ2xlIGltZyB7XG4gIHdpZHRoOiAxLjcxOXZ3O1xuICBoZWlnaHQ6IDEuNzE5dnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMS4xNDZ2dztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSBoMiB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc2l6ZTogMS41NjN2dztcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDUydnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIC5pbnB1dC13cmFwIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAxLjcxOXZ3O1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSAuaW5wdXQtd3JhcCBwIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiAjY2MwMDMzO1xufVxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSBpbnB1dCB7XG4gIHdpZHRoOiAyMi41dnc7XG59XG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIGZvcm0ge1xuICBwYWRkaW5nLXRvcDogMC40Njl2dztcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAuYnRuLXdyYXAge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSAubGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDAuOTl2dztcbn1cbi5sb2dpbi1yZWctd3JhcF9faXRlbSBmb3JtIHtcbiAgbWFyZ2luLWJvdHRvbTogMi41NTJ2dztcbn1cblxuLmJ0bi1jdXN0b206bm90KC5mYWNlYm9vayk6bm90KC5nb29nbGUpIHtcbiAgaGVpZ2h0OiAyLjM0NHZ3O1xuICB3aWR0aDogNy44MTN2dztcbn1cblxuLmZhY2Vib29rLFxuLmdvb2dsZSB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5mYWNlYm9vayBwLFxuLmdvb2dsZSBwIHtcbiAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG59XG5cbi5idG4tY3VzdG9tLmZhY2Vib29rLFxuLmJ0bi1jdXN0b20uZ29vZ2xlIHtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIC1tcy1mbGV4LXBhY2s6IHN0YXJ0O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG59XG5cbi5idG4tY3VzdG9tLmZhY2Vib29rIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNiNTk5ODtcbiAgYm9yZGVyLWNvbG9yOiAjM2I1OTk4O1xufVxuXG4uYnRuLWN1c3RvbS5nb29nbGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzc0YTJkO1xuICBib3JkZXItY29sb3I6ICNjNzRhMmQ7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1mb3JtIGgyIHtcbiAgICBmb250LXNpemU6IDE5cHg7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSBwIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayAudGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIHAge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIC5idG4tY3VzdG9tLmZhY2Vib29rLFxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayAuYnRuLWN1c3RvbS5nb29nbGUge1xuICAgIHdpZHRoOiAyMTZweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayBoMjphZnRlciB7XG4gICAgYm90dG9tOiAtMTFweDtcbiAgICB3aWR0aDogNTAlO1xuICAgIHRvcDogYXV0bztcbiAgICBoZWlnaHQ6IDZweDtcbiAgfVxuICAubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIC50aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZmFjZWJvb2ssXG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIC5idG4tY3VzdG9tLmdvb2dsZSB7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZmFjZWJvb2sgaW1nLFxuLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayAuYnRuLWN1c3RvbS5nb29nbGUgaW1nIHtcbiAgICB3aWR0aDogMjhweDtcbiAgICBoZWlnaHQ6IDI4cHg7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSB7XG4gICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gaDIge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gLmlucHV0LXdyYXAge1xuICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSAuaW5wdXQtd3JhcCBpbnB1dCB7XG4gICAgd2lkdGg6IDI1MHB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gLmJ0bi1jdXN0b20ge1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIGhlaWdodDogNDBweDtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0gLmxhc3QtY2hpbGQge1xuICAgIG1hcmdpbi1ib3R0b206IDAuOTl2dztcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAwIDIwcHggNDBweCAhaW1wb3J0YW50O1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLWZvcm0ge1xuICAgIHdpZHRoOiA1MCU7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSAuaW5wdXQtd3JhcCBpbnB1dCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctbmV0d29yayAudGl0bGUge1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSB7XG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XG4gICAgLW1zLWZsZXgtd3JhcDogd3JhcDtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gIH1cbiAgLmxvZ2luLXJlZy13cmFwX19pdGVtIC5sb2dpbi1yZWctZm9ybSB7XG4gICAgbWFyZ2luOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5sb2dpbi1yZWctd3JhcF9faXRlbSAubG9naW4tcmVnLW5ldHdvcmsgLmJ0bi1jdXN0b20uZmFjZWJvb2ssXG4ubG9naW4tcmVnLXdyYXBfX2l0ZW0gLmxvZ2luLXJlZy1uZXR3b3JrIC5idG4tY3VzdG9tLmdvb2dsZSB7XG4gICAgd2lkdGg6IDMwMHB4O1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/main/profile/profile.component.ts":
/*!***************************************************!*\
  !*** ./src/app/main/profile/profile.component.ts ***!
  \***************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProfileComponent = class ProfileComponent {
    constructor() { }
    ngOnInit() {
    }
};
ProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: __webpack_require__(/*! raw-loader!./profile.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/profile/profile.component.html"),
        styles: [__webpack_require__(/*! ./profile.component.scss */ "./src/app/main/profile/profile.component.scss")]
    })
], ProfileComponent);



/***/ }),

/***/ "./src/app/main/project-in-progress/project-in-progress.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/main/project-in-progress/project-in-progress.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".warning {\n  color: #db0000;\n  font-weight: 700;\n}\n\n/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/\n\n@media screen and (min-width: 992px) {\n  .warning {\n    font-size: 0.833vw;\n    margin-top: 0.417vw;\n  }\n\n  .gallery__wrap:last-child {\n    margin-bottom: 0.625vw;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9wcm9qZWN0LWluLXByb2dyZXNzL0M6XFxVc2Vyc1xcVmxhZERcXERlc2t0b3BcXE1lbW8gKGFsbClcXG1lbW8vc3JjXFxhcHBcXG1haW5cXHByb2plY3QtaW4tcHJvZ3Jlc3NcXHByb2plY3QtaW4tcHJvZ3Jlc3MuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21haW4vcHJvamVjdC1pbi1wcm9ncmVzcy9wcm9qZWN0LWluLXByb2dyZXNzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0FDQ0Y7O0FERUEseUhBQUE7O0FBRUE7RUFDRTtJQUNFLGtCQUFBO0lBQ0EsbUJBQUE7RUNBRjs7RURHQTtJQUNFLHNCQUFBO0VDQUY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL21haW4vcHJvamVjdC1pbi1wcm9ncmVzcy9wcm9qZWN0LWluLXByb2dyZXNzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndhcm5pbmcge1xyXG4gIGNvbG9yOiAjZGIwMDAwO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbi8qKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKyovXHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA5OTJweCkge1xyXG4gIC53YXJuaW5nIHtcclxuICAgIGZvbnQtc2l6ZTogMC44MzN2dztcclxuICAgIG1hcmdpbi10b3A6IDAuNDE3dnc7XHJcbiAgfVxyXG5cclxuICAuZ2FsbGVyeV9fd3JhcDpsYXN0LWNoaWxkIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuNjI1dnc7XHJcbiAgfVxyXG59XHJcbiIsIi53YXJuaW5nIHtcbiAgY29sb3I6ICNkYjAwMDA7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi8qKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKyovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA5OTJweCkge1xuICAud2FybmluZyB7XG4gICAgZm9udC1zaXplOiAwLjgzM3Z3O1xuICAgIG1hcmdpbi10b3A6IDAuNDE3dnc7XG4gIH1cblxuICAuZ2FsbGVyeV9fd3JhcDpsYXN0LWNoaWxkIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwLjYyNXZ3O1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/main/project-in-progress/project-in-progress.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/main/project-in-progress/project-in-progress.component.ts ***!
  \***************************************************************************/
/*! exports provided: ProjectInProgressComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectInProgressComponent", function() { return ProjectInProgressComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProjectInProgressComponent = class ProjectInProgressComponent {
    constructor() { }
    ngOnInit() {
    }
};
ProjectInProgressComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-project-in-progress',
        template: __webpack_require__(/*! raw-loader!./project-in-progress.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/project-in-progress/project-in-progress.component.html"),
        styles: [__webpack_require__(/*! ./project-in-progress.component.scss */ "./src/app/main/project-in-progress/project-in-progress.component.scss")]
    })
], ProjectInProgressComponent);



/***/ }),

/***/ "./src/app/main/projects/projects.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/main/projects/projects.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url(\"https://fonts.googleapis.com/css?family=Berkshire+Swash&display=swap\");\n@import url(\"https://fonts.googleapis.com/css?family=Berkshire+Swash&display=swap\");\n.page-title {\n  width: 62.5%;\n}\n.creating-wrap {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n}\n.creating-wrap__item {\n  width: 48%;\n}\n.creating-wrap__item.collage {\n  display: -webkit-box;\n  display: flex;\n  border: 1px solid #85a3bd;\n}\n.creating-wrap__item .left {\n  display: -webkit-box;\n  display: flex;\n  flex-wrap: wrap;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  width: 57%;\n  position: relative;\n}\n.creating-wrap__item .right {\n  width: 43%;\n  text-align: right;\n  position: relative;\n}\n.collage__title {\n  font-family: \"Berkshire Swash\", cursive;\n  color: #ffffff;\n  font-weight: 400;\n  background-color: #80cfd4;\n  opacity: 0.9;\n  border-radius: 50%;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  text-align: center;\n}\n.tools {\n  position: absolute;\n  top: 0;\n  left: 4px;\n  padding: 5px 9px 11px 11px;\n  background: rgba(255, 255, 255, 0.8);\n}\n.tools div:last-child {\n  margin-top: 8px;\n}\n.creating-wrap__item .right img {\n  display: block;\n  margin: 0 0 0 auto;\n}\n.creating-wrap__item .video {\n  height: 100%;\n  position: relative;\n}\n.add-photo {\n  width: 50%;\n  background-color: #d6e2ec;\n}\n.add-photo img {\n  display: block;\n  width: 100%;\n  height: 100%;\n}\n.my-project__title {\n  color: #000000;\n  font-weight: 700;\n  position: relative;\n}\n.my-project__title:before {\n  content: \"\";\n  position: absolute;\n  background-color: #000000;\n  bottom: 100%;\n}\n.progress-wrap {\n  position: absolute;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  background: rgba(0, 0, 0, 0.3);\n}\n.progress-wrap p {\n  font-weight: 700;\n  color: #fff;\n  text-transform: uppercase;\n}\n.progress {\n  border: 1px solid #000000;\n  background: rgba(255, 255, 255, 0.5);\n  box-sizing: content-box;\n  position: relative;\n}\n.progress__bar {\n  width: 75%;\n  background-color: #ffffff;\n}\n.progress__counter {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  font-weight: 700;\n}\n.music-add {\n  background-color: #d6e2ec;\n  position: relative;\n}\n.music-add .tools {\n  background: none;\n}\n.start-finish {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  position: absolute;\n  left: 0;\n  right: 0;\n  bottom: 100%;\n  font-weight: 700;\n}\n.choose-frame {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n}\n.choose-frame__item {\n  cursor: pointer;\n}\n.choose-frame__item p {\n  color: #000000;\n  font-weight: 700;\n  font-style: italic;\n  text-align: right;\n}\n.choose-frame__item img {\n  display: block;\n  border: 1px solid #85a3bd;\n}\n.my-project__wrap .btn-wrap {\n  text-align: center;\n}\n.save-loader {\n  text-align: center;\n}\n.save-loader .progress {\n  margin: auto;\n}\n.save-loader .wait {\n  font-weight: 400;\n}\n.save-loader p {\n  font-weight: 700;\n}\n.popup {\n  text-align: center;\n}\n.add-music .btn-wrap.first,\n.music-library .btn-wrap.first {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  justify-content: space-between;\n  margin-top: 0;\n}\n.choose-quality {\n  font-weight: 400;\n}\n.choose-quality .title {\n  font-weight: 700;\n}\n.choose-quality .popup p {\n  margin: 0;\n  font-weight: 400;\n}\n.quality__title {\n  font-weight: 700;\n}\n.quality__item {\n  display: -webkit-box;\n  display: flex;\n}\n.quality__title {\n  position: relative;\n  width: 25%;\n  text-align: left;\n}\n.quality__value {\n  width: 50%;\n  text-align: left;\n}\n.quality__price {\n  width: 25%;\n  text-align: right;\n  font-weight: 700;\n}\n.quality__title:before {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  background: url('quality.png');\n  background-size: 100% 100%;\n}\n.quality__title:after {\n  content: \"\";\n  position: absolute;\n  background: url('quality2.png');\n  background-size: 100% 100%;\n  display: none;\n}\n.active .quality__title:after {\n  display: block;\n}\n.music__name {\n  text-align: left;\n}\n.music-library .btn-wrap.first .active {\n  background-color: #d6e2ec;\n}\n.music__item {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  color: #180a21;\n  font-weight: 700;\n  border-bottom: 1px solid #d6e2ec;\n}\n.music-library .music .music__item .btn-custom {\n  border: 1px solid #000000;\n  background-color: #f6f4f7;\n  color: #000;\n  min-width: 0;\n  box-sizing: content-box;\n}\n.music-library .music .music__item .btn-custom.active {\n  background: #000;\n  color: #fff;\n}\n.music__item .pause,\n.music__item.active .play {\n  display: none;\n}\n.music__item.active .pause {\n  display: inline-block;\n}\n.music-library .popup .btn-wrap .first {\n  margin: 0;\n}\n.music-library .btn-wrap.first {\n  margin: 0;\n}\n.music__progress img {\n  display: none;\n}\n.music__item.active .music__progress img {\n  display: block;\n  width: 9.635vw;\n}\n/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/\n.my-project .page-title {\n  padding-top: 0.781vw;\n  padding-left: 4.375vw;\n}\n.creating-wrap {\n  padding: 0 117px;\n}\n.my-project__title {\n  font-size: 30px;\n  margin: 30px 0 8px;\n  padding: 0 59px;\n}\n.my-project__title img {\n  padding-left: 8px;\n}\n.creating-wrap__item .left {\n  border: 0.417vw solid #fff;\n}\n.collage__title {\n  width: 6.302vw;\n  height: 6.302vw;\n  font-size: 1.563vw;\n}\n.add-photo {\n  border: 0.417vw solid #fff;\n}\n.my-project__title:before {\n  width: 10.417vw;\n  height: 0.781vw;\n}\n.progress-wrap p {\n  font-size: 0.938vw;\n  padding-bottom: 2.083vw;\n}\n.progress {\n  width: 34.167vw;\n  height: 2.344vw;\n  margin-bottom: 5.521vw;\n  border-radius: 1.198vw;\n}\n.progress__bar {\n  height: 2.344vw;\n  border-radius: 1.198vw;\n}\n.progress__counter {\n  font-size: 0.938vw;\n}\n.music-add {\n  padding: 0.885vw 3.125vw;\n  margin-top: 1.563vw;\n}\n.start-finish {\n  padding: 0 6.094vw;\n}\n.choose-frame {\n  padding: 0.833vw 0 0.26vw 3.125vw;\n}\n.choose-frame__item p {\n  margin: 0.677vw 0 0;\n  font-size: 0.833vw;\n  padding-right: 0.521vw;\n}\n.choose-frame__item.active img {\n  box-shadow: -0.521vw 0.521vw 0px 0px black;\n}\n.my-project__wrap .btn-wrap {\n  padding-bottom: 0.729vw;\n}\n.my-project__wrap .btn-wrap .light {\n  width: 10.938vw;\n  margin-right: 2.083vw;\n}\n.save-loader p {\n  font-size: 0.938vw;\n}\n.popup {\n  width: 40.625vw;\n}\n.popup .btn-custom:not([type=submit]) {\n  width: 10.938vw;\n}\n.add-music .popup {\n  padding-top: 3.073vw;\n  padding-bottom: 4.583vw;\n}\n.add-music .btn-wrap.first,\n.music-library .btn-wrap.first {\n  padding: 0 4.635vw;\n  margin-bottom: 2.76vw;\n}\n.add-music .btn-wrap {\n  margin-top: 1.927vw;\n}\n.add-music .btn-wrap.first + p {\n  margin-bottom: 1.198vw;\n}\n.choose-quality {\n  font-size: 1.25vw;\n}\n.choose-quality .popup {\n  padding-left: 7.188vw;\n  padding-right: 7.188vw;\n  padding-top: 1.51vw;\n}\n.choose-quality .title {\n  margin: 0 0 1.979vw;\n  font-size: 1.354vw;\n}\n.choose-quality .popup .subtitle {\n  margin-bottom: 2.917vw;\n}\n.choose-quality .popup p {\n  font-size: 1.354vw;\n}\n.choose-quality .btn-wrap {\n  padding-top: 0.521vw;\n  padding-bottom: 3.073vw;\n}\n.quality__item {\n  margin: 0 0 1.667vw;\n}\n.quality__title {\n  padding: 0 0 0 2.969vw;\n  font-size: 1.354vw;\n}\n.quality__value {\n  font-size: 1.354vw;\n}\n.quality__price {\n  padding-right: 2.344vw;\n  font-size: 1.354vw;\n}\n.quality__title:before {\n  width: 1.458vw;\n  height: 1.458vw;\n}\n.quality__title:after {\n  width: 1.302vw;\n  height: 1.302vw;\n  top: 0.052vw;\n  left: 0.365vw;\n}\n.music {\n  border-top: 0.417vw solid #d6e2ec;\n  margin-top: 1.094vw;\n}\n.music__name {\n  width: 9.948vw;\n  padding-left: 0.938vw;\n}\n.music__item {\n  padding: 1.354vw 5vw 0.208vw 3.073vw;\n  font-size: 0.938vw;\n}\n.music-library .music .music__item .btn-custom {\n  width: 3.333vw;\n  height: 1.667vw;\n  border-radius: 0.833vw;\n}\n.play-btn {\n  width: 1.094vw;\n}\n.music-library .popup {\n  padding-top: 3.073vw;\n  padding-bottom: 0.781vw;\n}\n.music__progress {\n  min-width: 9.635vw;\n}\n@media screen and (max-width: 1200px) {\n  .popup {\n    width: 75%;\n  }\n\n  .progress-wrap p {\n    font-size: 16px;\n  }\n\n  .creating-wrap {\n    padding: 0 20px;\n  }\n\n  .music-add {\n    padding: 30px 40px;\n    margin-top: 40px;\n  }\n\n  .music-library .search {\n    padding: 0 20px;\n  }\n\n  .music-library .search form {\n    padding-left: 0;\n  }\n\n  .music__item.active .music__progress img,\n.music__progress {\n    width: 15.417vw;\n  }\n\n  .choose-quality .title,\n.choose-quality .popup p,\n.quality__title,\n.quality__value,\n.quality__price {\n    font-size: 16px;\n  }\n\n  .quality__item {\n    margin-bottom: 20px;\n  }\n\n  .quality__title {\n    padding-left: 35px;\n  }\n\n  .quality__title:before {\n    width: 30px;\n    height: 30px;\n    top: -4px;\n  }\n\n  .quality__title:after {\n    width: 20px;\n    height: 20px;\n    top: 0;\n    left: 15px;\n  }\n\n  .save-loader p {\n    font-size: 16px;\n  }\n\n  .progress__counter {\n    font-size: 16px;\n  }\n\n  .progress {\n    width: 500px;\n    max-width: 90%;\n  }\n\n  .choose-frame__item p {\n    font-size: 14px;\n  }\n}\n@media screen and (max-width: 992px) {\n  .popup .btn-custom:not([type=submit]) {\n    width: auto;\n  }\n\n  .progress,\n.progress__bar {\n    height: 40px;\n    border-radius: 20px;\n  }\n\n  .music-library .music .music__item .btn-custom {\n    border-radius: 20px;\n    height: 30px;\n  }\n\n  .music__item {\n    font-size: 14px;\n    padding: 10px 20px;\n  }\n\n  .play-btn {\n    width: 15px;\n  }\n\n  .my-project__wrap .btn-wrap .light {\n    width: auto;\n  }\n\n  .choose-frame {\n    flex-wrap: wrap;\n    padding: 20px;\n  }\n\n  .choose-frame__item {\n    width: 20%;\n    padding: 5px;\n  }\n\n  .collage__title {\n    width: 70px;\n    height: 70px;\n    font-size: 20px;\n  }\n\n  .my-project__title {\n    padding: 0 20px;\n  }\n}\n@media screen and (max-width: 767px) {\n  .popup {\n    width: 95%;\n    padding-top: 50px;\n  }\n\n  .my-project .page-title {\n    -webkit-box-ordinal-group: 3;\n            order: 2;\n    width: 100%;\n    padding: 0 20px;\n  }\n\n  .choose-frame__item {\n    width: 33%;\n  }\n\n  .creating-wrap {\n    flex-wrap: wrap;\n  }\n\n  .creating-wrap__item {\n    width: 100%;\n  }\n\n  .creating-wrap__item .video {\n    height: 52vw;\n  }\n\n  .music__item.active .music__progress img,\n.music__progress {\n    width: 24.12vw;\n  }\n}\n@media screen and (max-width: 492px) {\n  .choose-frame__item {\n    width: 50%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9wcm9qZWN0cy9DOlxcVXNlcnNcXFZsYWREXFxEZXNrdG9wXFxNZW1vIChhbGwpXFxtZW1vL3NyY1xcYXBwXFxtYWluXFxwcm9qZWN0c1xccHJvamVjdHMuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21haW4vcHJvamVjdHMvcHJvamVjdHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQVEsbUZBQUE7QUFFQSxtRkFBQTtBQUVSO0VBQ0UsWUFBQTtBQ0RGO0FESUE7RUFHRSxvQkFBQTtFQUFBLGFBQUE7RUFHQSx5QkFBQTtVQUFBLDhCQUFBO0FDREY7QURJQTtFQUNFLFVBQUE7QUNERjtBRElBO0VBR0Usb0JBQUE7RUFBQSxhQUFBO0VBQ0EseUJBQUE7QUNERjtBRElBO0VBR0Usb0JBQUE7RUFBQSxhQUFBO0VBR0EsZUFBQTtFQUdBLHlCQUFBO1VBQUEsOEJBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7QUNERjtBRElBO0VBQ0UsVUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNERjtBRElBO0VBQ0UsdUNBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSx3Q0FBQTtVQUFBLGdDQUFBO0VBR0Esb0JBQUE7RUFBQSxhQUFBO0VBR0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLGtCQUFBO0FDREY7QURJQTtFQUNFLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSwwQkFBQTtFQUNBLG9DQUFBO0FDREY7QURJQTtFQUNFLGVBQUE7QUNERjtBRElBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0FDREY7QURJQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtBQ0RGO0FESUE7RUFDRSxVQUFBO0VBQ0EseUJBQUE7QUNERjtBRElBO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDREY7QURJQTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDREY7QURJQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtBQ0RGO0FESUE7RUFDRSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFHQSxvQkFBQTtFQUFBLGFBQUE7RUFHQSw0QkFBQTtFQUFBLDZCQUFBO1VBQUEsc0JBQUE7RUFHQSx3QkFBQTtVQUFBLHVCQUFBO0VBR0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLDhCQUFBO0FDREY7QURJQTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0FDREY7QURJQTtFQUNFLHlCQUFBO0VBQ0Esb0NBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0FDREY7QURJQTtFQUNFLFVBQUE7RUFDQSx5QkFBQTtBQ0RGO0FESUE7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0Esd0NBQUE7VUFBQSxnQ0FBQTtFQUNBLGdCQUFBO0FDREY7QURJQTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7QUNERjtBRElBO0VBQ0UsZ0JBQUE7QUNERjtBRElBO0VBR0Usb0JBQUE7RUFBQSxhQUFBO0VBR0EseUJBQUE7VUFBQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNERjtBRElBO0VBR0Usb0JBQUE7RUFBQSxhQUFBO0VBR0EseUJBQUE7VUFBQSw4QkFBQTtBQ0RGO0FESUE7RUFDRSxlQUFBO0FDREY7QURJQTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNERjtBRElBO0VBQ0UsY0FBQTtFQUNBLHlCQUFBO0FDREY7QURJQTtFQUNFLGtCQUFBO0FDREY7QURJQTtFQUNFLGtCQUFBO0FDREY7QURJQTtFQUNFLFlBQUE7QUNERjtBRElBO0VBQ0UsZ0JBQUE7QUNERjtBRElBO0VBQ0UsZ0JBQUE7QUNERjtBRElBO0VBQ0Usa0JBQUE7QUNERjtBRElBOztFQUlFLG9CQUFBO0VBQUEsYUFBQTtFQUdBLHlCQUFBO1VBQUEsOEJBQUE7RUFDQSw4QkFBQTtFQUNBLGFBQUE7QUNERjtBRElBO0VBQ0UsZ0JBQUE7QUNERjtBRElBO0VBQ0UsZ0JBQUE7QUNERjtBRElBO0VBQ0UsU0FBQTtFQUNBLGdCQUFBO0FDREY7QURJQTtFQUNFLGdCQUFBO0FDREY7QURLQTtFQUdFLG9CQUFBO0VBQUEsYUFBQTtBQ0ZGO0FES0E7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQ0ZGO0FES0E7RUFDRSxVQUFBO0VBQ0EsZ0JBQUE7QUNGRjtBREtBO0VBQ0UsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNGRjtBREtBO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLDhCQUFBO0VBQ0EsMEJBQUE7QUNGRjtBREtBO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsK0JBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7QUNGRjtBREtBO0VBQ0UsY0FBQTtBQ0ZGO0FES0E7RUFDRSxnQkFBQTtBQ0ZGO0FES0E7RUFDRSx5QkFBQTtBQ0ZGO0FES0E7RUFHRSxvQkFBQTtFQUFBLGFBQUE7RUFHQSx5QkFBQTtVQUFBLG1CQUFBO0VBR0EseUJBQUE7VUFBQSw4QkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdDQUFBO0FDRkY7QURLQTtFQUNFLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0FDRkY7QURLQTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBQ0ZGO0FES0E7O0VBRUUsYUFBQTtBQ0ZGO0FES0E7RUFDRSxxQkFBQTtBQ0ZGO0FES0E7RUFDRSxTQUFBO0FDRkY7QURLQTtFQUNFLFNBQUE7QUNGRjtBREtBO0VBQ0UsYUFBQTtBQ0ZGO0FES0E7RUFDRSxjQUFBO0VBQ0EsY0FBQTtBQ0ZGO0FES0EseUhBQUE7QUFFQTtFQUNFLG9CQUFBO0VBQ0EscUJBQUE7QUNIRjtBRE1BO0VBQ0UsZ0JBQUE7QUNIRjtBRE1BO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ0hGO0FETUE7RUFDRSxpQkFBQTtBQ0hGO0FETUE7RUFDRSwwQkFBQTtBQ0hGO0FETUE7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDSEY7QURNQTtFQUNFLDBCQUFBO0FDSEY7QURNQTtFQUNFLGVBQUE7RUFDQSxlQUFBO0FDSEY7QURNQTtFQUNFLGtCQUFBO0VBQ0EsdUJBQUE7QUNIRjtBRE1BO0VBQ0UsZUFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0FDSEY7QURNQTtFQUNFLGVBQUE7RUFDQSxzQkFBQTtBQ0hGO0FETUE7RUFDRSxrQkFBQTtBQ0hGO0FETUE7RUFDRSx3QkFBQTtFQUNBLG1CQUFBO0FDSEY7QURNQTtFQUNFLGtCQUFBO0FDSEY7QURNQTtFQUNFLGlDQUFBO0FDSEY7QURNQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtBQ0hGO0FETUE7RUFHRSwwQ0FBQTtBQ0hGO0FETUE7RUFDRSx1QkFBQTtBQ0hGO0FETUE7RUFDRSxlQUFBO0VBQ0EscUJBQUE7QUNIRjtBRE1BO0VBQ0Usa0JBQUE7QUNIRjtBRE1BO0VBQ0UsZUFBQTtBQ0hGO0FETUE7RUFDRSxlQUFBO0FDSEY7QURNQTtFQUNFLG9CQUFBO0VBQ0EsdUJBQUE7QUNIRjtBRE1BOztFQUVFLGtCQUFBO0VBQ0EscUJBQUE7QUNIRjtBRE1BO0VBQ0UsbUJBQUE7QUNIRjtBRE1BO0VBQ0Usc0JBQUE7QUNIRjtBRE1BO0VBQ0UsaUJBQUE7QUNIRjtBRE1BO0VBQ0UscUJBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0FDSEY7QURNQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7QUNIRjtBRE1BO0VBQ0Usc0JBQUE7QUNIRjtBRE1BO0VBQ0Usa0JBQUE7QUNIRjtBRE1BO0VBQ0Usb0JBQUE7RUFDQSx1QkFBQTtBQ0hGO0FETUE7RUFDRSxtQkFBQTtBQ0hGO0FETUE7RUFDRSxzQkFBQTtFQUNBLGtCQUFBO0FDSEY7QURNQTtFQUNFLGtCQUFBO0FDSEY7QURNQTtFQUNFLHNCQUFBO0VBQ0Esa0JBQUE7QUNIRjtBRE1BO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUNIRjtBRE1BO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBQ0hGO0FETUE7RUFDRSxpQ0FBQTtFQUNBLG1CQUFBO0FDSEY7QURNQTtFQUNFLGNBQUE7RUFDQSxxQkFBQTtBQ0hGO0FETUE7RUFDRSxvQ0FBQTtFQUNBLGtCQUFBO0FDSEY7QURNQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7QUNIRjtBRE1BO0VBQ0UsY0FBQTtBQ0hGO0FETUE7RUFDRSxvQkFBQTtFQUNBLHVCQUFBO0FDSEY7QURNQTtFQUNFLGtCQUFBO0FDSEY7QURNQTtFQUNFO0lBQ0UsVUFBQTtFQ0hGOztFRE1BO0lBQ0UsZUFBQTtFQ0hGOztFRE1BO0lBQ0UsZUFBQTtFQ0hGOztFRE1BO0lBQ0Usa0JBQUE7SUFDQSxnQkFBQTtFQ0hGOztFRE9BO0lBQ0UsZUFBQTtFQ0pGOztFRE9BO0lBQ0UsZUFBQTtFQ0pGOztFRE9BOztJQUVFLGVBQUE7RUNKRjs7RURPQTs7Ozs7SUFLRSxlQUFBO0VDSkY7O0VET0E7SUFDRSxtQkFBQTtFQ0pGOztFRE9BO0lBQ0Usa0JBQUE7RUNKRjs7RURPQTtJQUNFLFdBQUE7SUFDQSxZQUFBO0lBQ0EsU0FBQTtFQ0pGOztFRE9BO0lBQ0UsV0FBQTtJQUNBLFlBQUE7SUFDQSxNQUFBO0lBQ0EsVUFBQTtFQ0pGOztFRE9BO0lBQ0UsZUFBQTtFQ0pGOztFRE9BO0lBQ0UsZUFBQTtFQ0pGOztFRE9BO0lBQ0UsWUFBQTtJQUNBLGNBQUE7RUNKRjs7RURPQTtJQUNFLGVBQUE7RUNKRjtBQUNGO0FET0E7RUFDRTtJQUNFLFdBQUE7RUNMRjs7RURPQTs7SUFFRSxZQUFBO0lBQ0EsbUJBQUE7RUNKRjs7RURRQTtJQUNFLG1CQUFBO0lBQ0EsWUFBQTtFQ0xGOztFRE9BO0lBQ0UsZUFBQTtJQUNBLGtCQUFBO0VDSkY7O0VETUE7SUFDRSxXQUFBO0VDSEY7O0VETUE7SUFDRSxXQUFBO0VDSEY7O0VETUE7SUFHRSxlQUFBO0lBQ0EsYUFBQTtFQ0hGOztFRE1BO0lBQ0UsVUFBQTtJQUNBLFlBQUE7RUNIRjs7RURNQTtJQUNFLFdBQUE7SUFDQSxZQUFBO0lBQ0EsZUFBQTtFQ0hGOztFRE1BO0lBQ0UsZUFBQTtFQ0hGO0FBQ0Y7QURNQTtFQUNFO0lBQ0UsVUFBQTtJQUNBLGlCQUFBO0VDSkY7O0VET0E7SUFDRSw0QkFBQTtZQUFBLFFBQUE7SUFDQSxXQUFBO0lBQ0EsZUFBQTtFQ0pGOztFRE9BO0lBQ0UsVUFBQTtFQ0pGOztFRE9BO0lBR0UsZUFBQTtFQ0pGOztFRE9BO0lBQ0UsV0FBQTtFQ0pGOztFRE9BO0lBQ0UsWUFBQTtFQ0pGOztFRE9BOztJQUVFLGNBQUE7RUNKRjtBQUNGO0FET0E7RUFDRTtJQUNFLFVBQUE7RUNMRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbWFpbi9wcm9qZWN0cy9wcm9qZWN0cy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9QmVya3NoaXJlK1N3YXNoJmRpc3BsYXk9c3dhcCcpO1xyXG5cclxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1CZXJrc2hpcmUrU3dhc2gmZGlzcGxheT1zd2FwJyk7XHJcblxyXG4ucGFnZS10aXRsZSB7XHJcbiAgd2lkdGg6IDYyLjUlO1xyXG59XHJcblxyXG4uY3JlYXRpbmctd3JhcCB7XHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuXHJcbi5jcmVhdGluZy13cmFwX19pdGVtIHtcclxuICB3aWR0aDogNDglO1xyXG59XHJcblxyXG4uY3JlYXRpbmctd3JhcF9faXRlbS5jb2xsYWdlIHtcclxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjODVhM2JkO1xyXG59XHJcblxyXG4uY3JlYXRpbmctd3JhcF9faXRlbSAubGVmdCB7XHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgd2lkdGg6IDU3JTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5jcmVhdGluZy13cmFwX19pdGVtIC5yaWdodCB7XHJcbiAgd2lkdGg6IDQzJTtcclxuICB0ZXh0LWFsaWduOiByaWdodDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5jb2xsYWdlX190aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdCZXJrc2hpcmUgU3dhc2gnLCBjdXJzaXZlO1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzgwY2ZkNDtcclxuICBvcGFjaXR5OiAwLjk7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDUwJTtcclxuICBsZWZ0OiA1MCU7XHJcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi50b29scyB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICBsZWZ0OiA0cHg7XHJcbiAgcGFkZGluZzogNXB4IDlweCAxMXB4IDExcHg7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjgpO1xyXG59XHJcblxyXG4udG9vbHMgZGl2Omxhc3QtY2hpbGQge1xyXG4gIG1hcmdpbi10b3A6IDhweDtcclxufVxyXG5cclxuLmNyZWF0aW5nLXdyYXBfX2l0ZW0gLnJpZ2h0IGltZyB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luOiAwIDAgMCBhdXRvO1xyXG59XHJcblxyXG4uY3JlYXRpbmctd3JhcF9faXRlbSAudmlkZW8ge1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5hZGQtcGhvdG8ge1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q2ZTJlYztcclxufVxyXG5cclxuLmFkZC1waG90byBpbWcge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuLm15LXByb2plY3RfX3RpdGxlIHtcclxuICBjb2xvcjogIzAwMDAwMDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuLm15LXByb2plY3RfX3RpdGxlOmJlZm9yZSB7XHJcbiAgY29udGVudDogJyc7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XHJcbiAgYm90dG9tOiAxMDAlO1xyXG59XHJcblxyXG4ucHJvZ3Jlc3Mtd3JhcCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICByaWdodDogMDtcclxuICBib3R0b206IDA7XHJcbiAgbGVmdDogMDtcclxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICAtd2Via2l0LWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4zKTtcclxufVxyXG5cclxuLnByb2dyZXNzLXdyYXAgcCB7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBjb2xvcjogI2ZmZjtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG4ucHJvZ3Jlc3Mge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDAwMDA7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpO1xyXG4gIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuLnByb2dyZXNzX19iYXIge1xyXG4gIHdpZHRoOiA3NSU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxufVxyXG5cclxuLnByb2dyZXNzX19jb3VudGVyIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgbGVmdDogNTAlO1xyXG4gIHRvcDogNTAlO1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbi5tdXNpYy1hZGQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkNmUyZWM7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4ubXVzaWMtYWRkIC50b29scyB7XHJcbiAgYmFja2dyb3VuZDogbm9uZTtcclxufVxyXG5cclxuLnN0YXJ0LWZpbmlzaCB7XHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgYm90dG9tOiAxMDAlO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbi5jaG9vc2UtZnJhbWUge1xyXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcblxyXG4uY2hvb3NlLWZyYW1lX19pdGVtIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5jaG9vc2UtZnJhbWVfX2l0ZW0gcCB7XHJcbiAgY29sb3I6ICMwMDAwMDA7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuXHJcbi5jaG9vc2UtZnJhbWVfX2l0ZW0gaW1nIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBib3JkZXI6IDFweCBzb2xpZCAjODVhM2JkO1xyXG59XHJcblxyXG4ubXktcHJvamVjdF9fd3JhcCAuYnRuLXdyYXAge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnNhdmUtbG9hZGVyIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zYXZlLWxvYWRlciAucHJvZ3Jlc3Mge1xyXG4gIG1hcmdpbjogYXV0bztcclxufVxyXG5cclxuLnNhdmUtbG9hZGVyIC53YWl0IHtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG4uc2F2ZS1sb2FkZXIgcCB7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuLnBvcHVwIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXJcclxufVxyXG5cclxuLmFkZC1tdXNpYyAuYnRuLXdyYXAuZmlyc3QsXHJcbi5tdXNpYy1saWJyYXJ5IC5idG4td3JhcC5maXJzdCB7XHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIG1hcmdpbi10b3A6IDA7XHJcbn1cclxuXHJcbi5jaG9vc2UtcXVhbGl0eSB7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxufVxyXG5cclxuLmNob29zZS1xdWFsaXR5IC50aXRsZSB7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuLmNob29zZS1xdWFsaXR5IC5wb3B1cCBwIHtcclxuICBtYXJnaW46IDA7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxufVxyXG5cclxuLnF1YWxpdHlfX3RpdGxlIHtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG5cclxuLnF1YWxpdHlfX2l0ZW0ge1xyXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4ucXVhbGl0eV9fdGl0bGUge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB3aWR0aDogMjUlO1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbn1cclxuXHJcbi5xdWFsaXR5X192YWx1ZSB7XHJcbiAgd2lkdGg6IDUwJTtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59XHJcblxyXG4ucXVhbGl0eV9fcHJpY2Uge1xyXG4gIHdpZHRoOiAyNSU7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuLnF1YWxpdHlfX3RpdGxlOmJlZm9yZSB7XHJcbiAgY29udGVudDogJyc7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGxlZnQ6IDA7XHJcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWcvaWNvbi9xdWFsaXR5LnBuZyk7XHJcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XHJcbn1cclxuXHJcbi5xdWFsaXR5X190aXRsZTphZnRlciB7XHJcbiAgY29udGVudDogJyc7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1nL2ljb24vcXVhbGl0eTIucG5nKTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4uYWN0aXZlIC5xdWFsaXR5X190aXRsZTphZnRlciB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5tdXNpY19fbmFtZSB7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG5cclxuLm11c2ljLWxpYnJhcnkgLmJ0bi13cmFwLmZpcnN0IC5hY3RpdmUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkNmUyZWM7XHJcbn1cclxuXHJcbi5tdXNpY19faXRlbSB7XHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICBjb2xvcjogIzE4MGEyMTtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDZlMmVjO1xyXG59XHJcblxyXG4ubXVzaWMtbGlicmFyeSAubXVzaWMgLm11c2ljX19pdGVtIC5idG4tY3VzdG9tIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjMDAwMDAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY0Zjc7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgbWluLXdpZHRoOiAwO1xyXG4gIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xyXG59XHJcblxyXG4ubXVzaWMtbGlicmFyeSAubXVzaWMgLm11c2ljX19pdGVtIC5idG4tY3VzdG9tLmFjdGl2ZSB7XHJcbiAgYmFja2dyb3VuZDogIzAwMDtcclxuICBjb2xvcjogI2ZmZjtcclxufVxyXG5cclxuLm11c2ljX19pdGVtIC5wYXVzZSxcclxuLm11c2ljX19pdGVtLmFjdGl2ZSAucGxheSB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLm11c2ljX19pdGVtLmFjdGl2ZSAucGF1c2Uge1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuLm11c2ljLWxpYnJhcnkgLnBvcHVwICAuYnRuLXdyYXAgLmZpcnN0IHtcclxuICBtYXJnaW46IDA7XHJcbn1cclxuXHJcbi5tdXNpYy1saWJyYXJ5IC5idG4td3JhcC5maXJzdCB7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4ubXVzaWNfX3Byb2dyZXNzIGltZyB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLm11c2ljX19pdGVtLmFjdGl2ZSAubXVzaWNfX3Byb2dyZXNzIGltZyB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDkuNjM1dnc7XHJcbn1cclxuXHJcbi8qKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKyovXHJcblxyXG4ubXktcHJvamVjdCAucGFnZS10aXRsZSB7XHJcbiAgcGFkZGluZy10b3A6IDAuNzgxdnc7XHJcbiAgcGFkZGluZy1sZWZ0OiA0LjM3NXZ3O1xyXG59XHJcblxyXG4uY3JlYXRpbmctd3JhcCB7XHJcbiAgcGFkZGluZzogMCAxMTdweDtcclxufVxyXG5cclxuLm15LXByb2plY3RfX3RpdGxlIHtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbiAgbWFyZ2luOiAzMHB4IDAgOHB4O1xyXG4gIHBhZGRpbmc6IDAgNTlweDtcclxufVxyXG5cclxuLm15LXByb2plY3RfX3RpdGxlIGltZyB7XHJcbiAgcGFkZGluZy1sZWZ0OiA4cHg7XHJcbn1cclxuXHJcbi5jcmVhdGluZy13cmFwX19pdGVtIC5sZWZ0IHtcclxuICBib3JkZXI6IDAuNDE3dncgc29saWQgI2ZmZjtcclxufVxyXG5cclxuLmNvbGxhZ2VfX3RpdGxlIHtcclxuICB3aWR0aDogNi4zMDJ2dztcclxuICBoZWlnaHQ6IDYuMzAydnc7XHJcbiAgZm9udC1zaXplOiAxLjU2M3Z3O1xyXG59XHJcblxyXG4uYWRkLXBob3RvIHtcclxuICBib3JkZXI6IDAuNDE3dncgc29saWQgI2ZmZjtcclxufVxyXG5cclxuLm15LXByb2plY3RfX3RpdGxlOmJlZm9yZSB7XHJcbiAgd2lkdGg6IDEwLjQxN3Z3O1xyXG4gIGhlaWdodDogMC43ODF2dztcclxufVxyXG5cclxuLnByb2dyZXNzLXdyYXAgcCB7XHJcbiAgZm9udC1zaXplOiAwLjkzOHZ3O1xyXG4gIHBhZGRpbmctYm90dG9tOiAyLjA4M3Z3O1xyXG59XHJcblxyXG4ucHJvZ3Jlc3Mge1xyXG4gIHdpZHRoOiAzNC4xNjd2dztcclxuICBoZWlnaHQ6IDIuMzQ0dnc7XHJcbiAgbWFyZ2luLWJvdHRvbTogNS41MjF2dztcclxuICBib3JkZXItcmFkaXVzOiAxLjE5OHZ3O1xyXG59XHJcblxyXG4ucHJvZ3Jlc3NfX2JhciB7XHJcbiAgaGVpZ2h0OiAyLjM0NHZ3O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEuMTk4dnc7XHJcbn1cclxuXHJcbi5wcm9ncmVzc19fY291bnRlciB7XHJcbiAgZm9udC1zaXplOiAwLjkzOHZ3O1xyXG59XHJcblxyXG4ubXVzaWMtYWRkIHtcclxuICBwYWRkaW5nOiAwLjg4NXZ3IDMuMTI1dnc7XHJcbiAgbWFyZ2luLXRvcDogMS41NjN2dztcclxufVxyXG5cclxuLnN0YXJ0LWZpbmlzaCB7XHJcbiAgcGFkZGluZzogMCA2LjA5NHZ3O1xyXG59XHJcblxyXG4uY2hvb3NlLWZyYW1lIHtcclxuICBwYWRkaW5nOiAwLjgzM3Z3IDAgMC4yNjB2dyAzLjEyNXZ3O1xyXG59XHJcblxyXG4uY2hvb3NlLWZyYW1lX19pdGVtIHAge1xyXG4gIG1hcmdpbjogMC42Nzd2dyAwIDA7XHJcbiAgZm9udC1zaXplOiAwLjgzM3Z3O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDAuNTIxdnc7XHJcbn1cclxuXHJcbi5jaG9vc2UtZnJhbWVfX2l0ZW0uYWN0aXZlIGltZyB7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAtMC41MjF2dyAwLjUyMXZ3IDBweCAwcHggcmdiYSgwLCAwLCAwLCAxKTtcclxuICAtbW96LWJveC1zaGFkb3c6IC0wLjUyMXZ3IDAuNTIxdncgMHB4IDBweCByZ2JhKDAsIDAsIDAsIDEpO1xyXG4gIGJveC1zaGFkb3c6IC0wLjUyMXZ3IDAuNTIxdncgMHB4IDBweCByZ2JhKDAsIDAsIDAsIDEpO1xyXG59XHJcblxyXG4ubXktcHJvamVjdF9fd3JhcCAuYnRuLXdyYXAge1xyXG4gIHBhZGRpbmctYm90dG9tOiAwLjcyOXZ3O1xyXG59XHJcblxyXG4ubXktcHJvamVjdF9fd3JhcCAuYnRuLXdyYXAgLmxpZ2h0IHtcclxuICB3aWR0aDogMTAuOTM4dnc7XHJcbiAgbWFyZ2luLXJpZ2h0OiAyLjA4M3Z3O1xyXG59XHJcblxyXG4uc2F2ZS1sb2FkZXIgcCB7XHJcbiAgZm9udC1zaXplOiAwLjkzOHZ3O1xyXG59XHJcblxyXG4ucG9wdXAge1xyXG4gIHdpZHRoOiA0MC42MjV2dztcclxufVxyXG5cclxuLnBvcHVwIC5idG4tY3VzdG9tOm5vdChbdHlwZT0nc3VibWl0J10pIHtcclxuICB3aWR0aDogMTAuOTM4dnc7XHJcbn1cclxuXHJcbi5hZGQtbXVzaWMgLnBvcHVwIHtcclxuICBwYWRkaW5nLXRvcDogMy4wNzN2dztcclxuICBwYWRkaW5nLWJvdHRvbTogNC41ODN2dztcclxufVxyXG5cclxuLmFkZC1tdXNpYyAuYnRuLXdyYXAuZmlyc3QsXHJcbi5tdXNpYy1saWJyYXJ5IC5idG4td3JhcC5maXJzdCB7XHJcbiAgcGFkZGluZzogMCA0LjYzNXZ3O1xyXG4gIG1hcmdpbi1ib3R0b206IDIuNzYwdnc7XHJcbn1cclxuXHJcbi5hZGQtbXVzaWMgLmJ0bi13cmFwIHtcclxuICBtYXJnaW4tdG9wOiAxLjkyN3Z3O1xyXG59XHJcblxyXG4uYWRkLW11c2ljIC5idG4td3JhcC5maXJzdCArIHAge1xyXG4gIG1hcmdpbi1ib3R0b206IDEuMTk4dnc7XHJcbn1cclxuXHJcbi5jaG9vc2UtcXVhbGl0eSB7XHJcbiAgZm9udC1zaXplOiAxLjI1MHZ3O1xyXG59XHJcblxyXG4uY2hvb3NlLXF1YWxpdHkgLnBvcHVwIHtcclxuICBwYWRkaW5nLWxlZnQ6IDcuMTg4dnc7XHJcbiAgcGFkZGluZy1yaWdodDogNy4xODh2dztcclxuICBwYWRkaW5nLXRvcDogMS41MTB2dztcclxufVxyXG5cclxuLmNob29zZS1xdWFsaXR5IC50aXRsZSB7XHJcbiAgbWFyZ2luOiAwIDAgMS45Nzl2dztcclxuICBmb250LXNpemU6IDEuMzU0dnc7XHJcbn1cclxuXHJcbi5jaG9vc2UtcXVhbGl0eSAucG9wdXAgLnN1YnRpdGxlIHtcclxuICBtYXJnaW4tYm90dG9tOiAyLjkxN3Z3O1xyXG59XHJcblxyXG4uY2hvb3NlLXF1YWxpdHkgLnBvcHVwIHAge1xyXG4gIGZvbnQtc2l6ZTogMS4zNTR2dztcclxufVxyXG5cclxuLmNob29zZS1xdWFsaXR5IC5idG4td3JhcCB7XHJcbiAgcGFkZGluZy10b3A6IDAuNTIxdnc7XHJcbiAgcGFkZGluZy1ib3R0b206IDMuMDczdnc7XHJcbn1cclxuXHJcbi5xdWFsaXR5X19pdGVtIHtcclxuICBtYXJnaW46IDAgMCAxLjY2N3Z3O1xyXG59XHJcblxyXG4ucXVhbGl0eV9fdGl0bGUge1xyXG4gIHBhZGRpbmc6IDAgMCAwIDIuOTY5dnc7XHJcbiAgZm9udC1zaXplOiAxLjM1NHZ3O1xyXG59XHJcblxyXG4ucXVhbGl0eV9fdmFsdWUge1xyXG4gIGZvbnQtc2l6ZTogMS4zNTR2dztcclxufVxyXG5cclxuLnF1YWxpdHlfX3ByaWNlIHtcclxuICBwYWRkaW5nLXJpZ2h0OiAyLjM0NHZ3O1xyXG4gIGZvbnQtc2l6ZTogMS4zNTR2dztcclxufVxyXG5cclxuLnF1YWxpdHlfX3RpdGxlOmJlZm9yZSB7XHJcbiAgd2lkdGg6IDEuNDU4dnc7XHJcbiAgaGVpZ2h0OiAxLjQ1OHZ3O1xyXG59XHJcblxyXG4ucXVhbGl0eV9fdGl0bGU6YWZ0ZXIge1xyXG4gIHdpZHRoOiAxLjMwMnZ3O1xyXG4gIGhlaWdodDogMS4zMDJ2dztcclxuICB0b3A6IDAuMDUydnc7XHJcbiAgbGVmdDogMC4zNjV2dztcclxufVxyXG5cclxuLm11c2ljIHtcclxuICBib3JkZXItdG9wOiAwLjQxN3Z3IHNvbGlkICNkNmUyZWM7XHJcbiAgbWFyZ2luLXRvcDogMS4wOTR2dztcclxufVxyXG5cclxuLm11c2ljX19uYW1lIHtcclxuICB3aWR0aDogOS45NDh2dztcclxuICBwYWRkaW5nLWxlZnQ6IDAuOTM4dnc7XHJcbn1cclxuXHJcbi5tdXNpY19faXRlbSB7XHJcbiAgcGFkZGluZzogMS4zNTR2dyA1dncgMC4yMDh2dyAzLjA3M3Z3O1xyXG4gIGZvbnQtc2l6ZTogMC45Mzh2dztcclxufVxyXG5cclxuLm11c2ljLWxpYnJhcnkgLm11c2ljIC5tdXNpY19faXRlbSAuYnRuLWN1c3RvbSB7XHJcbiAgd2lkdGg6IDMuMzMzdnc7XHJcbiAgaGVpZ2h0OiAxLjY2N3Z3O1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuODMzdnc7XHJcbn1cclxuXHJcbi5wbGF5LWJ0biB7XHJcbiAgd2lkdGg6IDEuMDk0dnc7XHJcbn1cclxuXHJcbi5tdXNpYy1saWJyYXJ5IC5wb3B1cCB7XHJcbiAgcGFkZGluZy10b3A6IDMuMDczdnc7XHJcbiAgcGFkZGluZy1ib3R0b206IDAuNzgxdnc7XHJcbn1cclxuXHJcbi5tdXNpY19fcHJvZ3Jlc3Mge1xyXG4gIG1pbi13aWR0aDogOS42MzV2dztcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSB7XHJcbiAgLnBvcHVwIHtcclxuICAgIHdpZHRoOiA3NSU7XHJcbiAgfVxyXG5cclxuICAucHJvZ3Jlc3Mtd3JhcCBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICB9XHJcblxyXG4gIC5jcmVhdGluZy13cmFwIHtcclxuICAgIHBhZGRpbmc6IDAgMjBweDtcclxuICB9XHJcblxyXG4gIC5tdXNpYy1hZGQge1xyXG4gICAgcGFkZGluZzogMzBweCA0MHB4O1xyXG4gICAgbWFyZ2luLXRvcDogNDBweDtcclxuICB9XHJcblxyXG5cclxuICAubXVzaWMtbGlicmFyeSAuc2VhcmNoIHtcclxuICAgIHBhZGRpbmc6IDAgMjBweDtcclxuICB9XHJcblxyXG4gIC5tdXNpYy1saWJyYXJ5IC5zZWFyY2ggZm9ybSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgfVxyXG5cclxuICAubXVzaWNfX2l0ZW0uYWN0aXZlIC5tdXNpY19fcHJvZ3Jlc3MgaW1nLFxyXG4gIC5tdXNpY19fcHJvZ3Jlc3Mge1xyXG4gICAgd2lkdGg6IDE1LjQxN3Z3O1xyXG4gIH1cclxuXHJcbiAgLmNob29zZS1xdWFsaXR5IC50aXRsZSxcclxuICAuY2hvb3NlLXF1YWxpdHkgLnBvcHVwIHAsXHJcbiAgLnF1YWxpdHlfX3RpdGxlLFxyXG4gIC5xdWFsaXR5X192YWx1ZSxcclxuICAucXVhbGl0eV9fcHJpY2Uge1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gIH1cclxuXHJcbiAgLnF1YWxpdHlfX2l0ZW0ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICB9XHJcblxyXG4gIC5xdWFsaXR5X190aXRsZSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDM1cHg7XHJcbiAgfVxyXG5cclxuICAucXVhbGl0eV9fdGl0bGU6YmVmb3JlIHtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgdG9wOiAtNHB4O1xyXG4gIH1cclxuXHJcbiAgLnF1YWxpdHlfX3RpdGxlOmFmdGVyIHtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMTVweDtcclxuICB9XHJcblxyXG4gIC5zYXZlLWxvYWRlciBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICB9XHJcblxyXG4gIC5wcm9ncmVzc19fY291bnRlciB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgfVxyXG5cclxuICAucHJvZ3Jlc3Mge1xyXG4gICAgd2lkdGg6IDUwMHB4O1xyXG4gICAgbWF4LXdpZHRoOiA5MCU7XHJcbiAgfVxyXG5cclxuICAuY2hvb3NlLWZyYW1lX19pdGVtIHAge1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAucG9wdXAgLmJ0bi1jdXN0b206bm90KFt0eXBlPSdzdWJtaXQnXSkge1xyXG4gICAgd2lkdGg6IGF1dG87XHJcbiAgfVxyXG4gIC5wcm9ncmVzcyxcclxuICAucHJvZ3Jlc3NfX2JhciB7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIH1cclxuXHJcblxyXG4gIC5tdXNpYy1saWJyYXJ5IC5tdXNpYyAubXVzaWNfX2l0ZW0gLmJ0bi1jdXN0b20ge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICB9XHJcbiAgLm11c2ljX19pdGVtIHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIHBhZGRpbmc6IDEwcHggMjBweDtcclxuICB9XHJcbiAgLnBsYXktYnRuIHtcclxuICAgIHdpZHRoOiAxNXB4O1xyXG4gIH1cclxuXHJcbiAgLm15LXByb2plY3RfX3dyYXAgLmJ0bi13cmFwIC5saWdodCB7XHJcbiAgICB3aWR0aDogYXV0bztcclxuICB9XHJcblxyXG4gIC5jaG9vc2UtZnJhbWUge1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgcGFkZGluZzogMjBweDtcclxuICB9XHJcblxyXG4gIC5jaG9vc2UtZnJhbWVfX2l0ZW0ge1xyXG4gICAgd2lkdGg6IDIwJTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICB9XHJcblxyXG4gIC5jb2xsYWdlX190aXRsZSB7XHJcbiAgICB3aWR0aDogNzBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICB9XHJcblxyXG4gIC5teS1wcm9qZWN0X190aXRsZSB7XHJcbiAgICBwYWRkaW5nOiAwIDIwcHg7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gIC5wb3B1cCB7XHJcbiAgICB3aWR0aDogOTUlO1xyXG4gICAgcGFkZGluZy10b3A6IDUwcHg7XHJcbiAgfVxyXG5cclxuICAubXktcHJvamVjdCAucGFnZS10aXRsZSB7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcGFkZGluZzogMCAyMHB4O1xyXG4gIH1cclxuXHJcbiAgLmNob29zZS1mcmFtZV9faXRlbSB7XHJcbiAgICB3aWR0aDogMzMlO1xyXG4gIH1cclxuXHJcbiAgLmNyZWF0aW5nLXdyYXAge1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gIH1cclxuXHJcbiAgLmNyZWF0aW5nLXdyYXBfX2l0ZW0ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuICAuY3JlYXRpbmctd3JhcF9faXRlbSAudmlkZW8ge1xyXG4gICAgaGVpZ2h0OiA1MnZ3O1xyXG4gIH1cclxuXHJcbiAgLm11c2ljX19pdGVtLmFjdGl2ZSAubXVzaWNfX3Byb2dyZXNzIGltZyxcclxuICAubXVzaWNfX3Byb2dyZXNzIHtcclxuICAgIHdpZHRoOiAyNC4xMjB2dztcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ5MnB4KSB7XHJcbiAgLmNob29zZS1mcmFtZV9faXRlbSB7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gIH1cclxufVxyXG5cclxuIiwiQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9QmVya3NoaXJlK1N3YXNoJmRpc3BsYXk9c3dhcFwiKTtcbkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PUJlcmtzaGlyZStTd2FzaCZkaXNwbGF5PXN3YXBcIik7XG4ucGFnZS10aXRsZSB7XG4gIHdpZHRoOiA2Mi41JTtcbn1cblxuLmNyZWF0aW5nLXdyYXAge1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuXG4uY3JlYXRpbmctd3JhcF9faXRlbSB7XG4gIHdpZHRoOiA0OCU7XG59XG5cbi5jcmVhdGluZy13cmFwX19pdGVtLmNvbGxhZ2Uge1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjODVhM2JkO1xufVxuXG4uY3JlYXRpbmctd3JhcF9faXRlbSAubGVmdCB7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xuICAtbXMtZmxleC13cmFwOiB3cmFwO1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHdpZHRoOiA1NyU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmNyZWF0aW5nLXdyYXBfX2l0ZW0gLnJpZ2h0IHtcbiAgd2lkdGg6IDQzJTtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmNvbGxhZ2VfX3RpdGxlIHtcbiAgZm9udC1mYW1pbHk6IFwiQmVya3NoaXJlIFN3YXNoXCIsIGN1cnNpdmU7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBmb250LXdlaWdodDogNDAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjODBjZmQ0O1xuICBvcGFjaXR5OiAwLjk7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi50b29scyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiA0cHg7XG4gIHBhZGRpbmc6IDVweCA5cHggMTFweCAxMXB4O1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCk7XG59XG5cbi50b29scyBkaXY6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi10b3A6IDhweDtcbn1cblxuLmNyZWF0aW5nLXdyYXBfX2l0ZW0gLnJpZ2h0IGltZyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDAgMCAwIGF1dG87XG59XG5cbi5jcmVhdGluZy13cmFwX19pdGVtIC52aWRlbyB7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uYWRkLXBob3RvIHtcbiAgd2lkdGg6IDUwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q2ZTJlYztcbn1cblxuLmFkZC1waG90byBpbWcge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLm15LXByb2plY3RfX3RpdGxlIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLm15LXByb2plY3RfX3RpdGxlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMDtcbiAgYm90dG9tOiAxMDAlO1xufVxuXG4ucHJvZ3Jlc3Mtd3JhcCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgYm90dG9tOiAwO1xuICBsZWZ0OiAwO1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4zKTtcbn1cblxuLnByb2dyZXNzLXdyYXAgcCB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGNvbG9yOiAjZmZmO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4ucHJvZ3Jlc3Mge1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDAwMDAwO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNSk7XG4gIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5wcm9ncmVzc19fYmFyIHtcbiAgd2lkdGg6IDc1JTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbn1cblxuLnByb2dyZXNzX19jb3VudGVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLm11c2ljLWFkZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkNmUyZWM7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLm11c2ljLWFkZCAudG9vbHMge1xuICBiYWNrZ3JvdW5kOiBub25lO1xufVxuXG4uc3RhcnQtZmluaXNoIHtcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgYm90dG9tOiAxMDAlO1xuICBmb250LXdlaWdodDogNzAwO1xufVxuXG4uY2hvb3NlLWZyYW1lIHtcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuLmNob29zZS1mcmFtZV9faXRlbSB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmNob29zZS1mcmFtZV9faXRlbSBwIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG5cbi5jaG9vc2UtZnJhbWVfX2l0ZW0gaW1nIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM4NWEzYmQ7XG59XG5cbi5teS1wcm9qZWN0X193cmFwIC5idG4td3JhcCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnNhdmUtbG9hZGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uc2F2ZS1sb2FkZXIgLnByb2dyZXNzIHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4uc2F2ZS1sb2FkZXIgLndhaXQge1xuICBmb250LXdlaWdodDogNDAwO1xufVxuXG4uc2F2ZS1sb2FkZXIgcCB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi5wb3B1cCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmFkZC1tdXNpYyAuYnRuLXdyYXAuZmlyc3QsXG4ubXVzaWMtbGlicmFyeSAuYnRuLXdyYXAuZmlyc3Qge1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIG1hcmdpbi10b3A6IDA7XG59XG5cbi5jaG9vc2UtcXVhbGl0eSB7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG59XG5cbi5jaG9vc2UtcXVhbGl0eSAudGl0bGUge1xuICBmb250LXdlaWdodDogNzAwO1xufVxuXG4uY2hvb3NlLXF1YWxpdHkgLnBvcHVwIHAge1xuICBtYXJnaW46IDA7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG59XG5cbi5xdWFsaXR5X190aXRsZSB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi5xdWFsaXR5X19pdGVtIHtcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbn1cblxuLnF1YWxpdHlfX3RpdGxlIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMjUlO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG4ucXVhbGl0eV9fdmFsdWUge1xuICB3aWR0aDogNTAlO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG4ucXVhbGl0eV9fcHJpY2Uge1xuICB3aWR0aDogMjUlO1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLnF1YWxpdHlfX3RpdGxlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWcvaWNvbi9xdWFsaXR5LnBuZyk7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuXG4ucXVhbGl0eV9fdGl0bGU6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1nL2ljb24vcXVhbGl0eTIucG5nKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5hY3RpdmUgLnF1YWxpdHlfX3RpdGxlOmFmdGVyIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5tdXNpY19fbmFtZSB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi5tdXNpYy1saWJyYXJ5IC5idG4td3JhcC5maXJzdCAuYWN0aXZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q2ZTJlYztcbn1cblxuLm11c2ljX19pdGVtIHtcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBjb2xvcjogIzE4MGEyMTtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkNmUyZWM7XG59XG5cbi5tdXNpYy1saWJyYXJ5IC5tdXNpYyAubXVzaWNfX2l0ZW0gLmJ0bi1jdXN0b20ge1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDAwMDAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjZmNGY3O1xuICBjb2xvcjogIzAwMDtcbiAgbWluLXdpZHRoOiAwO1xuICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcbn1cblxuLm11c2ljLWxpYnJhcnkgLm11c2ljIC5tdXNpY19faXRlbSAuYnRuLWN1c3RvbS5hY3RpdmUge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLm11c2ljX19pdGVtIC5wYXVzZSxcbi5tdXNpY19faXRlbS5hY3RpdmUgLnBsYXkge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ubXVzaWNfX2l0ZW0uYWN0aXZlIC5wYXVzZSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cblxuLm11c2ljLWxpYnJhcnkgLnBvcHVwIC5idG4td3JhcCAuZmlyc3Qge1xuICBtYXJnaW46IDA7XG59XG5cbi5tdXNpYy1saWJyYXJ5IC5idG4td3JhcC5maXJzdCB7XG4gIG1hcmdpbjogMDtcbn1cblxuLm11c2ljX19wcm9ncmVzcyBpbWcge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ubXVzaWNfX2l0ZW0uYWN0aXZlIC5tdXNpY19fcHJvZ3Jlc3MgaW1nIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiA5LjYzNXZ3O1xufVxuXG4vKisrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysqL1xuLm15LXByb2plY3QgLnBhZ2UtdGl0bGUge1xuICBwYWRkaW5nLXRvcDogMC43ODF2dztcbiAgcGFkZGluZy1sZWZ0OiA0LjM3NXZ3O1xufVxuXG4uY3JlYXRpbmctd3JhcCB7XG4gIHBhZGRpbmc6IDAgMTE3cHg7XG59XG5cbi5teS1wcm9qZWN0X190aXRsZSB7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgbWFyZ2luOiAzMHB4IDAgOHB4O1xuICBwYWRkaW5nOiAwIDU5cHg7XG59XG5cbi5teS1wcm9qZWN0X190aXRsZSBpbWcge1xuICBwYWRkaW5nLWxlZnQ6IDhweDtcbn1cblxuLmNyZWF0aW5nLXdyYXBfX2l0ZW0gLmxlZnQge1xuICBib3JkZXI6IDAuNDE3dncgc29saWQgI2ZmZjtcbn1cblxuLmNvbGxhZ2VfX3RpdGxlIHtcbiAgd2lkdGg6IDYuMzAydnc7XG4gIGhlaWdodDogNi4zMDJ2dztcbiAgZm9udC1zaXplOiAxLjU2M3Z3O1xufVxuXG4uYWRkLXBob3RvIHtcbiAgYm9yZGVyOiAwLjQxN3Z3IHNvbGlkICNmZmY7XG59XG5cbi5teS1wcm9qZWN0X190aXRsZTpiZWZvcmUge1xuICB3aWR0aDogMTAuNDE3dnc7XG4gIGhlaWdodDogMC43ODF2dztcbn1cblxuLnByb2dyZXNzLXdyYXAgcCB7XG4gIGZvbnQtc2l6ZTogMC45Mzh2dztcbiAgcGFkZGluZy1ib3R0b206IDIuMDgzdnc7XG59XG5cbi5wcm9ncmVzcyB7XG4gIHdpZHRoOiAzNC4xNjd2dztcbiAgaGVpZ2h0OiAyLjM0NHZ3O1xuICBtYXJnaW4tYm90dG9tOiA1LjUyMXZ3O1xuICBib3JkZXItcmFkaXVzOiAxLjE5OHZ3O1xufVxuXG4ucHJvZ3Jlc3NfX2JhciB7XG4gIGhlaWdodDogMi4zNDR2dztcbiAgYm9yZGVyLXJhZGl1czogMS4xOTh2dztcbn1cblxuLnByb2dyZXNzX19jb3VudGVyIHtcbiAgZm9udC1zaXplOiAwLjkzOHZ3O1xufVxuXG4ubXVzaWMtYWRkIHtcbiAgcGFkZGluZzogMC44ODV2dyAzLjEyNXZ3O1xuICBtYXJnaW4tdG9wOiAxLjU2M3Z3O1xufVxuXG4uc3RhcnQtZmluaXNoIHtcbiAgcGFkZGluZzogMCA2LjA5NHZ3O1xufVxuXG4uY2hvb3NlLWZyYW1lIHtcbiAgcGFkZGluZzogMC44MzN2dyAwIDAuMjZ2dyAzLjEyNXZ3O1xufVxuXG4uY2hvb3NlLWZyYW1lX19pdGVtIHAge1xuICBtYXJnaW46IDAuNjc3dncgMCAwO1xuICBmb250LXNpemU6IDAuODMzdnc7XG4gIHBhZGRpbmctcmlnaHQ6IDAuNTIxdnc7XG59XG5cbi5jaG9vc2UtZnJhbWVfX2l0ZW0uYWN0aXZlIGltZyB7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogLTAuNTIxdncgMC41MjF2dyAwcHggMHB4IGJsYWNrO1xuICAtbW96LWJveC1zaGFkb3c6IC0wLjUyMXZ3IDAuNTIxdncgMHB4IDBweCBibGFjaztcbiAgYm94LXNoYWRvdzogLTAuNTIxdncgMC41MjF2dyAwcHggMHB4IGJsYWNrO1xufVxuXG4ubXktcHJvamVjdF9fd3JhcCAuYnRuLXdyYXAge1xuICBwYWRkaW5nLWJvdHRvbTogMC43Mjl2dztcbn1cblxuLm15LXByb2plY3RfX3dyYXAgLmJ0bi13cmFwIC5saWdodCB7XG4gIHdpZHRoOiAxMC45Mzh2dztcbiAgbWFyZ2luLXJpZ2h0OiAyLjA4M3Z3O1xufVxuXG4uc2F2ZS1sb2FkZXIgcCB7XG4gIGZvbnQtc2l6ZTogMC45Mzh2dztcbn1cblxuLnBvcHVwIHtcbiAgd2lkdGg6IDQwLjYyNXZ3O1xufVxuXG4ucG9wdXAgLmJ0bi1jdXN0b206bm90KFt0eXBlPXN1Ym1pdF0pIHtcbiAgd2lkdGg6IDEwLjkzOHZ3O1xufVxuXG4uYWRkLW11c2ljIC5wb3B1cCB7XG4gIHBhZGRpbmctdG9wOiAzLjA3M3Z3O1xuICBwYWRkaW5nLWJvdHRvbTogNC41ODN2dztcbn1cblxuLmFkZC1tdXNpYyAuYnRuLXdyYXAuZmlyc3QsXG4ubXVzaWMtbGlicmFyeSAuYnRuLXdyYXAuZmlyc3Qge1xuICBwYWRkaW5nOiAwIDQuNjM1dnc7XG4gIG1hcmdpbi1ib3R0b206IDIuNzZ2dztcbn1cblxuLmFkZC1tdXNpYyAuYnRuLXdyYXAge1xuICBtYXJnaW4tdG9wOiAxLjkyN3Z3O1xufVxuXG4uYWRkLW11c2ljIC5idG4td3JhcC5maXJzdCArIHAge1xuICBtYXJnaW4tYm90dG9tOiAxLjE5OHZ3O1xufVxuXG4uY2hvb3NlLXF1YWxpdHkge1xuICBmb250LXNpemU6IDEuMjV2dztcbn1cblxuLmNob29zZS1xdWFsaXR5IC5wb3B1cCB7XG4gIHBhZGRpbmctbGVmdDogNy4xODh2dztcbiAgcGFkZGluZy1yaWdodDogNy4xODh2dztcbiAgcGFkZGluZy10b3A6IDEuNTF2dztcbn1cblxuLmNob29zZS1xdWFsaXR5IC50aXRsZSB7XG4gIG1hcmdpbjogMCAwIDEuOTc5dnc7XG4gIGZvbnQtc2l6ZTogMS4zNTR2dztcbn1cblxuLmNob29zZS1xdWFsaXR5IC5wb3B1cCAuc3VidGl0bGUge1xuICBtYXJnaW4tYm90dG9tOiAyLjkxN3Z3O1xufVxuXG4uY2hvb3NlLXF1YWxpdHkgLnBvcHVwIHAge1xuICBmb250LXNpemU6IDEuMzU0dnc7XG59XG5cbi5jaG9vc2UtcXVhbGl0eSAuYnRuLXdyYXAge1xuICBwYWRkaW5nLXRvcDogMC41MjF2dztcbiAgcGFkZGluZy1ib3R0b206IDMuMDczdnc7XG59XG5cbi5xdWFsaXR5X19pdGVtIHtcbiAgbWFyZ2luOiAwIDAgMS42Njd2dztcbn1cblxuLnF1YWxpdHlfX3RpdGxlIHtcbiAgcGFkZGluZzogMCAwIDAgMi45Njl2dztcbiAgZm9udC1zaXplOiAxLjM1NHZ3O1xufVxuXG4ucXVhbGl0eV9fdmFsdWUge1xuICBmb250LXNpemU6IDEuMzU0dnc7XG59XG5cbi5xdWFsaXR5X19wcmljZSB7XG4gIHBhZGRpbmctcmlnaHQ6IDIuMzQ0dnc7XG4gIGZvbnQtc2l6ZTogMS4zNTR2dztcbn1cblxuLnF1YWxpdHlfX3RpdGxlOmJlZm9yZSB7XG4gIHdpZHRoOiAxLjQ1OHZ3O1xuICBoZWlnaHQ6IDEuNDU4dnc7XG59XG5cbi5xdWFsaXR5X190aXRsZTphZnRlciB7XG4gIHdpZHRoOiAxLjMwMnZ3O1xuICBoZWlnaHQ6IDEuMzAydnc7XG4gIHRvcDogMC4wNTJ2dztcbiAgbGVmdDogMC4zNjV2dztcbn1cblxuLm11c2ljIHtcbiAgYm9yZGVyLXRvcDogMC40MTd2dyBzb2xpZCAjZDZlMmVjO1xuICBtYXJnaW4tdG9wOiAxLjA5NHZ3O1xufVxuXG4ubXVzaWNfX25hbWUge1xuICB3aWR0aDogOS45NDh2dztcbiAgcGFkZGluZy1sZWZ0OiAwLjkzOHZ3O1xufVxuXG4ubXVzaWNfX2l0ZW0ge1xuICBwYWRkaW5nOiAxLjM1NHZ3IDV2dyAwLjIwOHZ3IDMuMDczdnc7XG4gIGZvbnQtc2l6ZTogMC45Mzh2dztcbn1cblxuLm11c2ljLWxpYnJhcnkgLm11c2ljIC5tdXNpY19faXRlbSAuYnRuLWN1c3RvbSB7XG4gIHdpZHRoOiAzLjMzM3Z3O1xuICBoZWlnaHQ6IDEuNjY3dnc7XG4gIGJvcmRlci1yYWRpdXM6IDAuODMzdnc7XG59XG5cbi5wbGF5LWJ0biB7XG4gIHdpZHRoOiAxLjA5NHZ3O1xufVxuXG4ubXVzaWMtbGlicmFyeSAucG9wdXAge1xuICBwYWRkaW5nLXRvcDogMy4wNzN2dztcbiAgcGFkZGluZy1ib3R0b206IDAuNzgxdnc7XG59XG5cbi5tdXNpY19fcHJvZ3Jlc3Mge1xuICBtaW4td2lkdGg6IDkuNjM1dnc7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xuICAucG9wdXAge1xuICAgIHdpZHRoOiA3NSU7XG4gIH1cblxuICAucHJvZ3Jlc3Mtd3JhcCBwIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIH1cblxuICAuY3JlYXRpbmctd3JhcCB7XG4gICAgcGFkZGluZzogMCAyMHB4O1xuICB9XG5cbiAgLm11c2ljLWFkZCB7XG4gICAgcGFkZGluZzogMzBweCA0MHB4O1xuICAgIG1hcmdpbi10b3A6IDQwcHg7XG4gIH1cblxuICAubXVzaWMtbGlicmFyeSAuc2VhcmNoIHtcbiAgICBwYWRkaW5nOiAwIDIwcHg7XG4gIH1cblxuICAubXVzaWMtbGlicmFyeSAuc2VhcmNoIGZvcm0ge1xuICAgIHBhZGRpbmctbGVmdDogMDtcbiAgfVxuXG4gIC5tdXNpY19faXRlbS5hY3RpdmUgLm11c2ljX19wcm9ncmVzcyBpbWcsXG4ubXVzaWNfX3Byb2dyZXNzIHtcbiAgICB3aWR0aDogMTUuNDE3dnc7XG4gIH1cblxuICAuY2hvb3NlLXF1YWxpdHkgLnRpdGxlLFxuLmNob29zZS1xdWFsaXR5IC5wb3B1cCBwLFxuLnF1YWxpdHlfX3RpdGxlLFxuLnF1YWxpdHlfX3ZhbHVlLFxuLnF1YWxpdHlfX3ByaWNlIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIH1cblxuICAucXVhbGl0eV9faXRlbSB7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgfVxuXG4gIC5xdWFsaXR5X190aXRsZSB7XG4gICAgcGFkZGluZy1sZWZ0OiAzNXB4O1xuICB9XG5cbiAgLnF1YWxpdHlfX3RpdGxlOmJlZm9yZSB7XG4gICAgd2lkdGg6IDMwcHg7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICAgIHRvcDogLTRweDtcbiAgfVxuXG4gIC5xdWFsaXR5X190aXRsZTphZnRlciB7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAxNXB4O1xuICB9XG5cbiAgLnNhdmUtbG9hZGVyIHAge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxuXG4gIC5wcm9ncmVzc19fY291bnRlciB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG5cbiAgLnByb2dyZXNzIHtcbiAgICB3aWR0aDogNTAwcHg7XG4gICAgbWF4LXdpZHRoOiA5MCU7XG4gIH1cblxuICAuY2hvb3NlLWZyYW1lX19pdGVtIHAge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLnBvcHVwIC5idG4tY3VzdG9tOm5vdChbdHlwZT1zdWJtaXRdKSB7XG4gICAgd2lkdGg6IGF1dG87XG4gIH1cblxuICAucHJvZ3Jlc3MsXG4ucHJvZ3Jlc3NfX2JhciB7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIH1cblxuICAubXVzaWMtbGlicmFyeSAubXVzaWMgLm11c2ljX19pdGVtIC5idG4tY3VzdG9tIHtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgIGhlaWdodDogMzBweDtcbiAgfVxuXG4gIC5tdXNpY19faXRlbSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIHBhZGRpbmc6IDEwcHggMjBweDtcbiAgfVxuXG4gIC5wbGF5LWJ0biB7XG4gICAgd2lkdGg6IDE1cHg7XG4gIH1cblxuICAubXktcHJvamVjdF9fd3JhcCAuYnRuLXdyYXAgLmxpZ2h0IHtcbiAgICB3aWR0aDogYXV0bztcbiAgfVxuXG4gIC5jaG9vc2UtZnJhbWUge1xuICAgIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xuICAgIC1tcy1mbGV4LXdyYXA6IHdyYXA7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gIH1cblxuICAuY2hvb3NlLWZyYW1lX19pdGVtIHtcbiAgICB3aWR0aDogMjAlO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgfVxuXG4gIC5jb2xsYWdlX190aXRsZSB7XG4gICAgd2lkdGg6IDcwcHg7XG4gICAgaGVpZ2h0OiA3MHB4O1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgfVxuXG4gIC5teS1wcm9qZWN0X190aXRsZSB7XG4gICAgcGFkZGluZzogMCAyMHB4O1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAucG9wdXAge1xuICAgIHdpZHRoOiA5NSU7XG4gICAgcGFkZGluZy10b3A6IDUwcHg7XG4gIH1cblxuICAubXktcHJvamVjdCAucGFnZS10aXRsZSB7XG4gICAgb3JkZXI6IDI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMCAyMHB4O1xuICB9XG5cbiAgLmNob29zZS1mcmFtZV9faXRlbSB7XG4gICAgd2lkdGg6IDMzJTtcbiAgfVxuXG4gIC5jcmVhdGluZy13cmFwIHtcbiAgICAtd2Via2l0LWZsZXgtd3JhcDogd3JhcDtcbiAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xuICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgfVxuXG4gIC5jcmVhdGluZy13cmFwX19pdGVtIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5jcmVhdGluZy13cmFwX19pdGVtIC52aWRlbyB7XG4gICAgaGVpZ2h0OiA1MnZ3O1xuICB9XG5cbiAgLm11c2ljX19pdGVtLmFjdGl2ZSAubXVzaWNfX3Byb2dyZXNzIGltZyxcbi5tdXNpY19fcHJvZ3Jlc3Mge1xuICAgIHdpZHRoOiAyNC4xMnZ3O1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0OTJweCkge1xuICAuY2hvb3NlLWZyYW1lX19pdGVtIHtcbiAgICB3aWR0aDogNTAlO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/main/projects/projects.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/main/projects/projects.component.ts ***!
  \*****************************************************/
/*! exports provided: ProjectsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsComponent", function() { return ProjectsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let ProjectsComponent = class ProjectsComponent {
    constructor(formBuild) {
        this.formBuild = formBuild;
        this.progress = false;
        this.addMusic = false;
        this.addMusicBtn = false;
        this.musicList = false;
    }
    ngOnInit() {
        this.searchForm = this.formBuild.group({
            search: ['']
        });
    }
    changePopup() {
        this.addMusic = !this.addMusic;
        this.musicList = !this.musicList;
    }
    closePopup() {
        this.addMusicBtn = !this.addMusicBtn;
        this.musicList = !this.musicList;
    }
    openMusicList() {
        this.addMusicBtn = !this.addMusicBtn;
        this.musicList = !this.musicList;
    }
};
ProjectsComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
ProjectsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-projects',
        template: __webpack_require__(/*! raw-loader!./projects.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/projects/projects.component.html"),
        styles: [__webpack_require__(/*! ./projects.component.scss */ "./src/app/main/projects/projects.component.scss")]
    })
], ProjectsComponent);



/***/ }),

/***/ "./src/app/shared/components/components.module.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/components/components.module.ts ***!
  \********************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./header/header.component */ "./src/app/shared/components/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/shared/components/footer/footer.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _side_menu_side_menu_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./side-menu/side-menu.component */ "./src/app/shared/components/side-menu/side-menu.component.ts");







let ComponentsModule = class ComponentsModule {
};
ComponentsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"],
            _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"],
            _side_menu_side_menu_component__WEBPACK_IMPORTED_MODULE_6__["SideMenuComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"]
        ],
        exports: [
            _header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"],
            _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"],
            _side_menu_side_menu_component__WEBPACK_IMPORTED_MODULE_6__["SideMenuComponent"]
        ]
    })
], ComponentsModule);



/***/ }),

/***/ "./src/app/shared/components/footer/footer.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/shared/components/footer/footer.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".footer {\n  background-color: #d6e2ec;\n  display: -webkit-box;\n  display: flex;\n  flex-wrap: wrap;\n  padding: 1.823vw 2.604vw 6.51vw;\n  margin-bottom: 1.563vw;\n}\n.footer__social {\n  text-align: center;\n  padding: 30px 0 0;\n}\n.footer__social a {\n  margin-left: 0.885vw;\n  display: inline-block;\n}\n.footer__logo {\n  text-align: center;\n}\n.secondary-menu {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  padding-top: 0.938vw;\n  padding-left: 8.438vw;\n}\n.secondary-menu__item {\n  width: 8.698vw;\n}\n.secondary-menu__item:last-child {\n  text-align: right;\n}\n.secondary-menu__item:nth-child(2) {\n  padding-left: 0.625vw;\n}\n.secondary-menu__item .title {\n  font-weight: 700;\n  text-transform: uppercase;\n  font-size: 0.885vw;\n  margin-bottom: 0.417vw;\n}\n.secondary-menu__item a {\n  color: #000000;\n  font-weight: 400;\n  font-size: 0.833vw;\n  line-height: 1.25vw;\n}\n.secondary-menu__item p {\n  margin: 0;\n}\n@media screen and (max-width: 1200px) {\n  .secondary-menu__item .title {\n    font-size: 14px;\n  }\n}\n@media screen and (max-width: 992px) {\n  .footer {\n    padding: 20px;\n  }\n\n  .footer__social {\n    width: 100%;\n    text-align: center;\n    padding: 30px 0 0;\n  }\n\n  .secondary-menu__item {\n    width: auto;\n  }\n\n  .secondary-menu__item a {\n    font-size: 14px;\n  }\n\n  .secondary-menu {\n    padding-left: 0;\n  }\n\n  .secondary-menu__item .title {\n    font-size: 16px;\n  }\n}\n@media screen and (max-width: 767px) {\n  .footer__logo {\n    width: 100%;\n    text-align: center;\n  }\n\n  .secondary-menu {\n    width: 100%;\n  }\n}\n@media screen and (max-width: 492px) {\n  .secondary-menu {\n    flex-wrap: wrap;\n  }\n\n  .secondary-menu__item {\n    width: 100%;\n  }\n\n  .secondary-menu__item:last-child {\n    text-align: left;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvZm9vdGVyL0M6XFxVc2Vyc1xcVmxhZERcXERlc2t0b3BcXE1lbW8gKGFsbClcXG1lbW8vc3JjXFxhcHBcXHNoYXJlZFxcY29tcG9uZW50c1xcZm9vdGVyXFxmb290ZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBQTtFQUdBLG9CQUFBO0VBQUEsYUFBQTtFQUdBLGVBQUE7RUFDQSwrQkFBQTtFQUNBLHNCQUFBO0FDQ0Y7QURDRTtFQUlFLGtCQUFBO0VBQ0EsaUJBQUE7QUNGSjtBREdJO0VBQ0Usb0JBQUE7RUFDQSxxQkFBQTtBQ0ROO0FESUU7RUFFRSxrQkFBQTtBQ0hKO0FET0E7RUFHRSxvQkFBQTtFQUFBLGFBQUE7RUFHQSx5QkFBQTtVQUFBLDhCQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtBQ0pGO0FES0U7RUFDRSxjQUFBO0FDSEo7QURJSTtFQUNFLGlCQUFBO0FDRk47QURJSTtFQUNFLHFCQUFBO0FDRk47QURJSTtFQUNFLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0FDRk47QURJSTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNGTjtBRElJO0VBQ0UsU0FBQTtBQ0ZOO0FET0E7RUFDRTtJQUNFLGVBQUE7RUNKRjtBQUNGO0FETUE7RUFDRTtJQUNFLGFBQUE7RUNKRjs7RURPQTtJQUNFLFdBQUE7SUFDQSxrQkFBQTtJQUNBLGlCQUFBO0VDSkY7O0VET0E7SUFDRSxXQUFBO0VDSkY7O0VET0E7SUFDRSxlQUFBO0VDSkY7O0VET0E7SUFDRSxlQUFBO0VDSkY7O0VET0E7SUFDRSxlQUFBO0VDSkY7QUFDRjtBRE9BO0VBQ0U7SUFDRSxXQUFBO0lBQ0Esa0JBQUE7RUNMRjs7RURRQTtJQUNFLFdBQUE7RUNMRjtBQUNGO0FEUUE7RUFDRTtJQUdFLGVBQUE7RUNORjs7RURTQTtJQUNFLFdBQUE7RUNORjs7RURTQTtJQUNFLGdCQUFBO0VDTkY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9vdGVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDZlMmVjO1xyXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xyXG4gIC1tcy1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG4gIHBhZGRpbmc6IDEuODIzdncgMi42MDR2dyA2LjUxMHZ3O1xyXG4gIG1hcmdpbi1ib3R0b206IDEuNTYzdnc7XHJcbiAgLy9wYWRkaW5nOiAyMHB4O1xyXG4gICZfX3NvY2lhbCB7XHJcbiAgICAvL3BhZGRpbmctdG9wOiAxLjcxOXZ3O1xyXG4gICAgLy9wYWRkaW5nLXJpZ2h0OiAwLjE1NnZ3O1xyXG4gICAgLy93aWR0aDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDMwcHggMCAwO1xyXG4gICAgYSB7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAwLjg4NXZ3O1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB9XHJcbiAgfVxyXG4gICZfX2xvZ28ge1xyXG4gICAgLy93aWR0aDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbn1cclxuXHJcbi5zZWNvbmRhcnktbWVudSB7XHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgcGFkZGluZy10b3A6IDAuOTM4dnc7XHJcbiAgcGFkZGluZy1sZWZ0OiA4LjQzOHZ3O1xyXG4gICZfX2l0ZW0ge1xyXG4gICAgd2lkdGg6IDguNjk4dnc7XHJcbiAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgIH1cclxuICAgICY6bnRoLWNoaWxkKDIpIHtcclxuICAgICAgcGFkZGluZy1sZWZ0OiAwLjYyNXZ3O1xyXG4gICAgfVxyXG4gICAgLnRpdGxlIHtcclxuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgZm9udC1zaXplOiAwLjg4NXZ3O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwLjQxN3Z3O1xyXG4gICAgfVxyXG4gICAgYSB7XHJcbiAgICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgICBmb250LXNpemU6IDAuODMzdnc7XHJcbiAgICAgIGxpbmUtaGVpZ2h0OiAxLjI1MHZ3O1xyXG4gICAgfVxyXG4gICAgcCB7XHJcbiAgICAgIG1hcmdpbjogMDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xyXG4gIC5zZWNvbmRhcnktbWVudV9faXRlbSAudGl0bGUge1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gIH1cclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xyXG4gIC5mb290ZXIge1xyXG4gICAgcGFkZGluZzogMjBweDtcclxuICB9XHJcblxyXG4gIC5mb290ZXJfX3NvY2lhbCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDMwcHggMCAwO1xyXG4gIH1cclxuXHJcbiAgLnNlY29uZGFyeS1tZW51X19pdGVtIHtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gIH1cclxuXHJcbiAgLnNlY29uZGFyeS1tZW51X19pdGVtIGEge1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gIH1cclxuXHJcbiAgLnNlY29uZGFyeS1tZW51IHtcclxuICAgIHBhZGRpbmctbGVmdDogMDtcclxuICB9XHJcblxyXG4gIC5zZWNvbmRhcnktbWVudV9faXRlbSAudGl0bGUge1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcclxuICAuZm9vdGVyX19sb2dvIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgLnNlY29uZGFyeS1tZW51IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDkycHgpIHtcclxuICAuc2Vjb25kYXJ5LW1lbnUge1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gIH1cclxuXHJcbiAgLnNlY29uZGFyeS1tZW51X19pdGVtIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLnNlY29uZGFyeS1tZW51X19pdGVtOmxhc3QtY2hpbGQge1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcbn1cclxuXHJcbiIsIi5mb290ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDZlMmVjO1xuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWZsZXgtd3JhcDogd3JhcDtcbiAgLW1zLWZsZXgtd3JhcDogd3JhcDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBwYWRkaW5nOiAxLjgyM3Z3IDIuNjA0dncgNi41MXZ3O1xuICBtYXJnaW4tYm90dG9tOiAxLjU2M3Z3O1xufVxuLmZvb3Rlcl9fc29jaWFsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAzMHB4IDAgMDtcbn1cbi5mb290ZXJfX3NvY2lhbCBhIHtcbiAgbWFyZ2luLWxlZnQ6IDAuODg1dnc7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cbi5mb290ZXJfX2xvZ28ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5zZWNvbmRhcnktbWVudSB7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHBhZGRpbmctdG9wOiAwLjkzOHZ3O1xuICBwYWRkaW5nLWxlZnQ6IDguNDM4dnc7XG59XG4uc2Vjb25kYXJ5LW1lbnVfX2l0ZW0ge1xuICB3aWR0aDogOC42OTh2dztcbn1cbi5zZWNvbmRhcnktbWVudV9faXRlbTpsYXN0LWNoaWxkIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG4uc2Vjb25kYXJ5LW1lbnVfX2l0ZW06bnRoLWNoaWxkKDIpIHtcbiAgcGFkZGluZy1sZWZ0OiAwLjYyNXZ3O1xufVxuLnNlY29uZGFyeS1tZW51X19pdGVtIC50aXRsZSB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMC44ODV2dztcbiAgbWFyZ2luLWJvdHRvbTogMC40MTd2dztcbn1cbi5zZWNvbmRhcnktbWVudV9faXRlbSBhIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGZvbnQtc2l6ZTogMC44MzN2dztcbiAgbGluZS1oZWlnaHQ6IDEuMjV2dztcbn1cbi5zZWNvbmRhcnktbWVudV9faXRlbSBwIHtcbiAgbWFyZ2luOiAwO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcbiAgLnNlY29uZGFyeS1tZW51X19pdGVtIC50aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xuICAuZm9vdGVyIHtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICB9XG5cbiAgLmZvb3Rlcl9fc29jaWFsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZzogMzBweCAwIDA7XG4gIH1cblxuICAuc2Vjb25kYXJ5LW1lbnVfX2l0ZW0ge1xuICAgIHdpZHRoOiBhdXRvO1xuICB9XG5cbiAgLnNlY29uZGFyeS1tZW51X19pdGVtIGEge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxuXG4gIC5zZWNvbmRhcnktbWVudSB7XG4gICAgcGFkZGluZy1sZWZ0OiAwO1xuICB9XG5cbiAgLnNlY29uZGFyeS1tZW51X19pdGVtIC50aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAuZm9vdGVyX19sb2dvIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICAuc2Vjb25kYXJ5LW1lbnUge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0OTJweCkge1xuICAuc2Vjb25kYXJ5LW1lbnUge1xuICAgIC13ZWJraXQtZmxleC13cmFwOiB3cmFwO1xuICAgIC1tcy1mbGV4LXdyYXA6IHdyYXA7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9XG5cbiAgLnNlY29uZGFyeS1tZW51X19pdGVtIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5zZWNvbmRhcnktbWVudV9faXRlbTpsYXN0LWNoaWxkIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/shared/components/footer/footer.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/components/footer/footer.component.ts ***!
  \**************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FooterComponent = class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
};
FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer',
        template: __webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/components/footer/footer.component.html"),
        styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/shared/components/footer/footer.component.scss")]
    })
], FooterComponent);



/***/ }),

/***/ "./src/app/shared/components/header/header.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/shared/components/header/header.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header {\n  background: #fff;\n  position: relative;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  padding: 0 2.604vw;\n  height: 5.469vw;\n}\n.header__logo {\n  width: 17%;\n  position: relative;\n}\n.header__logo img {\n  min-width: 120px !important;\n}\n.header__menu {\n  width: 62.5%;\n}\n.header__menu ul {\n  list-style: none;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  padding-top: 1.771vw;\n  padding-left: 3.073vw;\n}\n.header__menu li {\n  padding: 0 1.719vw;\n}\n.header__menu a {\n  color: #000000;\n  font-weight: 700;\n  font-size: 0.938vw;\n  position: relative;\n}\n.header__menu a:after {\n  content: \"\";\n  position: absolute;\n  width: 0;\n  height: 0.781vw;\n  left: 50%;\n  top: 100%;\n  background: #000;\n  -webkit-transform: translate(-50%);\n          transform: translate(-50%);\n  -webkit-transition: ease 0.3s;\n  transition: ease 0.3s;\n}\n.header__buttons {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n          align-items: center;\n  width: 20.5%;\n  padding-top: 1.563vw;\n  padding-left: 2.083vw;\n}\n.header__buttons .log-in {\n  padding-left: 0;\n}\n.header__buttons .log-in img {\n  width: 2.135vw;\n}\n.header__buttons button {\n  font-weight: 700;\n  font-size: 0.938vw;\n}\n.header__buttons button:first-child {\n  margin-right: 0.99vw;\n}\n.header__menu a:hover:after,\n.header__menu .active:after {\n  width: 100%;\n}\n.nav-toggle {\n  display: none;\n  position: absolute;\n  left: 50%;\n  bottom: 50%;\n  -webkit-transform: translate(-50%, 50%);\n          transform: translate(-50%, 50%);\n}\n.nav-toggle span {\n  display: block;\n  background: #000;\n  height: 6px;\n  width: 40px;\n  border-radius: 3px;\n  margin-bottom: 7px;\n}\n.nav-toggle span:last-child {\n  margin-bottom: 0;\n}\n.light {\n  background: #fff;\n  color: #000;\n}\n.header__buttons.log-in {\n  padding-left: 0;\n}\n.header__buttons.log-in img {\n  width: 2.135vw;\n}\n@media screen and (max-width: 1200px) {\n  .header__menu a,\n.header__buttons button {\n    font-size: 14px;\n  }\n}\n@media screen and (max-width: 992px) {\n  .header__buttons .btn-custom {\n    width: auto !important;\n  }\n\n  .btn-custom, .header__menu a,\n.header__buttons button {\n    font-size: 16px;\n  }\n\n  .header {\n    height: 80px;\n  }\n\n  .header__menu {\n    width: 56%;\n  }\n\n  .header__buttons {\n    width: 26%;\n  }\n\n  .btn-custom {\n    height: 40px;\n    min-width: 80px;\n    border-radius: 21px;\n    padding: 0 12px;\n  }\n}\n@media screen and (max-width: 767px) {\n  .header__logo {\n    width: 120px;\n    position: static;\n  }\n\n  .header__buttons .light + p {\n    margin: 0 10px;\n  }\n\n  .header__buttons img {\n    width: 20px;\n  }\n\n  .header {\n    flex-wrap: wrap;\n    -webkit-box-pack: justify;\n            justify-content: space-between;\n  }\n\n  .header__logo,\n.header__buttons {\n    -webkit-box-ordinal-group: 2;\n            order: 1;\n  }\n\n  .header__buttons {\n    padding-top: 0;\n    width: auto;\n  }\n\n  .header__buttons .light {\n    margin-right: 5px;\n  }\n\n  .header__menu {\n    position: absolute;\n    -webkit-box-ordinal-group: 3;\n            order: 2;\n    width: 100%;\n    left: 0;\n    top: 100%;\n    background: #fff;\n    display: none;\n    z-index: 9;\n  }\n\n  .header__menu ul {\n    display: block;\n    padding-left: 0;\n    border-bottom: 1px solid #000;\n  }\n\n  .header__menu li {\n    padding: 10px 15px;\n    text-align: center;\n  }\n\n  .header__menu a {\n    font-size: 18px;\n  }\n\n  .nav-toggle {\n    display: block;\n  }\n\n  .active {\n    display: block;\n  }\n}\n@media screen and (max-width: 492px) {\n  .header {\n    height: 110px;\n    -webkit-box-align: start;\n            align-items: flex-start;\n    padding-top: 15px;\n  }\n\n  .nav-toggle {\n    position: absolute;\n    left: 50%;\n    bottom: 10px;\n    -webkit-transform: translate(-50%);\n            transform: translate(-50%);\n  }\n\n  .header__logo {\n    width: 100px;\n  }\n\n  .header__logo img {\n    min-width: 100px !important;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvaGVhZGVyL0M6XFxVc2Vyc1xcVmxhZERcXERlc2t0b3BcXE1lbW8gKGFsbClcXG1lbW8vc3JjXFxhcHBcXHNoYXJlZFxcY29tcG9uZW50c1xcaGVhZGVyXFxoZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBR0Esb0JBQUE7RUFBQSxhQUFBO0VBR0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ0NGO0FEQUU7RUFDRSxVQUFBO0VBQ0Esa0JBQUE7QUNFSjtBREFJO0VBQ0UsMkJBQUE7QUNFTjtBRENFO0VBQ0UsWUFBQTtBQ0NKO0FEQUk7RUFDRSxnQkFBQTtFQUtBLG9CQUFBO0VBQUEsYUFBQTtFQUdBLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSxvQkFBQTtFQUNBLHFCQUFBO0FDQU47QURFSTtFQUNFLGtCQUFBO0FDQU47QURFSTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNBTjtBREVJO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0NBQUE7VUFBQSwwQkFBQTtFQUNBLDZCQUFBO0VBSUEscUJBQUE7QUNBTjtBREdFO0VBR0Usb0JBQUE7RUFBQSxhQUFBO0VBR0EseUJBQUE7VUFBQSw4QkFBQTtFQUdBLHlCQUFBO1VBQUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtBQ0RKO0FERUk7RUFDRSxlQUFBO0FDQU47QURDTTtFQUNFLGNBQUE7QUNDUjtBREVJO0VBQ0UsZ0JBQUE7RUFDQSxrQkFBQTtBQ0FOO0FERUk7RUFDRSxvQkFBQTtBQ0FOO0FES0E7O0VBRUUsV0FBQTtBQ0ZGO0FES0E7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLHVDQUFBO1VBQUEsK0JBQUE7QUNGRjtBREdFO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDREo7QURFSTtFQUNFLGdCQUFBO0FDQU47QURLQTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBQ0ZGO0FES0E7RUFDRSxlQUFBO0FDRkY7QURHRTtFQUNFLGNBQUE7QUNESjtBREtBO0VBQ0U7O0lBRUUsZUFBQTtFQ0ZGO0FBQ0Y7QURLQTtFQUNFO0lBQ0Usc0JBQUE7RUNIRjs7RURNQTs7SUFFRSxlQUFBO0VDSEY7O0VETUE7SUFDRSxZQUFBO0VDSEY7O0VETUE7SUFDRSxVQUFBO0VDSEY7O0VETUE7SUFDRSxVQUFBO0VDSEY7O0VETUE7SUFDRSxZQUFBO0lBQ0EsZUFBQTtJQUNBLG1CQUFBO0lBQ0EsZUFBQTtFQ0hGO0FBQ0Y7QURNQTtFQUNFO0lBQ0UsWUFBQTtJQUNBLGdCQUFBO0VDSkY7O0VET0E7SUFDRSxjQUFBO0VDSkY7O0VET0E7SUFDRSxXQUFBO0VDSkY7O0VET0E7SUFHRSxlQUFBO0lBR0EseUJBQUE7WUFBQSw4QkFBQTtFQ0pGOztFRE9BOztJQUVFLDRCQUFBO1lBQUEsUUFBQTtFQ0pGOztFRE9BO0lBQ0UsY0FBQTtJQUNBLFdBQUE7RUNKRjs7RURPQTtJQUNFLGlCQUFBO0VDSkY7O0VET0E7SUFDRSxrQkFBQTtJQUNBLDRCQUFBO1lBQUEsUUFBQTtJQUNBLFdBQUE7SUFDQSxPQUFBO0lBQ0EsU0FBQTtJQUNBLGdCQUFBO0lBQ0EsYUFBQTtJQUNBLFVBQUE7RUNKRjs7RURPQTtJQUNFLGNBQUE7SUFDQSxlQUFBO0lBQ0EsNkJBQUE7RUNKRjs7RURPQTtJQUNFLGtCQUFBO0lBQ0Esa0JBQUE7RUNKRjs7RURPQTtJQUNFLGVBQUE7RUNKRjs7RURPQTtJQUNFLGNBQUE7RUNKRjs7RURPQTtJQUNFLGNBQUE7RUNKRjtBQUNGO0FET0E7RUFDRTtJQUNFLGFBQUE7SUFDQSx3QkFBQTtZQUFBLHVCQUFBO0lBQ0EsaUJBQUE7RUNMRjs7RURRQTtJQUNFLGtCQUFBO0lBQ0EsU0FBQTtJQUNBLFlBQUE7SUFDQSxrQ0FBQTtZQUFBLDBCQUFBO0VDTEY7O0VEUUE7SUFDRSxZQUFBO0VDTEY7O0VEUUE7SUFDRSwyQkFBQTtFQ0xGO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlciB7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBwYWRkaW5nOiAwIDIuNjA0dnc7XHJcbiAgaGVpZ2h0OiA1LjQ2OXZ3O1xyXG4gICZfX2xvZ28ge1xyXG4gICAgd2lkdGg6IDE3JTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIC8vdG9wOiAxLjk3OXZ3O1xyXG4gICAgaW1nIHtcclxuICAgICAgbWluLXdpZHRoOiAxMjBweCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH1cclxuICAmX19tZW51IHtcclxuICAgIHdpZHRoOiA2Mi41JTtcclxuICAgIHVsIHtcclxuICAgICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgICAgLy9tYXJnaW46IDA7XHJcbiAgICAgIC8vcGFkZGluZzogMDtcclxuICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICAgIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgcGFkZGluZy10b3A6IDEuNzcxdnc7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMy4wNzN2dztcclxuICAgIH1cclxuICAgIGxpIHtcclxuICAgICAgcGFkZGluZzogMCAxLjcxOXZ3O1xyXG4gICAgfVxyXG4gICAgYSB7XHJcbiAgICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICBmb250LXNpemU6IDAuOTM4dnc7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIH1cclxuICAgIGE6YWZ0ZXIge1xyXG4gICAgICBjb250ZW50OiAnJztcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB3aWR0aDogMDtcclxuICAgICAgaGVpZ2h0OiAwLjc4MXZ3O1xyXG4gICAgICBsZWZ0OiA1MCU7XHJcbiAgICAgIHRvcDogMTAwJTtcclxuICAgICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSk7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogZWFzZSAwLjNzO1xyXG4gICAgICAtbW96LXRyYW5zaXRpb246IGVhc2UgMC4zcztcclxuICAgICAgLW1zLXRyYW5zaXRpb246IGVhc2UgMC4zcztcclxuICAgICAgLW8tdHJhbnNpdGlvbjogZWFzZSAwLjNzO1xyXG4gICAgICB0cmFuc2l0aW9uOiBlYXNlIDAuM3M7XHJcbiAgICB9XHJcbiAgfVxyXG4gICZfX2J1dHRvbnMge1xyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMjAuNSU7XHJcbiAgICBwYWRkaW5nLXRvcDogMS41NjN2dztcclxuICAgIHBhZGRpbmctbGVmdDogMi4wODN2dztcclxuICAgIC5sb2ctaW4ge1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDIuMTM1dnc7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICAgIGZvbnQtc2l6ZTogMC45Mzh2dztcclxuICAgIH1cclxuICAgIGJ1dHRvbjpmaXJzdC1jaGlsZCB7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMC45OTB2dztcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5oZWFkZXJfX21lbnUgYTpob3ZlcjphZnRlcixcclxuLmhlYWRlcl9fbWVudSAuYWN0aXZlOmFmdGVyIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm5hdi10b2dnbGUge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGxlZnQ6IDUwJTtcclxuICBib3R0b206IDUwJTtcclxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCA1MCUpO1xyXG4gIHNwYW4ge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gICAgaGVpZ2h0OiA2cHg7XHJcbiAgICB3aWR0aDogNDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDdweDtcclxuICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4ubGlnaHQge1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgY29sb3I6ICMwMDA7XHJcbn1cclxuXHJcbi5oZWFkZXJfX2J1dHRvbnMubG9nLWluIHtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgaW1nIHtcclxuICAgIHdpZHRoOiAyLjEzNXZ3O1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTIwMHB4KSB7XHJcbiAgLmhlYWRlcl9fbWVudSBhLFxyXG4gIC5oZWFkZXJfX2J1dHRvbnMgYnV0dG9ue1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAuaGVhZGVyX19idXR0b25zIC5idG4tY3VzdG9te1xyXG4gICAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIC5idG4tY3VzdG9tLCAuaGVhZGVyX19tZW51IGEsXHJcbiAgLmhlYWRlcl9fYnV0dG9ucyBidXR0b257XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgfVxyXG5cclxuICAuaGVhZGVyIHtcclxuICAgIGhlaWdodDogODBweDtcclxuICB9XHJcblxyXG4gIC5oZWFkZXJfX21lbnUge1xyXG4gICAgd2lkdGg6IDU2JTtcclxuICB9XHJcblxyXG4gIC5oZWFkZXJfX2J1dHRvbnMge1xyXG4gICAgd2lkdGg6IDI2JTtcclxuICB9XHJcblxyXG4gIC5idG4tY3VzdG9tIHtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIG1pbi13aWR0aDogODBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XHJcbiAgICBwYWRkaW5nOiAwIDEycHg7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gIC5oZWFkZXJfX2xvZ28ge1xyXG4gICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgcG9zaXRpb246IHN0YXRpYztcclxuICB9XHJcblxyXG4gIC5oZWFkZXJfX2J1dHRvbnMgLmxpZ2h0ICsgcCB7XHJcbiAgICBtYXJnaW46IDAgMTBweDtcclxuICB9XHJcblxyXG4gIC5oZWFkZXJfX2J1dHRvbnMgaW1nIHtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gIH1cclxuXHJcbiAgLmhlYWRlciB7XHJcbiAgICAtd2Via2l0LWZsZXgtd3JhcDogd3JhcDtcclxuICAgIC1tcy1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgfVxyXG5cclxuICAuaGVhZGVyX19sb2dvLFxyXG4gIC5oZWFkZXJfX2J1dHRvbnMge1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgfVxyXG5cclxuICAuaGVhZGVyX19idXR0b25zIHtcclxuICAgIHBhZGRpbmctdG9wOiAwO1xyXG4gICAgd2lkdGg6IGF1dG87XHJcbiAgfVxyXG5cclxuICAuaGVhZGVyX19idXR0b25zIC5saWdodCB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICB9XHJcblxyXG4gIC5oZWFkZXJfX21lbnUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0b3A6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICAgIHotaW5kZXg6IDk7XHJcbiAgfVxyXG5cclxuICAuaGVhZGVyX19tZW51IHVsIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMwMDA7XHJcbiAgfVxyXG5cclxuICAuaGVhZGVyX19tZW51IGxpIHtcclxuICAgIHBhZGRpbmc6IDEwcHggMTVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG4gIC5oZWFkZXJfX21lbnUgYSB7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgfVxyXG5cclxuICAubmF2LXRvZ2dsZSB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICB9XHJcblxyXG4gIC5hY3RpdmV7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ5MnB4KSB7XHJcbiAgLmhlYWRlciB7XHJcbiAgICBoZWlnaHQ6IDExMHB4O1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuICB9XHJcblxyXG4gIC5uYXYtdG9nZ2xlIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIGJvdHRvbTogMTBweDtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUpO1xyXG4gIH1cclxuXHJcbiAgLmhlYWRlcl9fbG9nbyB7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgfVxyXG5cclxuICAuaGVhZGVyX19sb2dvIGltZyB7XHJcbiAgICBtaW4td2lkdGg6IDEwMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcbiIsIi5oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZzogMCAyLjYwNHZ3O1xuICBoZWlnaHQ6IDUuNDY5dnc7XG59XG4uaGVhZGVyX19sb2dvIHtcbiAgd2lkdGg6IDE3JTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmhlYWRlcl9fbG9nbyBpbWcge1xuICBtaW4td2lkdGg6IDEyMHB4ICFpbXBvcnRhbnQ7XG59XG4uaGVhZGVyX19tZW51IHtcbiAgd2lkdGg6IDYyLjUlO1xufVxuLmhlYWRlcl9fbWVudSB1bCB7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmctdG9wOiAxLjc3MXZ3O1xuICBwYWRkaW5nLWxlZnQ6IDMuMDczdnc7XG59XG4uaGVhZGVyX19tZW51IGxpIHtcbiAgcGFkZGluZzogMCAxLjcxOXZ3O1xufVxuLmhlYWRlcl9fbWVudSBhIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc2l6ZTogMC45Mzh2dztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmhlYWRlcl9fbWVudSBhOmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMDtcbiAgaGVpZ2h0OiAwLjc4MXZ3O1xuICBsZWZ0OiA1MCU7XG4gIHRvcDogMTAwJTtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSk7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogZWFzZSAwLjNzO1xuICAtbW96LXRyYW5zaXRpb246IGVhc2UgMC4zcztcbiAgLW1zLXRyYW5zaXRpb246IGVhc2UgMC4zcztcbiAgLW8tdHJhbnNpdGlvbjogZWFzZSAwLjNzO1xuICB0cmFuc2l0aW9uOiBlYXNlIDAuM3M7XG59XG4uaGVhZGVyX19idXR0b25zIHtcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aWR0aDogMjAuNSU7XG4gIHBhZGRpbmctdG9wOiAxLjU2M3Z3O1xuICBwYWRkaW5nLWxlZnQ6IDIuMDgzdnc7XG59XG4uaGVhZGVyX19idXR0b25zIC5sb2ctaW4ge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG4uaGVhZGVyX19idXR0b25zIC5sb2ctaW4gaW1nIHtcbiAgd2lkdGg6IDIuMTM1dnc7XG59XG4uaGVhZGVyX19idXR0b25zIGJ1dHRvbiB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc2l6ZTogMC45Mzh2dztcbn1cbi5oZWFkZXJfX2J1dHRvbnMgYnV0dG9uOmZpcnN0LWNoaWxkIHtcbiAgbWFyZ2luLXJpZ2h0OiAwLjk5dnc7XG59XG5cbi5oZWFkZXJfX21lbnUgYTpob3ZlcjphZnRlcixcbi5oZWFkZXJfX21lbnUgLmFjdGl2ZTphZnRlciB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubmF2LXRvZ2dsZSB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNTAlO1xuICBib3R0b206IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgNTAlKTtcbn1cbi5uYXYtdG9nZ2xlIHNwYW4ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgaGVpZ2h0OiA2cHg7XG4gIHdpZHRoOiA0MHB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIG1hcmdpbi1ib3R0b206IDdweDtcbn1cbi5uYXYtdG9nZ2xlIHNwYW46bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbi5saWdodCB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGNvbG9yOiAjMDAwO1xufVxuXG4uaGVhZGVyX19idXR0b25zLmxvZy1pbiB7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cbi5oZWFkZXJfX2J1dHRvbnMubG9nLWluIGltZyB7XG4gIHdpZHRoOiAyLjEzNXZ3O1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcbiAgLmhlYWRlcl9fbWVudSBhLFxuLmhlYWRlcl9fYnV0dG9ucyBidXR0b24ge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLmhlYWRlcl9fYnV0dG9ucyAuYnRuLWN1c3RvbSB7XG4gICAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcbiAgfVxuXG4gIC5idG4tY3VzdG9tLCAuaGVhZGVyX19tZW51IGEsXG4uaGVhZGVyX19idXR0b25zIGJ1dHRvbiB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG5cbiAgLmhlYWRlciB7XG4gICAgaGVpZ2h0OiA4MHB4O1xuICB9XG5cbiAgLmhlYWRlcl9fbWVudSB7XG4gICAgd2lkdGg6IDU2JTtcbiAgfVxuXG4gIC5oZWFkZXJfX2J1dHRvbnMge1xuICAgIHdpZHRoOiAyNiU7XG4gIH1cblxuICAuYnRuLWN1c3RvbSB7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIG1pbi13aWR0aDogODBweDtcbiAgICBib3JkZXItcmFkaXVzOiAyMXB4O1xuICAgIHBhZGRpbmc6IDAgMTJweDtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgLmhlYWRlcl9fbG9nbyB7XG4gICAgd2lkdGg6IDEyMHB4O1xuICAgIHBvc2l0aW9uOiBzdGF0aWM7XG4gIH1cblxuICAuaGVhZGVyX19idXR0b25zIC5saWdodCArIHAge1xuICAgIG1hcmdpbjogMCAxMHB4O1xuICB9XG5cbiAgLmhlYWRlcl9fYnV0dG9ucyBpbWcge1xuICAgIHdpZHRoOiAyMHB4O1xuICB9XG5cbiAgLmhlYWRlciB7XG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XG4gICAgLW1zLWZsZXgtd3JhcDogd3JhcDtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgLW1zLWZsZXgtcGFjazoganVzdGlmeTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIH1cblxuICAuaGVhZGVyX19sb2dvLFxuLmhlYWRlcl9fYnV0dG9ucyB7XG4gICAgb3JkZXI6IDE7XG4gIH1cblxuICAuaGVhZGVyX19idXR0b25zIHtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICB3aWR0aDogYXV0bztcbiAgfVxuXG4gIC5oZWFkZXJfX2J1dHRvbnMgLmxpZ2h0IHtcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgfVxuXG4gIC5oZWFkZXJfX21lbnUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBvcmRlcjogMjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgei1pbmRleDogOTtcbiAgfVxuXG4gIC5oZWFkZXJfX21lbnUgdWwge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBhZGRpbmctbGVmdDogMDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzAwMDtcbiAgfVxuXG4gIC5oZWFkZXJfX21lbnUgbGkge1xuICAgIHBhZGRpbmc6IDEwcHggMTVweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICAuaGVhZGVyX19tZW51IGEge1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgfVxuXG4gIC5uYXYtdG9nZ2xlIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5hY3RpdmUge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0OTJweCkge1xuICAuaGVhZGVyIHtcbiAgICBoZWlnaHQ6IDExMHB4O1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgIHBhZGRpbmctdG9wOiAxNXB4O1xuICB9XG5cbiAgLm5hdi10b2dnbGUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgYm90dG9tOiAxMHB4O1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUpO1xuICB9XG5cbiAgLmhlYWRlcl9fbG9nbyB7XG4gICAgd2lkdGg6IDEwMHB4O1xuICB9XG5cbiAgLmhlYWRlcl9fbG9nbyBpbWcge1xuICAgIG1pbi13aWR0aDogMTAwcHggIWltcG9ydGFudDtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/shared/components/header/header.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/components/header/header.component.ts ***!
  \**************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angularx-social-login */ "./node_modules/angularx-social-login/angularx-social-login.js");




let HeaderComponent = class HeaderComponent {
    constructor(router, authService) {
        this.router = router;
        this.authService = authService;
        this.userLogin = false;
        this.headerLogo = false;
        this.menuClose = true;
    }
    ngOnInit() {
        if (localStorage.getItem('token')) {
            this.userLogin = true;
            this.user = localStorage.getItem('user');
        }
        if (this.router.url === '/main/home' || this.router.url === '/auth/sign-in' || this.router.url === '/auth/sign-up') {
            this.headerLogo = true;
        }
    }
    openMenu() {
        document.querySelector('.header__menu').classList.toggle('active');
    }
    logOut() {
        localStorage.clear();
        this.userLogin = false;
        this.authService.signOut();
        this.router.navigate(['/auth/sign-in']);
    }
};
HeaderComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: angularx_social_login__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }
];
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/components/header/header.component.html"),
        styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/shared/components/header/header.component.scss")]
    })
], HeaderComponent);



/***/ }),

/***/ "./src/app/shared/components/side-menu/side-menu.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/shared/components/side-menu/side-menu.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".side-menu {\n  position: relative;\n  width: 21.7vw;\n  padding-left: 3.021vw;\n  top: -3.021vw;\n}\n.side-menu ul {\n  margin: 0;\n  padding: 0;\n  list-style: none;\n}\n.side-menu a {\n  color: #000000;\n  font-family: \"Copperplate Gothic\", serif;\n  font-weight: 700;\n  position: relative;\n  display: block;\n  font-size: 1.042vw;\n  margin-bottom: 1.615vw;\n}\n.side-menu a.active {\n  font-size: 1.563vw;\n}\n.side-menu a.active:before {\n  content: \"\";\n  position: absolute;\n  top: 50%;\n  background: url('side-active.png') no-repeat;\n  background-size: 100% 100%;\n  -webkit-transform: translateY(-47%);\n          transform: translateY(-47%);\n  left: -1.979vw;\n  width: 3.125vw;\n  height: 2.76vw;\n}\n.side-menu__toggle {\n  position: absolute;\n  width: 30px;\n  height: 50px;\n  box-sizing: content-box;\n  padding: 0 11px;\n  left: 100%;\n  top: -1px;\n  background: #fff;\n  -webkit-box-align: center;\n          align-items: center;\n  border: 1px solid #d6e2ec;\n  border-left: 0;\n  border-top-right-radius: 15px;\n  border-bottom-right-radius: 15px;\n  display: none;\n}\n@media screen and (max-width: 1200px) {\n  .side-menu {\n    width: 100%;\n  }\n  .side-menu a {\n    font-size: 17px;\n  }\n  .side-menu a.active {\n    font-size: 17px;\n  }\n}\n@media screen and (max-width: 767px) {\n  .side-menu {\n    position: absolute;\n    top: 0;\n    bottom: 0;\n    right: 100%;\n    z-index: 3;\n    width: auto;\n    max-width: 100%;\n    padding: 68px 40px;\n    border: 1px solid #d6e2ec;\n    background: url('gallery-bg.png') top left #fff no-repeat;\n    background-size: 100%;\n  }\n  .side-menu__toggle {\n    display: -webkit-box;\n    display: flex;\n  }\n  .side-menu a {\n    font-size: 20px;\n  }\n  .side-menu a .active {\n    font-size: 26px;\n  }\n  .side-menu a .active:before {\n    left: -18px;\n    width: 32px;\n    height: 25px;\n  }\n\n  .active {\n    right: auto;\n    left: 0;\n  }\n\n  .side-menu.active .side-menu__toggle img {\n    -webkit-transform: rotate(180deg);\n            transform: rotate(180deg);\n  }\n}\n@media screen and (max-width: 600px) {\n  .side-menu {\n    padding: 30px 11px;\n  }\n  .side-menu__toggle {\n    width: 18px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvc2lkZS1tZW51L0M6XFxVc2Vyc1xcVmxhZERcXERlc2t0b3BcXE1lbW8gKGFsbClcXG1lbW8vc3JjXFxhcHBcXHNoYXJlZFxcY29tcG9uZW50c1xcc2lkZS1tZW51XFxzaWRlLW1lbnUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL3NpZGUtbWVudS9zaWRlLW1lbnUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNBLGFBQUE7QUNDRjtBREFFO0VBQ0UsU0FBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQ0VKO0FEQUU7RUFDRSxjQUFBO0VBQ0Esd0NBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUNFSjtBRERJO0VBQ0Usa0JBQUE7QUNHTjtBREZNO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLDRDQUFBO0VBQ0EsMEJBQUE7RUFDQSxtQ0FBQTtVQUFBLDJCQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FDSVI7QURBRTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0VBR0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLDZCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxhQUFBO0FDRUo7QURFQTtFQUNFO0lBQ0UsV0FBQTtFQ0NGO0VEQUU7SUFDRSxlQUFBO0VDRUo7RURBRTtJQUNFLGVBQUE7RUNFSjtBQUNGO0FERUE7RUFDRTtJQUNFLGtCQUFBO0lBQ0EsTUFBQTtJQUNBLFNBQUE7SUFDQSxXQUFBO0lBQ0EsVUFBQTtJQUNBLFdBQUE7SUFDQSxlQUFBO0lBQ0Esa0JBQUE7SUFDQSx5QkFBQTtJQUNBLHlEQUFBO0lBQ0EscUJBQUE7RUNBRjtFRENFO0lBR0Usb0JBQUE7SUFBQSxhQUFBO0VDQ0o7RURDRTtJQUNFLGVBQUE7RUNDSjtFREFJO0lBQ0UsZUFBQTtFQ0VOO0VERE07SUFDRSxXQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7RUNHUjs7RURHQTtJQUNFLFdBQUE7SUFDQSxPQUFBO0VDQUY7O0VER0E7SUFDRSxpQ0FBQTtZQUFBLHlCQUFBO0VDQUY7QUFDRjtBREdBO0VBQ0U7SUFDRSxrQkFBQTtFQ0RGO0VERUU7SUFDRSxXQUFBO0VDQUo7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL3NpZGUtbWVudS9zaWRlLW1lbnUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2lkZS1tZW51IHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgd2lkdGg6IDIxLjd2dztcclxuICBwYWRkaW5nLWxlZnQ6IDMuMDIxdnc7XHJcbiAgdG9wOiAtMy4wMjF2dztcclxuICB1bCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICB9XHJcbiAgYSB7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkNvcHBlcnBsYXRlIEdvdGhpY1wiLCBzZXJpZjtcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMS4wNDJ2dztcclxuICAgIG1hcmdpbi1ib3R0b206IDEuNjE1dnc7XHJcbiAgICAmLmFjdGl2ZSB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMS41NjN2dztcclxuICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgIGNvbnRlbnQ6ICcnO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL2ltZy9pY29uL3NpZGUtYWN0aXZlLnBuZykgbm8tcmVwZWF0O1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNDclKTtcclxuICAgICAgICBsZWZ0OiAtMS45Nzl2dztcclxuICAgICAgICB3aWR0aDogMy4xMjV2dztcclxuICAgICAgICBoZWlnaHQ6IDIuNzYwdnc7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgJl9fdG9nZ2xlIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm94LXNpemluZzogY29udGVudC1ib3g7XHJcbiAgICBwYWRkaW5nOiAwIDExcHg7XHJcbiAgICBsZWZ0OiAxMDAlO1xyXG4gICAgdG9wOiAtMXB4O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2Q2ZTJlYztcclxuICAgIGJvcmRlci1sZWZ0OiAwO1xyXG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE1cHg7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTVweDtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjAwcHgpIHtcclxuICAuc2lkZS1tZW51IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYSB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIH1cclxuICAgIGEuYWN0aXZlIHtcclxuICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcclxuICAuc2lkZS1tZW51IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIHJpZ2h0OiAxMDAlO1xyXG4gICAgei1pbmRleDogMztcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgcGFkZGluZzogNjhweCA0MHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2Q2ZTJlYztcclxuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvaW1nL2dhbGxlcnktYmcucG5nKSB0b3AgbGVmdCAjZmZmIG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcclxuICAgICZfX3RvZ2dsZSB7XHJcbiAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gICAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICB9XHJcbiAgICBhIHtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAuYWN0aXZlIHtcclxuICAgICAgICBmb250LXNpemU6IDI2cHg7XHJcbiAgICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgICAgbGVmdDogLTE4cHg7XHJcbiAgICAgICAgICB3aWR0aDogMzJweDtcclxuICAgICAgICAgIGhlaWdodDogMjVweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5hY3RpdmUge1xyXG4gICAgcmlnaHQ6IGF1dG87XHJcbiAgICBsZWZ0OiAwO1xyXG4gIH1cclxuXHJcbiAgLnNpZGUtbWVudS5hY3RpdmUgLnNpZGUtbWVudV9fdG9nZ2xlIGltZyB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcclxuICAuc2lkZS1tZW51IHtcclxuICAgIHBhZGRpbmc6IDMwcHggMTFweDtcclxuICAgICZfX3RvZ2dsZSB7XHJcbiAgICAgIHdpZHRoOiAxOHB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuXHJcblxyXG4iLCIuc2lkZS1tZW51IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMjEuN3Z3O1xuICBwYWRkaW5nLWxlZnQ6IDMuMDIxdnc7XG4gIHRvcDogLTMuMDIxdnc7XG59XG4uc2lkZS1tZW51IHVsIHtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBsaXN0LXN0eWxlOiBub25lO1xufVxuLnNpZGUtbWVudSBhIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtZmFtaWx5OiBcIkNvcHBlcnBsYXRlIEdvdGhpY1wiLCBzZXJpZjtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiAxLjA0MnZ3O1xuICBtYXJnaW4tYm90dG9tOiAxLjYxNXZ3O1xufVxuLnNpZGUtbWVudSBhLmFjdGl2ZSB7XG4gIGZvbnQtc2l6ZTogMS41NjN2dztcbn1cbi5zaWRlLW1lbnUgYS5hY3RpdmU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9pbWcvaWNvbi9zaWRlLWFjdGl2ZS5wbmcpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNDclKTtcbiAgbGVmdDogLTEuOTc5dnc7XG4gIHdpZHRoOiAzLjEyNXZ3O1xuICBoZWlnaHQ6IDIuNzZ2dztcbn1cbi5zaWRlLW1lbnVfX3RvZ2dsZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDMwcHg7XG4gIGhlaWdodDogNTBweDtcbiAgYm94LXNpemluZzogY29udGVudC1ib3g7XG4gIHBhZGRpbmc6IDAgMTFweDtcbiAgbGVmdDogMTAwJTtcbiAgdG9wOiAtMXB4O1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNmUyZWM7XG4gIGJvcmRlci1sZWZ0OiAwO1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTVweDtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDE1cHg7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMDBweCkge1xuICAuc2lkZS1tZW51IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuICAuc2lkZS1tZW51IGEge1xuICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgfVxuICAuc2lkZS1tZW51IGEuYWN0aXZlIHtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gIC5zaWRlLW1lbnUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIHJpZ2h0OiAxMDAlO1xuICAgIHotaW5kZXg6IDM7XG4gICAgd2lkdGg6IGF1dG87XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDY4cHggNDBweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZDZlMmVjO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvaW1nL2dhbGxlcnktYmcucG5nKSB0b3AgbGVmdCAjZmZmIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7XG4gIH1cbiAgLnNpZGUtbWVudV9fdG9nZ2xlIHtcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgfVxuICAuc2lkZS1tZW51IGEge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgfVxuICAuc2lkZS1tZW51IGEgLmFjdGl2ZSB7XG4gICAgZm9udC1zaXplOiAyNnB4O1xuICB9XG4gIC5zaWRlLW1lbnUgYSAuYWN0aXZlOmJlZm9yZSB7XG4gICAgbGVmdDogLTE4cHg7XG4gICAgd2lkdGg6IDMycHg7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICB9XG5cbiAgLmFjdGl2ZSB7XG4gICAgcmlnaHQ6IGF1dG87XG4gICAgbGVmdDogMDtcbiAgfVxuXG4gIC5zaWRlLW1lbnUuYWN0aXZlIC5zaWRlLW1lbnVfX3RvZ2dsZSBpbWcge1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC5zaWRlLW1lbnUge1xuICAgIHBhZGRpbmc6IDMwcHggMTFweDtcbiAgfVxuICAuc2lkZS1tZW51X190b2dnbGUge1xuICAgIHdpZHRoOiAxOHB4O1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/shared/components/side-menu/side-menu.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/shared/components/side-menu/side-menu.component.ts ***!
  \********************************************************************/
/*! exports provided: SideMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideMenuComponent", function() { return SideMenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SideMenuComponent = class SideMenuComponent {
    constructor() { }
    ngOnInit() {
    }
    openSideMenu() {
        document.querySelector('.side-menu').classList.toggle('active');
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], SideMenuComponent.prototype, "sideMenu", void 0);
SideMenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-side-menu',
        template: __webpack_require__(/*! raw-loader!./side-menu.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/components/side-menu/side-menu.component.html"),
        styles: [__webpack_require__(/*! ./side-menu.component.scss */ "./src/app/shared/components/side-menu/side-menu.component.scss")]
    })
], SideMenuComponent);



/***/ }),

/***/ "./src/app/shared/constants/common.url.ts":
/*!************************************************!*\
  !*** ./src/app/shared/constants/common.url.ts ***!
  \************************************************/
/*! exports provided: CommonUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonUrl", function() { return CommonUrl; });
const api = 'https://memo.grassbusinesslabs.ml/api/';
const CommonUrl = {
    auth: {
        login: api + 'login',
        register: api + 'register',
        socialLogin: api + 'social-login'
    },
    contactUs: {
        send: api + 'contactUs'
    },
    staticText: {
        getAbout: api + 'static-text/2',
        getContactUs: api + 'static-text/3',
        getFAQ: api + 'static-text/4',
        getPricing: api + 'pricing'
    }
};


/***/ }),

/***/ "./src/app/shared/guards/auth.guard.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/guards/auth.guard.ts ***!
  \*********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let AuthGuard = class AuthGuard {
    constructor(router) {
        this.router = router;
    }
    canActivate(route, state) {
        if (!localStorage.getItem('token')) {
            this.router.navigate(['auth/sign-in']);
            return false;
        }
        return true;
    }
};
AuthGuard.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], AuthGuard);



/***/ }),

/***/ "./src/app/shared/guards/guards.module.ts":
/*!************************************************!*\
  !*** ./src/app/shared/guards/guards.module.ts ***!
  \************************************************/
/*! exports provided: GuardsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuardsModule", function() { return GuardsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth.guard */ "./src/app/shared/guards/auth.guard.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



let GuardsModule = class GuardsModule {
};
GuardsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        providers: [_auth_guard__WEBPACK_IMPORTED_MODULE_1__["AuthGuard"]]
    })
], GuardsModule);



/***/ }),

/***/ "./src/app/shared/interseptors/authentication.interseptors.ts":
/*!********************************************************************!*\
  !*** ./src/app/shared/interseptors/authentication.interseptors.ts ***!
  \********************************************************************/
/*! exports provided: AuthenticationInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationInterceptor", function() { return AuthenticationInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let AuthenticationInterceptor = class AuthenticationInterceptor {
    intercept(req, next) {
        if (typeof localStorage.token !== 'undefined') {
            const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]()
                .set('Authorization', 'Bearer ' + localStorage.token);
            req = req.clone({ headers });
        }
        return next.handle(req);
    }
};
AuthenticationInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], AuthenticationInterceptor);



/***/ }),

/***/ "./src/app/shared/services/auth.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/services/auth.service.ts ***!
  \*************************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./request.service */ "./src/app/shared/services/request.service.ts");
/* harmony import */ var _constants_common_url__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants/common.url */ "./src/app/shared/constants/common.url.ts");




let AuthService = class AuthService {
    constructor(request) {
        this.request = request;
    }
    login(data) {
        return this.request.post(_constants_common_url__WEBPACK_IMPORTED_MODULE_3__["CommonUrl"].auth.login, data);
    }
    register(data) {
        return this.request.post(_constants_common_url__WEBPACK_IMPORTED_MODULE_3__["CommonUrl"].auth.register, data);
    }
    socialRegister(data) {
        return this.request.post(_constants_common_url__WEBPACK_IMPORTED_MODULE_3__["CommonUrl"].auth.socialLogin, data);
    }
};
AuthService.ctorParameters = () => [
    { type: _request_service__WEBPACK_IMPORTED_MODULE_2__["RequestService"] }
];
AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], AuthService);



/***/ }),

/***/ "./src/app/shared/services/contact-us.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/services/contact-us.service.ts ***!
  \*******************************************************/
/*! exports provided: ContactUsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsService", function() { return ContactUsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./request.service */ "./src/app/shared/services/request.service.ts");
/* harmony import */ var _constants_common_url__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants/common.url */ "./src/app/shared/constants/common.url.ts");




let ContactUsService = class ContactUsService {
    constructor(request) {
        this.request = request;
    }
    send(data) {
        return this.request.post(_constants_common_url__WEBPACK_IMPORTED_MODULE_3__["CommonUrl"].contactUs.send, data);
    }
};
ContactUsService.ctorParameters = () => [
    { type: _request_service__WEBPACK_IMPORTED_MODULE_2__["RequestService"] }
];
ContactUsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], ContactUsService);



/***/ }),

/***/ "./src/app/shared/services/request.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/services/request.service.ts ***!
  \****************************************************/
/*! exports provided: RequestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestService", function() { return RequestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let RequestService = class RequestService {
    constructor(http) {
        this.http = http;
    }
    /**
     * @param url
     * @param body
     * @returns {Observable<ArrayBuffer>}
     */
    get(url, body = null) {
        return this.http.get(url);
    }
    /**
     * @param url
     * @param credentials
     * @returns {Observable<ArrayBuffer>}
     */
    post(url, credentials) {
        return this.http.post(url, credentials);
    }
    /**
     * @param url
     * @returns {Observable<ArrayBuffer>}
     */
    delete(url) {
        return this.http.delete(url);
    }
    /**
     * @param url
     * @param credentials
     * @returns {Promise<ArrayBuffer>|Promise<TResult|ArrayBuffer>}
     */
    put(url, credentials) {
        return this.http.put(url, credentials);
    }
};
RequestService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
RequestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], RequestService);



/***/ }),

/***/ "./src/app/shared/services/services.module.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/services/services.module.ts ***!
  \****************************************************/
/*! exports provided: ServicesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesModule", function() { return ServicesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _request_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./request.service */ "./src/app/shared/services/request.service.ts");
/* harmony import */ var _contact_us_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./contact-us.service */ "./src/app/shared/services/contact-us.service.ts");
/* harmony import */ var _static_texts_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./static-texts.service */ "./src/app/shared/services/static-texts.service.ts");






let ServicesModule = class ServicesModule {
};
ServicesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        providers: [
            _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _request_service__WEBPACK_IMPORTED_MODULE_3__["RequestService"],
            _contact_us_service__WEBPACK_IMPORTED_MODULE_4__["ContactUsService"],
            _static_texts_service__WEBPACK_IMPORTED_MODULE_5__["StaticTextsService"]
        ]
    })
], ServicesModule);



/***/ }),

/***/ "./src/app/shared/services/static-texts.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/shared/services/static-texts.service.ts ***!
  \*********************************************************/
/*! exports provided: StaticTextsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaticTextsService", function() { return StaticTextsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./request.service */ "./src/app/shared/services/request.service.ts");
/* harmony import */ var _constants_common_url__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants/common.url */ "./src/app/shared/constants/common.url.ts");




let StaticTextsService = class StaticTextsService {
    constructor(request) {
        this.request = request;
    }
    getAbout() {
        return this.request.get(_constants_common_url__WEBPACK_IMPORTED_MODULE_3__["CommonUrl"].staticText.getAbout);
    }
    getContactUs() {
        return this.request.get(_constants_common_url__WEBPACK_IMPORTED_MODULE_3__["CommonUrl"].staticText.getContactUs);
    }
    getFAQ() {
        return this.request.get(_constants_common_url__WEBPACK_IMPORTED_MODULE_3__["CommonUrl"].staticText.getFAQ);
    }
    getPricing() {
        return this.request.get(_constants_common_url__WEBPACK_IMPORTED_MODULE_3__["CommonUrl"].staticText.getPricing);
    }
};
StaticTextsService.ctorParameters = () => [
    { type: _request_service__WEBPACK_IMPORTED_MODULE_2__["RequestService"] }
];
StaticTextsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], StaticTextsService);



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/components.module */ "./src/app/shared/components/components.module.ts");
/* harmony import */ var _services_services_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/services.module */ "./src/app/shared/services/services.module.ts");
/* harmony import */ var _guards_guards_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./guards/guards.module */ "./src/app/shared/guards/guards.module.ts");






let SharedModule = class SharedModule {
};
SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_3__["ComponentsModule"],
            _services_services_module__WEBPACK_IMPORTED_MODULE_4__["ServicesModule"],
            _guards_guards_module__WEBPACK_IMPORTED_MODULE_5__["GuardsModule"]
        ],
        exports: [
            _components_components_module__WEBPACK_IMPORTED_MODULE_3__["ComponentsModule"],
            _services_services_module__WEBPACK_IMPORTED_MODULE_4__["ServicesModule"],
            _guards_guards_module__WEBPACK_IMPORTED_MODULE_5__["GuardsModule"]
        ],
    })
], SharedModule);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\VladD\Desktop\Memo (all)\memo\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map