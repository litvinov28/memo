<?php

return [
    'image_folder' => [
        'animation_photo' => [
            'save_path' => 'public/img/photo_animation/',
            'get_path' => '/storage/img/photo_animation/',
        ],
        'photo' =>[
            'save_path' => 'public/img/photo/',
            'get_path' => '/storage/img/photo/',
        ],
        'project_photo' =>[
            'save_path' => 'public/img/project_photo/',
            'get_path' => '/storage/img/project_photo/',
        ]

    ]


];