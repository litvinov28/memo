<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register','Auth\RegisterController@register');
Route::post('login','Auth\LoginController@authenticate');
Route::post('social-login', 'Auth\LoginController@socialLogin');

Route::post('admin/login','Auth\LoginController@adminLogin');

Route::get('pricing','PricingController@index');
Route::get('pricing/{pricing}','PricingController@show');
Route::get('static-text/{staticText}','StaticTextController@show');
Route::get('static-texts','StaticTextController@index');
Route::group(['middleware' => ['jwt.auth']], function () {
    Route::resource('contactUs','ContactUsController');

    Route::get('projects', 'ProjectController@index');
    Route::post('projects', 'ProjectController@store');

    Route::get('photos', 'PhotoController@index');
    Route::post('photos', 'PhotoController@store');
    Route::post('photos-check', 'PhotoController@photoCheck');


    Route::get('styles/{style}/templates', 'TemplateController@index');
    Route::get('styles', 'AnimationStyleController@index');
    Route::post('styles', 'AnimationStyleController@store');



    Route::group(['middleware' => 'roles', 'roles' => ['Admin']], function () {
    Route::get('users','AdminController@userList');
    Route::post('users/delete','AdminController@userDelete');
    Route::get('users/delete-list','AdminController@listDelete');
    Route::post('users/restore','AdminController@restore');
    Route::post('static-text/create','StaticTextController@store');
    Route::post('static-text/update','StaticTextController@update');
    Route::post('static-text/delete','StaticTextController@destroy');

    Route::resource('pricing','PricingController')->except(['index','show']);


});
});
