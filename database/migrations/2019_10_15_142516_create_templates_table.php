<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pics');
            $table->double('duration', 20 ,15);
            $table->string('types');
            $table->text('format')->nullable();
            $table->unsignedBigInteger('animation_style_id');
            $table->string('ae_file_name')->nullable(); // template
            $table->string('ae_file_path')->nullable(); // template
            $table->string('preview_video')->nullable(); // template
            $table->boolean('checked')->default(false);
            $table->timestamps();

            $table->foreign('animation_style_id')
                ->references('id')
                ->on('animation_styles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('templates');
    }
}
