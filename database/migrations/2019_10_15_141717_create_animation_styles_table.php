<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimationStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animation_styles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string("description", 777);
            $table->string('preview_image')->nullable();
            $table->boolean('including_video')->default(false);
            $table->integer('trending')->default(0);
            $table->integer('likes')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animation_styles');
    }
}
